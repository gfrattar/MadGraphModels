# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Wed 21 Sep 2016 09:46:11



from object_library import all_parameters, Parameter


from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot

# This is a default parameter object representing 0.
ZERO = Parameter(name = 'ZERO',
                 nature = 'internal',
                 type = 'real',
                 value = '0.0',
                 texname = '0')

# This is a default parameter object representing the renormalization scale (MU_R).
MU_R = Parameter(name = 'MU_R',
                 nature = 'external',
                 type = 'real',
                 value = 91.188,
                 texname = '\\text{\\mu_r}',
                 lhablock = 'LOOP',
                 lhacode = [1])

# User-defined parameters.
LM1 = Parameter(name = 'LM1',
                nature = 'external',
                type = 'real',
                value = 0.3,
                texname = '\\lambda _1',
                lhablock = 'LQGEN1',
                lhacode = [ 1 ])

beta1 = Parameter(name = 'beta1',
                  nature = 'external',
                  type = 'real',
                  value = 0.5,
                  texname = '\\beta _1',
                  lhablock = 'LQGEN1',
                  lhacode = [ 2 ])

LM2 = Parameter(name = 'LM2',
                nature = 'external',
                type = 'real',
                value = 0.0,
                texname = '\\lambda _2',
                lhablock = 'LQGEN2',
                lhacode = [ 1 ])

beta2 = Parameter(name = 'beta2',
                  nature = 'external',
                  type = 'real',
                  value = 0.5,
                  texname = '\\beta _2',
                  lhablock = 'LQGEN2',
                  lhacode = [ 2 ])

LM3 = Parameter(name = 'LM3',
                nature = 'external',
                type = 'real',
                value = 0.0,
                texname = '\\lambda _3',
                lhablock = 'LQGEN3',
                lhacode = [ 1 ])

beta3 = Parameter(name = 'beta3',
                  nature = 'external',
                  type = 'real',
                  value = 0.5,
                  texname = '\\beta _3',
                  lhablock = 'LQGEN3',
                  lhacode = [ 2 ])

aEWM1 = Parameter(name = 'aEWM1',
                  nature = 'external',
                  type = 'real',
                  value = 127.9,
                  texname = '\\text{aEWM1}',
                  lhablock = 'SMINPUTS',
                  lhacode = [ 1 ])

Gf = Parameter(name = 'Gf',
               nature = 'external',
               type = 'real',
               value = 0.0000116637,
               texname = 'G_f',
               lhablock = 'SMINPUTS',
               lhacode = [ 2 ])

aS = Parameter(name = 'aS',
               nature = 'external',
               type = 'real',
               value = 0.1184,
               texname = '\\alpha _s',
               lhablock = 'SMINPUTS',
               lhacode = [ 3 ])

ymb = Parameter(name = 'ymb',
                nature = 'external',
                type = 'real',
                value = 4.7,
                texname = '\\text{ymb}',
                lhablock = 'YUKAWA',
                lhacode = [ 5 ])

ymt = Parameter(name = 'ymt',
                nature = 'external',
                type = 'real',
                value = 172,
                texname = '\\text{ymt}',
                lhablock = 'YUKAWA',
                lhacode = [ 6 ])

ymtau = Parameter(name = 'ymtau',
                  nature = 'external',
                  type = 'real',
                  value = 1.777,
                  texname = '\\text{ymtau}',
                  lhablock = 'YUKAWA',
                  lhacode = [ 15 ])

MZ = Parameter(name = 'MZ',
               nature = 'external',
               type = 'real',
               value = 91.1876,
               texname = '\\text{MZ}',
               lhablock = 'MASS',
               lhacode = [ 23 ])

MTA = Parameter(name = 'MTA',
                nature = 'external',
                type = 'real',
                value = 1.777,
                texname = '\\text{MTA}',
                lhablock = 'MASS',
                lhacode = [ 15 ])

MT = Parameter(name = 'MT',
               nature = 'external',
               type = 'real',
               value = 172,
               texname = '\\text{MT}',
               lhablock = 'MASS',
               lhacode = [ 6 ])

MB = Parameter(name = 'MB',
               nature = 'external',
               type = 'real',
               value = 4.7,
               texname = '\\text{MB}',
               lhablock = 'MASS',
               lhacode = [ 5 ])

MH = Parameter(name = 'MH',
               nature = 'external',
               type = 'real',
               value = 125,
               texname = '\\text{MH}',
               lhablock = 'MASS',
               lhacode = [ 25 ])

Msu = Parameter(name = 'Msu',
                nature = 'external',
                type = 'real',
                value = 1000,
                texname = '\\text{Msu}',
                lhablock = 'MASS',
                lhacode = [ 601 ])

Msd = Parameter(name = 'Msd',
                nature = 'external',
                type = 'real',
                value = 1000,
                texname = '\\text{Msd}',
                lhablock = 'MASS',
                lhacode = [ 602 ])

Msc = Parameter(name = 'Msc',
                nature = 'external',
                type = 'real',
                value = 1000,
                texname = '\\text{Msc}',
                lhablock = 'MASS',
                lhacode = [ 603 ])

Mss = Parameter(name = 'Mss',
                nature = 'external',
                type = 'real',
                value = 1000,
                texname = '\\text{Mss}',
                lhablock = 'MASS',
                lhacode = [ 604 ])

Mst = Parameter(name = 'Mst',
                nature = 'external',
                type = 'real',
                value = 1000,
                texname = '\\text{Mst}',
                lhablock = 'MASS',
                lhacode = [ 605 ])

Msb = Parameter(name = 'Msb',
                nature = 'external',
                type = 'real',
                value = 1000,
                texname = '\\text{Msb}',
                lhablock = 'MASS',
                lhacode = [ 606 ])

WZ = Parameter(name = 'WZ',
               nature = 'external',
               type = 'real',
               value = 2.4952,
               texname = '\\text{WZ}',
               lhablock = 'DECAY',
               lhacode = [ 23 ])

WW = Parameter(name = 'WW',
               nature = 'external',
               type = 'real',
               value = 2.085,
               texname = '\\text{WW}',
               lhablock = 'DECAY',
               lhacode = [ 24 ])

WT = Parameter(name = 'WT',
               nature = 'external',
               type = 'real',
               value = 1.50833649,
               texname = '\\text{WT}',
               lhablock = 'DECAY',
               lhacode = [ 6 ])

WH = Parameter(name = 'WH',
               nature = 'external',
               type = 'real',
               value = 0.00407,
               texname = '\\text{WH}',
               lhablock = 'DECAY',
               lhacode = [ 25 ])

Wsu = Parameter(name = 'Wsu',
                nature = 'external',
                type = 'real',
                value = 10,
                texname = '\\text{Wsu}',
                lhablock = 'DECAY',
                lhacode = [ 601 ])

Wsd = Parameter(name = 'Wsd',
                nature = 'external',
                type = 'real',
                value = 10,
                texname = '\\text{Wsd}',
                lhablock = 'DECAY',
                lhacode = [ 602 ])

Wsc = Parameter(name = 'Wsc',
                nature = 'external',
                type = 'real',
                value = 10,
                texname = '\\text{Wsc}',
                lhablock = 'DECAY',
                lhacode = [ 603 ])

Wss = Parameter(name = 'Wss',
                nature = 'external',
                type = 'real',
                value = 10,
                texname = '\\text{Wss}',
                lhablock = 'DECAY',
                lhacode = [ 604 ])

Wst = Parameter(name = 'Wst',
                nature = 'external',
                type = 'real',
                value = 10,
                texname = '\\text{Wst}',
                lhablock = 'DECAY',
                lhacode = [ 605 ])

Wsb = Parameter(name = 'Wsb',
                nature = 'external',
                type = 'real',
                value = 10,
                texname = '\\text{Wsb}',
                lhablock = 'DECAY',
                lhacode = [ 606 ])

aEW = Parameter(name = 'aEW',
                nature = 'internal',
                type = 'real',
                value = '1/aEWM1',
                texname = '\\alpha _{\\text{EW}}')

G = Parameter(name = 'G',
              nature = 'internal',
              type = 'real',
              value = '2*cmath.sqrt(aS)*cmath.sqrt(cmath.pi)',
              texname = 'G')

gsbTL = Parameter(name = 'gsbTL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM3*cmath.sqrt(beta3)',
                  texname = 'g_{\\text{sbTL}}')

gsbTR = Parameter(name = 'gsbTR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{sbTR}}')

gsbVTL = Parameter(name = 'gsbVTL',
                   nature = 'internal',
                   type = 'real',
                   value = 'LM3*cmath.sqrt(1 - beta3)',
                   texname = 'g_{\\text{sbVTL}}')

gscML = Parameter(name = 'gscML',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM2*cmath.sqrt(beta2)',
                  texname = 'g_{\\text{scML}}')

gscMR = Parameter(name = 'gscMR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{scMR}}')

gscVML = Parameter(name = 'gscVML',
                   nature = 'internal',
                   type = 'real',
                   value = 'LM2*cmath.sqrt(1 - beta2)',
                   texname = 'g_{\\text{scVML}}')

gsdEL = Parameter(name = 'gsdEL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM1*cmath.sqrt(beta1)',
                  texname = 'g_{\\text{sdEL}}')

gsdER = Parameter(name = 'gsdER',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{sdER}}')

gsdVEL = Parameter(name = 'gsdVEL',
                   nature = 'internal',
                   type = 'real',
                   value = 'LM1*cmath.sqrt(1 - beta1)',
                   texname = 'g_{\\text{sdVEL}}')

gssML = Parameter(name = 'gssML',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM2*cmath.sqrt(beta2)',
                  texname = 'g_{\\text{ssML}}')

gssMR = Parameter(name = 'gssMR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{ssMR}}')

gssVML = Parameter(name = 'gssVML',
                   nature = 'internal',
                   type = 'real',
                   value = 'LM2*cmath.sqrt(1 - beta2)',
                   texname = 'g_{\\text{ssVML}}')

gstTL = Parameter(name = 'gstTL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM3*cmath.sqrt(beta3)',
                  texname = 'g_{\\text{stTL}}')

gstTR = Parameter(name = 'gstTR',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{stTR}}')

gstVTL = Parameter(name = 'gstVTL',
                   nature = 'internal',
                   type = 'real',
                   value = 'LM3*cmath.sqrt(1 - beta3)',
                   texname = 'g_{\\text{stVTL}}')

gsuEL = Parameter(name = 'gsuEL',
                  nature = 'internal',
                  type = 'real',
                  value = 'LM1*cmath.sqrt(beta1)',
                  texname = 'g_{\\text{suEL}}')

gsuER = Parameter(name = 'gsuER',
                  nature = 'internal',
                  type = 'real',
                  value = '0',
                  texname = 'g_{\\text{suER}}')

gsuVEL = Parameter(name = 'gsuVEL',
                   nature = 'internal',
                   type = 'real',
                   value = 'LM1*cmath.sqrt(1 - beta1)',
                   texname = 'g_{\\text{suVEL}}')

MW = Parameter(name = 'MW',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(MZ**2/2. + cmath.sqrt(MZ**4/4. - (aEW*cmath.pi*MZ**2)/(Gf*cmath.sqrt(2))))',
               texname = 'M_W')

ee = Parameter(name = 'ee',
               nature = 'internal',
               type = 'real',
               value = '2*cmath.sqrt(aEW)*cmath.sqrt(cmath.pi)',
               texname = 'e')

sw2 = Parameter(name = 'sw2',
                nature = 'internal',
                type = 'real',
                value = '1 - MW**2/MZ**2',
                texname = '\\text{sw2}')

cw = Parameter(name = 'cw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(1 - sw2)',
               texname = 'c_w')

sw = Parameter(name = 'sw',
               nature = 'internal',
               type = 'real',
               value = 'cmath.sqrt(sw2)',
               texname = 's_w')

g1 = Parameter(name = 'g1',
               nature = 'internal',
               type = 'real',
               value = 'ee/cw',
               texname = 'g_1')

gw = Parameter(name = 'gw',
               nature = 'internal',
               type = 'real',
               value = 'ee/sw',
               texname = 'g_w')

vev = Parameter(name = 'vev',
                nature = 'internal',
                type = 'real',
                value = '(2*MW*sw)/ee',
                texname = '\\text{vev}')

lam = Parameter(name = 'lam',
                nature = 'internal',
                type = 'real',
                value = 'MH**2/(2.*vev**2)',
                texname = '\\text{lam}')

yb = Parameter(name = 'yb',
               nature = 'internal',
               type = 'real',
               value = '(ymb*cmath.sqrt(2))/vev',
               texname = '\\text{yb}')

yt = Parameter(name = 'yt',
               nature = 'internal',
               type = 'real',
               value = '(ymt*cmath.sqrt(2))/vev',
               texname = '\\text{yt}')

ytau = Parameter(name = 'ytau',
                 nature = 'internal',
                 type = 'real',
                 value = '(ymtau*cmath.sqrt(2))/vev',
                 texname = '\\text{ytau}')

muH = Parameter(name = 'muH',
                nature = 'internal',
                type = 'real',
                value = 'cmath.sqrt(lam*vev**2)',
                texname = '\\mu')

I1a33 = Parameter(name = 'I1a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I1a33}')

I2a33 = Parameter(name = 'I2a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I2a33}')

I3a33 = Parameter(name = 'I3a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yt',
                  texname = '\\text{I3a33}')

I4a33 = Parameter(name = 'I4a33',
                  nature = 'internal',
                  type = 'complex',
                  value = 'yb',
                  texname = '\\text{I4a33}')

