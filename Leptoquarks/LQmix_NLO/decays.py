# This file was automatically created by FeynRules =.2.4
# Mathematica version: 8.0 for Linux x86 (32-bit) (October 10, 2011)
# Date: Sat 3 Sep 2016 20:49:32


from object_library import all_decays, Decay
import particles as P


Decay_b = Decay(name = 'Decay_b',
                particle = P.b,
                partial_widths = {(P.lqsd,P.ve__tilde__):'((MB**2 - Msd**2)*(3*gsbVEL**2*MB**2 - 3*gsbVEL**2*Msd**2))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.lqsd,P.vm__tilde__):'((MB**2 - Msd**2)*(3*gsbVML**2*MB**2 - 3*gsbVML**2*Msd**2))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.lqsd,P.vt__tilde__):'((MB**2 - Msd**2)*(3*gsbVTL**2*MB**2 - 3*gsbVTL**2*Msd**2))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.lqsu,P.e__minus__):'((MB**2 - Msu**2)*(3*gsbEL**2*MB**2 + 3*gsbER**2*MB**2 - 3*gsbEL**2*Msu**2 - 3*gsbER**2*Msu**2))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.lqsu,P.mu__minus__):'((MB**2 - Msu**2)*(3*gsbML**2*MB**2 + 3*gsbMR**2*MB**2 - 3*gsbML**2*Msu**2 - 3*gsbMR**2*Msu**2))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.lqsu,P.ta__minus__):'((3*gsbTL**2*MB**2 + 3*gsbTR**2*MB**2 - 3*gsbTL**2*Msu**2 - 3*gsbTR**2*Msu**2 + 12*gsbTL*gsbTR*MB*MTA + 3*gsbTL**2*MTA**2 + 3*gsbTR**2*MTA**2)*cmath.sqrt(MB**4 - 2*MB**2*Msu**2 + Msu**4 - 2*MB**2*MTA**2 - 2*Msu**2*MTA**2 + MTA**4))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.W__minus__,P.t):'(((3*ee**2*MB**2)/(2.*sw**2) + (3*ee**2*MT**2)/(2.*sw**2) + (3*ee**2*MB**4)/(2.*MW**2*sw**2) - (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) + (3*ee**2*MT**4)/(2.*MW**2*sw**2) - (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(96.*cmath.pi*abs(MB)**3)'})

Decay_H = Decay(name = 'Decay_H',
                particle = P.H,
                partial_widths = {(P.b,P.b__tilde__):'((-12*MB**2*yb**2 + 3*MH**2*yb**2)*cmath.sqrt(-4*MB**2*MH**2 + MH**4))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.t,P.t__tilde__):'((3*MH**2*yt**2 - 12*MT**2*yt**2)*cmath.sqrt(MH**4 - 4*MH**2*MT**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((MH**2*ytau**2 - 4*MTA**2*ytau**2)*cmath.sqrt(MH**4 - 4*MH**2*MTA**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.W__minus__,P.W__plus__):'(((3*ee**4*vev**2)/(4.*sw**4) + (ee**4*MH**4*vev**2)/(16.*MW**4*sw**4) - (ee**4*MH**2*vev**2)/(4.*MW**2*sw**4))*cmath.sqrt(MH**4 - 4*MH**2*MW**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.Z,P.Z):'(((9*ee**4*vev**2)/2. + (3*ee**4*MH**4*vev**2)/(8.*MZ**4) - (3*ee**4*MH**2*vev**2)/(2.*MZ**2) + (3*cw**4*ee**4*vev**2)/(4.*sw**4) + (cw**4*ee**4*MH**4*vev**2)/(16.*MZ**4*sw**4) - (cw**4*ee**4*MH**2*vev**2)/(4.*MZ**2*sw**4) + (3*cw**2*ee**4*vev**2)/sw**2 + (cw**2*ee**4*MH**4*vev**2)/(4.*MZ**4*sw**2) - (cw**2*ee**4*MH**2*vev**2)/(MZ**2*sw**2) + (3*ee**4*sw**2*vev**2)/cw**2 + (ee**4*MH**4*sw**2*vev**2)/(4.*cw**2*MZ**4) - (ee**4*MH**2*sw**2*vev**2)/(cw**2*MZ**2) + (3*ee**4*sw**4*vev**2)/(4.*cw**4) + (ee**4*MH**4*sw**4*vev**2)/(16.*cw**4*MZ**4) - (ee**4*MH**2*sw**4*vev**2)/(4.*cw**4*MZ**2))*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)'})

Decay_lqsd = Decay(name = 'Decay_lqsd',
                   particle = P.lqsd,
                   partial_widths = {(P.b,P.ve):'((-MB**2 + Msd**2)*(-3*gsbVEL**2*MB**2 + 3*gsbVEL**2*Msd**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.b,P.vm):'((-MB**2 + Msd**2)*(-3*gsbVML**2*MB**2 + 3*gsbVML**2*Msd**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.b,P.vt):'((-MB**2 + Msd**2)*(-3*gsbVTL**2*MB**2 + 3*gsbVTL**2*Msd**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.c,P.e__minus__):'(Msd**2*(3*gscEL**2*Msd**2 + 3*gscER**2*Msd**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.c,P.mu__minus__):'(Msd**2*(3*gscML**2*Msd**2 + 3*gscMR**2*Msd**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.c,P.ta__minus__):'((Msd**2 - MTA**2)*(3*gscTL**2*Msd**2 + 3*gscTR**2*Msd**2 - 3*gscTL**2*MTA**2 - 3*gscTR**2*MTA**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.d,P.ve):'(gsdVEL**2*Msd**4)/(16.*cmath.pi*abs(Msd)**3)',
                                     (P.d,P.vm):'(gsdVML**2*Msd**4)/(16.*cmath.pi*abs(Msd)**3)',
                                     (P.d,P.vt):'(gsdVTL**2*Msd**4)/(16.*cmath.pi*abs(Msd)**3)',
                                     (P.e__minus__,P.t):'((Msd**2 - MT**2)*(3*gstEL**2*Msd**2 + 3*gstER**2*Msd**2 - 3*gstEL**2*MT**2 - 3*gstER**2*MT**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.e__minus__,P.u):'(Msd**2*(3*gsuEL**2*Msd**2 + 3*gsuER**2*Msd**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.mu__minus__,P.t):'((Msd**2 - MT**2)*(3*gstML**2*Msd**2 + 3*gstMR**2*Msd**2 - 3*gstML**2*MT**2 - 3*gstMR**2*MT**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.mu__minus__,P.u):'(Msd**2*(3*gsuML**2*Msd**2 + 3*gsuMR**2*Msd**2))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.s,P.ve):'(gssVEL**2*Msd**4)/(16.*cmath.pi*abs(Msd)**3)',
                                     (P.s,P.vm):'(gssVML**2*Msd**4)/(16.*cmath.pi*abs(Msd)**3)',
                                     (P.s,P.vt):'(gssVTL**2*Msd**4)/(16.*cmath.pi*abs(Msd)**3)',
                                     (P.t,P.ta__minus__):'((3*gstTL**2*Msd**2 + 3*gstTR**2*Msd**2 - 3*gstTL**2*MT**2 - 3*gstTR**2*MT**2 - 12*gstTL*gstTR*MT*MTA - 3*gstTL**2*MTA**2 - 3*gstTR**2*MTA**2)*cmath.sqrt(Msd**4 - 2*Msd**2*MT**2 + MT**4 - 2*Msd**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(48.*cmath.pi*abs(Msd)**3)',
                                     (P.ta__minus__,P.u):'((Msd**2 - MTA**2)*(3*gsuTL**2*Msd**2 + 3*gsuTR**2*Msd**2 - 3*gsuTL**2*MTA**2 - 3*gsuTR**2*MTA**2))/(48.*cmath.pi*abs(Msd)**3)'})

Decay_lqsu = Decay(name = 'Decay_lqsu',
                   particle = P.lqsu,
                   partial_widths = {(P.b,P.e__plus__):'((-MB**2 + Msu**2)*(-3*gsbEL**2*MB**2 - 3*gsbER**2*MB**2 + 3*gsbEL**2*Msu**2 + 3*gsbER**2*Msu**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.b,P.mu__plus__):'((-MB**2 + Msu**2)*(-3*gsbML**2*MB**2 - 3*gsbMR**2*MB**2 + 3*gsbML**2*Msu**2 + 3*gsbMR**2*Msu**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.b,P.ta__plus__):'((-3*gsbTL**2*MB**2 - 3*gsbTR**2*MB**2 + 3*gsbTL**2*Msu**2 + 3*gsbTR**2*Msu**2 - 12*gsbTL*gsbTR*MB*MTA - 3*gsbTL**2*MTA**2 - 3*gsbTR**2*MTA**2)*cmath.sqrt(MB**4 - 2*MB**2*Msu**2 + Msu**4 - 2*MB**2*MTA**2 - 2*Msu**2*MTA**2 + MTA**4))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.c,P.ve__tilde__):'(gscVEL**2*Msu**4)/(16.*cmath.pi*abs(Msu)**3)',
                                     (P.c,P.vm__tilde__):'(gscVML**2*Msu**4)/(16.*cmath.pi*abs(Msu)**3)',
                                     (P.c,P.vt__tilde__):'(gscVTL**2*Msu**4)/(16.*cmath.pi*abs(Msu)**3)',
                                     (P.d,P.e__plus__):'(Msu**2*(3*gsdEL**2*Msu**2 + 3*gsdER**2*Msu**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.d,P.mu__plus__):'(Msu**2*(3*gsdML**2*Msu**2 + 3*gsdMR**2*Msu**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.d,P.ta__plus__):'((Msu**2 - MTA**2)*(3*gsdTL**2*Msu**2 + 3*gsdTR**2*Msu**2 - 3*gsdTL**2*MTA**2 - 3*gsdTR**2*MTA**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.s,P.e__plus__):'(Msu**2*(3*gssEL**2*Msu**2 + 3*gssER**2*Msu**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.s,P.mu__plus__):'(Msu**2*(3*gssML**2*Msu**2 + 3*gssMR**2*Msu**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.s,P.ta__plus__):'((Msu**2 - MTA**2)*(3*gssTL**2*Msu**2 + 3*gssTR**2*Msu**2 - 3*gssTL**2*MTA**2 - 3*gssTR**2*MTA**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.t,P.ve__tilde__):'((Msu**2 - MT**2)*(3*gstVEL**2*Msu**2 - 3*gstVEL**2*MT**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.t,P.vm__tilde__):'((Msu**2 - MT**2)*(3*gstVML**2*Msu**2 - 3*gstVML**2*MT**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.t,P.vt__tilde__):'((Msu**2 - MT**2)*(3*gstVTL**2*Msu**2 - 3*gstVTL**2*MT**2))/(48.*cmath.pi*abs(Msu)**3)',
                                     (P.u,P.ve__tilde__):'(gsuVEL**2*Msu**4)/(16.*cmath.pi*abs(Msu)**3)',
                                     (P.u,P.vm__tilde__):'(gsuVML**2*Msu**4)/(16.*cmath.pi*abs(Msu)**3)',
                                     (P.u,P.vt__tilde__):'(gsuVTL**2*Msu**4)/(16.*cmath.pi*abs(Msu)**3)'})

Decay_t = Decay(name = 'Decay_t',
                particle = P.t,
                partial_widths = {(P.lqsd,P.e__plus__):'((-Msd**2 + MT**2)*(-3*gstEL**2*Msd**2 - 3*gstER**2*Msd**2 + 3*gstEL**2*MT**2 + 3*gstER**2*MT**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.lqsd,P.mu__plus__):'((-Msd**2 + MT**2)*(-3*gstML**2*Msd**2 - 3*gstMR**2*Msd**2 + 3*gstML**2*MT**2 + 3*gstMR**2*MT**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.lqsd,P.ta__plus__):'((-3*gstTL**2*Msd**2 - 3*gstTR**2*Msd**2 + 3*gstTL**2*MT**2 + 3*gstTR**2*MT**2 + 12*gstTL*gstTR*MT*MTA + 3*gstTL**2*MTA**2 + 3*gstTR**2*MTA**2)*cmath.sqrt(Msd**4 - 2*Msd**2*MT**2 + MT**4 - 2*Msd**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.lqsu,P.ve):'((-Msu**2 + MT**2)*(-3*gstVEL**2*Msu**2 + 3*gstVEL**2*MT**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.lqsu,P.vm):'((-Msu**2 + MT**2)*(-3*gstVML**2*Msu**2 + 3*gstVML**2*MT**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.lqsu,P.vt):'((-Msu**2 + MT**2)*(-3*gstVTL**2*Msu**2 + 3*gstVTL**2*MT**2))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.W__plus__,P.b):'(((3*ee**2*MB**2)/(2.*sw**2) + (3*ee**2*MT**2)/(2.*sw**2) + (3*ee**2*MB**4)/(2.*MW**2*sw**2) - (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) + (3*ee**2*MT**4)/(2.*MW**2*sw**2) - (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(96.*cmath.pi*abs(MT)**3)'})

Decay_ta__minus__ = Decay(name = 'Decay_ta__minus__',
                          particle = P.ta__minus__,
                          partial_widths = {(P.lqsd,P.c__tilde__):'((-Msd**2 + MTA**2)*(-3*gscTL**2*Msd**2 - 3*gscTR**2*Msd**2 + 3*gscTL**2*MTA**2 + 3*gscTR**2*MTA**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.lqsd,P.t__tilde__):'((-3*gstTL**2*Msd**2 - 3*gstTR**2*Msd**2 + 3*gstTL**2*MT**2 + 3*gstTR**2*MT**2 + 12*gstTL*gstTR*MT*MTA + 3*gstTL**2*MTA**2 + 3*gstTR**2*MTA**2)*cmath.sqrt(Msd**4 - 2*Msd**2*MT**2 + MT**4 - 2*Msd**2*MTA**2 - 2*MT**2*MTA**2 + MTA**4))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.lqsd,P.u__tilde__):'((-Msd**2 + MTA**2)*(-3*gsuTL**2*Msd**2 - 3*gsuTR**2*Msd**2 + 3*gsuTL**2*MTA**2 + 3*gsuTR**2*MTA**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.lqsu__tilde__,P.b):'((3*gsbTL**2*MB**2 + 3*gsbTR**2*MB**2 - 3*gsbTL**2*Msu**2 - 3*gsbTR**2*Msu**2 + 12*gsbTL*gsbTR*MB*MTA + 3*gsbTL**2*MTA**2 + 3*gsbTR**2*MTA**2)*cmath.sqrt(MB**4 - 2*MB**2*Msu**2 + Msu**4 - 2*MB**2*MTA**2 - 2*Msu**2*MTA**2 + MTA**4))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.lqsu__tilde__,P.d):'((-Msu**2 + MTA**2)*(-3*gsdTL**2*Msu**2 - 3*gsdTR**2*Msu**2 + 3*gsdTL**2*MTA**2 + 3*gsdTR**2*MTA**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.lqsu__tilde__,P.s):'((-Msu**2 + MTA**2)*(-3*gssTL**2*Msu**2 - 3*gssTR**2*Msu**2 + 3*gssTL**2*MTA**2 + 3*gssTR**2*MTA**2))/(32.*cmath.pi*abs(MTA)**3)',
                                            (P.W__minus__,P.vt):'((MTA**2 - MW**2)*((ee**2*MTA**2)/(2.*sw**2) + (ee**2*MTA**4)/(2.*MW**2*sw**2) - (ee**2*MW**2)/sw**2))/(32.*cmath.pi*abs(MTA)**3)'})

Decay_W__plus__ = Decay(name = 'Decay_W__plus__',
                        particle = P.W__plus__,
                        partial_widths = {(P.c,P.s__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.t,P.b__tilde__):'(((-3*ee**2*MB**2)/(2.*sw**2) - (3*ee**2*MT**2)/(2.*sw**2) - (3*ee**2*MB**4)/(2.*MW**2*sw**2) + (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) - (3*ee**2*MT**4)/(2.*MW**2*sw**2) + (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(48.*cmath.pi*abs(MW)**3)',
                                          (P.u,P.d__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.ve,P.e__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vm,P.mu__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vt,P.ta__plus__):'((-MTA**2 + MW**2)*(-(ee**2*MTA**2)/(2.*sw**2) - (ee**2*MTA**4)/(2.*MW**2*sw**2) + (ee**2*MW**2)/sw**2))/(48.*cmath.pi*abs(MW)**3)'})

Decay_Z = Decay(name = 'Decay_Z',
                particle = P.Z,
                partial_widths = {(P.b,P.b__tilde__):'((-7*ee**2*MB**2 + ee**2*MZ**2 - (3*cw**2*ee**2*MB**2)/(2.*sw**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) - (17*ee**2*MB**2*sw**2)/(6.*cw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2))*cmath.sqrt(-4*MB**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.c,P.c__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.d,P.d__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.e__minus__,P.e__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.mu__minus__,P.mu__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.s,P.s__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.t,P.t__tilde__):'((-11*ee**2*MT**2 - ee**2*MZ**2 - (3*cw**2*ee**2*MT**2)/(2.*sw**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MT**2*sw**2)/(6.*cw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2))*cmath.sqrt(-4*MT**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((-5*ee**2*MTA**2 - ee**2*MZ**2 - (cw**2*ee**2*MTA**2)/(2.*sw**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MTA**2*sw**2)/(2.*cw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2))*cmath.sqrt(-4*MTA**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.u,P.u__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ve,P.ve__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vm,P.vm__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vt,P.vt__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.W__minus__,P.W__plus__):'(((-12*cw**2*ee**2*MW**2)/sw**2 - (17*cw**2*ee**2*MZ**2)/sw**2 + (4*cw**2*ee**2*MZ**4)/(MW**2*sw**2) + (cw**2*ee**2*MZ**6)/(4.*MW**4*sw**2))*cmath.sqrt(-4*MW**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)'})

