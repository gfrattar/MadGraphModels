# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.1.0 for Mac OS X ARM (64-bit) (June 16, 2022)
# Date: Tue 28 Nov 2023 12:50:08


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

EWL = CouplingOrder(name = 'EWL',
                    expansion_order = 99,
                    hierarchy = 2)

EWR = CouplingOrder(name = 'EWR',
                    expansion_order = 99,
                    hierarchy = 2)

HCP = CouplingOrder(name = 'HCP',
                    expansion_order = 99,
                    hierarchy = 1)

HIG = CouplingOrder(name = 'HIG',
                    expansion_order = 99,
                    hierarchy = 1)

HIW = CouplingOrder(name = 'HIW',
                    expansion_order = 99,
                    hierarchy = 1)

