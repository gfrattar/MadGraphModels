# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.2.0 for Mac OS X ARM (64-bit) (November 18, 2022)
# Date: Sat 2 Mar 2024 16:50:23


from object_library import all_vertices, all_CTvertices, Vertex, CTVertex
import particles as P
import CT_couplings as C
import lorentz as L


V_1 = CTVertex(name = 'V_1',
               type = 'R2',
               particles = [ P.g, P.g, P.g ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVV1, L.VVV2 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(0,0,0):C.R2GC_295_135,(0,1,1):C.R2GC_235_87})

V_2 = CTVertex(name = 'V_2',
               type = 'R2',
               particles = [ P.g, P.g, P.g, P.g ],
               color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
               lorentz = [ L.VVVV11, L.VVVV2, L.VVVV3, L.VVVV5 ],
               loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ] ],
               couplings = {(2,1,0):C.R2GC_248_99,(2,1,1):C.R2GC_248_100,(0,1,0):C.R2GC_248_99,(0,1,1):C.R2GC_248_100,(4,1,0):C.R2GC_246_95,(4,1,1):C.R2GC_246_96,(3,1,0):C.R2GC_246_95,(3,1,1):C.R2GC_246_96,(8,1,0):C.R2GC_247_97,(8,1,1):C.R2GC_247_98,(6,1,0):C.R2GC_251_104,(6,1,1):C.R2GC_300_141,(7,1,0):C.R2GC_252_105,(7,1,1):C.R2GC_299_140,(5,1,0):C.R2GC_246_95,(5,1,1):C.R2GC_246_96,(1,1,0):C.R2GC_246_95,(1,1,1):C.R2GC_246_96,(11,0,0):C.R2GC_250_102,(11,0,1):C.R2GC_250_103,(10,0,0):C.R2GC_250_102,(10,0,1):C.R2GC_250_103,(9,0,1):C.R2GC_249_101,(0,2,0):C.R2GC_248_99,(0,2,1):C.R2GC_248_100,(2,2,0):C.R2GC_248_99,(2,2,1):C.R2GC_248_100,(5,2,0):C.R2GC_246_95,(5,2,1):C.R2GC_246_96,(1,2,0):C.R2GC_246_95,(1,2,1):C.R2GC_246_96,(7,2,0):C.R2GC_252_105,(7,2,1):C.R2GC_247_98,(4,2,0):C.R2GC_246_95,(4,2,1):C.R2GC_246_96,(3,2,0):C.R2GC_246_95,(3,2,1):C.R2GC_246_96,(8,2,0):C.R2GC_247_97,(8,2,1):C.R2GC_299_140,(6,2,0):C.R2GC_296_136,(6,2,1):C.R2GC_296_137,(0,3,0):C.R2GC_248_99,(0,3,1):C.R2GC_248_100,(2,3,0):C.R2GC_248_99,(2,3,1):C.R2GC_248_100,(5,3,0):C.R2GC_246_95,(5,3,1):C.R2GC_246_96,(1,3,0):C.R2GC_246_95,(1,3,1):C.R2GC_246_96,(7,3,0):C.R2GC_297_138,(7,3,1):C.R2GC_297_139,(4,3,0):C.R2GC_246_95,(4,3,1):C.R2GC_246_96,(3,3,0):C.R2GC_246_95,(3,3,1):C.R2GC_246_96,(8,3,0):C.R2GC_247_97,(8,3,1):C.R2GC_297_139,(6,3,0):C.R2GC_251_104})

V_3 = CTVertex(name = 'V_3',
               type = 'R2',
               particles = [ P.d__tilde__, P.d, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.d, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_603_214,(0,1,0):C.R2GC_567_178})

V_4 = CTVertex(name = 'V_4',
               type = 'R2',
               particles = [ P.s__tilde__, P.d, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.d, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_604_215,(0,1,0):C.R2GC_568_179})

V_5 = CTVertex(name = 'V_5',
               type = 'R2',
               particles = [ P.b__tilde__, P.d, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.b, P.d, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_605_216,(0,1,0):C.R2GC_569_180})

V_6 = CTVertex(name = 'V_6',
               type = 'R2',
               particles = [ P.d__tilde__, P.s, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.d, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_615_226,(0,1,0):C.R2GC_579_190})

V_7 = CTVertex(name = 'V_7',
               type = 'R2',
               particles = [ P.s__tilde__, P.s, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_616_227,(0,1,0):C.R2GC_580_191})

V_8 = CTVertex(name = 'V_8',
               type = 'R2',
               particles = [ P.b__tilde__, P.s, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.b, P.g, P.s] ] ],
               couplings = {(0,0,0):C.R2GC_617_228,(0,1,0):C.R2GC_581_192})

V_9 = CTVertex(name = 'V_9',
               type = 'R2',
               particles = [ P.d__tilde__, P.b, P.h ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFS2, L.FFS4 ],
               loop_particles = [ [ [P.b, P.d, P.g] ] ],
               couplings = {(0,0,0):C.R2GC_627_238,(0,1,0):C.R2GC_591_202})

V_10 = CTVertex(name = 'V_10',
                type = 'R2',
                particles = [ P.s__tilde__, P.b, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_628_239,(0,1,0):C.R2GC_592_203})

V_11 = CTVertex(name = 'V_11',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_629_240,(0,1,0):C.R2GC_593_204})

V_12 = CTVertex(name = 'V_12',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_606_217,(0,1,0):C.R2GC_570_181})

V_13 = CTVertex(name = 'V_13',
                type = 'R2',
                particles = [ P.s__tilde__, P.d, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_607_218,(0,1,0):C.R2GC_571_182})

V_14 = CTVertex(name = 'V_14',
                type = 'R2',
                particles = [ P.b__tilde__, P.d, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_608_219,(0,1,0):C.R2GC_572_183})

V_15 = CTVertex(name = 'V_15',
                type = 'R2',
                particles = [ P.d__tilde__, P.s, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_618_229,(0,1,0):C.R2GC_582_193})

V_16 = CTVertex(name = 'V_16',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_619_230,(0,1,0):C.R2GC_583_194})

V_17 = CTVertex(name = 'V_17',
                type = 'R2',
                particles = [ P.b__tilde__, P.s, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_620_231,(0,1,0):C.R2GC_584_195})

V_18 = CTVertex(name = 'V_18',
                type = 'R2',
                particles = [ P.d__tilde__, P.b, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_630_241,(0,1,0):C.R2GC_594_205})

V_19 = CTVertex(name = 'V_19',
                type = 'R2',
                particles = [ P.s__tilde__, P.b, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_631_242,(0,1,0):C.R2GC_595_206})

V_20 = CTVertex(name = 'V_20',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_632_243,(0,1,0):C.R2GC_596_207})

V_21 = CTVertex(name = 'V_21',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_609_220,(0,1,0):C.R2GC_573_184})

V_22 = CTVertex(name = 'V_22',
                type = 'R2',
                particles = [ P.s__tilde__, P.d, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_610_221,(0,1,0):C.R2GC_574_185})

V_23 = CTVertex(name = 'V_23',
                type = 'R2',
                particles = [ P.b__tilde__, P.d, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_611_222,(0,1,0):C.R2GC_575_186})

V_24 = CTVertex(name = 'V_24',
                type = 'R2',
                particles = [ P.d__tilde__, P.s, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_621_232,(0,1,0):C.R2GC_585_196})

V_25 = CTVertex(name = 'V_25',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_622_233,(0,1,0):C.R2GC_586_197})

V_26 = CTVertex(name = 'V_26',
                type = 'R2',
                particles = [ P.b__tilde__, P.s, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_623_234,(0,1,0):C.R2GC_587_198})

V_27 = CTVertex(name = 'V_27',
                type = 'R2',
                particles = [ P.d__tilde__, P.b, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_633_244,(0,1,0):C.R2GC_597_208})

V_28 = CTVertex(name = 'V_28',
                type = 'R2',
                particles = [ P.s__tilde__, P.b, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_634_245,(0,1,0):C.R2GC_598_209})

V_29 = CTVertex(name = 'V_29',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_635_246,(0,1,0):C.R2GC_599_210})

V_30 = CTVertex(name = 'V_30',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_612_223,(0,1,0):C.R2GC_576_187})

V_31 = CTVertex(name = 'V_31',
                type = 'R2',
                particles = [ P.s__tilde__, P.d, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_613_224,(0,1,0):C.R2GC_577_188})

V_32 = CTVertex(name = 'V_32',
                type = 'R2',
                particles = [ P.b__tilde__, P.d, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_614_225,(0,1,0):C.R2GC_578_189})

V_33 = CTVertex(name = 'V_33',
                type = 'R2',
                particles = [ P.d__tilde__, P.s, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_624_235,(0,1,0):C.R2GC_588_199})

V_34 = CTVertex(name = 'V_34',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_625_236,(0,1,0):C.R2GC_589_200})

V_35 = CTVertex(name = 'V_35',
                type = 'R2',
                particles = [ P.b__tilde__, P.s, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_626_237,(0,1,0):C.R2GC_590_201})

V_36 = CTVertex(name = 'V_36',
                type = 'R2',
                particles = [ P.d__tilde__, P.b, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_636_247,(0,1,0):C.R2GC_600_211})

V_37 = CTVertex(name = 'V_37',
                type = 'R2',
                particles = [ P.s__tilde__, P.b, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_637_248,(0,1,0):C.R2GC_601_212})

V_38 = CTVertex(name = 'V_38',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_638_249,(0,1,0):C.R2GC_602_213})

V_39 = CTVertex(name = 'V_39',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.PhipL__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_691_250,(0,1,0):C.R2GC_1273_43})

V_40 = CTVertex(name = 'V_40',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.PhipL__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_692_251,(0,1,0):C.R2GC_1274_44})

V_41 = CTVertex(name = 'V_41',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.PhipL__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_693_252,(0,1,0):C.R2GC_1275_45})

V_42 = CTVertex(name = 'V_42',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.Hp__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_328_168,(0,0,0):C.R2GC_1259_29})

V_43 = CTVertex(name = 'V_43',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.Hp__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_329_169,(0,0,0):C.R2GC_1260_30})

V_44 = CTVertex(name = 'V_44',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.Hp__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_330_170,(0,0,0):C.R2GC_1261_31})

V_45 = CTVertex(name = 'V_45',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.PhipR__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_305_143})

V_46 = CTVertex(name = 'V_46',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.PhipR__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_306_144})

V_47 = CTVertex(name = 'V_47',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.PhipR__tilde__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_307_145})

V_48 = CTVertex(name = 'V_48',
                type = 'R2',
                particles = [ P.t__tilde__, P.d, P.Hp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_331_171,(0,1,0):C.R2GC_1270_40})

V_49 = CTVertex(name = 'V_49',
                type = 'R2',
                particles = [ P.t__tilde__, P.s, P.Hp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_332_172,(0,1,0):C.R2GC_1271_41})

V_50 = CTVertex(name = 'V_50',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.Hp ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_333_173,(0,1,0):C.R2GC_1272_42})

V_51 = CTVertex(name = 'V_51',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.h ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_308_146,(0,0,0):C.R2GC_1262_32,(0,1,0):C.R2GC_1263_33})

V_52 = CTVertex(name = 'V_52',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.DD ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_309_147,(0,0,0):C.R2GC_1264_34,(0,1,0):C.R2GC_1265_35})

V_53 = CTVertex(name = 'V_53',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.HH ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_310_148,(0,0,0):C.R2GC_1266_36,(0,1,0):C.R2GC_1267_37})

V_54 = CTVertex(name = 'V_54',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.AA ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,2,0):C.R2GC_311_149,(0,0,0):C.R2GC_1268_38,(0,1,0):C.R2GC_1269_39})

V_55 = CTVertex(name = 'V_55',
                type = 'R2',
                particles = [ P.t__tilde__, P.d, P.PhipR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_312_150})

V_56 = CTVertex(name = 'V_56',
                type = 'R2',
                particles = [ P.t__tilde__, P.s, P.PhipR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_313_151})

V_57 = CTVertex(name = 'V_57',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.PhipR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_314_152})

V_58 = CTVertex(name = 'V_58',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Phi0L ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_1528_68})

V_59 = CTVertex(name = 'V_59',
                type = 'R2',
                particles = [ P.t__tilde__, P.d, P.PhipL ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_694_253,(0,0,0):C.R2GC_1276_46})

V_60 = CTVertex(name = 'V_60',
                type = 'R2',
                particles = [ P.t__tilde__, P.s, P.PhipL ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_695_254,(0,0,0):C.R2GC_1277_47})

V_61 = CTVertex(name = 'V_61',
                type = 'R2',
                particles = [ P.t__tilde__, P.b, P.PhipL ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS2, L.FFS4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_696_255,(0,0,0):C.R2GC_1278_48})

V_62 = CTVertex(name = 'V_62',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Phi0R ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFS1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_1323_55})

V_63 = CTVertex(name = 'V_63',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_790_329})

V_64 = CTVertex(name = 'V_64',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_790_329})

V_65 = CTVertex(name = 'V_65',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_790_329})

V_66 = CTVertex(name = 'V_66',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_1491_66,(0,1,0):C.R2GC_1492_67})

V_67 = CTVertex(name = 'V_67',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_1491_66,(0,1,0):C.R2GC_1492_67})

V_68 = CTVertex(name = 'V_68',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_1491_66,(0,1,0):C.R2GC_1492_67})

V_69 = CTVertex(name = 'V_69',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.ZR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_1866_85,(0,1,0):C.R2GC_1867_86})

V_70 = CTVertex(name = 'V_70',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.ZR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_1866_85,(0,1,0):C.R2GC_1867_86})

V_71 = CTVertex(name = 'V_71',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.ZR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_1866_85,(0,1,0):C.R2GC_1867_86})

V_72 = CTVertex(name = 'V_72',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_285_124,(0,1,0):C.R2GC_974_367})

V_73 = CTVertex(name = 'V_73',
                type = 'R2',
                particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_289_128,(0,1,0):C.R2GC_978_371})

V_74 = CTVertex(name = 'V_74',
                type = 'R2',
                particles = [ P.b__tilde__, P.u, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_281_120,(0,1,0):C.R2GC_970_363})

V_75 = CTVertex(name = 'V_75',
                type = 'R2',
                particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_273_112,(0,1,0):C.R2GC_962_355})

V_76 = CTVertex(name = 'V_76',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_277_116,(0,1,0):C.R2GC_966_359})

V_77 = CTVertex(name = 'V_77',
                type = 'R2',
                particles = [ P.b__tilde__, P.c, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_269_108,(0,1,0):C.R2GC_958_351})

V_78 = CTVertex(name = 'V_78',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_315_153,(0,1,0):C.R2GC_984_377})

V_79 = CTVertex(name = 'V_79',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_316_154,(0,1,0):C.R2GC_985_378})

V_80 = CTVertex(name = 'V_80',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_317_155,(0,1,0):C.R2GC_986_379})

V_81 = CTVertex(name = 'V_81',
                type = 'R2',
                particles = [ P.d__tilde__, P.u, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_286_125,(0,0,0):C.R2GC_973_366})

V_82 = CTVertex(name = 'V_82',
                type = 'R2',
                particles = [ P.s__tilde__, P.u, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_290_129,(0,0,0):C.R2GC_977_370})

V_83 = CTVertex(name = 'V_83',
                type = 'R2',
                particles = [ P.b__tilde__, P.u, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g, P.u] ] ],
                couplings = {(0,1,0):C.R2GC_282_121,(0,0,0):C.R2GC_969_362})

V_84 = CTVertex(name = 'V_84',
                type = 'R2',
                particles = [ P.d__tilde__, P.c, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.d, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_274_113,(0,0,0):C.R2GC_961_354})

V_85 = CTVertex(name = 'V_85',
                type = 'R2',
                particles = [ P.s__tilde__, P.c, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.c, P.g, P.s] ] ],
                couplings = {(0,1,0):C.R2GC_278_117,(0,0,0):C.R2GC_965_358})

V_86 = CTVertex(name = 'V_86',
                type = 'R2',
                particles = [ P.b__tilde__, P.c, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.c, P.g] ] ],
                couplings = {(0,1,0):C.R2GC_270_109,(0,0,0):C.R2GC_957_350})

V_87 = CTVertex(name = 'V_87',
                type = 'R2',
                particles = [ P.d__tilde__, P.t, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_318_156,(0,0,0):C.R2GC_981_374})

V_88 = CTVertex(name = 'V_88',
                type = 'R2',
                particles = [ P.s__tilde__, P.t, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_319_157,(0,0,0):C.R2GC_982_375})

V_89 = CTVertex(name = 'V_89',
                type = 'R2',
                particles = [ P.b__tilde__, P.t, P.WR__minus__ ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g, P.t] ] ],
                couplings = {(0,1,0):C.R2GC_320_158,(0,0,0):C.R2GC_983_376})

V_90 = CTVertex(name = 'V_90',
                type = 'R2',
                particles = [ P.u__tilde__, P.u, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.u] ] ],
                couplings = {(0,0,0):C.R2GC_255_107})

V_91 = CTVertex(name = 'V_91',
                type = 'R2',
                particles = [ P.c__tilde__, P.c, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.c, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_255_107})

V_92 = CTVertex(name = 'V_92',
                type = 'R2',
                particles = [ P.t__tilde__, P.t, P.g ],
                color = [ 'T(3,2,1)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.t] ] ],
                couplings = {(0,0,0):C.R2GC_255_107})

V_93 = CTVertex(name = 'V_93',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_789_328})

V_94 = CTVertex(name = 'V_94',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_789_328})

V_95 = CTVertex(name = 'V_95',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.a ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV1 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_789_328})

V_96 = CTVertex(name = 'V_96',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_1489_64,(0,1,0):C.R2GC_1490_65})

V_97 = CTVertex(name = 'V_97',
                type = 'R2',
                particles = [ P.s__tilde__, P.s, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.g, P.s] ] ],
                couplings = {(0,0,0):C.R2GC_1489_64,(0,1,0):C.R2GC_1490_65})

V_98 = CTVertex(name = 'V_98',
                type = 'R2',
                particles = [ P.b__tilde__, P.b, P.Z ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.b, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_1489_64,(0,1,0):C.R2GC_1490_65})

V_99 = CTVertex(name = 'V_99',
                type = 'R2',
                particles = [ P.d__tilde__, P.d, P.ZR ],
                color = [ 'Identity(1,2)' ],
                lorentz = [ L.FFV3, L.FFV4 ],
                loop_particles = [ [ [P.d, P.g] ] ],
                couplings = {(0,0,0):C.R2GC_1864_83,(0,1,0):C.R2GC_1865_84})

V_100 = CTVertex(name = 'V_100',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_1864_83,(0,1,0):C.R2GC_1865_84})

V_101 = CTVertex(name = 'V_101',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_1864_83,(0,1,0):C.R2GC_1865_84})

V_102 = CTVertex(name = 'V_102',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_287_126,(0,1,0):C.R2GC_976_369})

V_103 = CTVertex(name = 'V_103',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_275_114,(0,1,0):C.R2GC_964_357})

V_104 = CTVertex(name = 'V_104',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_321_159,(0,1,0):C.R2GC_990_383})

V_105 = CTVertex(name = 'V_105',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_291_130,(0,1,0):C.R2GC_980_373})

V_106 = CTVertex(name = 'V_106',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_279_118,(0,1,0):C.R2GC_968_361})

V_107 = CTVertex(name = 'V_107',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_322_160,(0,1,0):C.R2GC_991_384})

V_108 = CTVertex(name = 'V_108',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_283_122,(0,1,0):C.R2GC_972_365})

V_109 = CTVertex(name = 'V_109',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_271_110,(0,1,0):C.R2GC_960_353})

V_110 = CTVertex(name = 'V_110',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_323_161,(0,1,0):C.R2GC_992_385})

V_111 = CTVertex(name = 'V_111',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_288_127,(0,0,0):C.R2GC_975_368})

V_112 = CTVertex(name = 'V_112',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ] ],
                 couplings = {(0,1,0):C.R2GC_276_115,(0,0,0):C.R2GC_963_356})

V_113 = CTVertex(name = 'V_113',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_324_162,(0,0,0):C.R2GC_987_380})

V_114 = CTVertex(name = 'V_114',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_292_131,(0,0,0):C.R2GC_979_372})

V_115 = CTVertex(name = 'V_115',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,1,0):C.R2GC_280_119,(0,0,0):C.R2GC_967_360})

V_116 = CTVertex(name = 'V_116',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_325_163,(0,0,0):C.R2GC_988_381})

V_117 = CTVertex(name = 'V_117',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_284_123,(0,0,0):C.R2GC_971_364})

V_118 = CTVertex(name = 'V_118',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ] ],
                 couplings = {(0,1,0):C.R2GC_272_111,(0,0,0):C.R2GC_959_352})

V_119 = CTVertex(name = 'V_119',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g, P.t] ] ],
                 couplings = {(0,1,0):C.R2GC_326_164,(0,0,0):C.R2GC_989_382})

V_120 = CTVertex(name = 'V_120',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_255_107})

V_121 = CTVertex(name = 'V_121',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_255_107})

V_122 = CTVertex(name = 'V_122',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_255_107})

V_123 = CTVertex(name = 'V_123',
                 type = 'R2',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_253_106})

V_124 = CTVertex(name = 'V_124',
                 type = 'R2',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_253_106})

V_125 = CTVertex(name = 'V_125',
                 type = 'R2',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_304_142,(0,2,0):C.R2GC_304_142,(0,1,0):C.R2GC_253_106,(0,3,0):C.R2GC_253_106})

V_126 = CTVertex(name = 'V_126',
                 type = 'R2',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_253_106})

V_127 = CTVertex(name = 'V_127',
                 type = 'R2',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.R2GC_253_106})

V_128 = CTVertex(name = 'V_128',
                 type = 'R2',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.R2GC_253_106})

V_129 = CTVertex(name = 'V_129',
                 type = 'R2',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV2, L.VV3 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.g] ], [ [P.t] ] ],
                 couplings = {(0,0,1):C.R2GC_294_134,(0,1,2):C.R2GC_236_88,(0,2,0):C.R2GC_293_132,(0,2,1):C.R2GC_293_133})

V_130 = CTVertex(name = 'V_130',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1253_19})

V_131 = CTVertex(name = 'V_131',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1254_20})

V_132 = CTVertex(name = 'V_132',
                 type = 'R2',
                 particles = [ P.g, P.g, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1255_21})

V_133 = CTVertex(name = 'V_133',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1256_22})

V_134 = CTVertex(name = 'V_134',
                 type = 'R2',
                 particles = [ P.a, P.a, P.g, P.g ],
                 color = [ 'Identity(3,4)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_808_330,(0,0,1):C.R2GC_808_331,(0,1,0):C.R2GC_808_330,(0,1,1):C.R2GC_808_331,(0,2,0):C.R2GC_808_330,(0,2,1):C.R2GC_808_331})

V_135 = CTVertex(name = 'V_135',
                 type = 'R2',
                 particles = [ P.g, P.g, P.W__plus__, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_955_332,(0,0,1):C.R2GC_955_333,(0,0,2):C.R2GC_955_334,(0,0,3):C.R2GC_955_335,(0,0,4):C.R2GC_955_336,(0,0,5):C.R2GC_955_337,(0,0,6):C.R2GC_955_338,(0,0,7):C.R2GC_955_339,(0,0,8):C.R2GC_955_340,(0,1,0):C.R2GC_955_332,(0,1,1):C.R2GC_955_333,(0,1,2):C.R2GC_955_334,(0,1,3):C.R2GC_955_335,(0,1,4):C.R2GC_955_336,(0,1,5):C.R2GC_955_337,(0,1,6):C.R2GC_955_338,(0,1,7):C.R2GC_955_339,(0,1,8):C.R2GC_955_340,(0,2,0):C.R2GC_955_332,(0,2,1):C.R2GC_955_333,(0,2,2):C.R2GC_955_334,(0,2,3):C.R2GC_955_335,(0,2,4):C.R2GC_955_336,(0,2,5):C.R2GC_955_337,(0,2,6):C.R2GC_955_338,(0,2,7):C.R2GC_955_339,(0,2,8):C.R2GC_955_340})

V_136 = CTVertex(name = 'V_136',
                 type = 'R2',
                 particles = [ P.g, P.g, P.W__minus__, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_956_341,(0,0,1):C.R2GC_956_342,(0,0,2):C.R2GC_956_343,(0,0,3):C.R2GC_956_344,(0,0,4):C.R2GC_956_345,(0,0,5):C.R2GC_956_346,(0,0,6):C.R2GC_956_347,(0,0,7):C.R2GC_956_348,(0,0,8):C.R2GC_956_349,(0,1,0):C.R2GC_956_341,(0,1,1):C.R2GC_956_342,(0,1,2):C.R2GC_956_343,(0,1,3):C.R2GC_956_344,(0,1,4):C.R2GC_956_345,(0,1,5):C.R2GC_956_346,(0,1,6):C.R2GC_956_347,(0,1,7):C.R2GC_956_348,(0,1,8):C.R2GC_956_349,(0,2,0):C.R2GC_956_341,(0,2,1):C.R2GC_956_342,(0,2,2):C.R2GC_956_343,(0,2,3):C.R2GC_956_344,(0,2,4):C.R2GC_956_345,(0,2,5):C.R2GC_956_346,(0,2,6):C.R2GC_956_347,(0,2,7):C.R2GC_956_348,(0,2,8):C.R2GC_956_349})

V_137 = CTVertex(name = 'V_137',
                 type = 'R2',
                 particles = [ P.g, P.g, P.WR__minus__, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1146_1,(0,0,1):C.R2GC_1146_2,(0,0,2):C.R2GC_1146_3,(0,0,3):C.R2GC_1146_4,(0,0,4):C.R2GC_1146_5,(0,0,5):C.R2GC_1146_6,(0,0,6):C.R2GC_1146_7,(0,0,7):C.R2GC_1146_8,(0,0,8):C.R2GC_1146_9,(0,1,0):C.R2GC_1146_1,(0,1,1):C.R2GC_1146_2,(0,1,2):C.R2GC_1146_3,(0,1,3):C.R2GC_1146_4,(0,1,4):C.R2GC_1146_5,(0,1,5):C.R2GC_1146_6,(0,1,6):C.R2GC_1146_7,(0,1,7):C.R2GC_1146_8,(0,1,8):C.R2GC_1146_9,(0,2,0):C.R2GC_1146_1,(0,2,1):C.R2GC_1146_2,(0,2,2):C.R2GC_1146_3,(0,2,3):C.R2GC_1146_4,(0,2,4):C.R2GC_1146_5,(0,2,5):C.R2GC_1146_6,(0,2,6):C.R2GC_1146_7,(0,2,7):C.R2GC_1146_8,(0,2,8):C.R2GC_1146_9})

V_138 = CTVertex(name = 'V_138',
                 type = 'R2',
                 particles = [ P.g, P.g, P.W__minus__, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b, P.c] ], [ [P.b, P.t] ], [ [P.b, P.u] ], [ [P.c, P.d] ], [ [P.c, P.s] ], [ [P.d, P.t] ], [ [P.d, P.u] ], [ [P.s, P.t] ], [ [P.s, P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1147_10,(0,0,1):C.R2GC_1147_11,(0,0,2):C.R2GC_1147_12,(0,0,3):C.R2GC_1147_13,(0,0,4):C.R2GC_1147_14,(0,0,5):C.R2GC_1147_15,(0,0,6):C.R2GC_1147_16,(0,0,7):C.R2GC_1147_17,(0,0,8):C.R2GC_1147_18,(0,1,0):C.R2GC_1147_10,(0,1,1):C.R2GC_1147_11,(0,1,2):C.R2GC_1147_12,(0,1,3):C.R2GC_1147_13,(0,1,4):C.R2GC_1147_14,(0,1,5):C.R2GC_1147_15,(0,1,6):C.R2GC_1147_16,(0,1,7):C.R2GC_1147_17,(0,1,8):C.R2GC_1147_18,(0,2,0):C.R2GC_1147_10,(0,2,1):C.R2GC_1147_11,(0,2,2):C.R2GC_1147_12,(0,2,3):C.R2GC_1147_13,(0,2,4):C.R2GC_1147_14,(0,2,5):C.R2GC_1147_15,(0,2,6):C.R2GC_1147_16,(0,2,7):C.R2GC_1147_17,(0,2,8):C.R2GC_1147_18})

V_139 = CTVertex(name = 'V_139',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.g ],
                 color = [ 'd(2,3,4)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_788_326,(0,0,1):C.R2GC_788_327,(0,1,0):C.R2GC_788_326,(0,1,1):C.R2GC_788_327,(0,2,0):C.R2GC_788_326,(0,2,1):C.R2GC_788_327})

V_140 = CTVertex(name = 'V_140',
                 type = 'R2',
                 particles = [ P.g, P.g, P.g, P.Z ],
                 color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                 lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_1488_62,(0,1,1):C.R2GC_1488_63,(0,2,0):C.R2GC_1488_62,(0,2,1):C.R2GC_1488_63,(0,3,0):C.R2GC_1488_62,(0,3,1):C.R2GC_1488_63,(1,0,0):C.R2GC_334_174,(1,0,1):C.R2GC_334_175})

V_141 = CTVertex(name = 'V_141',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.Z ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1287_52,(0,0,1):C.R2GC_1287_53,(0,1,0):C.R2GC_1287_52,(0,1,1):C.R2GC_1287_53,(0,2,0):C.R2GC_1287_52,(0,2,1):C.R2GC_1287_53})

V_142 = CTVertex(name = 'V_142',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Z, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_335_176,(0,0,1):C.R2GC_335_177,(0,1,0):C.R2GC_335_176,(0,1,1):C.R2GC_335_177,(0,2,0):C.R2GC_335_176,(0,2,1):C.R2GC_335_177})

V_143 = CTVertex(name = 'V_143',
                 type = 'R2',
                 particles = [ P.g, P.g, P.ZR, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1783_73,(0,0,1):C.R2GC_1783_74,(0,1,0):C.R2GC_1783_73,(0,1,1):C.R2GC_1783_74,(0,2,0):C.R2GC_1783_73,(0,2,1):C.R2GC_1783_74})

V_144 = CTVertex(name = 'V_144',
                 type = 'R2',
                 particles = [ P.a, P.g, P.g, P.ZR ],
                 color = [ 'Identity(2,3)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1786_79,(0,0,1):C.R2GC_1786_80,(0,1,0):C.R2GC_1786_79,(0,1,1):C.R2GC_1786_80,(0,2,0):C.R2GC_1786_79,(0,2,1):C.R2GC_1786_80})

V_145 = CTVertex(name = 'V_145',
                 type = 'R2',
                 particles = [ P.g, P.g, P.g, P.ZR ],
                 color = [ 'd(1,2,3)', 'f(1,2,3)' ],
                 lorentz = [ L.VVVV1, L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,1,0):C.R2GC_1863_81,(0,1,1):C.R2GC_1863_82,(0,2,0):C.R2GC_1863_81,(0,2,1):C.R2GC_1863_82,(0,3,0):C.R2GC_1863_81,(0,3,1):C.R2GC_1863_82,(1,0,0):C.R2GC_1784_75,(1,0,1):C.R2GC_1784_76})

V_146 = CTVertex(name = 'V_146',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Z, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.d], [P.s] ], [ [P.c], [P.t], [P.u] ] ],
                 couplings = {(0,0,0):C.R2GC_1785_77,(0,0,1):C.R2GC_1785_78,(0,1,0):C.R2GC_1785_77,(0,1,1):C.R2GC_1785_78,(0,2,0):C.R2GC_1785_77,(0,2,1):C.R2GC_1785_78})

V_147 = CTVertex(name = 'V_147',
                 type = 'R2',
                 particles = [ P.g, P.g, P.PhipL__tilde__, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_238_92,(0,0,1):C.R2GC_238_93,(0,0,2):C.R2GC_238_94})

V_148 = CTVertex(name = 'V_148',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp__tilde__, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_327_165,(0,0,1):C.R2GC_327_166,(0,0,2):C.R2GC_327_167})

V_149 = CTVertex(name = 'V_149',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_327_165,(0,0,1):C.R2GC_327_166,(0,0,2):C.R2GC_327_167})

V_150 = CTVertex(name = 'V_150',
                 type = 'R2',
                 particles = [ P.g, P.g, P.PhipR__tilde__, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_237_89,(0,0,1):C.R2GC_237_90,(0,0,2):C.R2GC_237_91})

V_151 = CTVertex(name = 'V_151',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_759_256,(0,0,3):C.R2GC_759_257,(0,0,5):C.R2GC_759_258,(0,0,6):C.R2GC_759_259,(0,0,1):C.R2GC_759_260,(0,0,2):C.R2GC_759_261,(0,0,4):C.R2GC_759_262})

V_152 = CTVertex(name = 'V_152',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_760_263,(0,0,3):C.R2GC_760_264,(0,0,5):C.R2GC_760_265,(0,0,6):C.R2GC_760_266,(0,0,1):C.R2GC_760_267,(0,0,2):C.R2GC_760_268,(0,0,4):C.R2GC_760_269})

V_153 = CTVertex(name = 'V_153',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_761_270,(0,0,3):C.R2GC_761_271,(0,0,5):C.R2GC_761_272,(0,0,6):C.R2GC_761_273,(0,0,1):C.R2GC_761_274,(0,0,2):C.R2GC_761_275,(0,0,4):C.R2GC_761_276})

V_154 = CTVertex(name = 'V_154',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_762_277,(0,0,3):C.R2GC_762_278,(0,0,5):C.R2GC_762_279,(0,0,6):C.R2GC_762_280,(0,0,1):C.R2GC_762_281,(0,0,2):C.R2GC_762_282,(0,0,4):C.R2GC_762_283})

V_155 = CTVertex(name = 'V_155',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_763_284,(0,0,3):C.R2GC_763_285,(0,0,5):C.R2GC_763_286,(0,0,6):C.R2GC_763_287,(0,0,1):C.R2GC_763_288,(0,0,2):C.R2GC_763_289,(0,0,4):C.R2GC_763_290})

V_156 = CTVertex(name = 'V_156',
                 type = 'R2',
                 particles = [ P.g, P.g, P.HH, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_764_291,(0,0,3):C.R2GC_764_292,(0,0,5):C.R2GC_764_293,(0,0,6):C.R2GC_764_294,(0,0,1):C.R2GC_764_295,(0,0,2):C.R2GC_764_296,(0,0,4):C.R2GC_764_297})

V_157 = CTVertex(name = 'V_157',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_765_298,(0,0,3):C.R2GC_765_299,(0,0,5):C.R2GC_765_300,(0,0,6):C.R2GC_765_301,(0,0,1):C.R2GC_765_302,(0,0,2):C.R2GC_765_303,(0,0,4):C.R2GC_765_304})

V_158 = CTVertex(name = 'V_158',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_766_305,(0,0,3):C.R2GC_766_306,(0,0,5):C.R2GC_766_307,(0,0,6):C.R2GC_766_308,(0,0,1):C.R2GC_766_309,(0,0,2):C.R2GC_766_310,(0,0,4):C.R2GC_766_311})

V_159 = CTVertex(name = 'V_159',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_767_312,(0,0,3):C.R2GC_767_313,(0,0,5):C.R2GC_767_314,(0,0,6):C.R2GC_767_315,(0,0,1):C.R2GC_767_316,(0,0,2):C.R2GC_767_317,(0,0,4):C.R2GC_767_318})

V_160 = CTVertex(name = 'V_160',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b] ], [ [P.b, P.d] ], [ [P.b, P.s] ], [ [P.d] ], [ [P.d, P.s] ], [ [P.s] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_768_319,(0,0,3):C.R2GC_768_320,(0,0,5):C.R2GC_768_321,(0,0,6):C.R2GC_768_322,(0,0,1):C.R2GC_768_323,(0,0,2):C.R2GC_768_324,(0,0,4):C.R2GC_768_325})

V_161 = CTVertex(name = 'V_161',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1258_26,(0,0,1):C.R2GC_1258_27,(0,0,2):C.R2GC_1258_28})

V_162 = CTVertex(name = 'V_162',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp__tilde__, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1257_23,(0,0,1):C.R2GC_1257_24,(0,0,2):C.R2GC_1257_25})

V_163 = CTVertex(name = 'V_163',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Hp__tilde__, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.b, P.t] ], [ [P.d, P.t] ], [ [P.s, P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1279_49,(0,0,1):C.R2GC_1279_50,(0,0,2):C.R2GC_1279_51})

V_164 = CTVertex(name = 'V_164',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1421_56})

V_165 = CTVertex(name = 'V_165',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1422_57})

V_166 = CTVertex(name = 'V_166',
                 type = 'R2',
                 particles = [ P.g, P.g, P.HH, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1423_58})

V_167 = CTVertex(name = 'V_167',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1424_59})

V_168 = CTVertex(name = 'V_168',
                 type = 'R2',
                 particles = [ P.g, P.g, P.h, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1686_69})

V_169 = CTVertex(name = 'V_169',
                 type = 'R2',
                 particles = [ P.g, P.g, P.DD, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1687_70})

V_170 = CTVertex(name = 'V_170',
                 type = 'R2',
                 particles = [ P.g, P.g, P.HH, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1688_71})

V_171 = CTVertex(name = 'V_171',
                 type = 'R2',
                 particles = [ P.g, P.g, P.AA, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1689_72})

V_172 = CTVertex(name = 'V_172',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Phi0L, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1322_54})

V_173 = CTVertex(name = 'V_173',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Phi0R, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1456_60})

V_174 = CTVertex(name = 'V_174',
                 type = 'R2',
                 particles = [ P.g, P.g, P.Phi0L, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VVSS1 ],
                 loop_particles = [ [ [P.t] ] ],
                 couplings = {(0,0,0):C.R2GC_1457_61})

V_175 = CTVertex(name = 'V_175',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g ],
                 color = [ 'f(1,2,3)' ],
                 lorentz = [ L.VVV1, L.VVV3, L.VVV4 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_295_122,(0,0,3):C.UVGC_295_123,(0,1,1):C.UVGC_239_51,(0,2,2):C.UVGC_240_52})

V_176 = CTVertex(name = 'V_176',
                 type = 'UV',
                 particles = [ P.g, P.g, P.g, P.g ],
                 color = [ 'd(-1,1,3)*d(-1,2,4)', 'd(-1,1,3)*f(-1,2,4)', 'd(-1,1,4)*d(-1,2,3)', 'd(-1,1,4)*f(-1,2,3)', 'd(-1,2,3)*f(-1,1,4)', 'd(-1,2,4)*f(-1,1,3)', 'f(-1,1,2)*f(-1,3,4)', 'f(-1,1,3)*f(-1,2,4)', 'f(-1,1,4)*f(-1,2,3)', 'Identity(1,2)*Identity(3,4)', 'Identity(1,3)*Identity(2,4)', 'Identity(1,4)*Identity(2,3)' ],
                 lorentz = [ L.VVVV11, L.VVVV2, L.VVVV3, L.VVVV5 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.t], [P.u] ], [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(2,1,2):C.UVGC_247_57,(2,1,3):C.UVGC_247_56,(0,1,2):C.UVGC_247_57,(0,1,3):C.UVGC_247_56,(4,1,2):C.UVGC_246_54,(4,1,3):C.UVGC_246_55,(3,1,2):C.UVGC_246_54,(3,1,3):C.UVGC_246_55,(8,1,2):C.UVGC_247_56,(8,1,3):C.UVGC_247_57,(6,1,1):C.UVGC_299_132,(6,1,2):C.UVGC_300_136,(6,1,3):C.UVGC_300_137,(6,1,4):C.UVGC_299_135,(7,1,1):C.UVGC_299_132,(7,1,2):C.UVGC_299_133,(7,1,3):C.UVGC_299_134,(7,1,4):C.UVGC_299_135,(5,1,2):C.UVGC_246_54,(5,1,3):C.UVGC_246_55,(1,1,2):C.UVGC_246_54,(1,1,3):C.UVGC_246_55,(11,0,2):C.UVGC_250_60,(11,0,3):C.UVGC_250_61,(10,0,2):C.UVGC_250_60,(10,0,3):C.UVGC_250_61,(9,0,2):C.UVGC_249_58,(9,0,3):C.UVGC_249_59,(0,2,2):C.UVGC_247_57,(0,2,3):C.UVGC_247_56,(2,2,2):C.UVGC_247_57,(2,2,3):C.UVGC_247_56,(5,2,2):C.UVGC_246_54,(5,2,3):C.UVGC_246_55,(1,2,2):C.UVGC_246_54,(1,2,3):C.UVGC_246_55,(7,2,0):C.UVGC_251_62,(7,2,2):C.UVGC_247_56,(7,2,3):C.UVGC_252_63,(4,2,2):C.UVGC_246_54,(4,2,3):C.UVGC_246_55,(3,2,2):C.UVGC_246_54,(3,2,3):C.UVGC_246_55,(8,2,1):C.UVGC_301_138,(8,2,2):C.UVGC_299_133,(8,2,3):C.UVGC_301_139,(8,2,4):C.UVGC_301_140,(6,2,2):C.UVGC_296_124,(6,2,3):C.UVGC_296_125,(6,2,4):C.UVGC_296_126,(0,3,2):C.UVGC_247_57,(0,3,3):C.UVGC_247_56,(2,3,2):C.UVGC_247_57,(2,3,3):C.UVGC_247_56,(5,3,2):C.UVGC_246_54,(5,3,3):C.UVGC_246_55,(1,3,2):C.UVGC_246_54,(1,3,3):C.UVGC_246_55,(7,3,2):C.UVGC_297_127,(7,3,3):C.UVGC_297_128,(7,3,4):C.UVGC_296_126,(4,3,2):C.UVGC_246_54,(4,3,3):C.UVGC_246_55,(3,3,2):C.UVGC_246_54,(3,3,3):C.UVGC_246_55,(8,3,1):C.UVGC_298_129,(8,3,2):C.UVGC_297_127,(8,3,3):C.UVGC_298_130,(8,3,4):C.UVGC_298_131,(6,3,0):C.UVGC_251_62,(6,3,3):C.UVGC_249_58})

V_177 = CTVertex(name = 'V_177',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_603_316,(0,0,1):C.UVGC_603_317,(0,1,0):C.UVGC_567_220,(0,1,1):C.UVGC_567_221})

V_178 = CTVertex(name = 'V_178',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.d, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_604_318,(0,0,2):C.UVGC_604_319,(0,0,1):C.UVGC_604_320,(0,1,0):C.UVGC_568_222,(0,1,2):C.UVGC_568_223,(0,1,1):C.UVGC_568_224})

V_179 = CTVertex(name = 'V_179',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.d, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g], [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_605_321,(0,0,2):C.UVGC_605_322,(0,0,0):C.UVGC_605_323,(0,1,1):C.UVGC_569_225,(0,1,2):C.UVGC_569_226,(0,1,0):C.UVGC_569_227})

V_180 = CTVertex(name = 'V_180',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.s, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_615_348,(0,0,2):C.UVGC_615_349,(0,0,1):C.UVGC_615_350,(0,1,0):C.UVGC_579_252,(0,1,2):C.UVGC_579_253,(0,1,1):C.UVGC_579_254})

V_181 = CTVertex(name = 'V_181',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_616_351,(0,0,1):C.UVGC_616_352,(0,1,0):C.UVGC_580_255,(0,1,1):C.UVGC_580_256})

V_182 = CTVertex(name = 'V_182',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.s, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.s] ], [ [P.b, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_617_353,(0,0,2):C.UVGC_617_354,(0,0,1):C.UVGC_617_355,(0,1,0):C.UVGC_581_257,(0,1,2):C.UVGC_581_258,(0,1,1):C.UVGC_581_259})

V_183 = CTVertex(name = 'V_183',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.b, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g], [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_627_380,(0,0,2):C.UVGC_627_381,(0,0,0):C.UVGC_627_382,(0,1,1):C.UVGC_591_284,(0,1,2):C.UVGC_591_285,(0,1,0):C.UVGC_591_286})

V_184 = CTVertex(name = 'V_184',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.b, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.s] ], [ [P.b, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_628_383,(0,0,2):C.UVGC_628_384,(0,0,1):C.UVGC_628_385,(0,1,0):C.UVGC_592_287,(0,1,2):C.UVGC_592_288,(0,1,1):C.UVGC_592_289})

V_185 = CTVertex(name = 'V_185',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_629_386,(0,0,1):C.UVGC_629_387,(0,1,0):C.UVGC_593_290,(0,1,1):C.UVGC_593_291})

V_186 = CTVertex(name = 'V_186',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_606_324,(0,0,1):C.UVGC_606_325,(0,1,0):C.UVGC_570_228,(0,1,1):C.UVGC_570_229})

V_187 = CTVertex(name = 'V_187',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.d, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_607_326,(0,0,2):C.UVGC_607_327,(0,0,1):C.UVGC_607_328,(0,1,0):C.UVGC_571_230,(0,1,2):C.UVGC_571_231,(0,1,1):C.UVGC_571_232})

V_188 = CTVertex(name = 'V_188',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.d, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g], [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_608_329,(0,0,2):C.UVGC_608_330,(0,0,0):C.UVGC_608_331,(0,1,1):C.UVGC_572_233,(0,1,2):C.UVGC_572_234,(0,1,0):C.UVGC_572_235})

V_189 = CTVertex(name = 'V_189',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.s, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_618_356,(0,0,2):C.UVGC_618_357,(0,0,1):C.UVGC_618_358,(0,1,0):C.UVGC_582_260,(0,1,2):C.UVGC_582_261,(0,1,1):C.UVGC_582_262})

V_190 = CTVertex(name = 'V_190',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_619_359,(0,0,1):C.UVGC_619_360,(0,1,0):C.UVGC_583_263,(0,1,1):C.UVGC_583_264})

V_191 = CTVertex(name = 'V_191',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.s, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.s] ], [ [P.b, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_620_361,(0,0,2):C.UVGC_620_362,(0,0,1):C.UVGC_620_363,(0,1,0):C.UVGC_584_265,(0,1,2):C.UVGC_584_266,(0,1,1):C.UVGC_584_267})

V_192 = CTVertex(name = 'V_192',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.b, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g], [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_630_388,(0,0,2):C.UVGC_630_389,(0,0,0):C.UVGC_630_390,(0,1,1):C.UVGC_594_292,(0,1,2):C.UVGC_594_293,(0,1,0):C.UVGC_594_294})

V_193 = CTVertex(name = 'V_193',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.b, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.s] ], [ [P.b, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_631_391,(0,0,2):C.UVGC_631_392,(0,0,1):C.UVGC_631_393,(0,1,0):C.UVGC_595_295,(0,1,2):C.UVGC_595_296,(0,1,1):C.UVGC_595_297})

V_194 = CTVertex(name = 'V_194',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_632_394,(0,0,1):C.UVGC_632_395,(0,1,0):C.UVGC_596_298,(0,1,1):C.UVGC_596_299})

V_195 = CTVertex(name = 'V_195',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_609_332,(0,0,1):C.UVGC_609_333,(0,1,0):C.UVGC_573_236,(0,1,1):C.UVGC_573_237})

V_196 = CTVertex(name = 'V_196',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.d, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_610_334,(0,0,2):C.UVGC_610_335,(0,0,1):C.UVGC_610_336,(0,1,0):C.UVGC_574_238,(0,1,2):C.UVGC_574_239,(0,1,1):C.UVGC_574_240})

V_197 = CTVertex(name = 'V_197',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.d, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g], [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_611_337,(0,0,2):C.UVGC_611_338,(0,0,0):C.UVGC_611_339,(0,1,1):C.UVGC_575_241,(0,1,2):C.UVGC_575_242,(0,1,0):C.UVGC_575_243})

V_198 = CTVertex(name = 'V_198',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.s, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_621_364,(0,0,2):C.UVGC_621_365,(0,0,1):C.UVGC_621_366,(0,1,0):C.UVGC_585_268,(0,1,2):C.UVGC_585_269,(0,1,1):C.UVGC_585_270})

V_199 = CTVertex(name = 'V_199',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_622_367,(0,0,1):C.UVGC_622_368,(0,1,0):C.UVGC_586_271,(0,1,1):C.UVGC_586_272})

V_200 = CTVertex(name = 'V_200',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.s, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.s] ], [ [P.b, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_623_369,(0,0,2):C.UVGC_623_370,(0,0,1):C.UVGC_623_371,(0,1,0):C.UVGC_587_273,(0,1,2):C.UVGC_587_274,(0,1,1):C.UVGC_587_275})

V_201 = CTVertex(name = 'V_201',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.b, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g], [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_633_396,(0,0,2):C.UVGC_633_397,(0,0,0):C.UVGC_633_398,(0,1,1):C.UVGC_597_300,(0,1,2):C.UVGC_597_301,(0,1,0):C.UVGC_597_302})

V_202 = CTVertex(name = 'V_202',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.b, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.s] ], [ [P.b, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_634_399,(0,0,2):C.UVGC_634_400,(0,0,1):C.UVGC_634_401,(0,1,0):C.UVGC_598_303,(0,1,2):C.UVGC_598_304,(0,1,1):C.UVGC_598_305})

V_203 = CTVertex(name = 'V_203',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_635_402,(0,0,1):C.UVGC_635_403,(0,1,0):C.UVGC_599_306,(0,1,1):C.UVGC_599_307})

V_204 = CTVertex(name = 'V_204',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_612_340,(0,0,1):C.UVGC_612_341,(0,1,0):C.UVGC_576_244,(0,1,1):C.UVGC_576_245})

V_205 = CTVertex(name = 'V_205',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.d, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_613_342,(0,0,2):C.UVGC_613_343,(0,0,1):C.UVGC_613_344,(0,1,0):C.UVGC_577_246,(0,1,2):C.UVGC_577_247,(0,1,1):C.UVGC_577_248})

V_206 = CTVertex(name = 'V_206',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.d, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g], [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_614_345,(0,0,2):C.UVGC_614_346,(0,0,0):C.UVGC_614_347,(0,1,1):C.UVGC_578_249,(0,1,2):C.UVGC_578_250,(0,1,0):C.UVGC_578_251})

V_207 = CTVertex(name = 'V_207',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.s, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.s] ], [ [P.d, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_624_372,(0,0,2):C.UVGC_624_373,(0,0,1):C.UVGC_624_374,(0,1,0):C.UVGC_588_276,(0,1,2):C.UVGC_588_277,(0,1,1):C.UVGC_588_278})

V_208 = CTVertex(name = 'V_208',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_625_375,(0,0,1):C.UVGC_625_376,(0,1,0):C.UVGC_589_279,(0,1,1):C.UVGC_589_280})

V_209 = CTVertex(name = 'V_209',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.s, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.s] ], [ [P.b, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_626_377,(0,0,2):C.UVGC_626_378,(0,0,1):C.UVGC_626_379,(0,1,0):C.UVGC_590_281,(0,1,2):C.UVGC_590_282,(0,1,1):C.UVGC_590_283})

V_210 = CTVertex(name = 'V_210',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.b, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.d, P.g] ], [ [P.b, P.g], [P.d, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,1):C.UVGC_636_404,(0,0,2):C.UVGC_636_405,(0,0,0):C.UVGC_636_406,(0,1,1):C.UVGC_600_308,(0,1,2):C.UVGC_600_309,(0,1,0):C.UVGC_600_310})

V_211 = CTVertex(name = 'V_211',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.b, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.s] ], [ [P.b, P.g, P.s] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_637_407,(0,0,2):C.UVGC_637_408,(0,0,1):C.UVGC_637_409,(0,1,0):C.UVGC_601_311,(0,1,2):C.UVGC_601_312,(0,1,1):C.UVGC_601_313})

V_212 = CTVertex(name = 'V_212',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_638_410,(0,0,1):C.UVGC_638_411,(0,1,0):C.UVGC_602_314,(0,1,1):C.UVGC_602_315})

V_213 = CTVertex(name = 'V_213',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_691_412,(0,0,2):C.UVGC_691_413,(0,0,1):C.UVGC_691_414,(0,1,0):C.UVGC_1273_27,(0,1,2):C.UVGC_1273_28,(0,1,1):C.UVGC_1273_29})

V_214 = CTVertex(name = 'V_214',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_692_415,(0,0,2):C.UVGC_692_416,(0,0,1):C.UVGC_692_417,(0,1,0):C.UVGC_1274_30,(0,1,2):C.UVGC_1274_31,(0,1,1):C.UVGC_1274_32})

V_215 = CTVertex(name = 'V_215',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.PhipL__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_693_418,(0,0,2):C.UVGC_693_419,(0,0,1):C.UVGC_693_420,(0,1,0):C.UVGC_1275_33,(0,1,2):C.UVGC_1275_34,(0,1,1):C.UVGC_1275_35})

V_216 = CTVertex(name = 'V_216',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.Hp__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_328_202,(0,1,2):C.UVGC_328_203,(0,1,1):C.UVGC_328_204,(0,0,0):C.UVGC_1259_1,(0,0,2):C.UVGC_1259_2,(0,0,1):C.UVGC_1259_3})

V_217 = CTVertex(name = 'V_217',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.Hp__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_329_205,(0,1,2):C.UVGC_329_206,(0,1,1):C.UVGC_329_207,(0,0,0):C.UVGC_1260_4,(0,0,2):C.UVGC_1260_5,(0,0,1):C.UVGC_1260_6})

V_218 = CTVertex(name = 'V_218',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.Hp__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_330_208,(0,1,2):C.UVGC_330_209,(0,1,1):C.UVGC_330_210,(0,0,0):C.UVGC_1261_7,(0,0,2):C.UVGC_1261_8,(0,0,1):C.UVGC_1261_9})

V_219 = CTVertex(name = 'V_219',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_305_144,(0,0,2):C.UVGC_305_145,(0,0,1):C.UVGC_305_146})

V_220 = CTVertex(name = 'V_220',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_306_147,(0,0,2):C.UVGC_306_148,(0,0,1):C.UVGC_306_149})

V_221 = CTVertex(name = 'V_221',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.PhipR__tilde__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_307_150,(0,0,2):C.UVGC_307_151,(0,0,1):C.UVGC_307_152})

V_222 = CTVertex(name = 'V_222',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_331_211,(0,0,2):C.UVGC_331_212,(0,0,1):C.UVGC_331_213,(0,1,0):C.UVGC_1270_18,(0,1,2):C.UVGC_1270_19,(0,1,1):C.UVGC_1270_20})

V_223 = CTVertex(name = 'V_223',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_332_214,(0,0,2):C.UVGC_332_215,(0,0,1):C.UVGC_332_216,(0,1,0):C.UVGC_1271_21,(0,1,2):C.UVGC_1271_22,(0,1,1):C.UVGC_1271_23})

V_224 = CTVertex(name = 'V_224',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.Hp ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_333_217,(0,0,2):C.UVGC_333_218,(0,0,1):C.UVGC_333_219,(0,1,0):C.UVGC_1272_24,(0,1,2):C.UVGC_1272_25,(0,1,1):C.UVGC_1272_26})

V_225 = CTVertex(name = 'V_225',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.h ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,2,0):C.UVGC_308_153,(0,0,0):C.UVGC_1262_10,(0,1,0):C.UVGC_1263_11})

V_226 = CTVertex(name = 'V_226',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.DD ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,2,0):C.UVGC_309_154,(0,0,0):C.UVGC_1264_12,(0,1,0):C.UVGC_1265_13})

V_227 = CTVertex(name = 'V_227',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.HH ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,2,0):C.UVGC_310_155,(0,0,0):C.UVGC_1266_14,(0,1,0):C.UVGC_1267_15})

V_228 = CTVertex(name = 'V_228',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.AA ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4, L.FFS5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,2,0):C.UVGC_311_156,(0,0,0):C.UVGC_1268_16,(0,1,0):C.UVGC_1269_17})

V_229 = CTVertex(name = 'V_229',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_312_157,(0,0,2):C.UVGC_312_158,(0,0,1):C.UVGC_312_159})

V_230 = CTVertex(name = 'V_230',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_313_160,(0,0,2):C.UVGC_313_161,(0,0,1):C.UVGC_313_162})

V_231 = CTVertex(name = 'V_231',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.PhipR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_314_163,(0,0,2):C.UVGC_314_164,(0,0,1):C.UVGC_314_165})

V_232 = CTVertex(name = 'V_232',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Phi0L ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1528_48})

V_233 = CTVertex(name = 'V_233',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_694_421,(0,1,2):C.UVGC_694_422,(0,1,1):C.UVGC_694_423,(0,0,0):C.UVGC_1276_36,(0,0,2):C.UVGC_1276_37,(0,0,1):C.UVGC_1276_38})

V_234 = CTVertex(name = 'V_234',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_695_424,(0,1,2):C.UVGC_695_425,(0,1,1):C.UVGC_695_426,(0,0,0):C.UVGC_1277_39,(0,0,2):C.UVGC_1277_40,(0,0,1):C.UVGC_1277_41})

V_235 = CTVertex(name = 'V_235',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.PhipL ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS2, L.FFS4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_696_427,(0,1,2):C.UVGC_696_428,(0,1,1):C.UVGC_696_429,(0,0,0):C.UVGC_1278_42,(0,0,2):C.UVGC_1278_43,(0,0,1):C.UVGC_1278_44})

V_236 = CTVertex(name = 'V_236',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Phi0R ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFS1 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1323_45})

V_237 = CTVertex(name = 'V_237',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_790_433,(0,1,0):C.UVGC_779_431,(0,2,0):C.UVGC_779_431})

V_238 = CTVertex(name = 'V_238',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_790_433,(0,1,0):C.UVGC_779_431,(0,2,0):C.UVGC_779_431})

V_239 = CTVertex(name = 'V_239',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_790_433,(0,1,0):C.UVGC_795_434,(0,2,0):C.UVGC_795_434})

V_240 = CTVertex(name = 'V_240',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.Z ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1499_46,(0,1,0):C.UVGC_1500_47})

V_241 = CTVertex(name = 'V_241',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.ZR ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_1874_49,(0,1,0):C.UVGC_1875_50})

V_242 = CTVertex(name = 'V_242',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_285_102,(0,0,1):C.UVGC_285_103,(0,1,0):C.UVGC_974_469,(0,1,1):C.UVGC_974_470})

V_243 = CTVertex(name = 'V_243',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_289_110,(0,0,1):C.UVGC_289_111,(0,1,0):C.UVGC_978_477,(0,1,1):C.UVGC_978_478})

V_244 = CTVertex(name = 'V_244',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.u, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.u] ], [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_281_94,(0,0,1):C.UVGC_281_95,(0,1,0):C.UVGC_970_461,(0,1,1):C.UVGC_970_462})

V_245 = CTVertex(name = 'V_245',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_273_78,(0,0,0):C.UVGC_273_79,(0,1,1):C.UVGC_962_445,(0,1,0):C.UVGC_962_446})

V_246 = CTVertex(name = 'V_246',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_277_86,(0,0,1):C.UVGC_277_87,(0,1,0):C.UVGC_966_453,(0,1,1):C.UVGC_966_454})

V_247 = CTVertex(name = 'V_247',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.c, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g], [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_269_70,(0,0,0):C.UVGC_269_71,(0,1,1):C.UVGC_958_437,(0,1,0):C.UVGC_958_438})

V_248 = CTVertex(name = 'V_248',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_315_166,(0,0,2):C.UVGC_315_167,(0,0,1):C.UVGC_315_168,(0,1,0):C.UVGC_984_492,(0,1,2):C.UVGC_984_493,(0,1,1):C.UVGC_984_494})

V_249 = CTVertex(name = 'V_249',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_316_169,(0,0,2):C.UVGC_316_170,(0,0,1):C.UVGC_316_171,(0,1,0):C.UVGC_985_495,(0,1,2):C.UVGC_985_496,(0,1,1):C.UVGC_985_497})

V_250 = CTVertex(name = 'V_250',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.W__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_317_172,(0,0,2):C.UVGC_317_173,(0,0,1):C.UVGC_317_174,(0,1,0):C.UVGC_986_498,(0,1,2):C.UVGC_986_499,(0,1,1):C.UVGC_986_500})

V_251 = CTVertex(name = 'V_251',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_286_104,(0,1,1):C.UVGC_286_105,(0,0,0):C.UVGC_973_467,(0,0,1):C.UVGC_973_468})

V_252 = CTVertex(name = 'V_252',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_290_112,(0,1,1):C.UVGC_290_113,(0,0,0):C.UVGC_977_475,(0,0,1):C.UVGC_977_476})

V_253 = CTVertex(name = 'V_253',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.u, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.u] ], [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_282_96,(0,1,1):C.UVGC_282_97,(0,0,0):C.UVGC_969_459,(0,0,1):C.UVGC_969_460})

V_254 = CTVertex(name = 'V_254',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_274_80,(0,1,0):C.UVGC_274_81,(0,0,1):C.UVGC_961_443,(0,0,0):C.UVGC_961_444})

V_255 = CTVertex(name = 'V_255',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,1,0):C.UVGC_278_88,(0,1,1):C.UVGC_278_89,(0,0,0):C.UVGC_965_451,(0,0,1):C.UVGC_965_452})

V_256 = CTVertex(name = 'V_256',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.c, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g], [P.c, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_270_72,(0,1,0):C.UVGC_270_73,(0,0,1):C.UVGC_957_435,(0,0,0):C.UVGC_957_436})

V_257 = CTVertex(name = 'V_257',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_318_175,(0,1,2):C.UVGC_318_176,(0,1,1):C.UVGC_318_177,(0,0,0):C.UVGC_981_483,(0,0,2):C.UVGC_981_484,(0,0,1):C.UVGC_981_485})

V_258 = CTVertex(name = 'V_258',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_319_178,(0,1,2):C.UVGC_319_179,(0,1,1):C.UVGC_319_180,(0,0,0):C.UVGC_982_486,(0,0,2):C.UVGC_982_487,(0,0,1):C.UVGC_982_488})

V_259 = CTVertex(name = 'V_259',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.t, P.WR__minus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_320_181,(0,1,2):C.UVGC_320_182,(0,1,1):C.UVGC_320_183,(0,0,0):C.UVGC_983_489,(0,0,2):C.UVGC_983_490,(0,0,1):C.UVGC_983_491})

V_260 = CTVertex(name = 'V_260',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.u] ] ],
                 couplings = {(0,0,3):C.UVGC_255_69,(0,1,0):C.UVGC_254_65,(0,1,1):C.UVGC_254_66,(0,1,2):C.UVGC_254_67,(0,1,3):C.UVGC_254_68,(0,2,0):C.UVGC_254_65,(0,2,1):C.UVGC_254_66,(0,2,2):C.UVGC_254_67,(0,2,3):C.UVGC_254_68})

V_261 = CTVertex(name = 'V_261',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.c, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                 couplings = {(0,0,1):C.UVGC_255_69,(0,1,0):C.UVGC_254_65,(0,1,2):C.UVGC_254_66,(0,1,3):C.UVGC_254_67,(0,1,1):C.UVGC_254_68,(0,2,0):C.UVGC_254_65,(0,2,2):C.UVGC_254_66,(0,2,3):C.UVGC_254_67,(0,2,1):C.UVGC_254_68})

V_262 = CTVertex(name = 'V_262',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,3):C.UVGC_255_69,(0,1,0):C.UVGC_254_65,(0,1,1):C.UVGC_254_66,(0,1,2):C.UVGC_254_67,(0,1,3):C.UVGC_303_142,(0,2,0):C.UVGC_254_65,(0,2,1):C.UVGC_254_66,(0,2,2):C.UVGC_254_67,(0,2,3):C.UVGC_303_142})

V_263 = CTVertex(name = 'V_263',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_789_432,(0,1,0):C.UVGC_778_430,(0,2,0):C.UVGC_778_430})

V_264 = CTVertex(name = 'V_264',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_789_432,(0,1,0):C.UVGC_778_430,(0,2,0):C.UVGC_778_430})

V_265 = CTVertex(name = 'V_265',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.a ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_789_432,(0,1,0):C.UVGC_778_430,(0,2,0):C.UVGC_778_430})

V_266 = CTVertex(name = 'V_266',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_287_106,(0,0,1):C.UVGC_287_107,(0,1,0):C.UVGC_976_473,(0,1,1):C.UVGC_976_474})

V_267 = CTVertex(name = 'V_267',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_275_82,(0,0,0):C.UVGC_275_83,(0,1,1):C.UVGC_964_449,(0,1,0):C.UVGC_964_450})

V_268 = CTVertex(name = 'V_268',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_321_184,(0,0,2):C.UVGC_321_185,(0,0,1):C.UVGC_321_186,(0,1,0):C.UVGC_990_510,(0,1,2):C.UVGC_990_511,(0,1,1):C.UVGC_990_512})

V_269 = CTVertex(name = 'V_269',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_291_114,(0,0,1):C.UVGC_291_115,(0,1,0):C.UVGC_980_481,(0,1,1):C.UVGC_980_482})

V_270 = CTVertex(name = 'V_270',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_279_90,(0,0,1):C.UVGC_279_91,(0,1,0):C.UVGC_968_457,(0,1,1):C.UVGC_968_458})

V_271 = CTVertex(name = 'V_271',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_322_187,(0,0,2):C.UVGC_322_188,(0,0,1):C.UVGC_322_189,(0,1,0):C.UVGC_991_513,(0,1,2):C.UVGC_991_514,(0,1,1):C.UVGC_991_515})

V_272 = CTVertex(name = 'V_272',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.u] ], [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_283_98,(0,0,1):C.UVGC_283_99,(0,1,0):C.UVGC_972_465,(0,1,1):C.UVGC_972_466})

V_273 = CTVertex(name = 'V_273',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g], [P.c, P.g] ] ],
                 couplings = {(0,0,1):C.UVGC_271_74,(0,0,0):C.UVGC_271_75,(0,1,1):C.UVGC_960_441,(0,1,0):C.UVGC_960_442})

V_274 = CTVertex(name = 'V_274',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.W__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_323_190,(0,0,2):C.UVGC_323_191,(0,0,1):C.UVGC_323_192,(0,1,0):C.UVGC_992_516,(0,1,2):C.UVGC_992_517,(0,1,1):C.UVGC_992_518})

V_275 = CTVertex(name = 'V_275',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g], [P.g, P.u] ], [ [P.d, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_288_108,(0,1,1):C.UVGC_288_109,(0,0,0):C.UVGC_975_471,(0,0,1):C.UVGC_975_472})

V_276 = CTVertex(name = 'V_276',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.d, P.g] ], [ [P.c, P.g], [P.d, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_276_84,(0,1,0):C.UVGC_276_85,(0,0,1):C.UVGC_963_447,(0,0,0):C.UVGC_963_448})

V_277 = CTVertex(name = 'V_277',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.d, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.d, P.g] ], [ [P.d, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_324_193,(0,1,2):C.UVGC_324_194,(0,1,1):C.UVGC_324_195,(0,0,0):C.UVGC_987_501,(0,0,2):C.UVGC_987_502,(0,0,1):C.UVGC_987_503})

V_278 = CTVertex(name = 'V_278',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s], [P.g, P.u] ], [ [P.g, P.s, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_292_116,(0,1,1):C.UVGC_292_117,(0,0,0):C.UVGC_979_479,(0,0,1):C.UVGC_979_480})

V_279 = CTVertex(name = 'V_279',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.c, P.g], [P.g, P.s] ], [ [P.c, P.g, P.s] ] ],
                 couplings = {(0,1,0):C.UVGC_280_92,(0,1,1):C.UVGC_280_93,(0,0,0):C.UVGC_967_455,(0,0,1):C.UVGC_967_456})

V_280 = CTVertex(name = 'V_280',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.s, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.g, P.s] ], [ [P.g, P.s, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_325_196,(0,1,2):C.UVGC_325_197,(0,1,1):C.UVGC_325_198,(0,0,0):C.UVGC_988_504,(0,0,2):C.UVGC_988_505,(0,0,1):C.UVGC_988_506})

V_281 = CTVertex(name = 'V_281',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g], [P.g, P.u] ], [ [P.b, P.g, P.u] ] ],
                 couplings = {(0,1,0):C.UVGC_284_100,(0,1,1):C.UVGC_284_101,(0,0,0):C.UVGC_971_463,(0,0,1):C.UVGC_971_464})

V_282 = CTVertex(name = 'V_282',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.c, P.g] ], [ [P.b, P.g], [P.c, P.g] ] ],
                 couplings = {(0,1,1):C.UVGC_272_76,(0,1,0):C.UVGC_272_77,(0,0,1):C.UVGC_959_439,(0,0,0):C.UVGC_959_440})

V_283 = CTVertex(name = 'V_283',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.b, P.WR__plus__ ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b, P.g] ], [ [P.b, P.g, P.t] ], [ [P.g, P.t] ] ],
                 couplings = {(0,1,0):C.UVGC_326_199,(0,1,2):C.UVGC_326_200,(0,1,1):C.UVGC_326_201,(0,0,0):C.UVGC_989_507,(0,0,2):C.UVGC_989_508,(0,0,1):C.UVGC_989_509})

V_284 = CTVertex(name = 'V_284',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.d, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                 couplings = {(0,0,1):C.UVGC_255_69,(0,1,0):C.UVGC_254_65,(0,1,2):C.UVGC_254_66,(0,1,3):C.UVGC_254_67,(0,1,1):C.UVGC_254_68,(0,2,0):C.UVGC_254_65,(0,2,2):C.UVGC_254_66,(0,2,3):C.UVGC_254_67,(0,2,1):C.UVGC_254_68})

V_285 = CTVertex(name = 'V_285',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.g] ], [ [P.ghG] ], [ [P.g, P.s] ] ],
                 couplings = {(0,0,3):C.UVGC_255_69,(0,1,0):C.UVGC_254_65,(0,1,1):C.UVGC_254_66,(0,1,2):C.UVGC_254_67,(0,1,3):C.UVGC_254_68,(0,2,0):C.UVGC_254_65,(0,2,1):C.UVGC_254_66,(0,2,2):C.UVGC_254_67,(0,2,3):C.UVGC_254_68})

V_286 = CTVertex(name = 'V_286',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b, P.g ],
                 color = [ 'T(3,2,1)' ],
                 lorentz = [ L.FFV1, L.FFV3, L.FFV4 ],
                 loop_particles = [ [ [P.b], [P.c], [P.d], [P.s], [P.u] ], [ [P.b, P.g] ], [ [P.g] ], [ [P.ghG] ] ],
                 couplings = {(0,0,1):C.UVGC_255_69,(0,1,0):C.UVGC_254_65,(0,1,2):C.UVGC_254_66,(0,1,3):C.UVGC_254_67,(0,1,1):C.UVGC_254_68,(0,2,0):C.UVGC_254_65,(0,2,2):C.UVGC_254_66,(0,2,3):C.UVGC_254_67,(0,2,1):C.UVGC_254_68})

V_287 = CTVertex(name = 'V_287',
                 type = 'UV',
                 particles = [ P.u__tilde__, P.u ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.u] ] ],
                 couplings = {(0,0,0):C.UVGC_253_64,(0,1,0):C.UVGC_241_53,(0,2,0):C.UVGC_241_53})

V_288 = CTVertex(name = 'V_288',
                 type = 'UV',
                 particles = [ P.c__tilde__, P.c ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.c, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_253_64,(0,1,0):C.UVGC_241_53,(0,2,0):C.UVGC_241_53})

V_289 = CTVertex(name = 'V_289',
                 type = 'UV',
                 particles = [ P.t__tilde__, P.t ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF2, L.FF3, L.FF4, L.FF5 ],
                 loop_particles = [ [ [P.g, P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_304_143,(0,2,0):C.UVGC_304_143,(0,1,0):C.UVGC_302_141,(0,3,0):C.UVGC_302_141})

V_290 = CTVertex(name = 'V_290',
                 type = 'UV',
                 particles = [ P.d__tilde__, P.d ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.d, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_253_64,(0,1,0):C.UVGC_241_53,(0,2,0):C.UVGC_241_53})

V_291 = CTVertex(name = 'V_291',
                 type = 'UV',
                 particles = [ P.s__tilde__, P.s ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.g, P.s] ] ],
                 couplings = {(0,0,0):C.UVGC_253_64,(0,1,0):C.UVGC_241_53,(0,2,0):C.UVGC_241_53})

V_292 = CTVertex(name = 'V_292',
                 type = 'UV',
                 particles = [ P.b__tilde__, P.b ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.FF1, L.FF3, L.FF5 ],
                 loop_particles = [ [ [P.b, P.g] ] ],
                 couplings = {(0,0,0):C.UVGC_253_64,(0,1,0):C.UVGC_241_53,(0,2,0):C.UVGC_241_53})

V_293 = CTVertex(name = 'V_293',
                 type = 'UV',
                 particles = [ P.g, P.g ],
                 color = [ 'Identity(1,2)' ],
                 lorentz = [ L.VV1, L.VV3 ],
                 loop_particles = [ [ [P.g] ], [ [P.ghG] ], [ [P.t] ] ],
                 couplings = {(0,0,0):C.UVGC_294_119,(0,0,1):C.UVGC_294_120,(0,0,2):C.UVGC_294_121,(0,1,2):C.UVGC_293_118})

