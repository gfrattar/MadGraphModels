# This file was automatically created by FeynRules 2.3.49
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Wed 10 May 2023 13:02:43


from object_library import all_decays, Decay
import particles as P


Decay_S1 = Decay(name = 'Decay_S1',
                 particle = P.S1,
                 partial_widths = {(P.e__minus__,P.e__plus__):'(((-8*gcL**2*Me**2*vev**2)/ff**2 + (2*gcL**2*MS1**2*vev**2)/ff**2)*cmath.sqrt(-4*Me**2*MS1**2 + MS1**4))/(16.*cmath.pi*abs(MS1)**3)',
                                   (P.mu__minus__,P.mu__plus__):'(((-8*gcL**2*MMU**2*vev**2)/ff**2 + (2*gcL**2*MS1**2*vev**2)/ff**2)*cmath.sqrt(-4*MMU**2*MS1**2 + MS1**4))/(16.*cmath.pi*abs(MS1)**3)'})

