# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:25:44


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS14 = Lorentz(name = 'UUS14',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV20 = Lorentz(name = 'UUV20',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2)')

UUV21 = Lorentz(name = 'UUV21',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,3)')

SSS14 = Lorentz(name = 'SSS14',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS28 = Lorentz(name = 'FFS28',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS29 = Lorentz(name = 'FFS29',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV58 = Lorentz(name = 'FFV58',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV59 = Lorentz(name = 'FFV59',
                spins = [ 2, 2, 3 ],
                structure = '-(Epsilon(3,-1,-2,-3)*P(-3,3)*P(-2,1)*Gamma5(-4,1)*Gamma(-1,2,-4))')

FFV60 = Lorentz(name = 'FFV60',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV61 = Lorentz(name = 'FFV61',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS14 = Lorentz(name = 'VSS14',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS14 = Lorentz(name = 'VVS14',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV14 = Lorentz(name = 'VVV14',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS14 = Lorentz(name = 'SSSS14',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

VVSS14 = Lorentz(name = 'VVSS14',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV66 = Lorentz(name = 'VVVV66',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV67 = Lorentz(name = 'VVVV67',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV68 = Lorentz(name = 'VVVV68',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV69 = Lorentz(name = 'VVVV69',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV70 = Lorentz(name = 'VVVV70',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

