Requestor: Christiano Alpigiani
Content: Long-lived particles consistent with baryogenisis
Paper: http://arxiv.org/abs/1409.6729
JIRA: https://its.cern.ch/jira/browse/AGENE-1049