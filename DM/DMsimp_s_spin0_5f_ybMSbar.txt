Requestor: Yoav Afik 
Content: S-channel dark matter (spin 0) simplified model at NLO (especially for bottom associated production (5FS))
Webpage: http://feynrules.irmp.ucl.ac.be/wiki/DMsimp
Paper: https://arxiv.org/abs/1802.07237
JIRA: https://its.cern.ch/jira/browse/AGENE-1644
