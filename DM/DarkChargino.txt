Requestor: Prathik Boyella
Contents: Minimal supersymmetric dark matter model with a singlet-triplet Higgs portal.
Source: https://github.com/arsahasransu/SoftDisplacedLeptons
Paper 1: https://arxiv.org/pdf/2007.03708.pdf
Paper 2: https://arxiv.org/pdf/1812.04628.pdf
