Requestor: Spyros Argyropoulos
Content: Simplified t-channel model including flavour matrices to allow all possible DM/SM couplings - to be used only for LO generation
Paper: t-channel white-paper (in preparation)
Source: Benjamin Fuks
Webpage: https://github.com/FeynRules/Models
