# This file was automatically created by FeynRules 2.3.1
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Wed 27 May 2015 17:22:32


from object_library import all_lorentz, Lorentz

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot
try:
   import form_factors as ForFac 
except ImportError:
   pass


UUS10 = Lorentz(name = 'UUS10',
                spins = [ -1, -1, 1 ],
                structure = '1')

UUV12 = Lorentz(name = 'UUV12',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,2)')

UUV13 = Lorentz(name = 'UUV13',
                spins = [ -1, -1, 3 ],
                structure = 'P(3,3)')

SSS10 = Lorentz(name = 'SSS10',
                spins = [ 1, 1, 1 ],
                structure = '1')

FFS20 = Lorentz(name = 'FFS20',
                spins = [ 2, 2, 1 ],
                structure = 'ProjM(2,1)')

FFS21 = Lorentz(name = 'FFS21',
                spins = [ 2, 2, 1 ],
                structure = 'ProjP(2,1)')

FFV40 = Lorentz(name = 'FFV40',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,1)')

FFV41 = Lorentz(name = 'FFV41',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjM(-1,1)')

FFV42 = Lorentz(name = 'FFV42',
                spins = [ 2, 2, 3 ],
                structure = 'Gamma(3,2,-1)*ProjP(-1,1)')

VSS10 = Lorentz(name = 'VSS10',
                spins = [ 3, 1, 1 ],
                structure = 'P(1,2) - P(1,3)')

VVS10 = Lorentz(name = 'VVS10',
                spins = [ 3, 3, 1 ],
                structure = 'Metric(1,2)')

VVV10 = Lorentz(name = 'VVV10',
                spins = [ 3, 3, 3 ],
                structure = 'P(3,1)*Metric(1,2) - P(3,2)*Metric(1,2) - P(2,1)*Metric(1,3) + P(2,3)*Metric(1,3) + P(1,2)*Metric(2,3) - P(1,3)*Metric(2,3)')

SSSS10 = Lorentz(name = 'SSSS10',
                 spins = [ 1, 1, 1, 1 ],
                 structure = '1')

FFVV21 = Lorentz(name = 'FFVV21',
                 spins = [ 2, 2, 3, 3 ],
                 structure = 'Gamma5(2,1)*Metric(3,4)')

VVSS10 = Lorentz(name = 'VVSS10',
                 spins = [ 3, 3, 1, 1 ],
                 structure = 'Metric(1,2)')

VVVV46 = Lorentz(name = 'VVVV46',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,3)*Metric(2,4)')

VVVV47 = Lorentz(name = 'VVVV47',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) + Metric(1,3)*Metric(2,4) - 2*Metric(1,2)*Metric(3,4)')

VVVV48 = Lorentz(name = 'VVVV48',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - Metric(1,2)*Metric(3,4)')

VVVV49 = Lorentz(name = 'VVVV49',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,3)*Metric(2,4) - Metric(1,2)*Metric(3,4)')

VVVV50 = Lorentz(name = 'VVVV50',
                 spins = [ 3, 3, 3, 3 ],
                 structure = 'Metric(1,4)*Metric(2,3) - (Metric(1,3)*Metric(2,4))/2. - (Metric(1,2)*Metric(3,4))/2.')

