# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 10.2.0 for Linux x86 (64-bit) (July 6, 2015)
# Date: Mon 31 Jul 2017 01:18:53


from object_library import all_decays, Decay
import particles as P


Decay_b = Decay(name = 'Decay_b',
                particle = P.b,
                partial_widths = {(P.phidown,P.chic):'((3*I1a23*MB**2*complexconjugate(I1a23) + 3*I1a23*Mchic**2*complexconjugate(I1a23) - 3*I1a23*Mphidown**2*complexconjugate(I1a23))*cmath.sqrt(MB**4 - 2*MB**2*Mchic**2 + Mchic**4 - 2*MB**2*Mphidown**2 - 2*Mchic**2*Mphidown**2 + Mphidown**4))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.phidown,P.chit):'((3*I1a33*MB**2*complexconjugate(I1a33) + 3*I1a33*Mchit**2*complexconjugate(I1a33) - 3*I1a33*Mphidown**2*complexconjugate(I1a33))*cmath.sqrt(MB**4 - 2*MB**2*Mchit**2 + Mchit**4 - 2*MB**2*Mphidown**2 - 2*Mchit**2*Mphidown**2 + Mphidown**4))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.phidown,P.chiu):'((3*I1a13*MB**2*complexconjugate(I1a13) + 3*I1a13*Mchiu**2*complexconjugate(I1a13) - 3*I1a13*Mphidown**2*complexconjugate(I1a13))*cmath.sqrt(MB**4 - 2*MB**2*Mchiu**2 + Mchiu**4 - 2*MB**2*Mphidown**2 - 2*Mchiu**2*Mphidown**2 + Mphidown**4))/(96.*cmath.pi*abs(MB)**3)',
                                  (P.W__minus__,P.t):'(((3*ee**2*MB**2)/(2.*sw**2) + (3*ee**2*MT**2)/(2.*sw**2) + (3*ee**2*MB**4)/(2.*MW**2*sw**2) - (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) + (3*ee**2*MT**4)/(2.*MW**2*sw**2) - (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(96.*cmath.pi*abs(MB)**3)'})

Decay_chic = Decay(name = 'Decay_chic',
                   particle = P.chic,
                   partial_widths = {(P.phidown__tilde__,P.b):'((3*I6a32*MB**2*complexconjugate(I6a32) + 3*I6a32*Mchic**2*complexconjugate(I6a32) - 3*I6a32*Mphidown**2*complexconjugate(I6a32))*cmath.sqrt(MB**4 - 2*MB**2*Mchic**2 + Mchic**4 - 2*MB**2*Mphidown**2 - 2*Mchic**2*Mphidown**2 + Mphidown**4))/(32.*cmath.pi*abs(Mchic)**3)',
                                     (P.phidown__tilde__,P.d):'((Mchic**2 - Mphidown**2)*(3*I6a12*Mchic**2*complexconjugate(I6a12) - 3*I6a12*Mphidown**2*complexconjugate(I6a12)))/(32.*cmath.pi*abs(Mchic)**3)',
                                     (P.phidown__tilde__,P.s):'((Mchic**2 - Mphidown**2)*(3*I6a22*Mchic**2*complexconjugate(I6a22) - 3*I6a22*Mphidown**2*complexconjugate(I6a22)))/(32.*cmath.pi*abs(Mchic)**3)',
                                     (P.phiup__tilde__,P.c):'((Mchic**2 - Mphiup**2)*(3*lambda2x2*Mchic**2*complexconjugate(lambda2x2) - 3*lambda2x2*Mphiup**2*complexconjugate(lambda2x2)))/(32.*cmath.pi*abs(Mchic)**3)',
                                     (P.phiup__tilde__,P.t):'((3*lambda3x2*Mchic**2*complexconjugate(lambda3x2) - 3*lambda3x2*Mphiup**2*complexconjugate(lambda3x2) + 3*lambda3x2*MT**2*complexconjugate(lambda3x2))*cmath.sqrt(Mchic**4 - 2*Mchic**2*Mphiup**2 + Mphiup**4 - 2*Mchic**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(32.*cmath.pi*abs(Mchic)**3)',
                                     (P.phiup__tilde__,P.u):'((Mchic**2 - Mphiup**2)*(3*lambda1x2*Mchic**2*complexconjugate(lambda1x2) - 3*lambda1x2*Mphiup**2*complexconjugate(lambda1x2)))/(32.*cmath.pi*abs(Mchic)**3)'})

Decay_chit = Decay(name = 'Decay_chit',
                   particle = P.chit,
                   partial_widths = {(P.phidown__tilde__,P.b):'((3*I6a33*MB**2*complexconjugate(I6a33) + 3*I6a33*Mchit**2*complexconjugate(I6a33) - 3*I6a33*Mphidown**2*complexconjugate(I6a33))*cmath.sqrt(MB**4 - 2*MB**2*Mchit**2 + Mchit**4 - 2*MB**2*Mphidown**2 - 2*Mchit**2*Mphidown**2 + Mphidown**4))/(32.*cmath.pi*abs(Mchit)**3)',
                                     (P.phidown__tilde__,P.d):'((Mchit**2 - Mphidown**2)*(3*I6a13*Mchit**2*complexconjugate(I6a13) - 3*I6a13*Mphidown**2*complexconjugate(I6a13)))/(32.*cmath.pi*abs(Mchit)**3)',
                                     (P.phidown__tilde__,P.s):'((Mchit**2 - Mphidown**2)*(3*I6a23*Mchit**2*complexconjugate(I6a23) - 3*I6a23*Mphidown**2*complexconjugate(I6a23)))/(32.*cmath.pi*abs(Mchit)**3)',
                                     (P.phiup__tilde__,P.c):'((Mchit**2 - Mphiup**2)*(3*lambda2x3*Mchit**2*complexconjugate(lambda2x3) - 3*lambda2x3*Mphiup**2*complexconjugate(lambda2x3)))/(32.*cmath.pi*abs(Mchit)**3)',
                                     (P.phiup__tilde__,P.t):'((3*lambda3x3*Mchit**2*complexconjugate(lambda3x3) - 3*lambda3x3*Mphiup**2*complexconjugate(lambda3x3) + 3*lambda3x3*MT**2*complexconjugate(lambda3x3))*cmath.sqrt(Mchit**4 - 2*Mchit**2*Mphiup**2 + Mphiup**4 - 2*Mchit**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(32.*cmath.pi*abs(Mchit)**3)',
                                     (P.phiup__tilde__,P.u):'((Mchit**2 - Mphiup**2)*(3*lambda1x3*Mchit**2*complexconjugate(lambda1x3) - 3*lambda1x3*Mphiup**2*complexconjugate(lambda1x3)))/(32.*cmath.pi*abs(Mchit)**3)'})

Decay_chiu = Decay(name = 'Decay_chiu',
                   particle = P.chiu,
                   partial_widths = {(P.phidown__tilde__,P.b):'((3*I6a31*MB**2*complexconjugate(I6a31) + 3*I6a31*Mchiu**2*complexconjugate(I6a31) - 3*I6a31*Mphidown**2*complexconjugate(I6a31))*cmath.sqrt(MB**4 - 2*MB**2*Mchiu**2 + Mchiu**4 - 2*MB**2*Mphidown**2 - 2*Mchiu**2*Mphidown**2 + Mphidown**4))/(32.*cmath.pi*abs(Mchiu)**3)',
                                     (P.phidown__tilde__,P.d):'((Mchiu**2 - Mphidown**2)*(3*I6a11*Mchiu**2*complexconjugate(I6a11) - 3*I6a11*Mphidown**2*complexconjugate(I6a11)))/(32.*cmath.pi*abs(Mchiu)**3)',
                                     (P.phidown__tilde__,P.s):'((Mchiu**2 - Mphidown**2)*(3*I6a21*Mchiu**2*complexconjugate(I6a21) - 3*I6a21*Mphidown**2*complexconjugate(I6a21)))/(32.*cmath.pi*abs(Mchiu)**3)',
                                     (P.phiup__tilde__,P.c):'((Mchiu**2 - Mphiup**2)*(3*lambda2x1*Mchiu**2*complexconjugate(lambda2x1) - 3*lambda2x1*Mphiup**2*complexconjugate(lambda2x1)))/(32.*cmath.pi*abs(Mchiu)**3)',
                                     (P.phiup__tilde__,P.t):'((3*lambda3x1*Mchiu**2*complexconjugate(lambda3x1) - 3*lambda3x1*Mphiup**2*complexconjugate(lambda3x1) + 3*lambda3x1*MT**2*complexconjugate(lambda3x1))*cmath.sqrt(Mchiu**4 - 2*Mchiu**2*Mphiup**2 + Mphiup**4 - 2*Mchiu**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(32.*cmath.pi*abs(Mchiu)**3)',
                                     (P.phiup__tilde__,P.u):'((Mchiu**2 - Mphiup**2)*(3*lambda1x1*Mchiu**2*complexconjugate(lambda1x1) - 3*lambda1x1*Mphiup**2*complexconjugate(lambda1x1)))/(32.*cmath.pi*abs(Mchiu)**3)'})

Decay_H = Decay(name = 'Decay_H',
                particle = P.H,
                partial_widths = {(P.b,P.b__tilde__):'((-12*MB**2*yb**2 + 3*MH**2*yb**2)*cmath.sqrt(-4*MB**2*MH**2 + MH**4))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.t,P.t__tilde__):'((3*MH**2*yt**2 - 12*MT**2*yt**2)*cmath.sqrt(MH**4 - 4*MH**2*MT**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((MH**2*ytau**2 - 4*MTA**2*ytau**2)*cmath.sqrt(MH**4 - 4*MH**2*MTA**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.W__minus__,P.W__plus__):'(((3*ee**4*vev**2)/(4.*sw**4) + (ee**4*MH**4*vev**2)/(16.*MW**4*sw**4) - (ee**4*MH**2*vev**2)/(4.*MW**2*sw**4))*cmath.sqrt(MH**4 - 4*MH**2*MW**2))/(16.*cmath.pi*abs(MH)**3)',
                                  (P.Z,P.Z):'(((9*ee**4*vev**2)/2. + (3*ee**4*MH**4*vev**2)/(8.*MZ**4) - (3*ee**4*MH**2*vev**2)/(2.*MZ**2) + (3*cw**4*ee**4*vev**2)/(4.*sw**4) + (cw**4*ee**4*MH**4*vev**2)/(16.*MZ**4*sw**4) - (cw**4*ee**4*MH**2*vev**2)/(4.*MZ**2*sw**4) + (3*cw**2*ee**4*vev**2)/sw**2 + (cw**2*ee**4*MH**4*vev**2)/(4.*MZ**4*sw**2) - (cw**2*ee**4*MH**2*vev**2)/(MZ**2*sw**2) + (3*ee**4*sw**2*vev**2)/cw**2 + (ee**4*MH**4*sw**2*vev**2)/(4.*cw**2*MZ**4) - (ee**4*MH**2*sw**2*vev**2)/(cw**2*MZ**2) + (3*ee**4*sw**4*vev**2)/(4.*cw**4) + (ee**4*MH**4*sw**4*vev**2)/(16.*cw**4*MZ**4) - (ee**4*MH**2*sw**4*vev**2)/(4.*cw**4*MZ**2))*cmath.sqrt(MH**4 - 4*MH**2*MZ**2))/(32.*cmath.pi*abs(MH)**3)'})

Decay_phidown = Decay(name = 'Decay_phidown',
                      particle = P.phidown,
                      partial_widths = {(P.b,P.chic__tilde__):'((-3*I6a32*MB**2*complexconjugate(I6a32) - 3*I6a32*Mchic**2*complexconjugate(I6a32) + 3*I6a32*Mphidown**2*complexconjugate(I6a32))*cmath.sqrt(MB**4 - 2*MB**2*Mchic**2 + Mchic**4 - 2*MB**2*Mphidown**2 - 2*Mchic**2*Mphidown**2 + Mphidown**4))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.b,P.chit__tilde__):'((-3*I6a33*MB**2*complexconjugate(I6a33) - 3*I6a33*Mchit**2*complexconjugate(I6a33) + 3*I6a33*Mphidown**2*complexconjugate(I6a33))*cmath.sqrt(MB**4 - 2*MB**2*Mchit**2 + Mchit**4 - 2*MB**2*Mphidown**2 - 2*Mchit**2*Mphidown**2 + Mphidown**4))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.b,P.chiu__tilde__):'((-3*I6a31*MB**2*complexconjugate(I6a31) - 3*I6a31*Mchiu**2*complexconjugate(I6a31) + 3*I6a31*Mphidown**2*complexconjugate(I6a31))*cmath.sqrt(MB**4 - 2*MB**2*Mchiu**2 + Mchiu**4 - 2*MB**2*Mphidown**2 - 2*Mchiu**2*Mphidown**2 + Mphidown**4))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.d,P.chic__tilde__):'((-Mchic**2 + Mphidown**2)*(-3*I6a12*Mchic**2*complexconjugate(I6a12) + 3*I6a12*Mphidown**2*complexconjugate(I6a12)))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.d,P.chit__tilde__):'((-Mchit**2 + Mphidown**2)*(-3*I6a13*Mchit**2*complexconjugate(I6a13) + 3*I6a13*Mphidown**2*complexconjugate(I6a13)))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.d,P.chiu__tilde__):'((-Mchiu**2 + Mphidown**2)*(-3*I6a11*Mchiu**2*complexconjugate(I6a11) + 3*I6a11*Mphidown**2*complexconjugate(I6a11)))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.phiup,P.W__minus__):'(((-3*ee**2*Mphidown**2)/sw**2 - (3*ee**2*Mphiup**2)/sw**2 + (3*ee**2*Mphidown**4)/(2.*MW**2*sw**2) - (3*ee**2*Mphidown**2*Mphiup**2)/(MW**2*sw**2) + (3*ee**2*Mphiup**4)/(2.*MW**2*sw**2) + (3*ee**2*MW**2)/(2.*sw**2))*cmath.sqrt(Mphidown**4 - 2*Mphidown**2*Mphiup**2 + Mphiup**4 - 2*Mphidown**2*MW**2 - 2*Mphiup**2*MW**2 + MW**4))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.s,P.chic__tilde__):'((-Mchic**2 + Mphidown**2)*(-3*I6a22*Mchic**2*complexconjugate(I6a22) + 3*I6a22*Mphidown**2*complexconjugate(I6a22)))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.s,P.chit__tilde__):'((-Mchit**2 + Mphidown**2)*(-3*I6a23*Mchit**2*complexconjugate(I6a23) + 3*I6a23*Mphidown**2*complexconjugate(I6a23)))/(48.*cmath.pi*abs(Mphidown)**3)',
                                        (P.s,P.chiu__tilde__):'((-Mchiu**2 + Mphidown**2)*(-3*I6a21*Mchiu**2*complexconjugate(I6a21) + 3*I6a21*Mphidown**2*complexconjugate(I6a21)))/(48.*cmath.pi*abs(Mphidown)**3)'})

Decay_phiup = Decay(name = 'Decay_phiup',
                    particle = P.phiup,
                    partial_widths = {(P.c,P.chic__tilde__):'((-Mchic**2 + Mphiup**2)*(-3*lambda2x2*Mchic**2*complexconjugate(lambda2x2) + 3*lambda2x2*Mphiup**2*complexconjugate(lambda2x2)))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.c,P.chit__tilde__):'((-Mchit**2 + Mphiup**2)*(-3*lambda2x3*Mchit**2*complexconjugate(lambda2x3) + 3*lambda2x3*Mphiup**2*complexconjugate(lambda2x3)))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.c,P.chiu__tilde__):'((-Mchiu**2 + Mphiup**2)*(-3*lambda2x1*Mchiu**2*complexconjugate(lambda2x1) + 3*lambda2x1*Mphiup**2*complexconjugate(lambda2x1)))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.phidown,P.W__plus__):'(((-3*ee**2*Mphidown**2)/sw**2 - (3*ee**2*Mphiup**2)/sw**2 + (3*ee**2*Mphidown**4)/(2.*MW**2*sw**2) - (3*ee**2*Mphidown**2*Mphiup**2)/(MW**2*sw**2) + (3*ee**2*Mphiup**4)/(2.*MW**2*sw**2) + (3*ee**2*MW**2)/(2.*sw**2))*cmath.sqrt(Mphidown**4 - 2*Mphidown**2*Mphiup**2 + Mphiup**4 - 2*Mphidown**2*MW**2 - 2*Mphiup**2*MW**2 + MW**4))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.t,P.chic__tilde__):'((-3*lambda3x2*Mchic**2*complexconjugate(lambda3x2) + 3*lambda3x2*Mphiup**2*complexconjugate(lambda3x2) - 3*lambda3x2*MT**2*complexconjugate(lambda3x2))*cmath.sqrt(Mchic**4 - 2*Mchic**2*Mphiup**2 + Mphiup**4 - 2*Mchic**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.t,P.chit__tilde__):'((-3*lambda3x3*Mchit**2*complexconjugate(lambda3x3) + 3*lambda3x3*Mphiup**2*complexconjugate(lambda3x3) - 3*lambda3x3*MT**2*complexconjugate(lambda3x3))*cmath.sqrt(Mchit**4 - 2*Mchit**2*Mphiup**2 + Mphiup**4 - 2*Mchit**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.t,P.chiu__tilde__):'((-3*lambda3x1*Mchiu**2*complexconjugate(lambda3x1) + 3*lambda3x1*Mphiup**2*complexconjugate(lambda3x1) - 3*lambda3x1*MT**2*complexconjugate(lambda3x1))*cmath.sqrt(Mchiu**4 - 2*Mchiu**2*Mphiup**2 + Mphiup**4 - 2*Mchiu**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.u,P.chic__tilde__):'((-Mchic**2 + Mphiup**2)*(-3*lambda1x2*Mchic**2*complexconjugate(lambda1x2) + 3*lambda1x2*Mphiup**2*complexconjugate(lambda1x2)))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.u,P.chit__tilde__):'((-Mchit**2 + Mphiup**2)*(-3*lambda1x3*Mchit**2*complexconjugate(lambda1x3) + 3*lambda1x3*Mphiup**2*complexconjugate(lambda1x3)))/(48.*cmath.pi*abs(Mphiup)**3)',
                                      (P.u,P.chiu__tilde__):'((-Mchiu**2 + Mphiup**2)*(-3*lambda1x1*Mchiu**2*complexconjugate(lambda1x1) + 3*lambda1x1*Mphiup**2*complexconjugate(lambda1x1)))/(48.*cmath.pi*abs(Mphiup)**3)'})

Decay_t = Decay(name = 'Decay_t',
                particle = P.t,
                partial_widths = {(P.phiup,P.chic):'((3*lambda3x2*Mchic**2*complexconjugate(lambda3x2) - 3*lambda3x2*Mphiup**2*complexconjugate(lambda3x2) + 3*lambda3x2*MT**2*complexconjugate(lambda3x2))*cmath.sqrt(Mchic**4 - 2*Mchic**2*Mphiup**2 + Mphiup**4 - 2*Mchic**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.phiup,P.chit):'((3*lambda3x3*Mchit**2*complexconjugate(lambda3x3) - 3*lambda3x3*Mphiup**2*complexconjugate(lambda3x3) + 3*lambda3x3*MT**2*complexconjugate(lambda3x3))*cmath.sqrt(Mchit**4 - 2*Mchit**2*Mphiup**2 + Mphiup**4 - 2*Mchit**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.phiup,P.chiu):'((3*lambda3x1*Mchiu**2*complexconjugate(lambda3x1) - 3*lambda3x1*Mphiup**2*complexconjugate(lambda3x1) + 3*lambda3x1*MT**2*complexconjugate(lambda3x1))*cmath.sqrt(Mchiu**4 - 2*Mchiu**2*Mphiup**2 + Mphiup**4 - 2*Mchiu**2*MT**2 - 2*Mphiup**2*MT**2 + MT**4))/(96.*cmath.pi*abs(MT)**3)',
                                  (P.W__plus__,P.b):'(((3*ee**2*MB**2)/(2.*sw**2) + (3*ee**2*MT**2)/(2.*sw**2) + (3*ee**2*MB**4)/(2.*MW**2*sw**2) - (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) + (3*ee**2*MT**4)/(2.*MW**2*sw**2) - (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(96.*cmath.pi*abs(MT)**3)'})

Decay_ta__minus__ = Decay(name = 'Decay_ta__minus__',
                          particle = P.ta__minus__,
                          partial_widths = {(P.W__minus__,P.vt):'((MTA**2 - MW**2)*((ee**2*MTA**2)/(2.*sw**2) + (ee**2*MTA**4)/(2.*MW**2*sw**2) - (ee**2*MW**2)/sw**2))/(32.*cmath.pi*abs(MTA)**3)'})

Decay_W__plus__ = Decay(name = 'Decay_W__plus__',
                        particle = P.W__plus__,
                        partial_widths = {(P.c,P.s__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.phidown__tilde__,P.phiup):'(((-3*ee**2*Mphidown**2)/sw**2 - (3*ee**2*Mphiup**2)/sw**2 + (3*ee**2*Mphidown**4)/(2.*MW**2*sw**2) - (3*ee**2*Mphidown**2*Mphiup**2)/(MW**2*sw**2) + (3*ee**2*Mphiup**4)/(2.*MW**2*sw**2) + (3*ee**2*MW**2)/(2.*sw**2))*cmath.sqrt(Mphidown**4 - 2*Mphidown**2*Mphiup**2 + Mphiup**4 - 2*Mphidown**2*MW**2 - 2*Mphiup**2*MW**2 + MW**4))/(48.*cmath.pi*abs(MW)**3)',
                                          (P.t,P.b__tilde__):'(((-3*ee**2*MB**2)/(2.*sw**2) - (3*ee**2*MT**2)/(2.*sw**2) - (3*ee**2*MB**4)/(2.*MW**2*sw**2) + (3*ee**2*MB**2*MT**2)/(MW**2*sw**2) - (3*ee**2*MT**4)/(2.*MW**2*sw**2) + (3*ee**2*MW**2)/sw**2)*cmath.sqrt(MB**4 - 2*MB**2*MT**2 + MT**4 - 2*MB**2*MW**2 - 2*MT**2*MW**2 + MW**4))/(48.*cmath.pi*abs(MW)**3)',
                                          (P.u,P.d__tilde__):'(ee**2*MW**4)/(16.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.ve,P.e__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vm,P.mu__plus__):'(ee**2*MW**4)/(48.*cmath.pi*sw**2*abs(MW)**3)',
                                          (P.vt,P.ta__plus__):'((-MTA**2 + MW**2)*(-(ee**2*MTA**2)/(2.*sw**2) - (ee**2*MTA**4)/(2.*MW**2*sw**2) + (ee**2*MW**2)/sw**2))/(48.*cmath.pi*abs(MW)**3)'})

Decay_Z = Decay(name = 'Decay_Z',
                particle = P.Z,
                partial_widths = {(P.b,P.b__tilde__):'((-7*ee**2*MB**2 + ee**2*MZ**2 - (3*cw**2*ee**2*MB**2)/(2.*sw**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) - (17*ee**2*MB**2*sw**2)/(6.*cw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2))*cmath.sqrt(-4*MB**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.c,P.c__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.d,P.d__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.e__minus__,P.e__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.mu__minus__,P.mu__plus__):'(MZ**2*(-(ee**2*MZ**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.phidown__tilde__,P.phidown):'((-2*ee**2*Mphidown**2 + (ee**2*MZ**2)/2. - (3*cw**2*ee**2*Mphidown**2)/sw**2 + (3*cw**2*ee**2*MZ**2)/(4.*sw**2) - (ee**2*Mphidown**2*sw**2)/(3.*cw**2) + (ee**2*MZ**2*sw**2)/(12.*cw**2))*cmath.sqrt(-4*Mphidown**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.phiup__tilde__,P.phiup):'((2*ee**2*Mphiup**2 - (ee**2*MZ**2)/2. - (3*cw**2*ee**2*Mphiup**2)/sw**2 + (3*cw**2*ee**2*MZ**2)/(4.*sw**2) - (ee**2*Mphiup**2*sw**2)/(3.*cw**2) + (ee**2*MZ**2*sw**2)/(12.*cw**2))*cmath.sqrt(-4*Mphiup**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.s,P.s__tilde__):'(MZ**2*(ee**2*MZ**2 + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (5*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.t,P.t__tilde__):'((-11*ee**2*MT**2 - ee**2*MZ**2 - (3*cw**2*ee**2*MT**2)/(2.*sw**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MT**2*sw**2)/(6.*cw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2))*cmath.sqrt(-4*MT**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ta__minus__,P.ta__plus__):'((-5*ee**2*MTA**2 - ee**2*MZ**2 - (cw**2*ee**2*MTA**2)/(2.*sw**2) + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (7*ee**2*MTA**2*sw**2)/(2.*cw**2) + (5*ee**2*MZ**2*sw**2)/(2.*cw**2))*cmath.sqrt(-4*MTA**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.u,P.u__tilde__):'(MZ**2*(-(ee**2*MZ**2) + (3*cw**2*ee**2*MZ**2)/(2.*sw**2) + (17*ee**2*MZ**2*sw**2)/(6.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.ve,P.ve__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vm,P.vm__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.vt,P.vt__tilde__):'(MZ**2*(ee**2*MZ**2 + (cw**2*ee**2*MZ**2)/(2.*sw**2) + (ee**2*MZ**2*sw**2)/(2.*cw**2)))/(48.*cmath.pi*abs(MZ)**3)',
                                  (P.W__minus__,P.W__plus__):'(((-12*cw**2*ee**2*MW**2)/sw**2 - (17*cw**2*ee**2*MZ**2)/sw**2 + (4*cw**2*ee**2*MZ**4)/(MW**2*sw**2) + (cw**2*ee**2*MZ**6)/(4.*MW**4*sw**2))*cmath.sqrt(-4*MW**2*MZ**2 + MZ**4))/(48.*cmath.pi*abs(MZ)**3)'})

