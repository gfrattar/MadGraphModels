Requestor: Maria Moreno Llacer
Content: NLO EFT ttV model
Paper: http://arxiv.org/abs/1601.08193
JIRA: https://its.cern.ch/jira/browse/AGENE-1218