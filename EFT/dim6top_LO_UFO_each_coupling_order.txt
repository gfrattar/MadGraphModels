Requestor: Thomas Peiffer
Contents: Top EFT dim6top UFO model. 
          An individual coupling order is additionally assigned to each EFT parameter: 
          the cQQ1 and cqq11x3331 parameters are for instance assigned DIM6_cQQ1 and FCNC_cqq11x3331 coupling orders.
Source: http://feynrules.irmp.ucl.ac.be/wiki/dim6top
Paper: https://arxiv.org/abs/1802.07237
JIRA: https://its.cern.ch/jira/browse/AGENE-1585
