# This file was automatically created by FeynRules 2.3.48
# Mathematica version: 12.3.1 for Mac OS X x86 (64-bit) (July 7, 2021)
# Date: Wed 24 Nov 2021 17:07:08


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



