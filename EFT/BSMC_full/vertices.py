# This file was automatically created by FeynRules 2.4.61
# Mathematica version: 10.4.0 for Linux x86 (64-bit) (February 26, 2016)
# Date: Tue 17 Jul 2018 21:06:46


from object_library import all_vertices, Vertex
import particles as P
import couplings as C
import lorentz as L


V_1 = Vertex(name = 'V_1',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_1})

V_2 = Vertex(name = 'V_2',
             particles = [ P.H, P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSSS1 ],
             couplings = {(0,0):C.GC_11})

V_3 = Vertex(name = 'V_3',
             particles = [ P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSS1 ],
             couplings = {(0,0):C.GC_684})

V_4 = Vertex(name = 'V_4',
             particles = [ P.H, P.H, P.H ],
             color = [ '1' ],
             lorentz = [ L.SSS1 ],
             couplings = {(0,0):C.GC_685})

V_5 = Vertex(name = 'V_5',
             particles = [ P.g, P.g, P.g ],
             color = [ 'f(1,2,3)' ],
             lorentz = [ L.VVV10, L.VVV7, L.VVV8 ],
             couplings = {(0,0):C.GC_270,(0,2):C.GC_213,(0,1):C.GC_8})

V_6 = Vertex(name = 'V_6',
             particles = [ P.g, P.g, P.g, P.g ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV1, L.VVVV10, L.VVVV12, L.VVVV13, L.VVVV17, L.VVVV2, L.VVVV21, L.VVVV4, L.VVVV5, L.VVVV6, L.VVVV7, L.VVVV8 ],
             couplings = {(0,1):C.GC_271,(0,0):C.GC_276,(1,11):C.GC_271,(1,10):C.GC_276,(2,9):C.GC_271,(2,8):C.GC_276,(0,7):C.GC_214,(1,6):C.GC_214,(2,4):C.GC_214,(1,2):C.GC_10,(0,5):C.GC_10,(2,3):C.GC_10})

V_7 = Vertex(name = 'V_7',
             particles = [ P.g, P.g, P.g, P.g ],
             color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
             lorentz = [ L.VVVV17, L.VVVV21, L.VVVV4 ],
             couplings = {(0,2):C.GC_216,(1,1):C.GC_216,(2,0):C.GC_216})

V_8 = Vertex(name = 'V_8',
             particles = [ P.b__tilde__, P.b, P.H ],
             color = [ 'Identity(1,2)' ],
             lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
             couplings = {(0,2):C.GC_688,(0,0):C.GC_453,(0,1):C.GC_448})

V_9 = Vertex(name = 'V_9',
             particles = [ P.ta__plus__, P.ta__minus__, P.H ],
             color = [ '1' ],
             lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
             couplings = {(0,2):C.GC_690,(0,0):C.GC_454,(0,1):C.GC_450})

V_10 = Vertex(name = 'V_10',
              particles = [ P.t__tilde__, P.t, P.H ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFS1, L.FFS2, L.FFS3 ],
              couplings = {(0,2):C.GC_689,(0,0):C.GC_455,(0,1):C.GC_449})

V_11 = Vertex(name = 'V_11',
              particles = [ P.a, P.W__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVV1, L.VVV4, L.VVV7, L.VVV8, L.VVV9 ],
              couplings = {(0,0):C.GC_137,(0,4):C.GC_139,(0,3):C.GC_12,(0,2):C.GC_5,(0,1):C.GC_6})

V_12 = Vertex(name = 'V_12',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS4, L.VVSS5 ],
              couplings = {(0,3):C.GC_230,(0,0):C.GC_284,(0,2):C.GC_231,(0,1):C.GC_15})

V_13 = Vertex(name = 'V_13',
              particles = [ P.W__minus__, P.W__plus__, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_18})

V_14 = Vertex(name = 'V_14',
              particles = [ P.W__minus__, P.W__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS4, L.VVS5 ],
              couplings = {(0,3):C.GC_456,(0,0):C.GC_591,(0,2):C.GC_457,(0,1):C.GC_451})

V_15 = Vertex(name = 'V_15',
              particles = [ P.W__minus__, P.W__plus__, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_686})

V_16 = Vertex(name = 'V_16',
              particles = [ P.a, P.a, P.W__minus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV11, L.VVVV16, L.VVVV22 ],
              couplings = {(0,1):C.GC_140,(0,2):C.GC_13,(0,0):C.GC_7})

V_17 = Vertex(name = 'V_17',
              particles = [ P.W__minus__, P.W__plus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVV2, L.VVV3, L.VVV5, L.VVV6, L.VVV7, L.VVV8 ],
              couplings = {(0,0):C.GC_138,(0,1):C.GC_145,(0,5):C.GC_69,(0,4):C.GC_35,(0,2):C.GC_36,(0,3):C.GC_64})

V_18 = Vertex(name = 'V_18',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV11, L.VVVV22, L.VVVV9 ],
              couplings = {(0,2):C.GC_144,(0,1):C.GC_22,(0,0):C.GC_16})

V_19 = Vertex(name = 'V_19',
              particles = [ P.W__minus__, P.W__minus__, P.W__plus__, P.W__plus__ ],
              color = [ '1' ],
              lorentz = [ L.VVVV11 ],
              couplings = {(0,0):C.GC_20})

V_20 = Vertex(name = 'V_20',
              particles = [ P.a, P.W__minus__, P.W__plus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVVV14, L.VVVV15, L.VVVV19, L.VVVV20, L.VVVV3 ],
              couplings = {(0,4):C.GC_141,(0,2):C.GC_143,(0,1):C.GC_67,(0,3):C.GC_68,(0,0):C.GC_65})

V_21 = Vertex(name = 'V_21',
              particles = [ P.a, P.W__minus__, P.W__plus__, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVVV14 ],
              couplings = {(0,0):C.GC_66})

V_22 = Vertex(name = 'V_22',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS1, L.VVSS2, L.VVSS4, L.VVSS5 ],
              couplings = {(0,3):C.GC_232,(0,0):C.GC_147,(0,2):C.GC_146,(0,1):C.GC_136})

V_23 = Vertex(name = 'V_23',
              particles = [ P.Z, P.Z, P.H, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVSS2 ],
              couplings = {(0,0):C.GC_14})

V_24 = Vertex(name = 'V_24',
              particles = [ P.Z, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS1, L.VVS2, L.VVS4, L.VVS5 ],
              couplings = {(0,3):C.GC_458,(0,0):C.GC_593,(0,2):C.GC_459,(0,1):C.GC_452})

V_25 = Vertex(name = 'V_25',
              particles = [ P.Z, P.Z, P.H ],
              color = [ '1' ],
              lorentz = [ L.VVS2 ],
              couplings = {(0,0):C.GC_687})

V_26 = Vertex(name = 'V_26',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVVV11, L.VVVV18, L.VVVV22 ],
              couplings = {(0,1):C.GC_142,(0,2):C.GC_21,(0,0):C.GC_17})

V_27 = Vertex(name = 'V_27',
              particles = [ P.W__minus__, P.W__plus__, P.Z, P.Z ],
              color = [ '1' ],
              lorentz = [ L.VVVV11 ],
              couplings = {(0,0):C.GC_19})

V_28 = Vertex(name = 'V_28',
              particles = [ P.e__plus__, P.e__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_4,(0,1):C.GC_385,(0,2):C.GC_603})

V_29 = Vertex(name = 'V_29',
              particles = [ P.mu__plus__, P.e__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_386,(0,1):C.GC_604})

V_30 = Vertex(name = 'V_30',
              particles = [ P.ta__plus__, P.e__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_387,(0,1):C.GC_605})

V_31 = Vertex(name = 'V_31',
              particles = [ P.e__plus__, P.mu__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_388,(0,1):C.GC_606})

V_32 = Vertex(name = 'V_32',
              particles = [ P.mu__plus__, P.mu__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_4,(0,1):C.GC_389,(0,2):C.GC_607})

V_33 = Vertex(name = 'V_33',
              particles = [ P.ta__plus__, P.mu__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_390,(0,1):C.GC_608})

V_34 = Vertex(name = 'V_34',
              particles = [ P.e__plus__, P.ta__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_391,(0,1):C.GC_609})

V_35 = Vertex(name = 'V_35',
              particles = [ P.mu__plus__, P.ta__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_392,(0,1):C.GC_610})

V_36 = Vertex(name = 'V_36',
              particles = [ P.ta__plus__, P.ta__minus__, P.a ],
              color = [ '1' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_4,(0,1):C.GC_393,(0,2):C.GC_611})

V_37 = Vertex(name = 'V_37',
              particles = [ P.u__tilde__, P.u, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_3,(0,1):C.GC_394,(0,2):C.GC_612})

V_38 = Vertex(name = 'V_38',
              particles = [ P.c__tilde__, P.u, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_395,(0,1):C.GC_613})

V_39 = Vertex(name = 'V_39',
              particles = [ P.t__tilde__, P.u, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_396,(0,1):C.GC_614})

V_40 = Vertex(name = 'V_40',
              particles = [ P.u__tilde__, P.c, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_397,(0,1):C.GC_615})

V_41 = Vertex(name = 'V_41',
              particles = [ P.c__tilde__, P.c, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_3,(0,1):C.GC_398,(0,2):C.GC_616})

V_42 = Vertex(name = 'V_42',
              particles = [ P.t__tilde__, P.c, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_399,(0,1):C.GC_617})

V_43 = Vertex(name = 'V_43',
              particles = [ P.u__tilde__, P.t, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_400,(0,1):C.GC_618})

V_44 = Vertex(name = 'V_44',
              particles = [ P.c__tilde__, P.t, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_401,(0,1):C.GC_619})

V_45 = Vertex(name = 'V_45',
              particles = [ P.t__tilde__, P.t, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_3,(0,1):C.GC_402,(0,2):C.GC_620})

V_46 = Vertex(name = 'V_46',
              particles = [ P.d__tilde__, P.d, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_2,(0,1):C.GC_376,(0,2):C.GC_594})

V_47 = Vertex(name = 'V_47',
              particles = [ P.s__tilde__, P.d, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_377,(0,1):C.GC_595})

V_48 = Vertex(name = 'V_48',
              particles = [ P.b__tilde__, P.d, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_378,(0,1):C.GC_596})

V_49 = Vertex(name = 'V_49',
              particles = [ P.d__tilde__, P.s, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_379,(0,1):C.GC_597})

V_50 = Vertex(name = 'V_50',
              particles = [ P.s__tilde__, P.s, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_2,(0,1):C.GC_380,(0,2):C.GC_598})

V_51 = Vertex(name = 'V_51',
              particles = [ P.b__tilde__, P.s, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_381,(0,1):C.GC_599})

V_52 = Vertex(name = 'V_52',
              particles = [ P.d__tilde__, P.b, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_382,(0,1):C.GC_600})

V_53 = Vertex(name = 'V_53',
              particles = [ P.s__tilde__, P.b, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_383,(0,1):C.GC_601})

V_54 = Vertex(name = 'V_54',
              particles = [ P.b__tilde__, P.b, P.a ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_2,(0,1):C.GC_384,(0,2):C.GC_602})

V_55 = Vertex(name = 'V_55',
              particles = [ P.u__tilde__, P.u, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_9,(0,1):C.GC_413,(0,2):C.GC_639})

V_56 = Vertex(name = 'V_56',
              particles = [ P.c__tilde__, P.u, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_414,(0,1):C.GC_641})

V_57 = Vertex(name = 'V_57',
              particles = [ P.t__tilde__, P.u, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_415,(0,1):C.GC_643})

V_58 = Vertex(name = 'V_58',
              particles = [ P.u__tilde__, P.c, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_416,(0,1):C.GC_645})

V_59 = Vertex(name = 'V_59',
              particles = [ P.c__tilde__, P.c, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_9,(0,1):C.GC_417,(0,2):C.GC_647})

V_60 = Vertex(name = 'V_60',
              particles = [ P.t__tilde__, P.c, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_418,(0,1):C.GC_649})

V_61 = Vertex(name = 'V_61',
              particles = [ P.u__tilde__, P.t, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_419,(0,1):C.GC_651})

V_62 = Vertex(name = 'V_62',
              particles = [ P.c__tilde__, P.t, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_420,(0,1):C.GC_653})

V_63 = Vertex(name = 'V_63',
              particles = [ P.t__tilde__, P.t, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_9,(0,1):C.GC_421,(0,2):C.GC_655})

V_64 = Vertex(name = 'V_64',
              particles = [ P.d__tilde__, P.d, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_9,(0,1):C.GC_404,(0,2):C.GC_621})

V_65 = Vertex(name = 'V_65',
              particles = [ P.s__tilde__, P.d, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_405,(0,1):C.GC_623})

V_66 = Vertex(name = 'V_66',
              particles = [ P.b__tilde__, P.d, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_406,(0,1):C.GC_625})

V_67 = Vertex(name = 'V_67',
              particles = [ P.d__tilde__, P.s, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_407,(0,1):C.GC_627})

V_68 = Vertex(name = 'V_68',
              particles = [ P.s__tilde__, P.s, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_9,(0,1):C.GC_408,(0,2):C.GC_629})

V_69 = Vertex(name = 'V_69',
              particles = [ P.b__tilde__, P.s, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_409,(0,1):C.GC_631})

V_70 = Vertex(name = 'V_70',
              particles = [ P.d__tilde__, P.b, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_410,(0,1):C.GC_633})

V_71 = Vertex(name = 'V_71',
              particles = [ P.s__tilde__, P.b, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_411,(0,1):C.GC_635})

V_72 = Vertex(name = 'V_72',
              particles = [ P.b__tilde__, P.b, P.g ],
              color = [ 'T(3,2,1)' ],
              lorentz = [ L.FFV1, L.FFV7, L.FFV9 ],
              couplings = {(0,0):C.GC_9,(0,1):C.GC_412,(0,2):C.GC_637})

V_73 = Vertex(name = 'V_73',
              particles = [ P.d__tilde__, P.u, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_24,(0,1):C.GC_55,(0,2):C.GC_799})

V_74 = Vertex(name = 'V_74',
              particles = [ P.d__tilde__, P.u, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_46})

V_75 = Vertex(name = 'V_75',
              particles = [ P.s__tilde__, P.u, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_25,(0,1):C.GC_56,(0,2):C.GC_802})

V_76 = Vertex(name = 'V_76',
              particles = [ P.s__tilde__, P.u, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_47})

V_77 = Vertex(name = 'V_77',
              particles = [ P.b__tilde__, P.u, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_26,(0,1):C.GC_57,(0,2):C.GC_805})

V_78 = Vertex(name = 'V_78',
              particles = [ P.b__tilde__, P.u, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_48})

V_79 = Vertex(name = 'V_79',
              particles = [ P.d__tilde__, P.c, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_27,(0,1):C.GC_58,(0,2):C.GC_800})

V_80 = Vertex(name = 'V_80',
              particles = [ P.d__tilde__, P.c, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_49})

V_81 = Vertex(name = 'V_81',
              particles = [ P.s__tilde__, P.c, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_28,(0,1):C.GC_59,(0,2):C.GC_803})

V_82 = Vertex(name = 'V_82',
              particles = [ P.s__tilde__, P.c, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_50})

V_83 = Vertex(name = 'V_83',
              particles = [ P.b__tilde__, P.c, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_29,(0,1):C.GC_60,(0,2):C.GC_806})

V_84 = Vertex(name = 'V_84',
              particles = [ P.b__tilde__, P.c, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_51})

V_85 = Vertex(name = 'V_85',
              particles = [ P.d__tilde__, P.t, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_30,(0,1):C.GC_61,(0,2):C.GC_801})

V_86 = Vertex(name = 'V_86',
              particles = [ P.d__tilde__, P.t, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_52})

V_87 = Vertex(name = 'V_87',
              particles = [ P.s__tilde__, P.t, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_31,(0,1):C.GC_62,(0,2):C.GC_804})

V_88 = Vertex(name = 'V_88',
              particles = [ P.s__tilde__, P.t, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_53})

V_89 = Vertex(name = 'V_89',
              particles = [ P.b__tilde__, P.t, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_32,(0,1):C.GC_63,(0,2):C.GC_807})

V_90 = Vertex(name = 'V_90',
              particles = [ P.b__tilde__, P.t, P.W__minus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_54})

V_91 = Vertex(name = 'V_91',
              particles = [ P.u__tilde__, P.d, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_691,(0,1):C.GC_745,(0,2):C.GC_781})

V_92 = Vertex(name = 'V_92',
              particles = [ P.u__tilde__, P.d, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_727})

V_93 = Vertex(name = 'V_93',
              particles = [ P.c__tilde__, P.d, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_694,(0,1):C.GC_748,(0,2):C.GC_784})

V_94 = Vertex(name = 'V_94',
              particles = [ P.c__tilde__, P.d, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_730})

V_95 = Vertex(name = 'V_95',
              particles = [ P.t__tilde__, P.d, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_697,(0,1):C.GC_751,(0,2):C.GC_787})

V_96 = Vertex(name = 'V_96',
              particles = [ P.t__tilde__, P.d, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_733})

V_97 = Vertex(name = 'V_97',
              particles = [ P.u__tilde__, P.s, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_692,(0,1):C.GC_746,(0,2):C.GC_782})

V_98 = Vertex(name = 'V_98',
              particles = [ P.u__tilde__, P.s, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2 ],
              couplings = {(0,0):C.GC_728})

V_99 = Vertex(name = 'V_99',
              particles = [ P.c__tilde__, P.s, P.W__plus__ ],
              color = [ 'Identity(1,2)' ],
              lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
              couplings = {(0,0):C.GC_695,(0,1):C.GC_749,(0,2):C.GC_785})

V_100 = Vertex(name = 'V_100',
               particles = [ P.c__tilde__, P.s, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_731})

V_101 = Vertex(name = 'V_101',
               particles = [ P.t__tilde__, P.s, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
               couplings = {(0,0):C.GC_698,(0,1):C.GC_752,(0,2):C.GC_788})

V_102 = Vertex(name = 'V_102',
               particles = [ P.t__tilde__, P.s, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_734})

V_103 = Vertex(name = 'V_103',
               particles = [ P.u__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
               couplings = {(0,0):C.GC_693,(0,1):C.GC_747,(0,2):C.GC_783})

V_104 = Vertex(name = 'V_104',
               particles = [ P.u__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_729})

V_105 = Vertex(name = 'V_105',
               particles = [ P.c__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
               couplings = {(0,0):C.GC_696,(0,1):C.GC_750,(0,2):C.GC_786})

V_106 = Vertex(name = 'V_106',
               particles = [ P.c__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_732})

V_107 = Vertex(name = 'V_107',
               particles = [ P.t__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV8 ],
               couplings = {(0,0):C.GC_699,(0,1):C.GC_753,(0,2):C.GC_789})

V_108 = Vertex(name = 'V_108',
               particles = [ P.t__tilde__, P.b, P.W__plus__ ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_735})

V_109 = Vertex(name = 'V_109',
               particles = [ P.e__plus__, P.ve, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_23,(0,1):C.GC_487})

V_110 = Vertex(name = 'V_110',
               particles = [ P.e__plus__, P.ve, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_37})

V_111 = Vertex(name = 'V_111',
               particles = [ P.mu__plus__, P.ve, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_38,(0,1):C.GC_488})

V_112 = Vertex(name = 'V_112',
               particles = [ P.ta__plus__, P.ve, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_39,(0,1):C.GC_489})

V_113 = Vertex(name = 'V_113',
               particles = [ P.e__plus__, P.vm, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_40,(0,1):C.GC_490})

V_114 = Vertex(name = 'V_114',
               particles = [ P.mu__plus__, P.vm, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_23,(0,1):C.GC_491})

V_115 = Vertex(name = 'V_115',
               particles = [ P.mu__plus__, P.vm, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_41})

V_116 = Vertex(name = 'V_116',
               particles = [ P.ta__plus__, P.vm, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_42,(0,1):C.GC_492})

V_117 = Vertex(name = 'V_117',
               particles = [ P.e__plus__, P.vt, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_43,(0,1):C.GC_493})

V_118 = Vertex(name = 'V_118',
               particles = [ P.mu__plus__, P.vt, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_44,(0,1):C.GC_494})

V_119 = Vertex(name = 'V_119',
               particles = [ P.ta__plus__, P.vt, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_23,(0,1):C.GC_495})

V_120 = Vertex(name = 'V_120',
               particles = [ P.ta__plus__, P.vt, P.W__minus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_45})

V_121 = Vertex(name = 'V_121',
               particles = [ P.ve__tilde__, P.e__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_23,(0,1):C.GC_790})

V_122 = Vertex(name = 'V_122',
               particles = [ P.ve__tilde__, P.e__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_718})

V_123 = Vertex(name = 'V_123',
               particles = [ P.vm__tilde__, P.e__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_721,(0,1):C.GC_793})

V_124 = Vertex(name = 'V_124',
               particles = [ P.vt__tilde__, P.e__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_724,(0,1):C.GC_796})

V_125 = Vertex(name = 'V_125',
               particles = [ P.ve__tilde__, P.mu__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_719,(0,1):C.GC_791})

V_126 = Vertex(name = 'V_126',
               particles = [ P.vm__tilde__, P.mu__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_23,(0,1):C.GC_794})

V_127 = Vertex(name = 'V_127',
               particles = [ P.vm__tilde__, P.mu__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_722})

V_128 = Vertex(name = 'V_128',
               particles = [ P.vt__tilde__, P.mu__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_725,(0,1):C.GC_797})

V_129 = Vertex(name = 'V_129',
               particles = [ P.ve__tilde__, P.ta__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_720,(0,1):C.GC_792})

V_130 = Vertex(name = 'V_130',
               particles = [ P.vm__tilde__, P.ta__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_723,(0,1):C.GC_795})

V_131 = Vertex(name = 'V_131',
               particles = [ P.vt__tilde__, P.ta__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV8 ],
               couplings = {(0,0):C.GC_23,(0,1):C.GC_798})

V_132 = Vertex(name = 'V_132',
               particles = [ P.vt__tilde__, P.ta__minus__, P.W__plus__ ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_726})

V_133 = Vertex(name = 'V_133',
               particles = [ P.u__tilde__, P.u, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV6, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_34,(0,2):C.GC_70,(0,1):C.GC_127,(0,3):C.GC_579,(0,4):C.GC_675})

V_134 = Vertex(name = 'V_134',
               particles = [ P.u__tilde__, P.u, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_91})

V_135 = Vertex(name = 'V_135',
               particles = [ P.c__tilde__, P.u, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_92,(0,1):C.GC_128,(0,2):C.GC_580,(0,3):C.GC_676})

V_136 = Vertex(name = 'V_136',
               particles = [ P.t__tilde__, P.u, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_93,(0,1):C.GC_129,(0,2):C.GC_581,(0,3):C.GC_677})

V_137 = Vertex(name = 'V_137',
               particles = [ P.u__tilde__, P.c, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_94,(0,1):C.GC_130,(0,2):C.GC_582,(0,3):C.GC_678})

V_138 = Vertex(name = 'V_138',
               particles = [ P.c__tilde__, P.c, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV6, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_34,(0,2):C.GC_70,(0,1):C.GC_131,(0,3):C.GC_583,(0,4):C.GC_679})

V_139 = Vertex(name = 'V_139',
               particles = [ P.c__tilde__, P.c, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_95})

V_140 = Vertex(name = 'V_140',
               particles = [ P.t__tilde__, P.c, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_96,(0,1):C.GC_132,(0,2):C.GC_584,(0,3):C.GC_680})

V_141 = Vertex(name = 'V_141',
               particles = [ P.u__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_97,(0,1):C.GC_133,(0,2):C.GC_585,(0,3):C.GC_681})

V_142 = Vertex(name = 'V_142',
               particles = [ P.c__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_98,(0,1):C.GC_134,(0,2):C.GC_586,(0,3):C.GC_682})

V_143 = Vertex(name = 'V_143',
               particles = [ P.t__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV6, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_34,(0,2):C.GC_70,(0,1):C.GC_135,(0,3):C.GC_587,(0,4):C.GC_683})

V_144 = Vertex(name = 'V_144',
               particles = [ P.t__tilde__, P.t, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_99})

V_145 = Vertex(name = 'V_145',
               particles = [ P.d__tilde__, P.d, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV4, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_33,(0,2):C.GC_70,(0,1):C.GC_109,(0,3):C.GC_561,(0,4):C.GC_657})

V_146 = Vertex(name = 'V_146',
               particles = [ P.d__tilde__, P.d, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_73})

V_147 = Vertex(name = 'V_147',
               particles = [ P.s__tilde__, P.d, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_74,(0,1):C.GC_110,(0,2):C.GC_562,(0,3):C.GC_658})

V_148 = Vertex(name = 'V_148',
               particles = [ P.b__tilde__, P.d, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_75,(0,1):C.GC_111,(0,2):C.GC_563,(0,3):C.GC_659})

V_149 = Vertex(name = 'V_149',
               particles = [ P.d__tilde__, P.s, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_76,(0,1):C.GC_112,(0,2):C.GC_564,(0,3):C.GC_660})

V_150 = Vertex(name = 'V_150',
               particles = [ P.s__tilde__, P.s, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV4, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_33,(0,2):C.GC_70,(0,1):C.GC_113,(0,3):C.GC_565,(0,4):C.GC_661})

V_151 = Vertex(name = 'V_151',
               particles = [ P.s__tilde__, P.s, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_77})

V_152 = Vertex(name = 'V_152',
               particles = [ P.b__tilde__, P.s, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_78,(0,1):C.GC_114,(0,2):C.GC_566,(0,3):C.GC_662})

V_153 = Vertex(name = 'V_153',
               particles = [ P.d__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_79,(0,1):C.GC_115,(0,2):C.GC_567,(0,3):C.GC_663})

V_154 = Vertex(name = 'V_154',
               particles = [ P.s__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_80,(0,1):C.GC_116,(0,2):C.GC_568,(0,3):C.GC_664})

V_155 = Vertex(name = 'V_155',
               particles = [ P.b__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV4, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_33,(0,2):C.GC_70,(0,1):C.GC_117,(0,3):C.GC_569,(0,4):C.GC_665})

V_156 = Vertex(name = 'V_156',
               particles = [ P.b__tilde__, P.b, P.Z ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_81})

V_157 = Vertex(name = 'V_157',
               particles = [ P.ve__tilde__, P.ve, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_72})

V_158 = Vertex(name = 'V_158',
               particles = [ P.ve__tilde__, P.ve, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_100})

V_159 = Vertex(name = 'V_159',
               particles = [ P.vm__tilde__, P.ve, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_101})

V_160 = Vertex(name = 'V_160',
               particles = [ P.vt__tilde__, P.ve, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_102})

V_161 = Vertex(name = 'V_161',
               particles = [ P.ve__tilde__, P.vm, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_103})

V_162 = Vertex(name = 'V_162',
               particles = [ P.vm__tilde__, P.vm, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_72})

V_163 = Vertex(name = 'V_163',
               particles = [ P.vm__tilde__, P.vm, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_104})

V_164 = Vertex(name = 'V_164',
               particles = [ P.vt__tilde__, P.vm, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_105})

V_165 = Vertex(name = 'V_165',
               particles = [ P.ve__tilde__, P.vt, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_106})

V_166 = Vertex(name = 'V_166',
               particles = [ P.vm__tilde__, P.vt, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_107})

V_167 = Vertex(name = 'V_167',
               particles = [ P.vt__tilde__, P.vt, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_72})

V_168 = Vertex(name = 'V_168',
               particles = [ P.vt__tilde__, P.vt, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_108})

V_169 = Vertex(name = 'V_169',
               particles = [ P.e__plus__, P.e__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV5, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_33,(0,2):C.GC_71,(0,1):C.GC_118,(0,3):C.GC_570,(0,4):C.GC_666})

V_170 = Vertex(name = 'V_170',
               particles = [ P.e__plus__, P.e__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_82})

V_171 = Vertex(name = 'V_171',
               particles = [ P.mu__plus__, P.e__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_83,(0,1):C.GC_119,(0,2):C.GC_571,(0,3):C.GC_667})

V_172 = Vertex(name = 'V_172',
               particles = [ P.ta__plus__, P.e__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_84,(0,1):C.GC_120,(0,2):C.GC_572,(0,3):C.GC_668})

V_173 = Vertex(name = 'V_173',
               particles = [ P.e__plus__, P.mu__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_85,(0,1):C.GC_121,(0,2):C.GC_573,(0,3):C.GC_669})

V_174 = Vertex(name = 'V_174',
               particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV5, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_33,(0,2):C.GC_71,(0,1):C.GC_122,(0,3):C.GC_574,(0,4):C.GC_670})

V_175 = Vertex(name = 'V_175',
               particles = [ P.mu__plus__, P.mu__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_86})

V_176 = Vertex(name = 'V_176',
               particles = [ P.ta__plus__, P.mu__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_87,(0,1):C.GC_123,(0,2):C.GC_575,(0,3):C.GC_671})

V_177 = Vertex(name = 'V_177',
               particles = [ P.e__plus__, P.ta__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_88,(0,1):C.GC_124,(0,2):C.GC_576,(0,3):C.GC_672})

V_178 = Vertex(name = 'V_178',
               particles = [ P.mu__plus__, P.ta__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_89,(0,1):C.GC_125,(0,2):C.GC_577,(0,3):C.GC_673})

V_179 = Vertex(name = 'V_179',
               particles = [ P.ta__plus__, P.ta__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2, L.FFV3, L.FFV5, L.FFV7, L.FFV9 ],
               couplings = {(0,0):C.GC_33,(0,2):C.GC_71,(0,1):C.GC_126,(0,3):C.GC_578,(0,4):C.GC_674})

V_180 = Vertex(name = 'V_180',
               particles = [ P.ta__plus__, P.ta__minus__, P.Z ],
               color = [ '1' ],
               lorentz = [ L.FFV2 ],
               couplings = {(0,0):C.GC_90})

V_181 = Vertex(name = 'V_181',
               particles = [ P.d__tilde__, P.d, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_622,(0,0):C.GC_423})

V_182 = Vertex(name = 'V_182',
               particles = [ P.s__tilde__, P.d, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_624,(0,0):C.GC_424})

V_183 = Vertex(name = 'V_183',
               particles = [ P.b__tilde__, P.d, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_626,(0,0):C.GC_425})

V_184 = Vertex(name = 'V_184',
               particles = [ P.d__tilde__, P.s, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_628,(0,0):C.GC_426})

V_185 = Vertex(name = 'V_185',
               particles = [ P.s__tilde__, P.s, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_630,(0,0):C.GC_427})

V_186 = Vertex(name = 'V_186',
               particles = [ P.b__tilde__, P.s, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_632,(0,0):C.GC_428})

V_187 = Vertex(name = 'V_187',
               particles = [ P.d__tilde__, P.b, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_634,(0,0):C.GC_429})

V_188 = Vertex(name = 'V_188',
               particles = [ P.s__tilde__, P.b, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_636,(0,0):C.GC_430})

V_189 = Vertex(name = 'V_189',
               particles = [ P.b__tilde__, P.b, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_638,(0,0):C.GC_431})

V_190 = Vertex(name = 'V_190',
               particles = [ P.u__tilde__, P.u, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_640,(0,0):C.GC_432})

V_191 = Vertex(name = 'V_191',
               particles = [ P.c__tilde__, P.u, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_642,(0,0):C.GC_433})

V_192 = Vertex(name = 'V_192',
               particles = [ P.t__tilde__, P.u, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_644,(0,0):C.GC_434})

V_193 = Vertex(name = 'V_193',
               particles = [ P.u__tilde__, P.c, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_646,(0,0):C.GC_435})

V_194 = Vertex(name = 'V_194',
               particles = [ P.c__tilde__, P.c, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_648,(0,0):C.GC_436})

V_195 = Vertex(name = 'V_195',
               particles = [ P.t__tilde__, P.c, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_650,(0,0):C.GC_437})

V_196 = Vertex(name = 'V_196',
               particles = [ P.u__tilde__, P.t, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_652,(0,0):C.GC_438})

V_197 = Vertex(name = 'V_197',
               particles = [ P.c__tilde__, P.t, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_654,(0,0):C.GC_439})

V_198 = Vertex(name = 'V_198',
               particles = [ P.t__tilde__, P.t, P.g, P.g ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVV1, L.FFVV2 ],
               couplings = {(0,1):C.GC_656,(0,0):C.GC_440})

V_199 = Vertex(name = 'V_199',
               particles = [ P.g, P.g, P.g, P.g, P.g ],
               color = [ 'f(-2,1,2)*f(-1,-2,3)*f(4,5,-1)', 'f(-2,1,2)*f(-1,-2,4)*f(3,5,-1)', 'f(-2,1,2)*f(-1,-2,5)*f(3,4,-1)', 'f(-2,1,3)*f(-1,-2,2)*f(4,5,-1)', 'f(-2,1,3)*f(-1,-2,4)*f(2,5,-1)', 'f(-2,1,3)*f(-1,-2,5)*f(2,4,-1)', 'f(-2,1,4)*f(-1,-2,2)*f(3,5,-1)', 'f(-2,1,4)*f(-1,-2,3)*f(2,5,-1)', 'f(-2,1,4)*f(-1,-2,5)*f(2,3,-1)', 'f(-2,1,5)*f(-1,-2,2)*f(3,4,-1)', 'f(-2,1,5)*f(-1,-2,3)*f(2,4,-1)', 'f(-2,1,5)*f(-1,-2,4)*f(2,3,-1)', 'f(-2,2,3)*f(-1,-2,1)*f(4,5,-1)', 'f(-2,2,3)*f(-1,-2,4)*f(1,5,-1)', 'f(-2,2,3)*f(-1,-2,5)*f(1,4,-1)', 'f(-2,2,4)*f(-1,-2,1)*f(3,5,-1)', 'f(-2,2,4)*f(-1,-2,3)*f(1,5,-1)', 'f(-2,2,4)*f(-1,-2,5)*f(1,3,-1)', 'f(-2,2,5)*f(-1,-2,1)*f(3,4,-1)', 'f(-2,2,5)*f(-1,-2,3)*f(1,4,-1)', 'f(-2,2,5)*f(-1,-2,4)*f(1,3,-1)', 'f(-2,3,4)*f(-1,-2,1)*f(2,5,-1)', 'f(-2,3,4)*f(-1,-2,2)*f(1,5,-1)', 'f(-2,3,4)*f(-1,-2,5)*f(1,2,-1)', 'f(-2,3,5)*f(-1,-2,1)*f(2,4,-1)', 'f(-2,3,5)*f(-1,-2,2)*f(1,4,-1)', 'f(-2,3,5)*f(-1,-2,4)*f(1,2,-1)', 'f(-2,4,5)*f(-1,-2,1)*f(2,3,-1)', 'f(-2,4,5)*f(-1,-2,2)*f(1,3,-1)', 'f(-2,4,5)*f(-1,-2,3)*f(1,2,-1)' ],
               lorentz = [ L.VVVVV1, L.VVVVV10, L.VVVVV11, L.VVVVV12, L.VVVVV13, L.VVVVV14, L.VVVVV15, L.VVVVV16, L.VVVVV17, L.VVVVV18, L.VVVVV19, L.VVVVV2, L.VVVVV20, L.VVVVV21, L.VVVVV22, L.VVVVV23, L.VVVVV24, L.VVVVV25, L.VVVVV26, L.VVVVV27, L.VVVVV28, L.VVVVV29, L.VVVVV3, L.VVVVV30, L.VVVVV31, L.VVVVV32, L.VVVVV33, L.VVVVV34, L.VVVVV35, L.VVVVV36, L.VVVVV37, L.VVVVV38, L.VVVVV39, L.VVVVV4, L.VVVVV40, L.VVVVV41, L.VVVVV42, L.VVVVV43, L.VVVVV44, L.VVVVV45, L.VVVVV46, L.VVVVV47, L.VVVVV48, L.VVVVV49, L.VVVVV5, L.VVVVV50, L.VVVVV51, L.VVVVV52, L.VVVVV53, L.VVVVV54, L.VVVVV55, L.VVVVV56, L.VVVVV57, L.VVVVV58, L.VVVVV59, L.VVVVV6, L.VVVVV60, L.VVVVV61, L.VVVVV62, L.VVVVV63, L.VVVVV64, L.VVVVV65, L.VVVVV66, L.VVVVV67, L.VVVVV68, L.VVVVV69, L.VVVVV7, L.VVVVV70, L.VVVVV71, L.VVVVV72, L.VVVVV73, L.VVVVV74, L.VVVVV75, L.VVVVV8, L.VVVVV9 ],
               couplings = {(27,74):C.GC_272,(27,1):C.GC_277,(24,23):C.GC_273,(24,20):C.GC_278,(21,26):C.GC_272,(21,28):C.GC_277,(18,25):C.GC_273,(18,27):C.GC_278,(15,24):C.GC_272,(15,21):C.GC_277,(12,73):C.GC_272,(12,66):C.GC_277,(28,4):C.GC_272,(28,2):C.GC_277,(25,34):C.GC_273,(25,36):C.GC_278,(22,32):C.GC_272,(22,30):C.GC_277,(9,35):C.GC_272,(9,37):C.GC_277,(6,31):C.GC_273,(6,29):C.GC_278,(3,5):C.GC_273,(3,3):C.GC_278,(29,0):C.GC_273,(29,11):C.GC_278,(19,42):C.GC_272,(19,45):C.GC_277,(16,41):C.GC_273,(16,39):C.GC_278,(10,40):C.GC_272,(10,38):C.GC_277,(7,43):C.GC_273,(7,46):C.GC_278,(0,6):C.GC_272,(0,7):C.GC_277,(26,22):C.GC_272,(26,33):C.GC_277,(20,9):C.GC_273,(20,8):C.GC_278,(13,51):C.GC_272,(13,52):C.GC_277,(11,10):C.GC_273,(11,12):C.GC_278,(4,48):C.GC_272,(4,47):C.GC_277,(1,50):C.GC_272,(1,49):C.GC_277,(23,55):C.GC_273,(23,44):C.GC_278,(17,15):C.GC_272,(17,16):C.GC_277,(14,54):C.GC_273,(14,53):C.GC_278,(8,14):C.GC_272,(8,13):C.GC_277,(5,58):C.GC_273,(5,59):C.GC_278,(2,56):C.GC_273,(2,57):C.GC_278,(24,60):C.GC_218,(21,61):C.GC_217,(18,61):C.GC_218,(15,60):C.GC_217,(28,18):C.GC_218,(22,65):C.GC_218,(9,65):C.GC_217,(3,18):C.GC_217,(29,19):C.GC_218,(16,67):C.GC_218,(10,67):C.GC_217,(0,19):C.GC_217,(26,70):C.GC_217,(20,69):C.GC_217,(4,69):C.GC_218,(1,70):C.GC_218,(25,64):C.GC_218,(6,64):C.GC_217,(19,68):C.GC_218,(7,68):C.GC_217,(23,72):C.GC_217,(17,71):C.GC_217,(5,71):C.GC_218,(2,72):C.GC_218,(27,17):C.GC_218,(12,17):C.GC_217,(13,62):C.GC_218,(11,62):C.GC_217,(14,63):C.GC_217,(8,63):C.GC_218})

V_200 = Vertex(name = 'V_200',
               particles = [ P.g, P.g, P.g, P.g, P.g ],
               color = [ 'f(-2,1,2)*f(-1,-2,3)*f(4,5,-1)', 'f(-2,1,2)*f(-1,-2,4)*f(3,5,-1)', 'f(-2,1,2)*f(-1,-2,5)*f(3,4,-1)', 'f(-2,1,3)*f(-1,-2,2)*f(4,5,-1)', 'f(-2,1,3)*f(-1,-2,4)*f(2,5,-1)', 'f(-2,1,3)*f(-1,-2,5)*f(2,4,-1)', 'f(-2,1,4)*f(-1,-2,2)*f(3,5,-1)', 'f(-2,1,4)*f(-1,-2,3)*f(2,5,-1)', 'f(-2,1,4)*f(-1,-2,5)*f(2,3,-1)', 'f(-2,1,5)*f(-1,-2,2)*f(3,4,-1)', 'f(-2,1,5)*f(-1,-2,3)*f(2,4,-1)', 'f(-2,1,5)*f(-1,-2,4)*f(2,3,-1)', 'f(-2,2,3)*f(-1,-2,1)*f(4,5,-1)', 'f(-2,2,3)*f(-1,-2,4)*f(1,5,-1)', 'f(-2,2,3)*f(-1,-2,5)*f(1,4,-1)', 'f(-2,2,4)*f(-1,-2,1)*f(3,5,-1)', 'f(-2,2,4)*f(-1,-2,3)*f(1,5,-1)', 'f(-2,2,4)*f(-1,-2,5)*f(1,3,-1)', 'f(-2,2,5)*f(-1,-2,1)*f(3,4,-1)', 'f(-2,2,5)*f(-1,-2,3)*f(1,4,-1)', 'f(-2,2,5)*f(-1,-2,4)*f(1,3,-1)', 'f(-2,3,4)*f(-1,-2,1)*f(2,5,-1)', 'f(-2,3,4)*f(-1,-2,2)*f(1,5,-1)', 'f(-2,3,4)*f(-1,-2,5)*f(1,2,-1)', 'f(-2,3,5)*f(-1,-2,1)*f(2,4,-1)', 'f(-2,3,5)*f(-1,-2,2)*f(1,4,-1)', 'f(-2,3,5)*f(-1,-2,4)*f(1,2,-1)', 'f(-2,4,5)*f(-1,-2,1)*f(2,3,-1)', 'f(-2,4,5)*f(-1,-2,2)*f(1,3,-1)', 'f(-2,4,5)*f(-1,-2,3)*f(1,2,-1)' ],
               lorentz = [ L.VVVVV25, L.VVVVV26, L.VVVVV27, L.VVVVV64, L.VVVVV65, L.VVVVV66, L.VVVVV67, L.VVVVV68, L.VVVVV69, L.VVVVV70, L.VVVVV71, L.VVVVV72, L.VVVVV73, L.VVVVV74, L.VVVVV75 ],
               couplings = {(24,3):C.GC_221,(21,4):C.GC_220,(18,4):C.GC_221,(15,3):C.GC_220,(28,1):C.GC_221,(22,8):C.GC_221,(9,8):C.GC_220,(3,1):C.GC_220,(29,2):C.GC_221,(16,9):C.GC_221,(10,9):C.GC_220,(0,2):C.GC_220,(26,12):C.GC_220,(20,11):C.GC_220,(4,11):C.GC_221,(1,12):C.GC_221,(25,7):C.GC_221,(6,7):C.GC_220,(19,10):C.GC_221,(7,10):C.GC_220,(23,14):C.GC_220,(17,13):C.GC_220,(5,13):C.GC_221,(2,14):C.GC_221,(27,0):C.GC_221,(12,0):C.GC_220,(13,5):C.GC_221,(11,5):C.GC_220,(14,6):C.GC_220,(8,6):C.GC_221})

V_201 = Vertex(name = 'V_201',
               particles = [ P.g, P.g, P.g, P.g, P.g, P.g ],
               color = [ 'f(-3,1,2)*f(-2,3,4)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,1,2)*f(-2,3,5)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,1,2)*f(-2,3,6)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,1,2)*f(-2,4,5)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,1,2)*f(-2,4,6)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,1,2)*f(-2,5,6)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,1,3)*f(-2,2,4)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,1,3)*f(-2,2,5)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,1,3)*f(-2,2,6)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,1,3)*f(-2,4,5)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,1,3)*f(-2,4,6)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,1,3)*f(-2,5,6)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,1,4)*f(-2,2,3)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,1,4)*f(-2,2,5)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,1,4)*f(-2,2,6)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,1,4)*f(-2,3,5)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,1,4)*f(-2,3,6)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,1,4)*f(-2,5,6)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,1,5)*f(-2,2,3)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,1,5)*f(-2,2,4)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,1,5)*f(-2,2,6)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,1,5)*f(-2,3,4)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,1,5)*f(-2,3,6)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,1,5)*f(-2,4,6)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,1,6)*f(-2,2,3)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,1,6)*f(-2,2,4)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,1,6)*f(-2,2,5)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,1,6)*f(-2,3,4)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,1,6)*f(-2,3,5)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,1,6)*f(-2,4,5)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,2,3)*f(-2,1,4)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,2,3)*f(-2,1,5)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,2,3)*f(-2,1,6)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,2,3)*f(-2,4,5)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,2,3)*f(-2,4,6)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,2,3)*f(-2,5,6)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,2,4)*f(-2,1,3)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,2,4)*f(-2,1,5)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,2,4)*f(-2,1,6)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,2,4)*f(-2,3,5)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,2,4)*f(-2,3,6)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,2,4)*f(-2,5,6)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,2,5)*f(-2,1,3)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,2,5)*f(-2,1,4)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,2,5)*f(-2,1,6)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,2,5)*f(-2,3,4)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,2,5)*f(-2,3,6)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,2,5)*f(-2,4,6)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,2,6)*f(-2,1,3)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,2,6)*f(-2,1,4)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,2,6)*f(-2,1,5)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,2,6)*f(-2,3,4)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,2,6)*f(-2,3,5)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,2,6)*f(-2,4,5)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,3,4)*f(-2,1,2)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,3,4)*f(-2,1,5)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,3,4)*f(-2,1,6)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,3,4)*f(-2,2,5)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,3,4)*f(-2,2,6)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,3,4)*f(-2,5,6)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,3,5)*f(-2,1,2)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,3,5)*f(-2,1,4)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,3,5)*f(-2,1,6)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,3,5)*f(-2,2,4)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,3,5)*f(-2,2,6)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,3,5)*f(-2,4,6)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,3,6)*f(-2,1,2)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,3,6)*f(-2,1,4)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,3,6)*f(-2,1,5)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,3,6)*f(-2,2,4)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,3,6)*f(-2,2,5)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,3,6)*f(-2,4,5)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,4,5)*f(-2,1,2)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,4,5)*f(-2,1,3)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,4,5)*f(-2,1,6)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,4,5)*f(-2,2,3)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,4,5)*f(-2,2,6)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,4,5)*f(-2,3,6)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,4,6)*f(-2,1,2)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,4,6)*f(-2,1,3)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,4,6)*f(-2,1,5)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,4,6)*f(-2,2,3)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,4,6)*f(-2,2,5)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,4,6)*f(-2,3,5)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,5,6)*f(-2,1,2)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,5,6)*f(-2,1,3)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,5,6)*f(-2,1,4)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,5,6)*f(-2,2,3)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,5,6)*f(-2,2,4)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,5,6)*f(-2,3,4)*f(-1,-2,-3)*f(1,2,-1)' ],
               lorentz = [ L.VVVVVV1, L.VVVVVV10, L.VVVVVV11, L.VVVVVV12, L.VVVVVV13, L.VVVVVV14, L.VVVVVV15, L.VVVVVV16, L.VVVVVV17, L.VVVVVV18, L.VVVVVV19, L.VVVVVV2, L.VVVVVV20, L.VVVVVV21, L.VVVVVV22, L.VVVVVV23, L.VVVVVV24, L.VVVVVV25, L.VVVVVV26, L.VVVVVV27, L.VVVVVV28, L.VVVVVV29, L.VVVVVV3, L.VVVVVV30, L.VVVVVV31, L.VVVVVV32, L.VVVVVV33, L.VVVVVV34, L.VVVVVV35, L.VVVVVV36, L.VVVVVV37, L.VVVVVV38, L.VVVVVV39, L.VVVVVV4, L.VVVVVV40, L.VVVVVV41, L.VVVVVV42, L.VVVVVV43, L.VVVVVV44, L.VVVVVV45, L.VVVVVV46, L.VVVVVV47, L.VVVVVV48, L.VVVVVV49, L.VVVVVV5, L.VVVVVV50, L.VVVVVV51, L.VVVVVV52, L.VVVVVV53, L.VVVVVV54, L.VVVVVV55, L.VVVVVV56, L.VVVVVV57, L.VVVVVV58, L.VVVVVV59, L.VVVVVV6, L.VVVVVV60, L.VVVVVV7, L.VVVVVV8, L.VVVVVV9 ],
               couplings = {(41,58):C.GC_274,(47,59):C.GC_275,(53,7):C.GC_274,(35,57):C.GC_275,(46,14):C.GC_274,(52,17):C.GC_275,(34,2):C.GC_274,(40,10):C.GC_275,(51,37):C.GC_274,(33,4):C.GC_275,(39,21):C.GC_274,(45,30):C.GC_275,(17,57):C.GC_274,(23,2):C.GC_275,(29,4):C.GC_274,(11,58):C.GC_275,(22,10):C.GC_274,(28,21):C.GC_275,(10,59):C.GC_274,(16,14):C.GC_275,(27,30):C.GC_274,(9,7):C.GC_275,(15,17):C.GC_274,(21,37):C.GC_275,(59,0):C.GC_274,(65,11):C.GC_275,(71,44):C.GC_274,(64,12):C.GC_274,(70,23):C.GC_275,(58,16):C.GC_275,(69,31):C.GC_274,(57,19):C.GC_274,(63,39):C.GC_275,(5,0):C.GC_275,(20,16):C.GC_274,(26,19):C.GC_275,(4,11):C.GC_274,(14,12):C.GC_275,(25,39):C.GC_274,(3,44):C.GC_275,(13,23):C.GC_274,(19,31):C.GC_275,(77,22):C.GC_275,(83,33):C.GC_274,(76,1):C.GC_274,(82,8):C.GC_275,(81,40):C.GC_274,(75,35):C.GC_275,(2,22):C.GC_274,(8,1):C.GC_275,(24,35):C.GC_274,(1,33):C.GC_275,(7,8):C.GC_274,(18,40):C.GC_275,(89,55):C.GC_274,(88,6):C.GC_275,(87,25):C.GC_274,(0,55):C.GC_275,(6,6):C.GC_274,(12,25):C.GC_275,(62,15):C.GC_274,(68,18):C.GC_275,(56,13):C.GC_275,(67,38):C.GC_274,(55,24):C.GC_274,(61,32):C.GC_275,(44,13):C.GC_274,(50,24):C.GC_275,(38,15):C.GC_275,(49,32):C.GC_274,(37,18):C.GC_274,(43,38):C.GC_275,(74,3):C.GC_274,(80,5):C.GC_275,(79,34):C.GC_274,(73,42):C.GC_275,(32,3):C.GC_275,(48,42):C.GC_274,(31,5):C.GC_274,(42,34):C.GC_275,(86,9):C.GC_275,(85,20):C.GC_274,(30,9):C.GC_274,(36,20):C.GC_275,(78,41):C.GC_274,(72,36):C.GC_275,(66,36):C.GC_274,(60,41):C.GC_275,(65,43):C.GC_223,(71,46):C.GC_222,(77,46):C.GC_223,(83,43):C.GC_222,(41,28):C.GC_223,(53,50):C.GC_223,(76,50):C.GC_222,(88,28):C.GC_222,(35,29):C.GC_223,(52,53):C.GC_223,(64,53):C.GC_222,(87,29):C.GC_222,(34,52):C.GC_222,(40,51):C.GC_222,(69,51):C.GC_223,(81,52):C.GC_223,(17,29):C.GC_222,(23,52):C.GC_223,(80,52):C.GC_222,(86,29):C.GC_223,(11,28):C.GC_222,(22,51):C.GC_223,(68,51):C.GC_222,(85,28):C.GC_223,(9,50):C.GC_222,(15,53):C.GC_222,(61,53):C.GC_223,(73,50):C.GC_223,(4,43):C.GC_222,(14,53):C.GC_223,(49,53):C.GC_222,(78,43):C.GC_223,(3,46):C.GC_223,(19,51):C.GC_222,(37,51):C.GC_223,(72,46):C.GC_222,(2,46):C.GC_222,(8,50):C.GC_223,(48,50):C.GC_222,(66,46):C.GC_223,(1,43):C.GC_223,(18,52):C.GC_222,(31,52):C.GC_223,(60,43):C.GC_222,(6,28):C.GC_223,(12,29):C.GC_223,(30,29):C.GC_222,(36,28):C.GC_222,(47,48):C.GC_223,(82,48):C.GC_222,(46,54):C.GC_223,(70,54):C.GC_222,(33,56):C.GC_222,(39,49):C.GC_222,(63,49):C.GC_223,(75,56):C.GC_223,(29,56):C.GC_223,(74,56):C.GC_222,(28,49):C.GC_223,(62,49):C.GC_222,(10,48):C.GC_222,(16,54):C.GC_222,(67,54):C.GC_223,(79,48):C.GC_223,(25,49):C.GC_222,(38,49):C.GC_223,(13,54):C.GC_223,(43,54):C.GC_222,(24,56):C.GC_222,(32,56):C.GC_223,(7,48):C.GC_223,(42,48):C.GC_222,(84,26):C.GC_274,(54,26):C.GC_275,(59,27):C.GC_223,(89,27):C.GC_222,(51,45):C.GC_223,(58,45):C.GC_222,(21,45):C.GC_222,(55,45):C.GC_223,(5,27):C.GC_222,(20,45):C.GC_223,(50,45):C.GC_222,(84,27):C.GC_223,(0,27):C.GC_223,(54,27):C.GC_222,(45,47):C.GC_222,(57,47):C.GC_223,(27,47):C.GC_223,(56,47):C.GC_222,(26,47):C.GC_222,(44,47):C.GC_223})

V_202 = Vertex(name = 'V_202',
               particles = [ P.g, P.g, P.g, P.g, P.g, P.g ],
               color = [ 'f(-3,1,2)*f(-2,3,4)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,1,2)*f(-2,3,5)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,1,2)*f(-2,3,6)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,1,2)*f(-2,4,5)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,1,2)*f(-2,4,6)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,1,2)*f(-2,5,6)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,1,3)*f(-2,2,4)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,1,3)*f(-2,2,5)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,1,3)*f(-2,2,6)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,1,3)*f(-2,4,5)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,1,3)*f(-2,4,6)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,1,3)*f(-2,5,6)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,1,4)*f(-2,2,3)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,1,4)*f(-2,2,5)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,1,4)*f(-2,2,6)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,1,4)*f(-2,3,5)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,1,4)*f(-2,3,6)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,1,4)*f(-2,5,6)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,1,5)*f(-2,2,3)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,1,5)*f(-2,2,4)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,1,5)*f(-2,2,6)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,1,5)*f(-2,3,4)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,1,5)*f(-2,3,6)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,1,5)*f(-2,4,6)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,1,6)*f(-2,2,3)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,1,6)*f(-2,2,4)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,1,6)*f(-2,2,5)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,1,6)*f(-2,3,4)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,1,6)*f(-2,3,5)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,1,6)*f(-2,4,5)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,2,3)*f(-2,1,4)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,2,3)*f(-2,1,5)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,2,3)*f(-2,1,6)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,2,3)*f(-2,4,5)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,2,3)*f(-2,4,6)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,2,3)*f(-2,5,6)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,2,4)*f(-2,1,3)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,2,4)*f(-2,1,5)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,2,4)*f(-2,1,6)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,2,4)*f(-2,3,5)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,2,4)*f(-2,3,6)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,2,4)*f(-2,5,6)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,2,5)*f(-2,1,3)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,2,5)*f(-2,1,4)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,2,5)*f(-2,1,6)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,2,5)*f(-2,3,4)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,2,5)*f(-2,3,6)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,2,5)*f(-2,4,6)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,2,6)*f(-2,1,3)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,2,6)*f(-2,1,4)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,2,6)*f(-2,1,5)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,2,6)*f(-2,3,4)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,2,6)*f(-2,3,5)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,2,6)*f(-2,4,5)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,3,4)*f(-2,1,2)*f(-1,-2,-3)*f(5,6,-1)', 'f(-3,3,4)*f(-2,1,5)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,3,4)*f(-2,1,6)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,3,4)*f(-2,2,5)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,3,4)*f(-2,2,6)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,3,4)*f(-2,5,6)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,3,5)*f(-2,1,2)*f(-1,-2,-3)*f(4,6,-1)', 'f(-3,3,5)*f(-2,1,4)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,3,5)*f(-2,1,6)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,3,5)*f(-2,2,4)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,3,5)*f(-2,2,6)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,3,5)*f(-2,4,6)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,3,6)*f(-2,1,2)*f(-1,-2,-3)*f(4,5,-1)', 'f(-3,3,6)*f(-2,1,4)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,3,6)*f(-2,1,5)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,3,6)*f(-2,2,4)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,3,6)*f(-2,2,5)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,3,6)*f(-2,4,5)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,4,5)*f(-2,1,2)*f(-1,-2,-3)*f(3,6,-1)', 'f(-3,4,5)*f(-2,1,3)*f(-1,-2,-3)*f(2,6,-1)', 'f(-3,4,5)*f(-2,1,6)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,4,5)*f(-2,2,3)*f(-1,-2,-3)*f(1,6,-1)', 'f(-3,4,5)*f(-2,2,6)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,4,5)*f(-2,3,6)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,4,6)*f(-2,1,2)*f(-1,-2,-3)*f(3,5,-1)', 'f(-3,4,6)*f(-2,1,3)*f(-1,-2,-3)*f(2,5,-1)', 'f(-3,4,6)*f(-2,1,5)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,4,6)*f(-2,2,3)*f(-1,-2,-3)*f(1,5,-1)', 'f(-3,4,6)*f(-2,2,5)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,4,6)*f(-2,3,5)*f(-1,-2,-3)*f(1,2,-1)', 'f(-3,5,6)*f(-2,1,2)*f(-1,-2,-3)*f(3,4,-1)', 'f(-3,5,6)*f(-2,1,3)*f(-1,-2,-3)*f(2,4,-1)', 'f(-3,5,6)*f(-2,1,4)*f(-1,-2,-3)*f(2,3,-1)', 'f(-3,5,6)*f(-2,2,3)*f(-1,-2,-3)*f(1,4,-1)', 'f(-3,5,6)*f(-2,2,4)*f(-1,-2,-3)*f(1,3,-1)', 'f(-3,5,6)*f(-2,3,4)*f(-1,-2,-3)*f(1,2,-1)' ],
               lorentz = [ L.VVVVVV1, L.VVVVVV10, L.VVVVVV11, L.VVVVVV12, L.VVVVVV13, L.VVVVVV14, L.VVVVVV15, L.VVVVVV16, L.VVVVVV17, L.VVVVVV18, L.VVVVVV19, L.VVVVVV2, L.VVVVVV20, L.VVVVVV21, L.VVVVVV22, L.VVVVVV23, L.VVVVVV24, L.VVVVVV25, L.VVVVVV26, L.VVVVVV27, L.VVVVVV28, L.VVVVVV29, L.VVVVVV3, L.VVVVVV30, L.VVVVVV31, L.VVVVVV32, L.VVVVVV33, L.VVVVVV34, L.VVVVVV35, L.VVVVVV36, L.VVVVVV37, L.VVVVVV38, L.VVVVVV39, L.VVVVVV4, L.VVVVVV40, L.VVVVVV41, L.VVVVVV42, L.VVVVVV43, L.VVVVVV44, L.VVVVVV45, L.VVVVVV46, L.VVVVVV47, L.VVVVVV48, L.VVVVVV49, L.VVVVVV5, L.VVVVVV50, L.VVVVVV51, L.VVVVVV52, L.VVVVVV53, L.VVVVVV54, L.VVVVVV55, L.VVVVVV56, L.VVVVVV57, L.VVVVVV58, L.VVVVVV59, L.VVVVVV6, L.VVVVVV60, L.VVVVVV7, L.VVVVVV8, L.VVVVVV9 ],
               couplings = {(41,58):C.GC_279,(47,59):C.GC_280,(53,7):C.GC_279,(35,57):C.GC_280,(46,14):C.GC_279,(52,17):C.GC_280,(34,2):C.GC_279,(40,10):C.GC_280,(51,37):C.GC_279,(33,4):C.GC_280,(39,21):C.GC_279,(45,30):C.GC_280,(17,57):C.GC_279,(23,2):C.GC_280,(29,4):C.GC_279,(11,58):C.GC_280,(22,10):C.GC_279,(28,21):C.GC_280,(10,59):C.GC_279,(16,14):C.GC_280,(27,30):C.GC_279,(9,7):C.GC_280,(15,17):C.GC_279,(21,37):C.GC_280,(59,0):C.GC_279,(65,11):C.GC_280,(71,44):C.GC_279,(64,12):C.GC_279,(70,23):C.GC_280,(58,16):C.GC_280,(69,31):C.GC_279,(57,19):C.GC_279,(63,39):C.GC_280,(5,0):C.GC_280,(20,16):C.GC_279,(26,19):C.GC_280,(4,11):C.GC_279,(14,12):C.GC_280,(25,39):C.GC_279,(3,44):C.GC_280,(13,23):C.GC_279,(19,31):C.GC_280,(77,22):C.GC_280,(83,33):C.GC_279,(76,1):C.GC_279,(82,8):C.GC_280,(81,40):C.GC_279,(75,35):C.GC_280,(2,22):C.GC_279,(8,1):C.GC_280,(24,35):C.GC_279,(1,33):C.GC_280,(7,8):C.GC_279,(18,40):C.GC_280,(89,55):C.GC_279,(88,6):C.GC_280,(87,25):C.GC_279,(0,55):C.GC_280,(6,6):C.GC_279,(12,25):C.GC_280,(62,15):C.GC_279,(68,18):C.GC_280,(56,13):C.GC_280,(67,38):C.GC_279,(55,24):C.GC_279,(61,32):C.GC_280,(44,13):C.GC_279,(50,24):C.GC_280,(38,15):C.GC_280,(49,32):C.GC_279,(37,18):C.GC_279,(43,38):C.GC_280,(74,3):C.GC_279,(80,5):C.GC_280,(79,34):C.GC_279,(73,42):C.GC_280,(32,3):C.GC_280,(48,42):C.GC_279,(31,5):C.GC_279,(42,34):C.GC_280,(86,9):C.GC_280,(85,20):C.GC_279,(30,9):C.GC_279,(36,20):C.GC_280,(78,41):C.GC_279,(72,36):C.GC_280,(66,36):C.GC_279,(60,41):C.GC_280,(65,43):C.GC_225,(71,46):C.GC_224,(77,46):C.GC_225,(83,43):C.GC_224,(41,28):C.GC_225,(53,50):C.GC_225,(76,50):C.GC_224,(88,28):C.GC_224,(35,29):C.GC_225,(52,53):C.GC_225,(64,53):C.GC_224,(87,29):C.GC_224,(34,52):C.GC_224,(40,51):C.GC_224,(69,51):C.GC_225,(81,52):C.GC_225,(17,29):C.GC_224,(23,52):C.GC_225,(80,52):C.GC_224,(86,29):C.GC_225,(11,28):C.GC_224,(22,51):C.GC_225,(68,51):C.GC_224,(85,28):C.GC_225,(9,50):C.GC_224,(15,53):C.GC_224,(61,53):C.GC_225,(73,50):C.GC_225,(4,43):C.GC_224,(14,53):C.GC_225,(49,53):C.GC_224,(78,43):C.GC_225,(3,46):C.GC_225,(19,51):C.GC_224,(37,51):C.GC_225,(72,46):C.GC_224,(2,46):C.GC_224,(8,50):C.GC_225,(48,50):C.GC_224,(66,46):C.GC_225,(1,43):C.GC_225,(18,52):C.GC_224,(31,52):C.GC_225,(60,43):C.GC_224,(6,28):C.GC_225,(12,29):C.GC_225,(30,29):C.GC_224,(36,28):C.GC_224,(47,48):C.GC_225,(82,48):C.GC_224,(46,54):C.GC_225,(70,54):C.GC_224,(33,56):C.GC_224,(39,49):C.GC_224,(63,49):C.GC_225,(75,56):C.GC_225,(29,56):C.GC_225,(74,56):C.GC_224,(28,49):C.GC_225,(62,49):C.GC_224,(10,48):C.GC_224,(16,54):C.GC_224,(67,54):C.GC_225,(79,48):C.GC_225,(25,49):C.GC_224,(38,49):C.GC_225,(13,54):C.GC_225,(43,54):C.GC_224,(24,56):C.GC_224,(32,56):C.GC_225,(7,48):C.GC_225,(42,48):C.GC_224,(84,26):C.GC_279,(54,26):C.GC_280,(59,27):C.GC_225,(89,27):C.GC_224,(51,45):C.GC_225,(58,45):C.GC_224,(21,45):C.GC_224,(55,45):C.GC_225,(5,27):C.GC_224,(20,45):C.GC_225,(50,45):C.GC_224,(84,27):C.GC_225,(0,27):C.GC_225,(54,27):C.GC_224,(45,47):C.GC_224,(57,47):C.GC_225,(27,47):C.GC_225,(56,47):C.GC_224,(26,47):C.GC_224,(44,47):C.GC_225})

V_203 = Vertex(name = 'V_203',
               particles = [ P.a, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1, L.VVS4 ],
               couplings = {(0,0):C.GC_588,(0,1):C.GC_403})

V_204 = Vertex(name = 'V_204',
               particles = [ P.a, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS4 ],
               couplings = {(0,0):C.GC_443})

V_205 = Vertex(name = 'V_205',
               particles = [ P.g, P.g, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.VVS1, L.VVS4 ],
               couplings = {(0,0):C.GC_589,(0,1):C.GC_422})

V_206 = Vertex(name = 'V_206',
               particles = [ P.g, P.g, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.VVS4 ],
               couplings = {(0,0):C.GC_444})

V_207 = Vertex(name = 'V_207',
               particles = [ P.a, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS1, L.VVS3, L.VVS4 ],
               couplings = {(0,1):C.GC_496,(0,0):C.GC_592,(0,2):C.GC_447})

V_208 = Vertex(name = 'V_208',
               particles = [ P.a, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVS4 ],
               couplings = {(0,0):C.GC_497})

V_209 = Vertex(name = 'V_209',
               particles = [ P.g, P.g, P.g, P.H ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVVS1, L.VVVS2 ],
               couplings = {(0,0):C.GC_590,(0,1):C.GC_441})

V_210 = Vertex(name = 'V_210',
               particles = [ P.g, P.g, P.g, P.H ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVVS2 ],
               couplings = {(0,0):C.GC_445})

V_211 = Vertex(name = 'V_211',
               particles = [ P.g, P.g, P.g, P.g, P.H ],
               color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
               lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
               couplings = {(1,1):C.GC_442,(0,0):C.GC_442,(2,2):C.GC_442})

V_212 = Vertex(name = 'V_212',
               particles = [ P.g, P.g, P.g, P.g, P.H ],
               color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
               lorentz = [ L.VVVVS1, L.VVVVS2, L.VVVVS3 ],
               couplings = {(1,1):C.GC_446,(0,0):C.GC_446,(2,2):C.GC_446})

V_213 = Vertex(name = 'V_213',
               particles = [ P.d__tilde__, P.u, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_772,(0,0):C.GC_469,(0,1):C.GC_478})

V_214 = Vertex(name = 'V_214',
               particles = [ P.s__tilde__, P.u, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_775,(0,0):C.GC_470,(0,1):C.GC_479})

V_215 = Vertex(name = 'V_215',
               particles = [ P.b__tilde__, P.u, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_778,(0,0):C.GC_471,(0,1):C.GC_480})

V_216 = Vertex(name = 'V_216',
               particles = [ P.d__tilde__, P.c, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_773,(0,0):C.GC_472,(0,1):C.GC_481})

V_217 = Vertex(name = 'V_217',
               particles = [ P.s__tilde__, P.c, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_776,(0,0):C.GC_473,(0,1):C.GC_482})

V_218 = Vertex(name = 'V_218',
               particles = [ P.b__tilde__, P.c, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_779,(0,0):C.GC_474,(0,1):C.GC_483})

V_219 = Vertex(name = 'V_219',
               particles = [ P.d__tilde__, P.t, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_774,(0,0):C.GC_475,(0,1):C.GC_484})

V_220 = Vertex(name = 'V_220',
               particles = [ P.s__tilde__, P.t, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_777,(0,0):C.GC_476,(0,1):C.GC_485})

V_221 = Vertex(name = 'V_221',
               particles = [ P.b__tilde__, P.t, P.W__minus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_780,(0,0):C.GC_477,(0,1):C.GC_486})

V_222 = Vertex(name = 'V_222',
               particles = [ P.e__plus__, P.ve, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_233,(0,0):C.GC_460})

V_223 = Vertex(name = 'V_223',
               particles = [ P.mu__plus__, P.ve, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_234,(0,0):C.GC_461})

V_224 = Vertex(name = 'V_224',
               particles = [ P.ta__plus__, P.ve, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_235,(0,0):C.GC_462})

V_225 = Vertex(name = 'V_225',
               particles = [ P.e__plus__, P.vm, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_236,(0,0):C.GC_463})

V_226 = Vertex(name = 'V_226',
               particles = [ P.mu__plus__, P.vm, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_237,(0,0):C.GC_464})

V_227 = Vertex(name = 'V_227',
               particles = [ P.ta__plus__, P.vm, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_238,(0,0):C.GC_465})

V_228 = Vertex(name = 'V_228',
               particles = [ P.e__plus__, P.vt, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_239,(0,0):C.GC_466})

V_229 = Vertex(name = 'V_229',
               particles = [ P.mu__plus__, P.vt, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_240,(0,0):C.GC_467})

V_230 = Vertex(name = 'V_230',
               particles = [ P.ta__plus__, P.vt, P.W__minus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_241,(0,0):C.GC_468})

V_231 = Vertex(name = 'V_231',
               particles = [ P.u__tilde__, P.d, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_754,(0,0):C.GC_709,(0,1):C.GC_736})

V_232 = Vertex(name = 'V_232',
               particles = [ P.c__tilde__, P.d, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_757,(0,0):C.GC_712,(0,1):C.GC_739})

V_233 = Vertex(name = 'V_233',
               particles = [ P.t__tilde__, P.d, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_760,(0,0):C.GC_715,(0,1):C.GC_742})

V_234 = Vertex(name = 'V_234',
               particles = [ P.u__tilde__, P.s, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_755,(0,0):C.GC_710,(0,1):C.GC_737})

V_235 = Vertex(name = 'V_235',
               particles = [ P.c__tilde__, P.s, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_758,(0,0):C.GC_713,(0,1):C.GC_740})

V_236 = Vertex(name = 'V_236',
               particles = [ P.t__tilde__, P.s, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_761,(0,0):C.GC_716,(0,1):C.GC_743})

V_237 = Vertex(name = 'V_237',
               particles = [ P.u__tilde__, P.b, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_756,(0,0):C.GC_711,(0,1):C.GC_738})

V_238 = Vertex(name = 'V_238',
               particles = [ P.c__tilde__, P.b, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_759,(0,0):C.GC_714,(0,1):C.GC_741})

V_239 = Vertex(name = 'V_239',
               particles = [ P.t__tilde__, P.b, P.W__plus__, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS4 ],
               couplings = {(0,2):C.GC_762,(0,0):C.GC_717,(0,1):C.GC_744})

V_240 = Vertex(name = 'V_240',
               particles = [ P.ve__tilde__, P.e__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_763,(0,0):C.GC_700})

V_241 = Vertex(name = 'V_241',
               particles = [ P.vm__tilde__, P.e__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_766,(0,0):C.GC_703})

V_242 = Vertex(name = 'V_242',
               particles = [ P.vt__tilde__, P.e__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_769,(0,0):C.GC_706})

V_243 = Vertex(name = 'V_243',
               particles = [ P.ve__tilde__, P.mu__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_764,(0,0):C.GC_701})

V_244 = Vertex(name = 'V_244',
               particles = [ P.vm__tilde__, P.mu__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_767,(0,0):C.GC_704})

V_245 = Vertex(name = 'V_245',
               particles = [ P.vt__tilde__, P.mu__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_770,(0,0):C.GC_707})

V_246 = Vertex(name = 'V_246',
               particles = [ P.ve__tilde__, P.ta__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_765,(0,0):C.GC_702})

V_247 = Vertex(name = 'V_247',
               particles = [ P.vm__tilde__, P.ta__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_768,(0,0):C.GC_705})

V_248 = Vertex(name = 'V_248',
               particles = [ P.vt__tilde__, P.ta__minus__, P.W__plus__, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS4 ],
               couplings = {(0,1):C.GC_771,(0,0):C.GC_708})

V_249 = Vertex(name = 'V_249',
               particles = [ P.d__tilde__, P.d, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_498,(0,1):C.GC_534,(0,2):C.GC_243,(0,3):C.GC_349})

V_250 = Vertex(name = 'V_250',
               particles = [ P.s__tilde__, P.d, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_499,(0,1):C.GC_535,(0,2):C.GC_244,(0,3):C.GC_350})

V_251 = Vertex(name = 'V_251',
               particles = [ P.b__tilde__, P.d, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_500,(0,1):C.GC_536,(0,2):C.GC_245,(0,3):C.GC_351})

V_252 = Vertex(name = 'V_252',
               particles = [ P.d__tilde__, P.s, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_501,(0,1):C.GC_537,(0,2):C.GC_246,(0,3):C.GC_352})

V_253 = Vertex(name = 'V_253',
               particles = [ P.s__tilde__, P.s, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_502,(0,1):C.GC_538,(0,2):C.GC_247,(0,3):C.GC_353})

V_254 = Vertex(name = 'V_254',
               particles = [ P.b__tilde__, P.s, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_503,(0,1):C.GC_539,(0,2):C.GC_248,(0,3):C.GC_354})

V_255 = Vertex(name = 'V_255',
               particles = [ P.d__tilde__, P.b, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_504,(0,1):C.GC_540,(0,2):C.GC_249,(0,3):C.GC_355})

V_256 = Vertex(name = 'V_256',
               particles = [ P.s__tilde__, P.b, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_505,(0,1):C.GC_541,(0,2):C.GC_250,(0,3):C.GC_356})

V_257 = Vertex(name = 'V_257',
               particles = [ P.b__tilde__, P.b, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_506,(0,1):C.GC_542,(0,2):C.GC_251,(0,3):C.GC_357})

V_258 = Vertex(name = 'V_258',
               particles = [ P.e__plus__, P.e__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_507,(0,1):C.GC_543,(0,2):C.GC_252,(0,3):C.GC_358})

V_259 = Vertex(name = 'V_259',
               particles = [ P.mu__plus__, P.e__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_508,(0,1):C.GC_544,(0,2):C.GC_253,(0,3):C.GC_359})

V_260 = Vertex(name = 'V_260',
               particles = [ P.ta__plus__, P.e__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_509,(0,1):C.GC_545,(0,2):C.GC_254,(0,3):C.GC_360})

V_261 = Vertex(name = 'V_261',
               particles = [ P.e__plus__, P.mu__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_510,(0,1):C.GC_546,(0,2):C.GC_255,(0,3):C.GC_361})

V_262 = Vertex(name = 'V_262',
               particles = [ P.mu__plus__, P.mu__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_511,(0,1):C.GC_547,(0,2):C.GC_256,(0,3):C.GC_362})

V_263 = Vertex(name = 'V_263',
               particles = [ P.ta__plus__, P.mu__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_512,(0,1):C.GC_548,(0,2):C.GC_257,(0,3):C.GC_363})

V_264 = Vertex(name = 'V_264',
               particles = [ P.e__plus__, P.ta__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_513,(0,1):C.GC_549,(0,2):C.GC_258,(0,3):C.GC_364})

V_265 = Vertex(name = 'V_265',
               particles = [ P.mu__plus__, P.ta__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_514,(0,1):C.GC_550,(0,2):C.GC_259,(0,3):C.GC_365})

V_266 = Vertex(name = 'V_266',
               particles = [ P.ta__plus__, P.ta__minus__, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_515,(0,1):C.GC_551,(0,2):C.GC_260,(0,3):C.GC_366})

V_267 = Vertex(name = 'V_267',
               particles = [ P.u__tilde__, P.u, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_516,(0,1):C.GC_552,(0,2):C.GC_261,(0,3):C.GC_367})

V_268 = Vertex(name = 'V_268',
               particles = [ P.c__tilde__, P.u, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_517,(0,1):C.GC_553,(0,2):C.GC_262,(0,3):C.GC_368})

V_269 = Vertex(name = 'V_269',
               particles = [ P.t__tilde__, P.u, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_518,(0,1):C.GC_554,(0,2):C.GC_263,(0,3):C.GC_369})

V_270 = Vertex(name = 'V_270',
               particles = [ P.u__tilde__, P.c, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_519,(0,1):C.GC_555,(0,2):C.GC_264,(0,3):C.GC_370})

V_271 = Vertex(name = 'V_271',
               particles = [ P.c__tilde__, P.c, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_520,(0,1):C.GC_556,(0,2):C.GC_265,(0,3):C.GC_371})

V_272 = Vertex(name = 'V_272',
               particles = [ P.t__tilde__, P.c, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_521,(0,1):C.GC_557,(0,2):C.GC_266,(0,3):C.GC_372})

V_273 = Vertex(name = 'V_273',
               particles = [ P.u__tilde__, P.t, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_522,(0,1):C.GC_558,(0,2):C.GC_267,(0,3):C.GC_373})

V_274 = Vertex(name = 'V_274',
               particles = [ P.c__tilde__, P.t, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_523,(0,1):C.GC_559,(0,2):C.GC_268,(0,3):C.GC_374})

V_275 = Vertex(name = 'V_275',
               particles = [ P.t__tilde__, P.t, P.Z, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS1, L.FFVS2, L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_524,(0,1):C.GC_560,(0,2):C.GC_269,(0,3):C.GC_375})

V_276 = Vertex(name = 'V_276',
               particles = [ P.ve__tilde__, P.ve, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_525})

V_277 = Vertex(name = 'V_277',
               particles = [ P.vm__tilde__, P.ve, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_526})

V_278 = Vertex(name = 'V_278',
               particles = [ P.vt__tilde__, P.ve, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_527})

V_279 = Vertex(name = 'V_279',
               particles = [ P.ve__tilde__, P.vm, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_528})

V_280 = Vertex(name = 'V_280',
               particles = [ P.vm__tilde__, P.vm, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_529})

V_281 = Vertex(name = 'V_281',
               particles = [ P.vt__tilde__, P.vm, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_530})

V_282 = Vertex(name = 'V_282',
               particles = [ P.ve__tilde__, P.vt, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_531})

V_283 = Vertex(name = 'V_283',
               particles = [ P.vm__tilde__, P.vt, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_532})

V_284 = Vertex(name = 'V_284',
               particles = [ P.vt__tilde__, P.vt, P.Z, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS1 ],
               couplings = {(0,0):C.GC_533})

V_285 = Vertex(name = 'V_285',
               particles = [ P.d__tilde__, P.d, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_148,(0,1):C.GC_286})

V_286 = Vertex(name = 'V_286',
               particles = [ P.s__tilde__, P.d, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_149,(0,1):C.GC_287})

V_287 = Vertex(name = 'V_287',
               particles = [ P.b__tilde__, P.d, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_150,(0,1):C.GC_288})

V_288 = Vertex(name = 'V_288',
               particles = [ P.d__tilde__, P.s, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_151,(0,1):C.GC_289})

V_289 = Vertex(name = 'V_289',
               particles = [ P.s__tilde__, P.s, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_152,(0,1):C.GC_290})

V_290 = Vertex(name = 'V_290',
               particles = [ P.b__tilde__, P.s, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_153,(0,1):C.GC_291})

V_291 = Vertex(name = 'V_291',
               particles = [ P.d__tilde__, P.b, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_154,(0,1):C.GC_292})

V_292 = Vertex(name = 'V_292',
               particles = [ P.s__tilde__, P.b, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_155,(0,1):C.GC_293})

V_293 = Vertex(name = 'V_293',
               particles = [ P.b__tilde__, P.b, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_156,(0,1):C.GC_294})

V_294 = Vertex(name = 'V_294',
               particles = [ P.e__plus__, P.e__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_157,(0,1):C.GC_295})

V_295 = Vertex(name = 'V_295',
               particles = [ P.mu__plus__, P.e__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_158,(0,1):C.GC_296})

V_296 = Vertex(name = 'V_296',
               particles = [ P.ta__plus__, P.e__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_159,(0,1):C.GC_297})

V_297 = Vertex(name = 'V_297',
               particles = [ P.e__plus__, P.mu__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_160,(0,1):C.GC_298})

V_298 = Vertex(name = 'V_298',
               particles = [ P.mu__plus__, P.mu__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_161,(0,1):C.GC_299})

V_299 = Vertex(name = 'V_299',
               particles = [ P.ta__plus__, P.mu__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_162,(0,1):C.GC_300})

V_300 = Vertex(name = 'V_300',
               particles = [ P.e__plus__, P.ta__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_163,(0,1):C.GC_301})

V_301 = Vertex(name = 'V_301',
               particles = [ P.mu__plus__, P.ta__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_164,(0,1):C.GC_302})

V_302 = Vertex(name = 'V_302',
               particles = [ P.ta__plus__, P.ta__minus__, P.a, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_165,(0,1):C.GC_303})

V_303 = Vertex(name = 'V_303',
               particles = [ P.u__tilde__, P.u, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_166,(0,1):C.GC_304})

V_304 = Vertex(name = 'V_304',
               particles = [ P.c__tilde__, P.u, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_167,(0,1):C.GC_305})

V_305 = Vertex(name = 'V_305',
               particles = [ P.t__tilde__, P.u, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_168,(0,1):C.GC_306})

V_306 = Vertex(name = 'V_306',
               particles = [ P.u__tilde__, P.c, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_169,(0,1):C.GC_307})

V_307 = Vertex(name = 'V_307',
               particles = [ P.c__tilde__, P.c, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_170,(0,1):C.GC_308})

V_308 = Vertex(name = 'V_308',
               particles = [ P.t__tilde__, P.c, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_171,(0,1):C.GC_309})

V_309 = Vertex(name = 'V_309',
               particles = [ P.u__tilde__, P.t, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_172,(0,1):C.GC_310})

V_310 = Vertex(name = 'V_310',
               particles = [ P.c__tilde__, P.t, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_173,(0,1):C.GC_311})

V_311 = Vertex(name = 'V_311',
               particles = [ P.t__tilde__, P.t, P.a, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_174,(0,1):C.GC_312})

V_312 = Vertex(name = 'V_312',
               particles = [ P.d__tilde__, P.d, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_176,(0,1):C.GC_313})

V_313 = Vertex(name = 'V_313',
               particles = [ P.s__tilde__, P.d, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_177,(0,1):C.GC_315})

V_314 = Vertex(name = 'V_314',
               particles = [ P.b__tilde__, P.d, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_178,(0,1):C.GC_317})

V_315 = Vertex(name = 'V_315',
               particles = [ P.d__tilde__, P.s, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_179,(0,1):C.GC_319})

V_316 = Vertex(name = 'V_316',
               particles = [ P.s__tilde__, P.s, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_180,(0,1):C.GC_321})

V_317 = Vertex(name = 'V_317',
               particles = [ P.b__tilde__, P.s, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_181,(0,1):C.GC_323})

V_318 = Vertex(name = 'V_318',
               particles = [ P.d__tilde__, P.b, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_182,(0,1):C.GC_325})

V_319 = Vertex(name = 'V_319',
               particles = [ P.s__tilde__, P.b, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_183,(0,1):C.GC_327})

V_320 = Vertex(name = 'V_320',
               particles = [ P.b__tilde__, P.b, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_184,(0,1):C.GC_329})

V_321 = Vertex(name = 'V_321',
               particles = [ P.u__tilde__, P.u, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_185,(0,1):C.GC_331})

V_322 = Vertex(name = 'V_322',
               particles = [ P.c__tilde__, P.u, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_186,(0,1):C.GC_333})

V_323 = Vertex(name = 'V_323',
               particles = [ P.t__tilde__, P.u, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_187,(0,1):C.GC_335})

V_324 = Vertex(name = 'V_324',
               particles = [ P.u__tilde__, P.c, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_188,(0,1):C.GC_337})

V_325 = Vertex(name = 'V_325',
               particles = [ P.c__tilde__, P.c, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_189,(0,1):C.GC_339})

V_326 = Vertex(name = 'V_326',
               particles = [ P.t__tilde__, P.c, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_190,(0,1):C.GC_341})

V_327 = Vertex(name = 'V_327',
               particles = [ P.u__tilde__, P.t, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_191,(0,1):C.GC_343})

V_328 = Vertex(name = 'V_328',
               particles = [ P.c__tilde__, P.t, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_192,(0,1):C.GC_345})

V_329 = Vertex(name = 'V_329',
               particles = [ P.t__tilde__, P.t, P.g, P.H ],
               color = [ 'T(3,2,1)' ],
               lorentz = [ L.FFVS3, L.FFVS5 ],
               couplings = {(0,0):C.GC_193,(0,1):C.GC_347})

V_330 = Vertex(name = 'V_330',
               particles = [ P.d__tilde__, P.d, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_314,(0,0):C.GC_195})

V_331 = Vertex(name = 'V_331',
               particles = [ P.s__tilde__, P.d, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_316,(0,0):C.GC_196})

V_332 = Vertex(name = 'V_332',
               particles = [ P.b__tilde__, P.d, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_318,(0,0):C.GC_197})

V_333 = Vertex(name = 'V_333',
               particles = [ P.d__tilde__, P.s, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_320,(0,0):C.GC_198})

V_334 = Vertex(name = 'V_334',
               particles = [ P.s__tilde__, P.s, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_322,(0,0):C.GC_199})

V_335 = Vertex(name = 'V_335',
               particles = [ P.b__tilde__, P.s, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_324,(0,0):C.GC_200})

V_336 = Vertex(name = 'V_336',
               particles = [ P.d__tilde__, P.b, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_326,(0,0):C.GC_201})

V_337 = Vertex(name = 'V_337',
               particles = [ P.s__tilde__, P.b, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_328,(0,0):C.GC_202})

V_338 = Vertex(name = 'V_338',
               particles = [ P.b__tilde__, P.b, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_330,(0,0):C.GC_203})

V_339 = Vertex(name = 'V_339',
               particles = [ P.u__tilde__, P.u, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_332,(0,0):C.GC_204})

V_340 = Vertex(name = 'V_340',
               particles = [ P.c__tilde__, P.u, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_334,(0,0):C.GC_205})

V_341 = Vertex(name = 'V_341',
               particles = [ P.t__tilde__, P.u, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_336,(0,0):C.GC_206})

V_342 = Vertex(name = 'V_342',
               particles = [ P.u__tilde__, P.c, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_338,(0,0):C.GC_207})

V_343 = Vertex(name = 'V_343',
               particles = [ P.c__tilde__, P.c, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_340,(0,0):C.GC_208})

V_344 = Vertex(name = 'V_344',
               particles = [ P.t__tilde__, P.c, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_342,(0,0):C.GC_209})

V_345 = Vertex(name = 'V_345',
               particles = [ P.u__tilde__, P.t, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_344,(0,0):C.GC_210})

V_346 = Vertex(name = 'V_346',
               particles = [ P.c__tilde__, P.t, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_346,(0,0):C.GC_211})

V_347 = Vertex(name = 'V_347',
               particles = [ P.t__tilde__, P.t, P.g, P.g, P.H ],
               color = [ 'f(-1,3,4)*T(-1,2,1)' ],
               lorentz = [ L.FFVVS1, L.FFVVS2 ],
               couplings = {(0,1):C.GC_348,(0,0):C.GC_212})

V_348 = Vertex(name = 'V_348',
               particles = [ P.a, P.a, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1, L.VVSS4 ],
               couplings = {(0,0):C.GC_281,(0,1):C.GC_175})

V_349 = Vertex(name = 'V_349',
               particles = [ P.g, P.g, P.H, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.VVSS1, L.VVSS4 ],
               couplings = {(0,0):C.GC_282,(0,1):C.GC_194})

V_350 = Vertex(name = 'V_350',
               particles = [ P.a, P.Z, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.VVSS1, L.VVSS3, L.VVSS4 ],
               couplings = {(0,1):C.GC_229,(0,0):C.GC_285,(0,2):C.GC_242})

V_351 = Vertex(name = 'V_351',
               particles = [ P.g, P.g, P.g, P.H, P.H ],
               color = [ 'f(1,2,3)' ],
               lorentz = [ L.VVVSS1, L.VVVSS2 ],
               couplings = {(0,0):C.GC_283,(0,1):C.GC_215})

V_352 = Vertex(name = 'V_352',
               particles = [ P.g, P.g, P.g, P.g, P.H, P.H ],
               color = [ 'f(-1,1,2)*f(3,4,-1)', 'f(-1,1,3)*f(2,4,-1)', 'f(-1,1,4)*f(2,3,-1)' ],
               lorentz = [ L.VVVVSS1, L.VVVVSS2, L.VVVVSS3 ],
               couplings = {(1,1):C.GC_219,(0,0):C.GC_219,(2,2):C.GC_219})

V_353 = Vertex(name = 'V_353',
               particles = [ P.b__tilde__, P.b, P.H, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFSS1, L.FFSS2 ],
               couplings = {(0,0):C.GC_226,(0,1):C.GC_808})

V_354 = Vertex(name = 'V_354',
               particles = [ P.ta__plus__, P.ta__minus__, P.H, P.H ],
               color = [ '1' ],
               lorentz = [ L.FFSS1, L.FFSS2 ],
               couplings = {(0,0):C.GC_228,(0,1):C.GC_809})

V_355 = Vertex(name = 'V_355',
               particles = [ P.t__tilde__, P.t, P.H, P.H ],
               color = [ 'Identity(1,2)' ],
               lorentz = [ L.FFSS1, L.FFSS2 ],
               couplings = {(0,0):C.GC_227,(0,1):C.GC_810})

