# This file was automatically created by FeynRules 2.3.26
# Mathematica version: 10.3.0 for Linux x86 (64-bit) (October 9, 2015)
# Date: Wed 21 Aug 2019 10:11:56


from object_library import all_orders, CouplingOrder


NP = CouplingOrder(name = 'NP',
                   expansion_order = 99,
                   hierarchy = 1)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

