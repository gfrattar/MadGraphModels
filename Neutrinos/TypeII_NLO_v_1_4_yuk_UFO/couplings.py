# This file was automatically created by FeynRules 2.3.49_fne
# Mathematica version: 13.1.0 for Mac OS X ARM (64-bit) (June 16, 2022)
# Date: Thu 7 Dec 2023 15:03:02


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-0.3333333333333333*(ee*complex(0,1))',
                order = {'QED':1})

GC_10 = Coupling(name = 'GC_10',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(muHD*vev)/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_101 = Coupling(name = 'GC_101',
                  value = '-((ee**2*vev*cmath.sqrt(2))/(sw**2*cmath.sqrt(vev**2 + 4*vevD**2)))',
                  order = {'QED':2})

GC_102 = Coupling(name = 'GC_102',
                  value = '(ee**2*vev*cmath.sqrt(2))/(sw**2*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_103 = Coupling(name = 'GC_103',
                  value = '(-2*muHD*vevD)/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_104 = Coupling(name = 'GC_104',
                  value = '(2*muHD*vevD)/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_105 = Coupling(name = 'GC_105',
                  value = '(-2*ee**2*vevD*cmath.sqrt(2))/(sw**2*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_106 = Coupling(name = 'GC_106',
                  value = '(2*ee**2*vevD*cmath.sqrt(2))/(sw**2*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = '-0.5*(lamHD2*vev*vevD)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_108 = Coupling(name = 'GC_108',
                  value = '(lamHD2*vev*vevD)/(2.*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_109 = Coupling(name = 'GC_109',
                  value = '(ee*cmath.sqrt(vev**2/2. + vevD**2))/(sw*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_11 = Coupling(name = 'GC_11',
                 value = 'G',
                 order = {'QCD':1})

GC_110 = Coupling(name = 'GC_110',
                  value = '-((ee**2*cmath.sqrt(vev**2/2. + vevD**2))/(sw*cmath.sqrt(vev**2 + 4*vevD**2)))',
                  order = {'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '(ee**2*cmath.sqrt(vev**2/2. + vevD**2))/(sw*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_112 = Coupling(name = 'GC_112',
                  value = '-((ee*vev*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)))',
                  order = {'QED':1})

GC_113 = Coupling(name = 'GC_113',
                  value = '-((ee**2*vev*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)))',
                  order = {'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = '(ee**2*vev*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = '(ee*cmath.sqrt(vev**2 + 4*vevD**2))/(2.*sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_116 = Coupling(name = 'GC_116',
                  value = '-0.5*(ee**2*cmath.sqrt(vev**2 + 4*vevD**2))/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_117 = Coupling(name = 'GC_117',
                  value = '(ee**2*cmath.sqrt(vev**2 + 4*vevD**2))/(2.*sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_118 = Coupling(name = 'GC_118',
                  value = '(2*ee**2*complex(0,1)*vevD)/(cw*cmath.sqrt(vev**2/2. + vevD**2)) - (cw*ee**2*complex(0,1)*vevD)/(sw**2*cmath.sqrt(vev**2/2. + vevD**2))',
                  order = {'QED':2})

GC_119 = Coupling(name = 'GC_119',
                  value = '(-8*complex(0,1)*lamD1*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (4*complex(0,1)*lamD2*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (8*complex(0,1)*lamH*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 + (8*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 + (4*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2',
                  order = {'QED':2})

GC_12 = Coupling(name = 'GC_12',
                 value = '-(complex(0,1)*G**2)',
                 order = {'QCD':2})

GC_120 = Coupling(name = 'GC_120',
                  value = '(-4*complex(0,1)*lamD1*vev**3*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 - (2*complex(0,1)*lamD2*vev**3*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 + (2*complex(0,1)*lamHD1*vev**3*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 + (complex(0,1)*lamHD2*vev**3*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 + (8*complex(0,1)*lamH*vev*vevD**3*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 - (4*complex(0,1)*lamHD1*vev*vevD**3*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 - (2*complex(0,1)*lamHD2*vev*vevD**3*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2',
                  order = {'QED':2})

GC_121 = Coupling(name = 'GC_121',
                  value = '(4*complex(0,1)*lamH*vev**3*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 - (2*complex(0,1)*lamHD1*vev**3*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 - (complex(0,1)*lamHD2*vev**3*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 - (8*complex(0,1)*lamD1*vev*vevD**3*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 - (4*complex(0,1)*lamD2*vev*vevD**3*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 + (4*complex(0,1)*lamHD1*vev*vevD**3*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2 + (2*complex(0,1)*lamHD2*vev*vevD**3*cmath.sqrt(2))/(vev**2 + 2*vevD**2)**2',
                  order = {'QED':2})

GC_122 = Coupling(name = 'GC_122',
                  value = '(-4*complex(0,1)*lamH*vev**4)/(vev**2 + 2*vevD**2)**2 - (8*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (4*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (16*complex(0,1)*lamD1*vevD**4)/(vev**2 + 2*vevD**2)**2 - (8*complex(0,1)*lamD2*vevD**4)/(vev**2 + 2*vevD**2)**2',
                  order = {'QED':2})

GC_123 = Coupling(name = 'GC_123',
                  value = '(-4*complex(0,1)*lamD1*vev**4)/(vev**2 + 2*vevD**2)**2 - (2*complex(0,1)*lamD2*vev**4)/(vev**2 + 2*vevD**2)**2 - (8*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (4*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (16*complex(0,1)*lamH*vevD**4)/(vev**2 + 2*vevD**2)**2',
                  order = {'QED':2})

GC_124 = Coupling(name = 'GC_124',
                  value = '-((complex(0,1)*lamHD1*vev**4)/(vev**2 + 2*vevD**2)**2) - (complex(0,1)*lamHD2*vev**4)/(2.*(vev**2 + 2*vevD**2)**2) - (8*complex(0,1)*lamD1*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (4*complex(0,1)*lamD2*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (8*complex(0,1)*lamH*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 + (4*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 + (2*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**2 + 2*vevD**2)**2 - (4*complex(0,1)*lamHD1*vevD**4)/(vev**2 + 2*vevD**2)**2 - (2*complex(0,1)*lamHD2*vevD**4)/(vev**2 + 2*vevD**2)**2',
                  order = {'QED':2})

GC_125 = Coupling(name = 'GC_125',
                  value = '(-2*complex(0,1)*lamD1*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamD2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD1*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_126 = Coupling(name = 'GC_126',
                  value = '(complex(0,1)*lamD2*sxi*vev**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (cxi*complex(0,1)*lamHD2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_127 = Coupling(name = 'GC_127',
                  value = '-((cw*ee*complex(0,1)*vev*vevD)/(sw*(vev**2 + 2*vevD**2)*cmath.sqrt(2))) - (ee*complex(0,1)*sw*vev*vevD)/(cw*(vev**2 + 2*vevD**2)*cmath.sqrt(2))',
                  order = {'QED':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(cw*ee*complex(0,1)*vev*vevD)/(sw*(vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (ee*complex(0,1)*sw*vev*vevD)/(cw*(vev**2 + 2*vevD**2)*cmath.sqrt(2))',
                  order = {'QED':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '-((cw*ee**2*complex(0,1)*vev*vevD*cmath.sqrt(2))/(sw*(vev**2 + 2*vevD**2))) - (ee**2*complex(0,1)*sw*vev*vevD*cmath.sqrt(2))/(cw*(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_13 = Coupling(name = 'GC_13',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_130 = Coupling(name = 'GC_130',
                  value = '-((cw**2*ee**2*complex(0,1)*vev*vevD)/(sw**2*(vev**2 + 2*vevD**2)*cmath.sqrt(2))) + (3*ee**2*complex(0,1)*sw**2*vev*vevD)/(cw**2*(vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (ee**2*complex(0,1)*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_131 = Coupling(name = 'GC_131',
                  value = '(cxi*complex(0,1)*lamD2*vev**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD2*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_132 = Coupling(name = 'GC_132',
                  value = '(complex(0,1)*lamD2*vev**2*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD2*vev**2*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '-((complex(0,1)*lamHD1*vev**2)/(vev**2 + 2*vevD**2)) - (complex(0,1)*lamHD2*vev**2)/(vev**2 + 2*vevD**2) - (4*complex(0,1)*lamD1*vevD**2)/(vev**2 + 2*vevD**2) - (4*complex(0,1)*lamD2*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_134 = Coupling(name = 'GC_134',
                  value = '-((complex(0,1)*lamHD2*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)) + (2*cxi*complex(0,1)*lamD2*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_135 = Coupling(name = 'GC_135',
                  value = '(-2*complex(0,1)*lamD1*vev**2)/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamD2*vev**2)/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamHD1*vevD**2)/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamHD2*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_136 = Coupling(name = 'GC_136',
                  value = '(cxi*complex(0,1)*lamHD2*vev**2)/(2.*(vev**2 + 2*vevD**2)) + (2*complex(0,1)*lamD2*sxi*vev*vevD)/(vev**2 + 2*vevD**2) - (cxi*complex(0,1)*lamHD2*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_137 = Coupling(name = 'GC_137',
                  value = '(cxi*complex(0,1)*muHD*vev**2)/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*muHD*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_138 = Coupling(name = 'GC_138',
                  value = '(2*ee**2*complex(0,1)*vev**2)/(sw**2*(vev**2 + 2*vevD**2)) + (ee**2*complex(0,1)*vevD**2)/(sw**2*(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_139 = Coupling(name = 'GC_139',
                  value = '(ee**2*complex(0,1)*vev**2)/(2.*sw**2*(vev**2 + 2*vevD**2)) + (4*ee**2*complex(0,1)*vevD**2)/(sw**2*(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '-4*complex(0,1)*lamD1 - 4*complex(0,1)*lamD2',
                 order = {'QED':2})

GC_140 = Coupling(name = 'GC_140',
                  value = '(ee*complex(0,1)*sw*vev**2)/(cw*(vev**2 + 2*vevD**2)) - (cw*ee*complex(0,1)*vevD**2)/(sw*(vev**2 + 2*vevD**2)) + (ee*complex(0,1)*sw*vevD**2)/(cw*(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '-0.5*(cw*ee*complex(0,1)*vev**2)/(sw*(vev**2 + 2*vevD**2)) + (ee*complex(0,1)*sw*vev**2)/(2.*cw*(vev**2 + 2*vevD**2)) + (2*ee*complex(0,1)*sw*vevD**2)/(cw*(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '(-2*ee**2*complex(0,1)*sw*vev**2)/(cw*(vev**2 + 2*vevD**2)) + (2*cw*ee**2*complex(0,1)*vevD**2)/(sw*(vev**2 + 2*vevD**2)) - (2*ee**2*complex(0,1)*sw*vevD**2)/(cw*(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_143 = Coupling(name = 'GC_143',
                  value = '(cw*ee**2*complex(0,1)*vev**2)/(sw*(vev**2 + 2*vevD**2)) - (ee**2*complex(0,1)*sw*vev**2)/(cw*(vev**2 + 2*vevD**2)) - (4*ee**2*complex(0,1)*sw*vevD**2)/(cw*(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_144 = Coupling(name = 'GC_144',
                  value = '(2*ee**2*complex(0,1)*sw**2*vev**2)/(cw**2*(vev**2 + 2*vevD**2)) - (2*ee**2*complex(0,1)*vevD**2)/(vev**2 + 2*vevD**2) + (cw**2*ee**2*complex(0,1)*vevD**2)/(sw**2*(vev**2 + 2*vevD**2)) + (ee**2*complex(0,1)*sw**2*vevD**2)/(cw**2*(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_145 = Coupling(name = 'GC_145',
                  value = '-((ee**2*complex(0,1)*vev**2)/(vev**2 + 2*vevD**2)) + (cw**2*ee**2*complex(0,1)*vev**2)/(2.*sw**2*(vev**2 + 2*vevD**2)) + (ee**2*complex(0,1)*sw**2*vev**2)/(2.*cw**2*(vev**2 + 2*vevD**2)) + (4*ee**2*complex(0,1)*sw**2*vevD**2)/(cw**2*(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_146 = Coupling(name = 'GC_146',
                  value = '(cxi*complex(0,1)*lamHD2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (2*complex(0,1)*lamD2*sxi*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_147 = Coupling(name = 'GC_147',
                  value = '(-2*cxi*complex(0,1)*lamD1*sxi*vev**2)/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamD2*sxi*vev**2)/(vev**2 + 2*vevD**2) + (cxi*complex(0,1)*lamHD1*sxi*vev**2)/(vev**2 + 2*vevD**2) + (cxi*complex(0,1)*lamHD2*sxi*vev**2)/(2.*(vev**2 + 2*vevD**2)) + (cxi**2*complex(0,1)*lamHD2*vev*vevD)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD2*sxi**2*vev*vevD)/(vev**2 + 2*vevD**2) + (4*cxi*complex(0,1)*lamH*sxi*vevD**2)/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamHD1*sxi*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_148 = Coupling(name = 'GC_148',
                  value = '-0.5*(complex(0,1)*lamHD2*sxi*vev**2)/(vev**2 + 2*vevD**2) + (2*cxi*complex(0,1)*lamD2*vev*vevD)/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD2*sxi*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_149 = Coupling(name = 'GC_149',
                  value = '(2*cxi*complex(0,1)*lamH*sxi*vev**2)/(vev**2 + 2*vevD**2) - (cxi*complex(0,1)*lamHD1*sxi*vev**2)/(vev**2 + 2*vevD**2) - (cxi**2*complex(0,1)*lamHD2*vev*vevD)/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD2*sxi**2*vev*vevD)/(vev**2 + 2*vevD**2) - (4*cxi*complex(0,1)*lamD1*sxi*vevD**2)/(vev**2 + 2*vevD**2) - (4*cxi*complex(0,1)*lamD2*sxi*vevD**2)/(vev**2 + 2*vevD**2) + (2*cxi*complex(0,1)*lamHD1*sxi*vevD**2)/(vev**2 + 2*vevD**2) + (cxi*complex(0,1)*lamHD2*sxi*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_15 = Coupling(name = 'GC_15',
                 value = '-((ee**2*complex(0,1))/sw**2)',
                 order = {'QED':2})

GC_150 = Coupling(name = 'GC_150',
                  value = '(cxi*complex(0,1)*lamHD2*sxi*vev**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) - (complex(0,1)*lamHD2*sxi**2*vev*vevD)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) - (2*cxi**2*complex(0,1)*lamD1*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*cxi**2*complex(0,1)*lamD2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (cxi**2*complex(0,1)*lamHD1*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (2*complex(0,1)*lamH*sxi**2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD1*sxi**2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (cxi*complex(0,1)*lamHD2*sxi*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_151 = Coupling(name = 'GC_151',
                  value = '-((cxi*complex(0,1)*lamHD2*sxi*vev**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(2))) - (cxi**2*complex(0,1)*lamHD2*vev*vevD)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (2*cxi**2*complex(0,1)*lamH*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (cxi**2*complex(0,1)*lamHD1*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamD1*sxi**2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamD2*sxi**2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD1*sxi**2*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (cxi*complex(0,1)*lamHD2*sxi*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_152 = Coupling(name = 'GC_152',
                  value = '-((complex(0,1)*muHD*sxi*vev**2)/(vev**2 + 2*vevD**2)) + (2*complex(0,1)*muHD*sxi*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_153 = Coupling(name = 'GC_153',
                  value = '(-2*cxi**2*complex(0,1)*lamH*vev**2)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD1*sxi**2*vev**2)/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamHD2*sxi*vev*vevD)/(vev**2 + 2*vevD**2) - (2*cxi**2*complex(0,1)*lamHD1*vevD**2)/(vev**2 + 2*vevD**2) - (cxi**2*complex(0,1)*lamHD2*vevD**2)/(vev**2 + 2*vevD**2) - (4*complex(0,1)*lamD1*sxi**2*vevD**2)/(vev**2 + 2*vevD**2) - (4*complex(0,1)*lamD2*sxi**2*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = '(-2*cxi**2*complex(0,1)*lamD1*vev**2)/(vev**2 + 2*vevD**2) - (2*cxi**2*complex(0,1)*lamD2*vev**2)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD1*sxi**2*vev**2)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD2*sxi**2*vev**2)/(2.*(vev**2 + 2*vevD**2)) - (2*cxi*complex(0,1)*lamHD2*sxi*vev*vevD)/(vev**2 + 2*vevD**2) - (2*cxi**2*complex(0,1)*lamHD1*vevD**2)/(vev**2 + 2*vevD**2) - (4*complex(0,1)*lamH*sxi**2*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_155 = Coupling(name = 'GC_155',
                  value = '-((cxi**2*complex(0,1)*lamHD1*vev**2)/(vev**2 + 2*vevD**2)) - (cxi**2*complex(0,1)*lamHD2*vev**2)/(2.*(vev**2 + 2*vevD**2)) - (2*complex(0,1)*lamD1*sxi**2*vev**2)/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamD2*sxi**2*vev**2)/(vev**2 + 2*vevD**2) + (2*cxi*complex(0,1)*lamHD2*sxi*vev*vevD)/(vev**2 + 2*vevD**2) - (4*cxi**2*complex(0,1)*lamH*vevD**2)/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamHD1*sxi**2*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = '-((cxi**2*complex(0,1)*lamHD1*vev**2)/(vev**2 + 2*vevD**2)) - (2*complex(0,1)*lamH*sxi**2*vev**2)/(vev**2 + 2*vevD**2) + (2*cxi*complex(0,1)*lamHD2*sxi*vev*vevD)/(vev**2 + 2*vevD**2) - (4*cxi**2*complex(0,1)*lamD1*vevD**2)/(vev**2 + 2*vevD**2) - (4*cxi**2*complex(0,1)*lamD2*vevD**2)/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamHD1*sxi**2*vevD**2)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD2*sxi**2*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_157 = Coupling(name = 'GC_157',
                  value = '-0.5*(cxi**2*complex(0,1)*lamHD2*vev**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (complex(0,1)*lamHD2*sxi**2*vev**2)/(2.*(vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (cxi*complex(0,1)*lamHD2*sxi*vev*vevD)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (cxi**2*complex(0,1)*lamHD2*vevD**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) - (complex(0,1)*lamHD2*sxi**2*vevD**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) - (2*cxi*complex(0,1)*lamD1*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamD2*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamH*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (2*cxi*complex(0,1)*lamHD1*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':2})

GC_158 = Coupling(name = 'GC_158',
                  value = '(complex(0,1)*lamHD2*vev**3)/(2.*(vev**2 + 2*vevD**2)) + (2*complex(0,1)*lamD2*vev*vevD**2)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD2*vev*vevD**2)/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_159 = Coupling(name = 'GC_159',
                  value = '(complex(0,1)*lamHD2*vev**2*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (2*complex(0,1)*lamD2*vevD**3*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '(ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_160 = Coupling(name = 'GC_160',
                  value = '(2*complex(0,1)*lamH*sxi*vev**3)/(vev**2 + 2*vevD**2) - (cxi*complex(0,1)*lamHD1*vev**2*vevD)/(vev**2 + 2*vevD**2) - (cxi*complex(0,1)*lamHD2*vev**2*vevD)/(vev**2 + 2*vevD**2) + (2*complex(0,1)*lamHD1*sxi*vev*vevD**2)/(vev**2 + 2*vevD**2) + (2*complex(0,1)*lamHD2*sxi*vev*vevD**2)/(vev**2 + 2*vevD**2) - (4*cxi*complex(0,1)*lamD1*vevD**3)/(vev**2 + 2*vevD**2) - (4*cxi*complex(0,1)*lamD2*vevD**3)/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_161 = Coupling(name = 'GC_161',
                  value = '(complex(0,1)*lamHD1*sxi*vev**3)/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD2*sxi*vev**3)/(2.*(vev**2 + 2*vevD**2)) - (2*cxi*complex(0,1)*lamD1*vev**2*vevD)/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamD2*vev**2*vevD)/(vev**2 + 2*vevD**2) + (cxi*complex(0,1)*lamHD2*vev**2*vevD)/(vev**2 + 2*vevD**2) + (4*complex(0,1)*lamH*sxi*vev*vevD**2)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD2*sxi*vev*vevD**2)/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamHD1*vevD**3)/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '-0.5*(complex(0,1)*lamHD2*sxi*vev**3)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) - (3*cxi*complex(0,1)*lamHD2*vev**2*vevD)/(2.*(vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (complex(0,1)*lamHD2*sxi*vev*vevD**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (cxi*complex(0,1)*lamHD2*vevD**3)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (2*cxi*complex(0,1)*lamH*vev**2*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (cxi*complex(0,1)*lamHD1*vev**2*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamD1*sxi*vev*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamD2*sxi*vev*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD1*sxi*vev*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '(-2*cxi*complex(0,1)*lamH*vev**3)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD1*sxi*vev**2*vevD)/(vev**2 + 2*vevD**2) - (complex(0,1)*lamHD2*sxi*vev**2*vevD)/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamHD1*vev*vevD**2)/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamHD2*vev*vevD**2)/(vev**2 + 2*vevD**2) - (4*complex(0,1)*lamD1*sxi*vevD**3)/(vev**2 + 2*vevD**2) - (4*complex(0,1)*lamD2*sxi*vevD**3)/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '-((cxi*complex(0,1)*lamHD1*vev**3)/(vev**2 + 2*vevD**2)) - (cxi*complex(0,1)*lamHD2*vev**3)/(2.*(vev**2 + 2*vevD**2)) - (2*complex(0,1)*lamD1*sxi*vev**2*vevD)/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamD2*sxi*vev**2*vevD)/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD2*sxi*vev**2*vevD)/(vev**2 + 2*vevD**2) - (4*cxi*complex(0,1)*lamH*vev*vevD**2)/(vev**2 + 2*vevD**2) + (cxi*complex(0,1)*lamHD2*vev*vevD**2)/(vev**2 + 2*vevD**2) - (2*complex(0,1)*lamHD1*sxi*vevD**3)/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '-0.5*(cxi*complex(0,1)*lamHD2*vev**3)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (3*complex(0,1)*lamHD2*sxi*vev**2*vevD)/(2.*(vev**2 + 2*vevD**2)*cmath.sqrt(2)) + (cxi*complex(0,1)*lamHD2*vev*vevD**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) - (complex(0,1)*lamHD2*sxi*vevD**3)/((vev**2 + 2*vevD**2)*cmath.sqrt(2)) - (2*complex(0,1)*lamH*sxi*vev**2*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (complex(0,1)*lamHD1*sxi*vev**2*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamD1*vev*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2) - (2*cxi*complex(0,1)*lamD2*vev*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2) + (cxi*complex(0,1)*lamHD1*vev*vevD**2*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '(2*ee**2*complex(0,1)*vev)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)) - (cw*ee**2*complex(0,1)*vev)/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_167 = Coupling(name = 'GC_167',
                  value = '-((cw*ee**2*complex(0,1)*sxi*vev)/(sw**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) + (cxi*ee**2*complex(0,1)*vevD)/(cw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (ee**2*complex(0,1)*sxi*vev*cmath.sqrt(2))/(cw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_168 = Coupling(name = 'GC_168',
                  value = '(ee**2*complex(0,1)*sxi*vev)/(2.*cw*cmath.sqrt(vev**2 + 2*vevD**2)) - (2*cxi*ee**2*complex(0,1)*vevD)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)) - (cw*cxi*ee**2*complex(0,1)*vevD)/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_169 = Coupling(name = 'GC_169',
                  value = '(ee*complex(0,1)*sxi*vev)/(2.*sw*cmath.sqrt(vev**2 + 2*vevD**2)) - (cxi*ee*complex(0,1)*vevD)/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '(cw**2*ee**2*complex(0,1))/sw**2',
                 order = {'QED':2})

GC_170 = Coupling(name = 'GC_170',
                  value = '-0.5*(ee*complex(0,1)*sxi*vev)/(sw*cmath.sqrt(vev**2 + 2*vevD**2)) + (cxi*ee*complex(0,1)*vevD)/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '(ee*complex(0,1)*sxi*vev)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (cxi*ee*complex(0,1)*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '-((ee*complex(0,1)*sxi*vev)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) + (cxi*ee*complex(0,1)*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '-0.5*(ee**2*complex(0,1)*sxi*vev)/(sw*cmath.sqrt(vev**2 + 2*vevD**2)) + (cxi*ee**2*complex(0,1)*vevD)/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_174 = Coupling(name = 'GC_174',
                  value = '(ee**2*complex(0,1)*sxi*vev)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (cxi*ee**2*complex(0,1)*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_175 = Coupling(name = 'GC_175',
                  value = '-((cw*cxi*ee**2*complex(0,1)*vev)/(sw**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (ee**2*complex(0,1)*sxi*vevD)/(cw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (cxi*ee**2*complex(0,1)*vev*cmath.sqrt(2))/(cw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_176 = Coupling(name = 'GC_176',
                  value = '-0.5*(cxi*ee**2*complex(0,1)*vev)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)) - (2*ee**2*complex(0,1)*sxi*vevD)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)) - (cw*ee**2*complex(0,1)*sxi*vevD)/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_177 = Coupling(name = 'GC_177',
                  value = '-0.5*(cxi*ee*complex(0,1)*vev)/(sw*cmath.sqrt(vev**2 + 2*vevD**2)) - (ee*complex(0,1)*sxi*vevD)/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '(cxi*ee*complex(0,1)*vev)/(2.*sw*cmath.sqrt(vev**2 + 2*vevD**2)) + (ee*complex(0,1)*sxi*vevD)/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-((cxi*ee*complex(0,1)*vev)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (ee*complex(0,1)*sxi*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_18 = Coupling(name = 'GC_18',
                 value = '-((cxi*ee**2*complex(0,1)*cmath.sqrt(2))/sw**2)',
                 order = {'QED':2})

GC_180 = Coupling(name = 'GC_180',
                  value = '(cxi*ee*complex(0,1)*vev)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) + (ee*complex(0,1)*sxi*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '(cxi*ee**2*complex(0,1)*vev)/(2.*sw*cmath.sqrt(vev**2 + 2*vevD**2)) + (ee**2*complex(0,1)*sxi*vevD)/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_182 = Coupling(name = 'GC_182',
                  value = '(cxi*ee**2*complex(0,1)*vev)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) + (ee**2*complex(0,1)*sxi*vevD)/(sw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':2})

GC_183 = Coupling(name = 'GC_183',
                  value = '-((ee**2*complex(0,1)*vev*vevD)/(cw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (cw*ee**2*complex(0,1)*vev*vevD)/(sw**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '-0.5*(ee**2*complex(0,1)*vev**2)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)) - (2*ee**2*complex(0,1)*vevD**2)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)) - (cw*ee**2*complex(0,1)*vevD**2)/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '(-12*complex(0,1)*lamD1*vev**3*vevD)/(vev**2 + 4*vevD**2)**2 - (12*complex(0,1)*lamD2*vev**3*vevD)/(vev**2 + 4*vevD**2)**2 + (6*complex(0,1)*lamHD1*vev**3*vevD)/(vev**2 + 4*vevD**2)**2 + (6*complex(0,1)*lamHD2*vev**3*vevD)/(vev**2 + 4*vevD**2)**2 + (48*complex(0,1)*lamH*vev*vevD**3)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamHD1*vev*vevD**3)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamHD2*vev*vevD**3)/(vev**2 + 4*vevD**2)**2',
                  order = {'QED':2})

GC_186 = Coupling(name = 'GC_186',
                  value = '(12*complex(0,1)*lamH*vev**3*vevD)/(vev**2 + 4*vevD**2)**2 - (6*complex(0,1)*lamHD1*vev**3*vevD)/(vev**2 + 4*vevD**2)**2 - (6*complex(0,1)*lamHD2*vev**3*vevD)/(vev**2 + 4*vevD**2)**2 - (48*complex(0,1)*lamD1*vev*vevD**3)/(vev**2 + 4*vevD**2)**2 - (48*complex(0,1)*lamD2*vev*vevD**3)/(vev**2 + 4*vevD**2)**2 + (24*complex(0,1)*lamHD1*vev*vevD**3)/(vev**2 + 4*vevD**2)**2 + (24*complex(0,1)*lamHD2*vev*vevD**3)/(vev**2 + 4*vevD**2)**2',
                  order = {'QED':2})

GC_187 = Coupling(name = 'GC_187',
                  value = '(-6*complex(0,1)*lamH*vev**4)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 - (96*complex(0,1)*lamD1*vevD**4)/(vev**2 + 4*vevD**2)**2 - (96*complex(0,1)*lamD2*vevD**4)/(vev**2 + 4*vevD**2)**2',
                  order = {'QED':2})

GC_188 = Coupling(name = 'GC_188',
                  value = '(-6*complex(0,1)*lamD1*vev**4)/(vev**2 + 4*vevD**2)**2 - (6*complex(0,1)*lamD2*vev**4)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 - (96*complex(0,1)*lamH*vevD**4)/(vev**2 + 4*vevD**2)**2',
                  order = {'QED':2})

GC_189 = Coupling(name = 'GC_189',
                  value = '-((complex(0,1)*lamHD1*vev**4)/(vev**2 + 4*vevD**2)**2) - (complex(0,1)*lamHD2*vev**4)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamD1*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamD2*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 - (24*complex(0,1)*lamH*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 + (16*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 + (16*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**2 + 4*vevD**2)**2 - (16*complex(0,1)*lamHD1*vevD**4)/(vev**2 + 4*vevD**2)**2 - (16*complex(0,1)*lamHD2*vevD**4)/(vev**2 + 4*vevD**2)**2',
                  order = {'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '(-4*complex(0,1)*lamD1*vev*vevD)/(vev**2 + 4*vevD**2) + (2*complex(0,1)*lamHD1*vev*vevD)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_191 = Coupling(name = 'GC_191',
                  value = '-((complex(0,1)*muHD*sxi*vev**2*cmath.sqrt(2))/(vev**2 + 4*vevD**2)) + (4*cxi*complex(0,1)*muHD*vev*vevD*cmath.sqrt(2))/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_192 = Coupling(name = 'GC_192',
                  value = '(6*ee**2*complex(0,1)*vev*vevD)/(vev**2 + 4*vevD**2) + (3*cw**2*ee**2*complex(0,1)*vev*vevD)/(sw**2*(vev**2 + 4*vevD**2)) + (3*ee**2*complex(0,1)*sw**2*vev*vevD)/(cw**2*(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_193 = Coupling(name = 'GC_193',
                  value = '(-4*cxi*complex(0,1)*lamD1*sxi*vev*vevD)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamD2*sxi*vev*vevD)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamH*sxi*vev*vevD)/(vev**2 + 4*vevD**2) + (4*cxi*complex(0,1)*lamHD1*sxi*vev*vevD)/(vev**2 + 4*vevD**2) + (4*cxi*complex(0,1)*lamHD2*sxi*vev*vevD)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_194 = Coupling(name = 'GC_194',
                  value = '-((cxi*complex(0,1)*muHD*vev**2*cmath.sqrt(2))/(vev**2 + 4*vevD**2)) - (4*complex(0,1)*muHD*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_195 = Coupling(name = 'GC_195',
                  value = '(-4*cxi**2*complex(0,1)*lamD1*vev*vevD)/(vev**2 + 4*vevD**2) - (4*cxi**2*complex(0,1)*lamD2*vev*vevD)/(vev**2 + 4*vevD**2) + (2*cxi**2*complex(0,1)*lamHD1*vev*vevD)/(vev**2 + 4*vevD**2) + (2*cxi**2*complex(0,1)*lamHD2*vev*vevD)/(vev**2 + 4*vevD**2) + (4*complex(0,1)*lamH*sxi**2*vev*vevD)/(vev**2 + 4*vevD**2) - (2*complex(0,1)*lamHD1*sxi**2*vev*vevD)/(vev**2 + 4*vevD**2) - (2*complex(0,1)*lamHD2*sxi**2*vev*vevD)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_196 = Coupling(name = 'GC_196',
                  value = '(4*cxi**2*complex(0,1)*lamH*vev*vevD)/(vev**2 + 4*vevD**2) - (2*cxi**2*complex(0,1)*lamHD1*vev*vevD)/(vev**2 + 4*vevD**2) - (2*cxi**2*complex(0,1)*lamHD2*vev*vevD)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamD1*sxi**2*vev*vevD)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamD2*sxi**2*vev*vevD)/(vev**2 + 4*vevD**2) + (2*complex(0,1)*lamHD1*sxi**2*vev*vevD)/(vev**2 + 4*vevD**2) + (2*complex(0,1)*lamHD2*sxi**2*vev*vevD)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_197 = Coupling(name = 'GC_197',
                  value = '-((complex(0,1)*lamHD1*vev**2)/(vev**2 + 4*vevD**2)) - (8*complex(0,1)*lamD1*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_198 = Coupling(name = 'GC_198',
                  value = '(-2*complex(0,1)*lamD1*vev**2)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamHD1*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_199 = Coupling(name = 'GC_199',
                  value = '(cxi*complex(0,1)*muHD*vev**2*cmath.sqrt(2))/(vev**2 + 4*vevD**2) + (2*complex(0,1)*muHD*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*muHD*vevD**2*cmath.sqrt(2))/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_20 = Coupling(name = 'GC_20',
                 value = '-((cw*ee*complex(0,1))/sw)',
                 order = {'QED':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '(4*complex(0,1)*muHD*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*muHD*vevD**2*cmath.sqrt(2))/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_201 = Coupling(name = 'GC_201',
                  value = '(ee**2*complex(0,1)*vev**2)/(sw**2*(vev**2 + 4*vevD**2)) + (2*ee**2*complex(0,1)*vevD**2)/(sw**2*(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_202 = Coupling(name = 'GC_202',
                  value = '(ee**2*complex(0,1)*vev**2)/(2.*sw**2*(vev**2 + 4*vevD**2)) + (4*ee**2*complex(0,1)*vevD**2)/(sw**2*(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_203 = Coupling(name = 'GC_203',
                  value = '(4*ee**2*complex(0,1)*vev**2)/(vev**2 + 4*vevD**2) + (2*cw**2*ee**2*complex(0,1)*vev**2)/(sw**2*(vev**2 + 4*vevD**2)) + (2*ee**2*complex(0,1)*sw**2*vev**2)/(cw**2*(vev**2 + 4*vevD**2)) + (4*ee**2*complex(0,1)*vevD**2)/(vev**2 + 4*vevD**2) + (2*cw**2*ee**2*complex(0,1)*vevD**2)/(sw**2*(vev**2 + 4*vevD**2)) + (2*ee**2*complex(0,1)*sw**2*vevD**2)/(cw**2*(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_204 = Coupling(name = 'GC_204',
                  value = '(ee**2*complex(0,1)*vev**2)/(vev**2 + 4*vevD**2) + (cw**2*ee**2*complex(0,1)*vev**2)/(2.*sw**2*(vev**2 + 4*vevD**2)) + (ee**2*complex(0,1)*sw**2*vev**2)/(2.*cw**2*(vev**2 + 4*vevD**2)) + (16*ee**2*complex(0,1)*vevD**2)/(vev**2 + 4*vevD**2) + (8*cw**2*ee**2*complex(0,1)*vevD**2)/(sw**2*(vev**2 + 4*vevD**2)) + (8*ee**2*complex(0,1)*sw**2*vevD**2)/(cw**2*(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_205 = Coupling(name = 'GC_205',
                  value = '(-2*cxi*complex(0,1)*lamD1*sxi*vev**2)/(vev**2 + 4*vevD**2) - (2*cxi*complex(0,1)*lamD2*sxi*vev**2)/(vev**2 + 4*vevD**2) + (cxi*complex(0,1)*lamHD1*sxi*vev**2)/(vev**2 + 4*vevD**2) + (cxi*complex(0,1)*lamHD2*sxi*vev**2)/(vev**2 + 4*vevD**2) + (8*cxi*complex(0,1)*lamH*sxi*vevD**2)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamHD1*sxi*vevD**2)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamHD2*sxi*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_206 = Coupling(name = 'GC_206',
                  value = '(2*cxi*complex(0,1)*lamH*sxi*vev**2)/(vev**2 + 4*vevD**2) - (cxi*complex(0,1)*lamHD1*sxi*vev**2)/(vev**2 + 4*vevD**2) - (cxi*complex(0,1)*lamHD2*sxi*vev**2)/(vev**2 + 4*vevD**2) - (8*cxi*complex(0,1)*lamD1*sxi*vevD**2)/(vev**2 + 4*vevD**2) - (8*cxi*complex(0,1)*lamD2*sxi*vevD**2)/(vev**2 + 4*vevD**2) + (4*cxi*complex(0,1)*lamHD1*sxi*vevD**2)/(vev**2 + 4*vevD**2) + (4*cxi*complex(0,1)*lamHD2*sxi*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_207 = Coupling(name = 'GC_207',
                  value = '(-4*cxi*complex(0,1)*muHD*vev*vevD*cmath.sqrt(2))/(vev**2 + 4*vevD**2) - (4*complex(0,1)*muHD*sxi*vevD**2*cmath.sqrt(2))/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_208 = Coupling(name = 'GC_208',
                  value = '-((complex(0,1)*muHD*sxi*vev**2*cmath.sqrt(2))/(vev**2 + 4*vevD**2)) + (2*cxi*complex(0,1)*muHD*vev*vevD*cmath.sqrt(2))/(vev**2 + 4*vevD**2) + (4*complex(0,1)*muHD*sxi*vevD**2*cmath.sqrt(2))/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_209 = Coupling(name = 'GC_209',
                  value = '(-2*cxi**2*complex(0,1)*lamH*vev**2)/(vev**2 + 4*vevD**2) - (complex(0,1)*lamHD1*sxi**2*vev**2)/(vev**2 + 4*vevD**2) - (complex(0,1)*lamHD2*sxi**2*vev**2)/(vev**2 + 4*vevD**2) - (4*cxi**2*complex(0,1)*lamHD1*vevD**2)/(vev**2 + 4*vevD**2) - (4*cxi**2*complex(0,1)*lamHD2*vevD**2)/(vev**2 + 4*vevD**2) - (8*complex(0,1)*lamD1*sxi**2*vevD**2)/(vev**2 + 4*vevD**2) - (8*complex(0,1)*lamD2*sxi**2*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_21 = Coupling(name = 'GC_21',
                 value = '(cw*ee*complex(0,1))/sw',
                 order = {'QED':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '(-2*cxi**2*complex(0,1)*lamD1*vev**2)/(vev**2 + 4*vevD**2) - (2*cxi**2*complex(0,1)*lamD2*vev**2)/(vev**2 + 4*vevD**2) - (complex(0,1)*lamHD1*sxi**2*vev**2)/(vev**2 + 4*vevD**2) - (complex(0,1)*lamHD2*sxi**2*vev**2)/(vev**2 + 4*vevD**2) - (4*cxi**2*complex(0,1)*lamHD1*vevD**2)/(vev**2 + 4*vevD**2) - (4*cxi**2*complex(0,1)*lamHD2*vevD**2)/(vev**2 + 4*vevD**2) - (8*complex(0,1)*lamH*sxi**2*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_211 = Coupling(name = 'GC_211',
                  value = '-((cxi**2*complex(0,1)*lamHD1*vev**2)/(vev**2 + 4*vevD**2)) - (cxi**2*complex(0,1)*lamHD2*vev**2)/(vev**2 + 4*vevD**2) - (2*complex(0,1)*lamH*sxi**2*vev**2)/(vev**2 + 4*vevD**2) - (8*cxi**2*complex(0,1)*lamD1*vevD**2)/(vev**2 + 4*vevD**2) - (8*cxi**2*complex(0,1)*lamD2*vevD**2)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamHD1*sxi**2*vevD**2)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamHD2*sxi**2*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_212 = Coupling(name = 'GC_212',
                  value = '-((cxi**2*complex(0,1)*lamHD1*vev**2)/(vev**2 + 4*vevD**2)) - (cxi**2*complex(0,1)*lamHD2*vev**2)/(vev**2 + 4*vevD**2) - (2*complex(0,1)*lamD1*sxi**2*vev**2)/(vev**2 + 4*vevD**2) - (2*complex(0,1)*lamD2*sxi**2*vev**2)/(vev**2 + 4*vevD**2) - (8*cxi**2*complex(0,1)*lamH*vevD**2)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamHD1*sxi**2*vevD**2)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamHD2*sxi**2*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':2})

GC_213 = Coupling(name = 'GC_213',
                  value = '(-4*complex(0,1)*lamH*sxi*vev**2*vevD)/(vev**2 + 4*vevD**2) + (2*complex(0,1)*lamHD1*sxi*vev**2*vevD)/(vev**2 + 4*vevD**2) + (2*complex(0,1)*lamHD2*sxi*vev**2*vevD)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamD1*vev*vevD**2)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamD2*vev*vevD**2)/(vev**2 + 4*vevD**2) + (2*cxi*complex(0,1)*lamHD1*vev*vevD**2)/(vev**2 + 4*vevD**2) + (2*cxi*complex(0,1)*lamHD2*vev*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '(4*cxi*complex(0,1)*lamH*vev**2*vevD)/(vev**2 + 4*vevD**2) - (2*cxi*complex(0,1)*lamHD1*vev**2*vevD)/(vev**2 + 4*vevD**2) - (2*cxi*complex(0,1)*lamHD2*vev**2*vevD)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamD1*sxi*vev*vevD**2)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamD2*sxi*vev*vevD**2)/(vev**2 + 4*vevD**2) + (2*complex(0,1)*lamHD1*sxi*vev*vevD**2)/(vev**2 + 4*vevD**2) + (2*complex(0,1)*lamHD2*sxi*vev*vevD**2)/(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '(2*complex(0,1)*lamH*sxi*vev**3)/(vev**2 + 4*vevD**2) - (cxi*complex(0,1)*lamHD1*vev**2*vevD)/(vev**2 + 4*vevD**2) - (cxi*complex(0,1)*lamHD2*vev**2*vevD)/(vev**2 + 4*vevD**2) + (4*complex(0,1)*lamHD1*sxi*vev*vevD**2)/(vev**2 + 4*vevD**2) + (4*complex(0,1)*lamHD2*sxi*vev*vevD**2)/(vev**2 + 4*vevD**2) - (8*cxi*complex(0,1)*lamD1*vevD**3)/(vev**2 + 4*vevD**2) - (8*cxi*complex(0,1)*lamD2*vevD**3)/(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '(complex(0,1)*lamHD1*sxi*vev**3)/(vev**2 + 4*vevD**2) + (complex(0,1)*lamHD2*sxi*vev**3)/(vev**2 + 4*vevD**2) - (2*cxi*complex(0,1)*lamD1*vev**2*vevD)/(vev**2 + 4*vevD**2) - (2*cxi*complex(0,1)*lamD2*vev**2*vevD)/(vev**2 + 4*vevD**2) + (8*complex(0,1)*lamH*sxi*vev*vevD**2)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamHD1*vevD**3)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamHD2*vevD**3)/(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '(-2*cxi*complex(0,1)*lamH*vev**3)/(vev**2 + 4*vevD**2) - (complex(0,1)*lamHD1*sxi*vev**2*vevD)/(vev**2 + 4*vevD**2) - (complex(0,1)*lamHD2*sxi*vev**2*vevD)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamHD1*vev*vevD**2)/(vev**2 + 4*vevD**2) - (4*cxi*complex(0,1)*lamHD2*vev*vevD**2)/(vev**2 + 4*vevD**2) - (8*complex(0,1)*lamD1*sxi*vevD**3)/(vev**2 + 4*vevD**2) - (8*complex(0,1)*lamD2*sxi*vevD**3)/(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '-((cxi*complex(0,1)*lamHD1*vev**3)/(vev**2 + 4*vevD**2)) - (cxi*complex(0,1)*lamHD2*vev**3)/(vev**2 + 4*vevD**2) - (2*complex(0,1)*lamD1*sxi*vev**2*vevD)/(vev**2 + 4*vevD**2) - (2*complex(0,1)*lamD2*sxi*vev**2*vevD)/(vev**2 + 4*vevD**2) - (8*cxi*complex(0,1)*lamH*vev*vevD**2)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamHD1*sxi*vevD**3)/(vev**2 + 4*vevD**2) - (4*complex(0,1)*lamHD2*sxi*vevD**3)/(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '(lamHD2*sxi*vev)/(2.*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (cxi*lamHD2*vevD)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_22 = Coupling(name = 'GC_22',
                 value = '(-2*cw*ee**2*complex(0,1))/sw',
                 order = {'QED':2})

GC_220 = Coupling(name = 'GC_220',
                  value = '-0.5*(lamHD2*sxi*vev)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (cxi*lamHD2*vevD)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_221 = Coupling(name = 'GC_221',
                  value = '-((cw*ee*sxi*vev)/(sw*cmath.sqrt(vev**2 + 4*vevD**2))) - (ee*sw*sxi*vev)/(cw*cmath.sqrt(vev**2 + 4*vevD**2)) + (cw*cxi*ee*vevD)/(sw*cmath.sqrt(vev**2 + 4*vevD**2)) + (cxi*ee*sw*vevD)/(cw*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '-0.5*(cw*ee*sxi*vev)/(sw*cmath.sqrt(vev**2 + 4*vevD**2)) - (ee*sw*sxi*vev)/(2.*cw*cmath.sqrt(vev**2 + 4*vevD**2)) + (2*cw*cxi*ee*vevD)/(sw*cmath.sqrt(vev**2 + 4*vevD**2)) + (2*cxi*ee*sw*vevD)/(cw*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '-0.5*(cxi*lamHD2*vev)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (lamHD2*sxi*vevD)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_224 = Coupling(name = 'GC_224',
                  value = '(cxi*lamHD2*vev)/(2.*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (lamHD2*sxi*vevD)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_225 = Coupling(name = 'GC_225',
                  value = '-0.5*(cw*cxi*ee*vev)/(sw*cmath.sqrt(vev**2 + 4*vevD**2)) - (cxi*ee*sw*vev)/(2.*cw*cmath.sqrt(vev**2 + 4*vevD**2)) - (2*cw*ee*sxi*vevD)/(sw*cmath.sqrt(vev**2 + 4*vevD**2)) - (2*ee*sw*sxi*vevD)/(cw*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_226 = Coupling(name = 'GC_226',
                  value = '-((cw*cxi*ee*vev)/(sw*cmath.sqrt(vev**2 + 4*vevD**2))) - (cxi*ee*sw*vev)/(cw*cmath.sqrt(vev**2 + 4*vevD**2)) - (cw*ee*sxi*vevD)/(sw*cmath.sqrt(vev**2 + 4*vevD**2)) - (ee*sw*sxi*vevD)/(cw*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_227 = Coupling(name = 'GC_227',
                  value = '-0.5*(lamHD2*vev**2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (lamHD2*vevD**2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_228 = Coupling(name = 'GC_228',
                  value = '(lamHD2*vev**2)/(2.*cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (lamHD2*vevD**2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_229 = Coupling(name = 'GC_229',
                  value = '(2*lamD2*vev**2*vevD*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (lamHD2*vev**2*vevD*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_23 = Coupling(name = 'GC_23',
                 value = '(ee*complex(0,1)*PMNS1x1)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_230 = Coupling(name = 'GC_230',
                  value = '(-2*lamD2*vev**2*vevD*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (lamHD2*vev**2*vevD*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_231 = Coupling(name = 'GC_231',
                  value = '(lamHD2*vev**3)/(2.*(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (4*lamD2*vev*vevD**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (lamHD2*vev*vevD**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_232 = Coupling(name = 'GC_232',
                  value = '-0.5*(lamHD2*vev**3)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (4*lamD2*vev*vevD**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (lamHD2*vev*vevD**2)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_233 = Coupling(name = 'GC_233',
                  value = '-((lamD2*vev**3*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))) - (2*lamHD2*vev*vevD**2*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_234 = Coupling(name = 'GC_234',
                  value = '(2*lamD2*vev*vevD**2*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (2*lamHD2*vev*vevD**2*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_235 = Coupling(name = 'GC_235',
                  value = '(lamD2*vev**3*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (2*lamHD2*vev*vevD**2*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_236 = Coupling(name = 'GC_236',
                  value = '(-2*lamD2*vev*vevD**2*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (2*lamHD2*vev*vevD**2*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_237 = Coupling(name = 'GC_237',
                  value = '-((lamHD2*vev**2*vevD*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))) - (4*lamD2*vevD**3*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_238 = Coupling(name = 'GC_238',
                  value = '(lamHD2*vev**2*vevD*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (4*lamD2*vevD**3*cmath.sqrt(2))/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_239 = Coupling(name = 'GC_239',
                  value = '(-2*lamD2*vev**2*vevD)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (lamHD2*vev**2*vevD)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (2*lamHD2*vevD**3)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_24 = Coupling(name = 'GC_24',
                 value = '(ee*complex(0,1)*PMNS1x2)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_240 = Coupling(name = 'GC_240',
                  value = '(2*lamD2*vev**2*vevD)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (lamHD2*vev**2*vevD)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (2*lamHD2*vevD**3)/((vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_241 = Coupling(name = 'GC_241',
                  value = '-((ee**2*vev*vevD)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))) - (cw*ee**2*vev*vevD)/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_242 = Coupling(name = 'GC_242',
                  value = '(ee**2*vev*vevD)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (cw*ee**2*vev*vevD)/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_243 = Coupling(name = 'GC_243',
                  value = '(-3*ee**2*vev*vevD)/(cw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (cw*ee**2*vev*vevD*cmath.sqrt(2))/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_244 = Coupling(name = 'GC_244',
                  value = '(3*ee**2*vev*vevD)/(cw*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (cw*ee**2*vev*vevD*cmath.sqrt(2))/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_245 = Coupling(name = 'GC_245',
                  value = '-((cw*ee**2*vev**2)/(sw**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))) - (ee**2*vev**2*cmath.sqrt(2))/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (ee**2*vevD**2*cmath.sqrt(2))/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_246 = Coupling(name = 'GC_246',
                  value = '(cw*ee**2*vev**2)/(sw**2*cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (ee**2*vev**2*cmath.sqrt(2))/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (ee**2*vevD**2*cmath.sqrt(2))/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_247 = Coupling(name = 'GC_247',
                  value = '-0.5*(ee**2*vev**2)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (4*ee**2*vevD**2)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (2*cw*ee**2*vevD**2)/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_248 = Coupling(name = 'GC_248',
                  value = '(ee**2*vev**2)/(2.*cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (4*ee**2*vevD**2)/(cw*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (2*cw*ee**2*vevD**2)/(sw**2*cmath.sqrt(vev**2 + 2*vevD**2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':2})

GC_249 = Coupling(name = 'GC_249',
                  value = '(-4*complex(0,1)*lamD1*vev**3*vevD)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamD2*vev**3*vevD)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (2*complex(0,1)*lamHD1*vev**3*vevD)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (2*complex(0,1)*lamHD2*vev**3*vevD)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (8*complex(0,1)*lamH*vev*vevD**3)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamHD1*vev*vevD**3)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamHD2*vev*vevD**3)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_25 = Coupling(name = 'GC_25',
                 value = '(ee*complex(0,1)*PMNS1x3)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '(4*complex(0,1)*lamH*vev**3*vevD)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (2*complex(0,1)*lamHD1*vev**3*vevD)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (complex(0,1)*lamHD2*vev**3*vevD)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (8*complex(0,1)*lamD1*vev*vevD**3)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (8*complex(0,1)*lamD2*vev*vevD**3)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (4*complex(0,1)*lamHD1*vev*vevD**3)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (6*complex(0,1)*lamHD2*vev*vevD**3)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_251 = Coupling(name = 'GC_251',
                  value = '(-3*complex(0,1)*lamHD2*vev**3*vevD)/((vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)*cmath.sqrt(2)) + (2*complex(0,1)*lamH*vev**3*vevD*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (complex(0,1)*lamHD1*vev**3*vevD*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (8*complex(0,1)*lamD1*vev*vevD**3*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (8*complex(0,1)*lamD2*vev*vevD**3*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (4*complex(0,1)*lamHD1*vev*vevD**3*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (2*complex(0,1)*lamHD2*vev*vevD**3*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_252 = Coupling(name = 'GC_252',
                  value = '(-2*complex(0,1)*lamD1*vev**3*vevD*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (2*complex(0,1)*lamD2*vev**3*vevD*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (complex(0,1)*lamHD1*vev**3*vevD*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (complex(0,1)*lamHD2*vev**3*vevD*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (8*complex(0,1)*lamH*vev*vevD**3*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamHD1*vev*vevD**3*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamHD2*vev*vevD**3*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_253 = Coupling(name = 'GC_253',
                  value = '(-2*complex(0,1)*lamH*vev**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (6*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (5*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (16*complex(0,1)*lamD1*vevD**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (16*complex(0,1)*lamD2*vevD**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_254 = Coupling(name = 'GC_254',
                  value = '(-2*complex(0,1)*lamD1*vev**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (2*complex(0,1)*lamD2*vev**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (6*complex(0,1)*lamHD1*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (6*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (16*complex(0,1)*lamH*vevD**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_255 = Coupling(name = 'GC_255',
                  value = '-((complex(0,1)*lamHD1*vev**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)) - (complex(0,1)*lamHD2*vev**4)/(2.*(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)) - (8*complex(0,1)*lamD1*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (8*complex(0,1)*lamD2*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamH*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (4*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (8*complex(0,1)*lamHD1*vevD**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_256 = Coupling(name = 'GC_256',
                  value = '-((complex(0,1)*lamHD1*vev**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)) - (4*complex(0,1)*lamD1*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamD2*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (8*complex(0,1)*lamH*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (4*complex(0,1)*lamHD2*vev**2*vevD**2)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (8*complex(0,1)*lamHD1*vevD**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamHD2*vevD**4)/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_257 = Coupling(name = 'GC_257',
                  value = '-0.5*(complex(0,1)*lamHD2*vev**4)/((vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)*cmath.sqrt(2)) + (5*complex(0,1)*lamHD2*vev**2*vevD**2)/((vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)*cmath.sqrt(2)) - (4*complex(0,1)*lamD1*vev**2*vevD**2*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamD2*vev**2*vevD**2*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (4*complex(0,1)*lamH*vev**2*vevD**2*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) + (4*complex(0,1)*lamHD1*vev**2*vevD**2*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4) - (2*complex(0,1)*lamHD2*vevD**4*cmath.sqrt(2))/(vev**4 + 6*vev**2*vevD**2 + 8*vevD**4)',
                  order = {'QED':2})

GC_258 = Coupling(name = 'GC_258',
                  value = '-((cxi*complex(0,1)*yb)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_259 = Coupling(name = 'GC_259',
                  value = '(complex(0,1)*sxi*yb)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '(ee*complex(0,1)*PMNS2x1)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '-((complex(0,1)*vev*yb)/cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_261 = Coupling(name = 'GC_261',
                  value = '(complex(0,1)*vevD*yb*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_262 = Coupling(name = 'GC_262',
                  value = '-((vev*yb)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)))',
                  order = {'QED':1})

GC_263 = Coupling(name = 'GC_263',
                  value = '(vev*yb)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_264 = Coupling(name = 'GC_264',
                  value = '-((vevD*yb*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_265 = Coupling(name = 'GC_265',
                  value = '(vevD*yb*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_266 = Coupling(name = 'GC_266',
                  value = '-2*complex(0,1)*yDL1x1',
                  order = {'QED':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '-(complex(0,1)*yDL1x2) - complex(0,1)*yDL2x1',
                  order = {'QED':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '-2*complex(0,1)*yDL2x2',
                  order = {'QED':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '-(complex(0,1)*yDL1x3) - complex(0,1)*yDL3x1',
                  order = {'QED':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '(ee*complex(0,1)*PMNS2x2)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_270 = Coupling(name = 'GC_270',
                  value = '-((complex(0,1)*PMNS2x1*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS3x1*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x1*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x1*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x1*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_271 = Coupling(name = 'GC_271',
                  value = '-((complex(0,1)*PMNS2x2*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS3x2*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x2*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x2*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x2*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_272 = Coupling(name = 'GC_272',
                  value = '-((complex(0,1)*PMNS2x3*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS3x3*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x3*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x3*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x3*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_273 = Coupling(name = 'GC_273',
                  value = '(-2*complex(0,1)*PMNS1x1*vevD*yDL1x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x1*vevD*yDL1x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x1*vevD*yDL1x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x1*vevD*yDL2x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x1*vevD*yDL3x1)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_274 = Coupling(name = 'GC_274',
                  value = '(-2*complex(0,1)*PMNS1x2*vevD*yDL1x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x2*vevD*yDL1x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x2*vevD*yDL1x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x2*vevD*yDL2x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x2*vevD*yDL3x1)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_275 = Coupling(name = 'GC_275',
                  value = '(-2*complex(0,1)*PMNS1x3*vevD*yDL1x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x3*vevD*yDL1x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x3*vevD*yDL1x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x3*vevD*yDL2x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x3*vevD*yDL3x1)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_276 = Coupling(name = 'GC_276',
                  value = '-(complex(0,1)*yDL2x3) - complex(0,1)*yDL3x2',
                  order = {'QED':1})

GC_277 = Coupling(name = 'GC_277',
                  value = '-((complex(0,1)*PMNS1x1*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS1x1*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x1*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x1*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x1*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_278 = Coupling(name = 'GC_278',
                  value = '-((complex(0,1)*PMNS1x2*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS1x2*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x2*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x2*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x2*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '-((complex(0,1)*PMNS1x3*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS1x3*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x3*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x3*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x3*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '(ee*complex(0,1)*PMNS2x3)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_280 = Coupling(name = 'GC_280',
                  value = '-((complex(0,1)*PMNS1x1*vevD*yDL1x2)/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x1*vevD*yDL2x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*PMNS2x1*vevD*yDL2x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x1*vevD*yDL2x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x1*vevD*yDL3x2)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_281 = Coupling(name = 'GC_281',
                  value = '-((complex(0,1)*PMNS1x2*vevD*yDL1x2)/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x2*vevD*yDL2x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*PMNS2x2*vevD*yDL2x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x2*vevD*yDL2x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x2*vevD*yDL3x2)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '-((complex(0,1)*PMNS1x3*vevD*yDL1x2)/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x3*vevD*yDL2x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*PMNS2x3*vevD*yDL2x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x3*vevD*yDL2x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS3x3*vevD*yDL3x2)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '-2*complex(0,1)*yDL3x3',
                  order = {'QED':1})

GC_284 = Coupling(name = 'GC_284',
                  value = 'cxi*complex(0,1)*PMNS1x1**2*yDL1x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x1*PMNS2x1*yDL1x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x1*PMNS3x1*yDL1x3*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x1*PMNS2x1*yDL2x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x1**2*yDL2x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x1*PMNS3x1*yDL2x3*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x1*PMNS3x1*yDL3x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x1*PMNS3x1*yDL3x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS3x1**2*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_285 = Coupling(name = 'GC_285',
                  value = '(cxi*complex(0,1)*PMNS1x2*PMNS2x1*yDL1x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x1*PMNS2x2*yDL1x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x2*PMNS3x1*yDL1x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x1*PMNS3x2*yDL1x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x2*PMNS2x1*yDL2x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x1*PMNS2x2*yDL2x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x2*PMNS3x1*yDL2x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x1*PMNS3x2*yDL2x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x2*PMNS3x1*yDL3x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x1*PMNS3x2*yDL3x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x2*PMNS3x1*yDL3x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x1*PMNS3x2*yDL3x2)/cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x1*PMNS1x2*yDL1x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x1*PMNS2x2*yDL2x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS3x1*PMNS3x2*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_286 = Coupling(name = 'GC_286',
                  value = 'cxi*complex(0,1)*PMNS1x2**2*yDL1x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x2*PMNS2x2*yDL1x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x2*PMNS3x2*yDL1x3*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x2*PMNS2x2*yDL2x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x2**2*yDL2x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x2*PMNS3x2*yDL2x3*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x2*PMNS3x2*yDL3x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x2*PMNS3x2*yDL3x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS3x2**2*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_287 = Coupling(name = 'GC_287',
                  value = '(cxi*complex(0,1)*PMNS1x3*PMNS2x1*yDL1x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x1*PMNS2x3*yDL1x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x3*PMNS3x1*yDL1x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x1*PMNS3x3*yDL1x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x3*PMNS2x1*yDL2x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x1*PMNS2x3*yDL2x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x3*PMNS3x1*yDL2x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x1*PMNS3x3*yDL2x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x3*PMNS3x1*yDL3x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x1*PMNS3x3*yDL3x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x3*PMNS3x1*yDL3x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x1*PMNS3x3*yDL3x2)/cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x1*PMNS1x3*yDL1x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x1*PMNS2x3*yDL2x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS3x1*PMNS3x3*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_288 = Coupling(name = 'GC_288',
                  value = '(cxi*complex(0,1)*PMNS1x3*PMNS2x2*yDL1x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x2*PMNS2x3*yDL1x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x3*PMNS3x2*yDL1x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x2*PMNS3x3*yDL1x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x3*PMNS2x2*yDL2x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x2*PMNS2x3*yDL2x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x3*PMNS3x2*yDL2x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x2*PMNS3x3*yDL2x3)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x3*PMNS3x2*yDL3x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS1x2*PMNS3x3*yDL3x1)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x3*PMNS3x2*yDL3x2)/cmath.sqrt(2) + (cxi*complex(0,1)*PMNS2x2*PMNS3x3*yDL3x2)/cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x2*PMNS1x3*yDL1x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x2*PMNS2x3*yDL2x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS3x2*PMNS3x3*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_289 = Coupling(name = 'GC_289',
                  value = 'cxi*complex(0,1)*PMNS1x3**2*yDL1x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x3*PMNS2x3*yDL1x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x3*PMNS3x3*yDL1x3*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x3*PMNS2x3*yDL2x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x3**2*yDL2x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x3*PMNS3x3*yDL2x3*cmath.sqrt(2) + cxi*complex(0,1)*PMNS1x3*PMNS3x3*yDL3x1*cmath.sqrt(2) + cxi*complex(0,1)*PMNS2x3*PMNS3x3*yDL3x2*cmath.sqrt(2) + cxi*complex(0,1)*PMNS3x3**2*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '(ee*complex(0,1)*PMNS3x1)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_290 = Coupling(name = 'GC_290',
                  value = 'complex(0,1)*PMNS1x1**2*sxi*yDL1x1*cmath.sqrt(2) + complex(0,1)*PMNS1x1*PMNS2x1*sxi*yDL1x2*cmath.sqrt(2) + complex(0,1)*PMNS1x1*PMNS3x1*sxi*yDL1x3*cmath.sqrt(2) + complex(0,1)*PMNS1x1*PMNS2x1*sxi*yDL2x1*cmath.sqrt(2) + complex(0,1)*PMNS2x1**2*sxi*yDL2x2*cmath.sqrt(2) + complex(0,1)*PMNS2x1*PMNS3x1*sxi*yDL2x3*cmath.sqrt(2) + complex(0,1)*PMNS1x1*PMNS3x1*sxi*yDL3x1*cmath.sqrt(2) + complex(0,1)*PMNS2x1*PMNS3x1*sxi*yDL3x2*cmath.sqrt(2) + complex(0,1)*PMNS3x1**2*sxi*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_291 = Coupling(name = 'GC_291',
                  value = '(complex(0,1)*PMNS1x2*PMNS2x1*sxi*yDL1x2)/cmath.sqrt(2) + (complex(0,1)*PMNS1x1*PMNS2x2*sxi*yDL1x2)/cmath.sqrt(2) + (complex(0,1)*PMNS1x2*PMNS3x1*sxi*yDL1x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x1*PMNS3x2*sxi*yDL1x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x2*PMNS2x1*sxi*yDL2x1)/cmath.sqrt(2) + (complex(0,1)*PMNS1x1*PMNS2x2*sxi*yDL2x1)/cmath.sqrt(2) + (complex(0,1)*PMNS2x2*PMNS3x1*sxi*yDL2x3)/cmath.sqrt(2) + (complex(0,1)*PMNS2x1*PMNS3x2*sxi*yDL2x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x2*PMNS3x1*sxi*yDL3x1)/cmath.sqrt(2) + (complex(0,1)*PMNS1x1*PMNS3x2*sxi*yDL3x1)/cmath.sqrt(2) + (complex(0,1)*PMNS2x2*PMNS3x1*sxi*yDL3x2)/cmath.sqrt(2) + (complex(0,1)*PMNS2x1*PMNS3x2*sxi*yDL3x2)/cmath.sqrt(2) + complex(0,1)*PMNS1x1*PMNS1x2*sxi*yDL1x1*cmath.sqrt(2) + complex(0,1)*PMNS2x1*PMNS2x2*sxi*yDL2x2*cmath.sqrt(2) + complex(0,1)*PMNS3x1*PMNS3x2*sxi*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_292 = Coupling(name = 'GC_292',
                  value = 'complex(0,1)*PMNS1x2**2*sxi*yDL1x1*cmath.sqrt(2) + complex(0,1)*PMNS1x2*PMNS2x2*sxi*yDL1x2*cmath.sqrt(2) + complex(0,1)*PMNS1x2*PMNS3x2*sxi*yDL1x3*cmath.sqrt(2) + complex(0,1)*PMNS1x2*PMNS2x2*sxi*yDL2x1*cmath.sqrt(2) + complex(0,1)*PMNS2x2**2*sxi*yDL2x2*cmath.sqrt(2) + complex(0,1)*PMNS2x2*PMNS3x2*sxi*yDL2x3*cmath.sqrt(2) + complex(0,1)*PMNS1x2*PMNS3x2*sxi*yDL3x1*cmath.sqrt(2) + complex(0,1)*PMNS2x2*PMNS3x2*sxi*yDL3x2*cmath.sqrt(2) + complex(0,1)*PMNS3x2**2*sxi*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '(complex(0,1)*PMNS1x3*PMNS2x1*sxi*yDL1x2)/cmath.sqrt(2) + (complex(0,1)*PMNS1x1*PMNS2x3*sxi*yDL1x2)/cmath.sqrt(2) + (complex(0,1)*PMNS1x3*PMNS3x1*sxi*yDL1x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x1*PMNS3x3*sxi*yDL1x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x3*PMNS2x1*sxi*yDL2x1)/cmath.sqrt(2) + (complex(0,1)*PMNS1x1*PMNS2x3*sxi*yDL2x1)/cmath.sqrt(2) + (complex(0,1)*PMNS2x3*PMNS3x1*sxi*yDL2x3)/cmath.sqrt(2) + (complex(0,1)*PMNS2x1*PMNS3x3*sxi*yDL2x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x3*PMNS3x1*sxi*yDL3x1)/cmath.sqrt(2) + (complex(0,1)*PMNS1x1*PMNS3x3*sxi*yDL3x1)/cmath.sqrt(2) + (complex(0,1)*PMNS2x3*PMNS3x1*sxi*yDL3x2)/cmath.sqrt(2) + (complex(0,1)*PMNS2x1*PMNS3x3*sxi*yDL3x2)/cmath.sqrt(2) + complex(0,1)*PMNS1x1*PMNS1x3*sxi*yDL1x1*cmath.sqrt(2) + complex(0,1)*PMNS2x1*PMNS2x3*sxi*yDL2x2*cmath.sqrt(2) + complex(0,1)*PMNS3x1*PMNS3x3*sxi*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_294 = Coupling(name = 'GC_294',
                  value = '(complex(0,1)*PMNS1x3*PMNS2x2*sxi*yDL1x2)/cmath.sqrt(2) + (complex(0,1)*PMNS1x2*PMNS2x3*sxi*yDL1x2)/cmath.sqrt(2) + (complex(0,1)*PMNS1x3*PMNS3x2*sxi*yDL1x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x2*PMNS3x3*sxi*yDL1x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x3*PMNS2x2*sxi*yDL2x1)/cmath.sqrt(2) + (complex(0,1)*PMNS1x2*PMNS2x3*sxi*yDL2x1)/cmath.sqrt(2) + (complex(0,1)*PMNS2x3*PMNS3x2*sxi*yDL2x3)/cmath.sqrt(2) + (complex(0,1)*PMNS2x2*PMNS3x3*sxi*yDL2x3)/cmath.sqrt(2) + (complex(0,1)*PMNS1x3*PMNS3x2*sxi*yDL3x1)/cmath.sqrt(2) + (complex(0,1)*PMNS1x2*PMNS3x3*sxi*yDL3x1)/cmath.sqrt(2) + (complex(0,1)*PMNS2x3*PMNS3x2*sxi*yDL3x2)/cmath.sqrt(2) + (complex(0,1)*PMNS2x2*PMNS3x3*sxi*yDL3x2)/cmath.sqrt(2) + complex(0,1)*PMNS1x2*PMNS1x3*sxi*yDL1x1*cmath.sqrt(2) + complex(0,1)*PMNS2x2*PMNS2x3*sxi*yDL2x2*cmath.sqrt(2) + complex(0,1)*PMNS3x2*PMNS3x3*sxi*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_295 = Coupling(name = 'GC_295',
                  value = 'complex(0,1)*PMNS1x3**2*sxi*yDL1x1*cmath.sqrt(2) + complex(0,1)*PMNS1x3*PMNS2x3*sxi*yDL1x2*cmath.sqrt(2) + complex(0,1)*PMNS1x3*PMNS3x3*sxi*yDL1x3*cmath.sqrt(2) + complex(0,1)*PMNS1x3*PMNS2x3*sxi*yDL2x1*cmath.sqrt(2) + complex(0,1)*PMNS2x3**2*sxi*yDL2x2*cmath.sqrt(2) + complex(0,1)*PMNS2x3*PMNS3x3*sxi*yDL2x3*cmath.sqrt(2) + complex(0,1)*PMNS1x3*PMNS3x3*sxi*yDL3x1*cmath.sqrt(2) + complex(0,1)*PMNS2x3*PMNS3x3*sxi*yDL3x2*cmath.sqrt(2) + complex(0,1)*PMNS3x3**2*sxi*yDL3x3*cmath.sqrt(2)',
                  order = {'QED':1})

GC_296 = Coupling(name = 'GC_296',
                  value = '-((complex(0,1)*PMNS1x1*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS2x1*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x1*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x1*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x1*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_297 = Coupling(name = 'GC_297',
                  value = '-((complex(0,1)*PMNS1x2*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS2x2*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x2*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x2*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x2*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_298 = Coupling(name = 'GC_298',
                  value = '-((complex(0,1)*PMNS1x3*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*PMNS2x3*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS1x3*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x3*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS3x3*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_299 = Coupling(name = 'GC_299',
                  value = '-((complex(0,1)*PMNS1x1*vevD*yDL1x3)/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x1*vevD*yDL2x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS1x1*vevD*yDL3x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x1*vevD*yDL3x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*PMNS3x1*vevD*yDL3x3)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '(ee*complex(0,1)*PMNS3x2)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_300 = Coupling(name = 'GC_300',
                  value = '-((complex(0,1)*PMNS1x2*vevD*yDL1x3)/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x2*vevD*yDL2x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS1x2*vevD*yDL3x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x2*vevD*yDL3x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*PMNS3x2*vevD*yDL3x3)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_301 = Coupling(name = 'GC_301',
                  value = '-((complex(0,1)*PMNS1x3*vevD*yDL1x3)/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*PMNS2x3*vevD*yDL2x3)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS1x3*vevD*yDL3x1)/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*PMNS2x3*vevD*yDL3x2)/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*PMNS3x3*vevD*yDL3x3)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '-((PMNS1x1**2*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS2x1*vev*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS3x1*vev*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS2x1*vev*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1**2*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1*PMNS3x1*vev*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS3x1*vev*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1*PMNS3x1*vev*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS3x1**2*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '-((PMNS1x2*PMNS2x1*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))) - (PMNS1x1*PMNS2x2*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x2*PMNS3x1*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS3x2*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x2*PMNS2x1*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS2x2*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x2*PMNS3x1*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x1*PMNS3x2*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x2*PMNS3x1*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS3x2*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x2*PMNS3x1*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x1*PMNS3x2*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS1x2*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1*PMNS2x2*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS3x1*PMNS3x2*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_304 = Coupling(name = 'GC_304',
                  value = '-((PMNS1x2**2*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x2*PMNS2x2*vev*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS3x2*vev*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS2x2*vev*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x2**2*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x2*PMNS3x2*vev*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS3x2*vev*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x2*PMNS3x2*vev*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS3x2**2*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '-((PMNS1x3*PMNS2x1*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))) - (PMNS1x1*PMNS2x3*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x3*PMNS3x1*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS3x3*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x3*PMNS2x1*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS2x3*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x3*PMNS3x1*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x1*PMNS3x3*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x3*PMNS3x1*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS3x3*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x3*PMNS3x1*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x1*PMNS3x3*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x1*PMNS1x3*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1*PMNS2x3*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS3x1*PMNS3x3*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_306 = Coupling(name = 'GC_306',
                  value = '-((PMNS1x3*PMNS2x2*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))) - (PMNS1x2*PMNS2x3*vev*yDL1x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x3*PMNS3x2*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x2*PMNS3x3*vev*yDL1x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x3*PMNS2x2*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x2*PMNS2x3*vev*yDL2x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x3*PMNS3x2*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x2*PMNS3x3*vev*yDL2x3)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x3*PMNS3x2*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x2*PMNS3x3*vev*yDL3x1)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x3*PMNS3x2*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS2x2*PMNS3x3*vev*yDL3x2)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x2*PMNS1x3*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x2*PMNS2x3*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS3x2*PMNS3x3*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '-((PMNS1x3**2*vev*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)) - (PMNS1x3*PMNS2x3*vev*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS3x3*vev*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS2x3*vev*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x3**2*vev*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x3*PMNS3x3*vev*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS3x3*vev*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x3*PMNS3x3*vev*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS3x3**2*vev*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '(-2*PMNS1x1**2*vevD*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x1*PMNS2x1*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x1*PMNS3x1*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x1*PMNS2x1*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x1**2*vevD*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x1*PMNS3x1*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x1*PMNS3x1*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x1*PMNS3x1*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS3x1**2*vevD*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '(-2*PMNS1x1*PMNS1x2*vevD*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS2x1*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS2x2*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS3x1*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS3x2*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS2x1*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS2x2*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x1*PMNS2x2*vevD*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x2*PMNS3x1*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1*PMNS3x2*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS3x1*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS3x2*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x2*PMNS3x1*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1*PMNS3x2*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS3x1*PMNS3x2*vevD*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '(ee*complex(0,1)*PMNS3x3)/(sw*cmath.sqrt(2))',
                 order = {'QED':1})

GC_310 = Coupling(name = 'GC_310',
                  value = '(-2*PMNS1x2**2*vevD*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x2*PMNS2x2*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x2*PMNS3x2*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x2*PMNS2x2*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x2**2*vevD*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x2*PMNS3x2*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x2*PMNS3x2*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x2*PMNS3x2*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS3x2**2*vevD*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '(-2*PMNS1x1*PMNS1x3*vevD*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS2x1*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS2x3*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS3x1*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS3x3*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS2x1*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS2x3*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x1*PMNS2x3*vevD*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x3*PMNS3x1*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1*PMNS3x3*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS3x1*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x1*PMNS3x3*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x3*PMNS3x1*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x1*PMNS3x3*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS3x1*PMNS3x3*vevD*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_312 = Coupling(name = 'GC_312',
                  value = '(-2*PMNS1x2*PMNS1x3*vevD*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS2x2*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS2x3*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS3x2*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS3x3*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS2x2*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS2x3*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x2*PMNS2x3*vevD*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x3*PMNS3x2*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x2*PMNS3x3*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x3*PMNS3x2*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS1x2*PMNS3x3*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x3*PMNS3x2*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (PMNS2x2*PMNS3x3*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS3x2*PMNS3x3*vevD*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_313 = Coupling(name = 'GC_313',
                  value = '(-2*PMNS1x3**2*vevD*yDL1x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x3*PMNS2x3*vevD*yDL1x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x3*PMNS3x3*vevD*yDL1x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x3*PMNS2x3*vevD*yDL2x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x3**2*vevD*yDL2x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x3*PMNS3x3*vevD*yDL2x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS1x3*PMNS3x3*vevD*yDL3x1*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS2x3*PMNS3x3*vevD*yDL3x2*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) - (2*PMNS3x3**2*vevD*yDL3x3*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_314 = Coupling(name = 'GC_314',
                  value = '-((cxi*complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_315 = Coupling(name = 'GC_315',
                  value = '(complex(0,1)*sxi*yt)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_316 = Coupling(name = 'GC_316',
                  value = '(complex(0,1)*vev*yt)/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_317 = Coupling(name = 'GC_317',
                  value = '-((complex(0,1)*vevD*yt*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_318 = Coupling(name = 'GC_318',
                  value = '-((vev*yt)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)))',
                  order = {'QED':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '(vev*yt)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '(ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_320 = Coupling(name = 'GC_320',
                  value = '-((vevD*yt*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_321 = Coupling(name = 'GC_321',
                  value = '(vevD*yt*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_322 = Coupling(name = 'GC_322',
                  value = '-((cxi*complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_323 = Coupling(name = 'GC_323',
                  value = '(complex(0,1)*sxi*ytau)/cmath.sqrt(2)',
                  order = {'QED':1})

GC_324 = Coupling(name = 'GC_324',
                  value = '-((complex(0,1)*PMNS3x1*vev*ytau)/cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_325 = Coupling(name = 'GC_325',
                  value = '-((complex(0,1)*PMNS3x2*vev*ytau)/cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_326 = Coupling(name = 'GC_326',
                  value = '-((complex(0,1)*PMNS3x3*vev*ytau)/cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_327 = Coupling(name = 'GC_327',
                  value = '(complex(0,1)*PMNS3x1*vevD*ytau*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_328 = Coupling(name = 'GC_328',
                  value = '(complex(0,1)*PMNS3x2*vevD*ytau*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_329 = Coupling(name = 'GC_329',
                  value = '(complex(0,1)*PMNS3x3*vevD*ytau*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '(-2*ee*complex(0,1)*sw)/(3.*cw)',
                 order = {'QED':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '(vev*ytau)/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_331 = Coupling(name = 'GC_331',
                  value = '-((vevD*ytau*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2))',
                  order = {'QED':1})

GC_332 = Coupling(name = 'GC_332',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS1x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_335 = Coupling(name = 'GC_335',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_336 = Coupling(name = 'GC_336',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_337 = Coupling(name = 'GC_337',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS2x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS3x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '-((complex(0,1)*vev*ytau*complexconjugate(PMNS3x1))/cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '(ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '(complex(0,1)*vevD*ytau*complexconjugate(PMNS3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_341 = Coupling(name = 'GC_341',
                  value = '-0.5*(cw*ee*complex(0,1)*PMNS1x1*complexconjugate(PMNS1x1))/sw - (ee*complex(0,1)*PMNS1x1*sw*complexconjugate(PMNS1x1))/(2.*cw) - (cw*ee*complex(0,1)*PMNS2x1*complexconjugate(PMNS2x1))/(2.*sw) - (ee*complex(0,1)*PMNS2x1*sw*complexconjugate(PMNS2x1))/(2.*cw) - (cw*ee*complex(0,1)*PMNS3x1*complexconjugate(PMNS3x1))/(2.*sw) - (ee*complex(0,1)*PMNS3x1*sw*complexconjugate(PMNS3x1))/(2.*cw)',
                  order = {'QED':1})

GC_342 = Coupling(name = 'GC_342',
                  value = '(cw*ee*complex(0,1)*PMNS1x2*complexconjugate(PMNS1x1))/(2.*sw) + (ee*complex(0,1)*PMNS1x2*sw*complexconjugate(PMNS1x1))/(2.*cw) + (cw*ee*complex(0,1)*PMNS2x2*complexconjugate(PMNS2x1))/(2.*sw) + (ee*complex(0,1)*PMNS2x2*sw*complexconjugate(PMNS2x1))/(2.*cw) + (cw*ee*complex(0,1)*PMNS3x2*complexconjugate(PMNS3x1))/(2.*sw) + (ee*complex(0,1)*PMNS3x2*sw*complexconjugate(PMNS3x1))/(2.*cw)',
                  order = {'QED':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '(cw*ee*complex(0,1)*PMNS1x3*complexconjugate(PMNS1x1))/(2.*sw) + (ee*complex(0,1)*PMNS1x3*sw*complexconjugate(PMNS1x1))/(2.*cw) + (cw*ee*complex(0,1)*PMNS2x3*complexconjugate(PMNS2x1))/(2.*sw) + (ee*complex(0,1)*PMNS2x3*sw*complexconjugate(PMNS2x1))/(2.*cw) + (cw*ee*complex(0,1)*PMNS3x3*complexconjugate(PMNS3x1))/(2.*sw) + (ee*complex(0,1)*PMNS3x3*sw*complexconjugate(PMNS3x1))/(2.*cw)',
                  order = {'QED':1})

GC_344 = Coupling(name = 'GC_344',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS3x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_345 = Coupling(name = 'GC_345',
                  value = '-((complex(0,1)*vev*ytau*complexconjugate(PMNS3x2))/cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_346 = Coupling(name = 'GC_346',
                  value = '(complex(0,1)*vevD*ytau*complexconjugate(PMNS3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_347 = Coupling(name = 'GC_347',
                  value = '-0.5*(cw*ee*complex(0,1)*PMNS1x1*complexconjugate(PMNS1x2))/sw - (ee*complex(0,1)*PMNS1x1*sw*complexconjugate(PMNS1x2))/(2.*cw) - (cw*ee*complex(0,1)*PMNS2x1*complexconjugate(PMNS2x2))/(2.*sw) - (ee*complex(0,1)*PMNS2x1*sw*complexconjugate(PMNS2x2))/(2.*cw) - (cw*ee*complex(0,1)*PMNS3x1*complexconjugate(PMNS3x2))/(2.*sw) - (ee*complex(0,1)*PMNS3x1*sw*complexconjugate(PMNS3x2))/(2.*cw)',
                  order = {'QED':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '-0.5*(cw*ee*complex(0,1)*PMNS1x2*complexconjugate(PMNS1x2))/sw - (ee*complex(0,1)*PMNS1x2*sw*complexconjugate(PMNS1x2))/(2.*cw) - (cw*ee*complex(0,1)*PMNS2x2*complexconjugate(PMNS2x2))/(2.*sw) - (ee*complex(0,1)*PMNS2x2*sw*complexconjugate(PMNS2x2))/(2.*cw) - (cw*ee*complex(0,1)*PMNS3x2*complexconjugate(PMNS3x2))/(2.*sw) - (ee*complex(0,1)*PMNS3x2*sw*complexconjugate(PMNS3x2))/(2.*cw)',
                  order = {'QED':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '(cw*ee*complex(0,1)*PMNS1x3*complexconjugate(PMNS1x2))/(2.*sw) + (ee*complex(0,1)*PMNS1x3*sw*complexconjugate(PMNS1x2))/(2.*cw) + (cw*ee*complex(0,1)*PMNS2x3*complexconjugate(PMNS2x2))/(2.*sw) + (ee*complex(0,1)*PMNS2x3*sw*complexconjugate(PMNS2x2))/(2.*cw) + (cw*ee*complex(0,1)*PMNS3x3*complexconjugate(PMNS3x2))/(2.*sw) + (ee*complex(0,1)*PMNS3x3*sw*complexconjugate(PMNS3x2))/(2.*cw)',
                  order = {'QED':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '-0.5*(cw*ee*complex(0,1))/sw - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '(ee*complex(0,1)*complexconjugate(PMNS3x3))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_351 = Coupling(name = 'GC_351',
                  value = '-((complex(0,1)*vev*ytau*complexconjugate(PMNS3x3))/cmath.sqrt(vev**2 + 2*vevD**2))',
                  order = {'QED':1})

GC_352 = Coupling(name = 'GC_352',
                  value = '(complex(0,1)*vevD*ytau*complexconjugate(PMNS3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '-0.5*(cw*ee*complex(0,1)*PMNS1x1*complexconjugate(PMNS1x3))/sw - (ee*complex(0,1)*PMNS1x1*sw*complexconjugate(PMNS1x3))/(2.*cw) - (cw*ee*complex(0,1)*PMNS2x1*complexconjugate(PMNS2x3))/(2.*sw) - (ee*complex(0,1)*PMNS2x1*sw*complexconjugate(PMNS2x3))/(2.*cw) - (cw*ee*complex(0,1)*PMNS3x1*complexconjugate(PMNS3x3))/(2.*sw) - (ee*complex(0,1)*PMNS3x1*sw*complexconjugate(PMNS3x3))/(2.*cw)',
                  order = {'QED':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '-0.5*(cw*ee*complex(0,1)*PMNS1x2*complexconjugate(PMNS1x3))/sw - (ee*complex(0,1)*PMNS1x2*sw*complexconjugate(PMNS1x3))/(2.*cw) - (cw*ee*complex(0,1)*PMNS2x2*complexconjugate(PMNS2x3))/(2.*sw) - (ee*complex(0,1)*PMNS2x2*sw*complexconjugate(PMNS2x3))/(2.*cw) - (cw*ee*complex(0,1)*PMNS3x2*complexconjugate(PMNS3x3))/(2.*sw) - (ee*complex(0,1)*PMNS3x2*sw*complexconjugate(PMNS3x3))/(2.*cw)',
                  order = {'QED':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '-0.5*(cw*ee*complex(0,1)*PMNS1x3*complexconjugate(PMNS1x3))/sw - (ee*complex(0,1)*PMNS1x3*sw*complexconjugate(PMNS1x3))/(2.*cw) - (cw*ee*complex(0,1)*PMNS2x3*complexconjugate(PMNS2x3))/(2.*sw) - (ee*complex(0,1)*PMNS2x3*sw*complexconjugate(PMNS2x3))/(2.*cw) - (cw*ee*complex(0,1)*PMNS3x3*complexconjugate(PMNS3x3))/(2.*sw) - (ee*complex(0,1)*PMNS3x3*sw*complexconjugate(PMNS3x3))/(2.*cw)',
                  order = {'QED':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '-2*complex(0,1)*complexconjugate(yDL1x1)',
                  order = {'QED':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '-(complex(0,1)*complexconjugate(yDL1x2)) - complex(0,1)*complexconjugate(yDL2x1)',
                  order = {'QED':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '-2*complex(0,1)*complexconjugate(yDL2x2)',
                  order = {'QED':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '-(complex(0,1)*complexconjugate(yDL1x3)) - complex(0,1)*complexconjugate(yDL3x1)',
                  order = {'QED':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '(cw*ee*complex(0,1))/(2.*sw) - (ee*complex(0,1)*sw)/(6.*cw)',
                 order = {'QED':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS1x1)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '(-2*complex(0,1)*vevD*complexconjugate(PMNS1x1)*complexconjugate(yDL1x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_362 = Coupling(name = 'GC_362',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS1x2)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_363 = Coupling(name = 'GC_363',
                  value = '(-2*complex(0,1)*vevD*complexconjugate(PMNS1x2)*complexconjugate(yDL1x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_364 = Coupling(name = 'GC_364',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_365 = Coupling(name = 'GC_365',
                  value = '(-2*complex(0,1)*vevD*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_366 = Coupling(name = 'GC_366',
                  value = '-(complex(0,1)*complexconjugate(yDL2x3)) - complex(0,1)*complexconjugate(yDL3x2)',
                  order = {'QED':1})

GC_367 = Coupling(name = 'GC_367',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS1x1)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS1x1)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x1)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '-((complex(0,1)*vevD*complexconjugate(PMNS1x1)*complexconjugate(yDL1x2))/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vevD*complexconjugate(PMNS1x1)*complexconjugate(yDL2x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*vevD*complexconjugate(PMNS2x1)*complexconjugate(yDL2x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS1x2)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS1x2)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x2)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '-0.5*(cw*ee*complex(0,1))/sw + (ee*complex(0,1)*sw)/(2.*cw)',
                 order = {'QED':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '-((complex(0,1)*vevD*complexconjugate(PMNS1x2)*complexconjugate(yDL1x2))/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vevD*complexconjugate(PMNS1x2)*complexconjugate(yDL2x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*vevD*complexconjugate(PMNS2x2)*complexconjugate(yDL2x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS1x3)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS1x3)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_372 = Coupling(name = 'GC_372',
                  value = '-((complex(0,1)*vevD*complexconjugate(PMNS1x3)*complexconjugate(yDL1x2))/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vevD*complexconjugate(PMNS1x3)*complexconjugate(yDL2x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*vevD*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_373 = Coupling(name = 'GC_373',
                  value = '-2*complex(0,1)*complexconjugate(yDL3x3)',
                  order = {'QED':1})

GC_374 = Coupling(name = 'GC_374',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS1x1)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS2x1)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS1x1)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x1)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x1)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_375 = Coupling(name = 'GC_375',
                  value = '-((complex(0,1)*vevD*complexconjugate(PMNS1x1)*complexconjugate(yDL1x3))/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vevD*complexconjugate(PMNS2x1)*complexconjugate(yDL2x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS1x1)*complexconjugate(yDL3x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x1)*complexconjugate(yDL3x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*vevD*complexconjugate(PMNS3x1)*complexconjugate(yDL3x3))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_376 = Coupling(name = 'GC_376',
                  value = 'cxi*complex(0,1)*complexconjugate(PMNS1x1)**2*complexconjugate(yDL1x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x1)**2*complexconjugate(yDL2x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS3x1)**2*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_377 = Coupling(name = 'GC_377',
                  value = 'complex(0,1)*sxi*complexconjugate(PMNS1x1)**2*complexconjugate(yDL1x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x1)**2*complexconjugate(yDL2x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS3x1)**2*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '(vev*complexconjugate(PMNS1x1)**2*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x1)**2*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS3x1)**2*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '(2*vevD*complexconjugate(PMNS1x1)**2*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x1)**2*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS3x1)**2*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '-((cw*ee*complex(0,1))/sw) + (ee*complex(0,1)*sw)/cw',
                 order = {'QED':1})

GC_380 = Coupling(name = 'GC_380',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS1x2)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS2x2)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS1x2)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x2)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x2)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_381 = Coupling(name = 'GC_381',
                  value = '-((complex(0,1)*vevD*complexconjugate(PMNS1x2)*complexconjugate(yDL1x3))/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vevD*complexconjugate(PMNS2x2)*complexconjugate(yDL2x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS1x2)*complexconjugate(yDL3x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x2)*complexconjugate(yDL3x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*vevD*complexconjugate(PMNS3x2)*complexconjugate(yDL3x3))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_382 = Coupling(name = 'GC_382',
                  value = '(cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2))/cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS1x2)*complexconjugate(yDL1x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS3x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '(complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2))/cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS1x2)*complexconjugate(yDL1x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS3x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '(vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS1x2)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS3x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_385 = Coupling(name = 'GC_385',
                  value = '(2*vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS1x2)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x1)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS3x1)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_386 = Coupling(name = 'GC_386',
                  value = 'cxi*complex(0,1)*complexconjugate(PMNS1x2)**2*complexconjugate(yDL1x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x2)**2*complexconjugate(yDL2x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS3x2)**2*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_387 = Coupling(name = 'GC_387',
                  value = 'complex(0,1)*sxi*complexconjugate(PMNS1x2)**2*complexconjugate(yDL1x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x2)**2*complexconjugate(yDL2x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS3x2)**2*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_388 = Coupling(name = 'GC_388',
                  value = '(vev*complexconjugate(PMNS1x2)**2*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x2)**2*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS3x2)**2*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '(2*vevD*complexconjugate(PMNS1x2)**2*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x2)**2*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS3x2)**2*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '(4*cw*ee**2*complex(0,1))/sw - (4*ee**2*complex(0,1)*sw)/cw',
                 order = {'QED':2})

GC_390 = Coupling(name = 'GC_390',
                  value = '-((complex(0,1)*vev*complexconjugate(PMNS1x3)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2))) - (complex(0,1)*vev*complexconjugate(PMNS2x3)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS1x3)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS2x3)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vev*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '-((complex(0,1)*vevD*complexconjugate(PMNS1x3)*complexconjugate(yDL1x3))/cmath.sqrt(vev**2 + 2*vevD**2)) - (complex(0,1)*vevD*complexconjugate(PMNS2x3)*complexconjugate(yDL2x3))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS1x3)*complexconjugate(yDL3x1))/cmath.sqrt(vev**2 + 2*vevD**2) - (complex(0,1)*vevD*complexconjugate(PMNS2x3)*complexconjugate(yDL3x2))/cmath.sqrt(vev**2 + 2*vevD**2) - (2*complex(0,1)*vevD*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3))/cmath.sqrt(vev**2 + 2*vevD**2)',
                  order = {'QED':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '(cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2))/cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x1)*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS3x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '(complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2))/cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x1)*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS3x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_394 = Coupling(name = 'GC_394',
                  value = '(vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x1)*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS3x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '(2*vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x1)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x1)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x1)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x1)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS3x1)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '(cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2))/cmath.sqrt(2) + (cxi*complex(0,1)*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2))/cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x2)*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS3x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_397 = Coupling(name = 'GC_397',
                  value = '(complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2))/cmath.sqrt(2) + (complex(0,1)*sxi*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2))/cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x2)*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS3x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_398 = Coupling(name = 'GC_398',
                  value = '(vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2))/(cmath.sqrt(2)*cmath.sqrt(vev**2 + 4*vevD**2)) + (vev*complexconjugate(PMNS1x2)*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS3x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_399 = Coupling(name = 'GC_399',
                  value = '(2*vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS1x3)*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x2)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x2)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x2)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS1x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x2)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vevD*complexconjugate(PMNS2x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS3x2)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '-4*ee**2*complex(0,1) + (2*cw**2*ee**2*complex(0,1))/sw**2 + (2*ee**2*complex(0,1)*sw**2)/cw**2',
                 order = {'QED':2})

GC_400 = Coupling(name = 'GC_400',
                  value = 'cxi*complex(0,1)*complexconjugate(PMNS1x3)**2*complexconjugate(yDL1x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x3)**2*complexconjugate(yDL2x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2)*cmath.sqrt(2) + cxi*complex(0,1)*complexconjugate(PMNS3x3)**2*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_401 = Coupling(name = 'GC_401',
                  value = 'complex(0,1)*sxi*complexconjugate(PMNS1x3)**2*complexconjugate(yDL1x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x3)**2*complexconjugate(yDL2x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2)*cmath.sqrt(2) + complex(0,1)*sxi*complexconjugate(PMNS3x3)**2*complexconjugate(yDL3x3)*cmath.sqrt(2)',
                  order = {'QED':1})

GC_402 = Coupling(name = 'GC_402',
                  value = '(vev*complexconjugate(PMNS1x3)**2*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x3)**2*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (vev*complexconjugate(PMNS3x3)**2*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_403 = Coupling(name = 'GC_403',
                  value = '(2*vevD*complexconjugate(PMNS1x3)**2*complexconjugate(yDL1x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x3)*complexconjugate(yDL1x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL1x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS2x3)*complexconjugate(yDL2x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x3)**2*complexconjugate(yDL2x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL2x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS1x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x1)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS2x3)*complexconjugate(PMNS3x3)*complexconjugate(yDL3x2)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2) + (2*vevD*complexconjugate(PMNS3x3)**2*complexconjugate(yDL3x3)*cmath.sqrt(2))/cmath.sqrt(vev**2 + 4*vevD**2)',
                  order = {'QED':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '3*cxi**2*complex(0,1)*muHD*sxi*cmath.sqrt(2)',
                 order = {'QED':2})

GC_42 = Coupling(name = 'GC_42',
                 value = '-((ee**2*complex(0,1)*sxi*cmath.sqrt(2))/sw**2)',
                 order = {'QED':2})

GC_43 = Coupling(name = 'GC_43',
                 value = '(cxi*ee**2*complex(0,1)*sxi)/(2.*sw**2)',
                 order = {'QED':2})

GC_44 = Coupling(name = 'GC_44',
                 value = '3*cxi*complex(0,1)*muHD*sxi**2*cmath.sqrt(2)',
                 order = {'QED':2})

GC_45 = Coupling(name = 'GC_45',
                 value = '-2*cxi*complex(0,1)*lamD1*sxi + cxi*complex(0,1)*lamHD1*sxi',
                 order = {'QED':2})

GC_46 = Coupling(name = 'GC_46',
                 value = '3*cxi*ee**2*complex(0,1)*sxi + (3*cw**2*cxi*ee**2*complex(0,1)*sxi)/(2.*sw**2) + (3*cxi*ee**2*complex(0,1)*sw**2*sxi)/(2.*cw**2)',
                 order = {'QED':2})

GC_47 = Coupling(name = 'GC_47',
                 value = '-(cxi**2*complex(0,1)*lamHD1) - 2*complex(0,1)*lamD1*sxi**2',
                 order = {'QED':2})

GC_48 = Coupling(name = 'GC_48',
                 value = '-2*cxi**2*complex(0,1)*lamD1 - complex(0,1)*lamHD1*sxi**2',
                 order = {'QED':2})

GC_49 = Coupling(name = 'GC_49',
                 value = 'cxi**3*complex(0,1)*muHD*cmath.sqrt(2) - 2*cxi*complex(0,1)*muHD*sxi**2*cmath.sqrt(2)',
                 order = {'QED':2})

GC_5 = Coupling(name = 'GC_5',
                value = '-2*ee*complex(0,1)',
                order = {'QED':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '(cxi**2*ee**2*complex(0,1))/sw**2 + (ee**2*complex(0,1)*sxi**2)/(2.*sw**2)',
                 order = {'QED':2})

GC_51 = Coupling(name = 'GC_51',
                 value = '(cxi**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sxi**2)/sw**2',
                 order = {'QED':2})

GC_52 = Coupling(name = 'GC_52',
                 value = '4*cxi**2*ee**2*complex(0,1) + (2*cw**2*cxi**2*ee**2*complex(0,1))/sw**2 + (2*cxi**2*ee**2*complex(0,1)*sw**2)/cw**2 + ee**2*complex(0,1)*sxi**2 + (cw**2*ee**2*complex(0,1)*sxi**2)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*sxi**2)/(2.*cw**2)',
                 order = {'QED':2})

GC_53 = Coupling(name = 'GC_53',
                 value = 'cxi**2*ee**2*complex(0,1) + (cw**2*cxi**2*ee**2*complex(0,1))/(2.*sw**2) + (cxi**2*ee**2*complex(0,1)*sw**2)/(2.*cw**2) + 4*ee**2*complex(0,1)*sxi**2 + (2*cw**2*ee**2*complex(0,1)*sxi**2)/sw**2 + (2*ee**2*complex(0,1)*sw**2*sxi**2)/cw**2',
                 order = {'QED':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-6*cxi**3*complex(0,1)*lamD1*sxi - 6*cxi**3*complex(0,1)*lamD2*sxi + 3*cxi**3*complex(0,1)*lamHD1*sxi + 3*cxi**3*complex(0,1)*lamHD2*sxi + 6*cxi*complex(0,1)*lamH*sxi**3 - 3*cxi*complex(0,1)*lamHD1*sxi**3 - 3*cxi*complex(0,1)*lamHD2*sxi**3',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '6*cxi**3*complex(0,1)*lamH*sxi - 3*cxi**3*complex(0,1)*lamHD1*sxi - 3*cxi**3*complex(0,1)*lamHD2*sxi - 6*cxi*complex(0,1)*lamD1*sxi**3 - 6*cxi*complex(0,1)*lamD2*sxi**3 + 3*cxi*complex(0,1)*lamHD1*sxi**3 + 3*cxi*complex(0,1)*lamHD2*sxi**3',
                 order = {'QED':2})

GC_56 = Coupling(name = 'GC_56',
                 value = '-2*cxi**2*complex(0,1)*muHD*sxi*cmath.sqrt(2) + complex(0,1)*muHD*sxi**3*cmath.sqrt(2)',
                 order = {'QED':2})

GC_57 = Coupling(name = 'GC_57',
                 value = '-6*cxi**4*complex(0,1)*lamH - 6*cxi**2*complex(0,1)*lamHD1*sxi**2 - 6*cxi**2*complex(0,1)*lamHD2*sxi**2 - 6*complex(0,1)*lamD1*sxi**4 - 6*complex(0,1)*lamD2*sxi**4',
                 order = {'QED':2})

GC_58 = Coupling(name = 'GC_58',
                 value = '-6*cxi**4*complex(0,1)*lamD1 - 6*cxi**4*complex(0,1)*lamD2 - 6*cxi**2*complex(0,1)*lamHD1*sxi**2 - 6*cxi**2*complex(0,1)*lamHD2*sxi**2 - 6*complex(0,1)*lamH*sxi**4',
                 order = {'QED':2})

GC_59 = Coupling(name = 'GC_59',
                 value = '-(cxi**4*complex(0,1)*lamHD1) - cxi**4*complex(0,1)*lamHD2 - 6*cxi**2*complex(0,1)*lamD1*sxi**2 - 6*cxi**2*complex(0,1)*lamD2*sxi**2 - 6*cxi**2*complex(0,1)*lamH*sxi**2 + 4*cxi**2*complex(0,1)*lamHD1*sxi**2 + 4*cxi**2*complex(0,1)*lamHD2*sxi**2 - complex(0,1)*lamHD1*sxi**4 - complex(0,1)*lamHD2*sxi**4',
                 order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_60 = Coupling(name = 'GC_60',
                 value = '-0.25*(ee**2*vev)/sw**2',
                 order = {'QED':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(ee**2*vev)/(4.*sw**2)',
                 order = {'QED':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '-0.25*(cxi*ee**2*complex(0,1)*vev)/sw**2',
                 order = {'QED':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '-0.5*(ee**2*vev)/sw',
                 order = {'QED':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '(ee**2*vev)/(2.*sw)',
                 order = {'QED':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(ee**2*complex(0,1)*sxi*vev)/(4.*sw**2)',
                 order = {'QED':1})

GC_66 = Coupling(name = 'GC_66',
                 value = '-0.25*(ee**2*vev)/cw - (cw*ee**2*vev)/(4.*sw**2)',
                 order = {'QED':1})

GC_67 = Coupling(name = 'GC_67',
                 value = '(ee**2*vev)/(4.*cw) - (cw*ee**2*vev)/(4.*sw**2)',
                 order = {'QED':1})

GC_68 = Coupling(name = 'GC_68',
                 value = '-0.25*(ee**2*vev)/cw + (cw*ee**2*vev)/(4.*sw**2)',
                 order = {'QED':1})

GC_69 = Coupling(name = 'GC_69',
                 value = '(ee**2*vev)/(4.*cw) + (cw*ee**2*vev)/(4.*sw**2)',
                 order = {'QED':1})

GC_7 = Coupling(name = 'GC_7',
                value = '2*ee**2*complex(0,1)',
                order = {'QED':2})

GC_70 = Coupling(name = 'GC_70',
                 value = '-0.5*(cxi*ee**2*complex(0,1)*vev) - (cw**2*cxi*ee**2*complex(0,1)*vev)/(4.*sw**2) - (cxi*ee**2*complex(0,1)*sw**2*vev)/(4.*cw**2)',
                 order = {'QED':1})

GC_71 = Coupling(name = 'GC_71',
                 value = '(ee**2*complex(0,1)*sxi*vev)/2. + (cw**2*ee**2*complex(0,1)*sxi*vev)/(4.*sw**2) + (ee**2*complex(0,1)*sw**2*sxi*vev)/(4.*cw**2)',
                 order = {'QED':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '-((ee**2*complex(0,1)*vevD*cmath.sqrt(2))/sw**2)',
                 order = {'QED':1})

GC_73 = Coupling(name = 'GC_73',
                 value = 'complex(0,1)*lamHD1*sxi*vev - 2*cxi*complex(0,1)*lamD1*vevD',
                 order = {'QED':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '-0.5*(ee**2*complex(0,1)*sxi*vev)/sw**2 + (cxi*ee**2*complex(0,1)*vevD)/sw**2',
                 order = {'QED':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '-(ee**2*complex(0,1)*sxi*vev) - (cw**2*ee**2*complex(0,1)*sxi*vev)/(2.*sw**2) - (ee**2*complex(0,1)*sw**2*sxi*vev)/(2.*cw**2) + 4*cxi*ee**2*complex(0,1)*vevD + (2*cw**2*cxi*ee**2*complex(0,1)*vevD)/sw**2 + (2*cxi*ee**2*complex(0,1)*sw**2*vevD)/cw**2',
                 order = {'QED':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '-(cxi*complex(0,1)*lamHD1*vev) - 2*complex(0,1)*lamD1*sxi*vevD',
                 order = {'QED':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(cxi*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sxi*vevD)/sw**2',
                 order = {'QED':1})

GC_78 = Coupling(name = 'GC_78',
                 value = 'cxi*ee**2*complex(0,1)*vev + (cw**2*cxi*ee**2*complex(0,1)*vev)/(2.*sw**2) + (cxi*ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2) + 4*ee**2*complex(0,1)*sxi*vevD + (2*cw**2*ee**2*complex(0,1)*sxi*vevD)/sw**2 + (2*ee**2*complex(0,1)*sw**2*sxi*vevD)/cw**2',
                 order = {'QED':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '6*cxi**2*complex(0,1)*lamH*sxi*vev - 2*cxi**2*complex(0,1)*lamHD1*sxi*vev - 2*cxi**2*complex(0,1)*lamHD2*sxi*vev + complex(0,1)*lamHD1*sxi**3*vev + complex(0,1)*lamHD2*sxi**3*vev - cxi**3*complex(0,1)*lamHD1*vevD - cxi**3*complex(0,1)*lamHD2*vevD - 6*cxi*complex(0,1)*lamD1*sxi**2*vevD - 6*cxi*complex(0,1)*lamD2*sxi**2*vevD + 2*cxi*complex(0,1)*lamHD1*sxi**2*vevD + 2*cxi*complex(0,1)*lamHD2*sxi**2*vevD',
                 order = {'QED':1})

GC_8 = Coupling(name = 'GC_8',
                value = '8*ee**2*complex(0,1)',
                order = {'QED':2})

GC_80 = Coupling(name = 'GC_80',
                 value = '3*cxi**2*complex(0,1)*lamHD1*sxi*vev + 3*cxi**2*complex(0,1)*lamHD2*sxi*vev + 6*complex(0,1)*lamH*sxi**3*vev - 6*cxi**3*complex(0,1)*lamD1*vevD - 6*cxi**3*complex(0,1)*lamD2*vevD - 3*cxi*complex(0,1)*lamHD1*sxi**2*vevD - 3*cxi*complex(0,1)*lamHD2*sxi**2*vevD',
                 order = {'QED':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '-6*cxi**3*complex(0,1)*lamH*vev - 3*cxi*complex(0,1)*lamHD1*sxi**2*vev - 3*cxi*complex(0,1)*lamHD2*sxi**2*vev - 3*cxi**2*complex(0,1)*lamHD1*sxi*vevD - 3*cxi**2*complex(0,1)*lamHD2*sxi*vevD - 6*complex(0,1)*lamD1*sxi**3*vevD - 6*complex(0,1)*lamD2*sxi**3*vevD',
                 order = {'QED':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '-(cxi**3*complex(0,1)*lamHD1*vev) - cxi**3*complex(0,1)*lamHD2*vev - 6*cxi*complex(0,1)*lamH*sxi**2*vev + 2*cxi*complex(0,1)*lamHD1*sxi**2*vev + 2*cxi*complex(0,1)*lamHD2*sxi**2*vev - 6*cxi**2*complex(0,1)*lamD1*sxi*vevD - 6*cxi**2*complex(0,1)*lamD2*sxi*vevD + 2*cxi**2*complex(0,1)*lamHD1*sxi*vevD + 2*cxi**2*complex(0,1)*lamHD2*sxi*vevD - complex(0,1)*lamHD1*sxi**3*vevD - complex(0,1)*lamHD2*sxi**3*vevD',
                 order = {'QED':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '-((ee*complex(0,1)*vevD)/(sw*cmath.sqrt(vev**2/2. + vevD**2)))',
                 order = {'QED':1})

GC_84 = Coupling(name = 'GC_84',
                 value = '(ee*complex(0,1)*vevD)/(sw*cmath.sqrt(vev**2/2. + vevD**2))',
                 order = {'QED':1})

GC_85 = Coupling(name = 'GC_85',
                 value = '(-3*ee**2*complex(0,1)*vevD)/(sw*cmath.sqrt(vev**2/2. + vevD**2))',
                 order = {'QED':2})

GC_86 = Coupling(name = 'GC_86',
                 value = '(-2*complex(0,1)*muHD*vev**2)/(vev**2 + 2*vevD**2)',
                 order = {'QED':2})

GC_87 = Coupling(name = 'GC_87',
                 value = '(2*complex(0,1)*muHD*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                 order = {'QED':2})

GC_88 = Coupling(name = 'GC_88',
                 value = '(-2*cxi*complex(0,1)*muHD*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                 order = {'QED':2})

GC_89 = Coupling(name = 'GC_89',
                 value = '(2*cxi*complex(0,1)*muHD*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                 order = {'QED':2})

GC_9 = Coupling(name = 'GC_9',
                value = '-G',
                order = {'QCD':1})

GC_90 = Coupling(name = 'GC_90',
                 value = '(3*ee**2*complex(0,1)*vev*vevD)/(sw**2*(vev**2 + 2*vevD**2)*cmath.sqrt(2))',
                 order = {'QED':2})

GC_91 = Coupling(name = 'GC_91',
                 value = '(-2*complex(0,1)*muHD*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                 order = {'QED':2})

GC_92 = Coupling(name = 'GC_92',
                 value = '(2*complex(0,1)*muHD*sxi*vev*vevD*cmath.sqrt(2))/(vev**2 + 2*vevD**2)',
                 order = {'QED':2})

GC_93 = Coupling(name = 'GC_93',
                 value = '(-4*complex(0,1)*muHD*vevD**2)/(vev**2 + 2*vevD**2)',
                 order = {'QED':2})

GC_94 = Coupling(name = 'GC_94',
                 value = '-((ee*complex(0,1)*vev)/(sw*cmath.sqrt(vev**2 + 2*vevD**2)))',
                 order = {'QED':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '(ee*complex(0,1)*vev)/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                 order = {'QED':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '(-3*ee**2*complex(0,1)*vev)/(sw*cmath.sqrt(vev**2 + 2*vevD**2))',
                 order = {'QED':2})

GC_97 = Coupling(name = 'GC_97',
                 value = '(ee**2*complex(0,1)*cmath.sqrt(vev**2 + 2*vevD**2))/(2.*sw)',
                 order = {'QED':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(ee**2*complex(0,1)*vev*vevD)/(sw**2*(vev**2 + 4*vevD**2))',
                 order = {'QED':2})

GC_99 = Coupling(name = 'GC_99',
                 value = '-((muHD*vev)/cmath.sqrt(vev**2 + 4*vevD**2))',
                 order = {'QED':2})

