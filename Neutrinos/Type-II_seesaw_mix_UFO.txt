Requestor: Romain Kukla
Contents: DTHM Type II for Higgs associated production
Authors: Lorenzo Basso, validation by Venugopal Ellajosyula and Gilbert Moultaka
Paper: https://arxiv.org/abs/1105.1925
JIRA: https://its.cern.ch/jira/browse/AGENE-1494