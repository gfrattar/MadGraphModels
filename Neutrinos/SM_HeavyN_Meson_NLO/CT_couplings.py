# This file was automatically created by FeynRules 2.3.36
# Mathematica version: 12.0.0 for Linux x86 (64-bit) (April 7, 2019)
# Date: Tue 25 Apr 2023 15:27:21


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_582_1 = Coupling(name = 'R2GC_582_1',
                      value = '(3*G**3)/(16.*cmath.pi**2)',
                      order = {'QCD':3})

R2GC_583_2 = Coupling(name = 'R2GC_583_2',
                      value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_584_3 = Coupling(name = 'R2GC_584_3',
                      value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',
                      order = {'QCD':2,'QED':1})

R2GC_587_4 = Coupling(name = 'R2GC_587_4',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_587_5 = Coupling(name = 'R2GC_587_5',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_588_6 = Coupling(name = 'R2GC_588_6',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_588_7 = Coupling(name = 'R2GC_588_7',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_589_8 = Coupling(name = 'R2GC_589_8',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_589_9 = Coupling(name = 'R2GC_589_9',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_590_10 = Coupling(name = 'R2GC_590_10',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_590_11 = Coupling(name = 'R2GC_590_11',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_591_12 = Coupling(name = 'R2GC_591_12',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_591_13 = Coupling(name = 'R2GC_591_13',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_592_14 = Coupling(name = 'R2GC_592_14',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_592_15 = Coupling(name = 'R2GC_592_15',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_593_16 = Coupling(name = 'R2GC_593_16',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_593_17 = Coupling(name = 'R2GC_593_17',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_594_18 = Coupling(name = 'R2GC_594_18',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_594_19 = Coupling(name = 'R2GC_594_19',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2)',
                       order = {'QCD':3,'QED':1})

R2GC_595_20 = Coupling(name = 'R2GC_595_20',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_595_21 = Coupling(name = 'R2GC_595_21',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_596_22 = Coupling(name = 'R2GC_596_22',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_597_23 = Coupling(name = 'R2GC_597_23',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_599_24 = Coupling(name = 'R2GC_599_24',
                       value = '-(CKM2x3*complex(0,1)*G**2*yb**2*complexconjugate(CKM2x3))/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_599_25 = Coupling(name = 'R2GC_599_25',
                       value = '-(CKM3x3*complex(0,1)*G**2*yb**2*complexconjugate(CKM3x3))/(16.*cmath.pi**2) - (CKM3x3*complex(0,1)*G**2*yt**2*complexconjugate(CKM3x3))/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_599_26 = Coupling(name = 'R2GC_599_26',
                       value = '-(CKM1x3*complex(0,1)*G**2*yb**2*complexconjugate(CKM1x3))/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_599_27 = Coupling(name = 'R2GC_599_27',
                       value = '-(CKM3x1*complex(0,1)*G**2*yt**2*complexconjugate(CKM3x1))/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_599_28 = Coupling(name = 'R2GC_599_28',
                       value = '-(CKM3x2*complex(0,1)*G**2*yt**2*complexconjugate(CKM3x2))/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_29 = Coupling(name = 'R2GC_601_29',
                       value = '(CKM2x3*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x3))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_30 = Coupling(name = 'R2GC_601_30',
                       value = '(CKM3x3*ee**2*complex(0,1)*G**2*complexconjugate(CKM3x3))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_31 = Coupling(name = 'R2GC_601_31',
                       value = '(CKM1x3*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x3))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_32 = Coupling(name = 'R2GC_601_32',
                       value = '(CKM2x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_33 = Coupling(name = 'R2GC_601_33',
                       value = '(CKM2x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM2x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_34 = Coupling(name = 'R2GC_601_34',
                       value = '(CKM3x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM3x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_35 = Coupling(name = 'R2GC_601_35',
                       value = '(CKM1x1*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x1))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_36 = Coupling(name = 'R2GC_601_36',
                       value = '(CKM3x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM3x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_601_37 = Coupling(name = 'R2GC_601_37',
                       value = '(CKM1x2*ee**2*complex(0,1)*G**2*complexconjugate(CKM1x2))/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_612_38 = Coupling(name = 'R2GC_612_38',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_612_39 = Coupling(name = 'R2GC_612_39',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_613_40 = Coupling(name = 'R2GC_613_40',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_613_41 = Coupling(name = 'R2GC_613_41',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_614_42 = Coupling(name = 'R2GC_614_42',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_614_43 = Coupling(name = 'R2GC_614_43',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_615_44 = Coupling(name = 'R2GC_615_44',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_616_45 = Coupling(name = 'R2GC_616_45',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_616_46 = Coupling(name = 'R2GC_616_46',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_617_47 = Coupling(name = 'R2GC_617_47',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_618_48 = Coupling(name = 'R2GC_618_48',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_619_49 = Coupling(name = 'R2GC_619_49',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_620_50 = Coupling(name = 'R2GC_620_50',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_621_51 = Coupling(name = 'R2GC_621_51',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_622_52 = Coupling(name = 'R2GC_622_52',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_639_53 = Coupling(name = 'R2GC_639_53',
                       value = '-(CKM2x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_640_54 = Coupling(name = 'R2GC_640_54',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_641_55 = Coupling(name = 'R2GC_641_55',
                       value = '-(CKM2x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_642_56 = Coupling(name = 'R2GC_642_56',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_643_57 = Coupling(name = 'R2GC_643_57',
                       value = '-(CKM1x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_644_58 = Coupling(name = 'R2GC_644_58',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_645_59 = Coupling(name = 'R2GC_645_59',
                       value = '-(CKM1x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_646_60 = Coupling(name = 'R2GC_646_60',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_650_61 = Coupling(name = 'R2GC_650_61',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_651_62 = Coupling(name = 'R2GC_651_62',
                       value = '-(CKM1x3*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_652_63 = Coupling(name = 'R2GC_652_63',
                       value = '-(CKM2x3*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_655_64 = Coupling(name = 'R2GC_655_64',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_656_65 = Coupling(name = 'R2GC_656_65',
                       value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_657_66 = Coupling(name = 'R2GC_657_66',
                       value = '(CKM1x3*G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_658_67 = Coupling(name = 'R2GC_658_67',
                       value = '(CKM2x3*G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_659_68 = Coupling(name = 'R2GC_659_68',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM1x3))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_660_69 = Coupling(name = 'R2GC_660_69',
                       value = '-(G**2*yb*complexconjugate(CKM1x3))/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_661_70 = Coupling(name = 'R2GC_661_70',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM2x3))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_662_71 = Coupling(name = 'R2GC_662_71',
                       value = '-(G**2*yb*complexconjugate(CKM2x3))/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_663_72 = Coupling(name = 'R2GC_663_72',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_663_73 = Coupling(name = 'R2GC_663_73',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_664_74 = Coupling(name = 'R2GC_664_74',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_665_75 = Coupling(name = 'R2GC_665_75',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_666_76 = Coupling(name = 'R2GC_666_76',
                       value = '(5*complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_666_77 = Coupling(name = 'R2GC_666_77',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_667_78 = Coupling(name = 'R2GC_667_78',
                       value = '(23*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_667_79 = Coupling(name = 'R2GC_667_79',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_669_80 = Coupling(name = 'R2GC_669_80',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_670_81 = Coupling(name = 'R2GC_670_81',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_675_82 = Coupling(name = 'R2GC_675_82',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_676_83 = Coupling(name = 'R2GC_676_83',
                       value = '-(CKM3x1*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_677_84 = Coupling(name = 'R2GC_677_84',
                       value = '-(CKM3x2*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_678_85 = Coupling(name = 'R2GC_678_85',
                       value = '-(CKM3x3*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_681_86 = Coupling(name = 'R2GC_681_86',
                       value = '(CKM3x3*G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_682_87 = Coupling(name = 'R2GC_682_87',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_683_88 = Coupling(name = 'R2GC_683_88',
                       value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_684_89 = Coupling(name = 'R2GC_684_89',
                       value = '-(CKM3x1*G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_685_90 = Coupling(name = 'R2GC_685_90',
                       value = '-(CKM3x2*G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_686_91 = Coupling(name = 'R2GC_686_91',
                       value = '-(CKM3x3*G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_687_92 = Coupling(name = 'R2GC_687_92',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM3x1))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_688_93 = Coupling(name = 'R2GC_688_93',
                       value = '(G**2*yt*complexconjugate(CKM3x1))/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_689_94 = Coupling(name = 'R2GC_689_94',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM3x2))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_690_95 = Coupling(name = 'R2GC_690_95',
                       value = '(G**2*yt*complexconjugate(CKM3x2))/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_691_96 = Coupling(name = 'R2GC_691_96',
                       value = '-(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_692_97 = Coupling(name = 'R2GC_692_97',
                       value = '-(G**2*yb*complexconjugate(CKM3x3))/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_693_98 = Coupling(name = 'R2GC_693_98',
                       value = '(G**2*yt*complexconjugate(CKM3x3))/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

UVGC_602_1 = Coupling(name = 'UVGC_602_1',
                      value = {-1:'(53*G**3)/(128.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_603_2 = Coupling(name = 'UVGC_603_2',
                      value = {-1:'G**3/(32.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_604_3 = Coupling(name = 'UVGC_604_3',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_605_4 = Coupling(name = 'UVGC_605_4',
                      value = {-1:'(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_607_5 = Coupling(name = 'UVGC_607_5',
                      value = {-1:'-(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                      order = {'QCD':2,'QED':1})

UVGC_612_6 = Coupling(name = 'UVGC_612_6',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_612_7 = Coupling(name = 'UVGC_612_7',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_613_8 = Coupling(name = 'UVGC_613_8',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_613_9 = Coupling(name = 'UVGC_613_9',
                      value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_615_10 = Coupling(name = 'UVGC_615_10',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_615_11 = Coupling(name = 'UVGC_615_11',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_616_12 = Coupling(name = 'UVGC_616_12',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_616_13 = Coupling(name = 'UVGC_616_13',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_617_14 = Coupling(name = 'UVGC_617_14',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_618_15 = Coupling(name = 'UVGC_618_15',
                       value = {-1:'(5*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_619_16 = Coupling(name = 'UVGC_619_16',
                       value = {-1:'(ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_620_17 = Coupling(name = 'UVGC_620_17',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_621_18 = Coupling(name = 'UVGC_621_18',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_622_19 = Coupling(name = 'UVGC_622_19',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_623_20 = Coupling(name = 'UVGC_623_20',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_623_21 = Coupling(name = 'UVGC_623_21',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_623_22 = Coupling(name = 'UVGC_623_22',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_623_23 = Coupling(name = 'UVGC_623_23',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_623_24 = Coupling(name = 'UVGC_623_24',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_623_25 = Coupling(name = 'UVGC_623_25',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_639_26 = Coupling(name = 'UVGC_639_26',
                       value = {-1:'(CKM2x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_639_27 = Coupling(name = 'UVGC_639_27',
                       value = {-1:'-(CKM2x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_640_28 = Coupling(name = 'UVGC_640_28',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_640_29 = Coupling(name = 'UVGC_640_29',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_641_30 = Coupling(name = 'UVGC_641_30',
                       value = {-1:'(CKM2x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_641_31 = Coupling(name = 'UVGC_641_31',
                       value = {-1:'-(CKM2x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_642_32 = Coupling(name = 'UVGC_642_32',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_642_33 = Coupling(name = 'UVGC_642_33',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_643_34 = Coupling(name = 'UVGC_643_34',
                       value = {-1:'(CKM1x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_643_35 = Coupling(name = 'UVGC_643_35',
                       value = {-1:'-(CKM1x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_644_36 = Coupling(name = 'UVGC_644_36',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_644_37 = Coupling(name = 'UVGC_644_37',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_645_38 = Coupling(name = 'UVGC_645_38',
                       value = {-1:'(CKM1x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_645_39 = Coupling(name = 'UVGC_645_39',
                       value = {-1:'-(CKM1x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_646_40 = Coupling(name = 'UVGC_646_40',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_646_41 = Coupling(name = 'UVGC_646_41',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_647_42 = Coupling(name = 'UVGC_647_42',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_648_43 = Coupling(name = 'UVGC_648_43',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) )',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_649_44 = Coupling(name = 'UVGC_649_44',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_650_45 = Coupling(name = 'UVGC_650_45',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_651_46 = Coupling(name = 'UVGC_651_46',
                       value = {-1:'( -(CKM1x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (CKM1x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*CKM1x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (CKM1x3*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(CKM1x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (CKM1x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_651_47 = Coupling(name = 'UVGC_651_47',
                       value = {-1:'(CKM1x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_651_48 = Coupling(name = 'UVGC_651_48',
                       value = {-1:'-(CKM1x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_652_49 = Coupling(name = 'UVGC_652_49',
                       value = {-1:'( -(CKM2x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (CKM2x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*CKM2x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (CKM2x3*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(CKM2x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (CKM2x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_652_50 = Coupling(name = 'UVGC_652_50',
                       value = {-1:'(CKM2x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_652_51 = Coupling(name = 'UVGC_652_51',
                       value = {-1:'-(CKM2x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_653_52 = Coupling(name = 'UVGC_653_52',
                       value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MB else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) if MB else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_654_53 = Coupling(name = 'UVGC_654_53',
                       value = {-1:'( -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) if MB else (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) if MB else -(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_655_54 = Coupling(name = 'UVGC_655_54',
                       value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_656_55 = Coupling(name = 'UVGC_656_55',
                       value = {-1:'( -(G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_657_56 = Coupling(name = 'UVGC_657_56',
                       value = {-1:'( (CKM1x3*G**2*yb)/(12.*cmath.pi**2) if MB else -(CKM1x3*G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*CKM1x3*G**2*yb)/(24.*cmath.pi**2) - (3*CKM1x3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (CKM1x3*G**2*yb)/(24.*cmath.pi**2) ) - (CKM1x3*G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_657_57 = Coupling(name = 'UVGC_657_57',
                       value = {-1:'-(CKM1x3*G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_657_58 = Coupling(name = 'UVGC_657_58',
                       value = {-1:'(CKM1x3*G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_658_59 = Coupling(name = 'UVGC_658_59',
                       value = {-1:'( (CKM2x3*G**2*yb)/(12.*cmath.pi**2) if MB else -(CKM2x3*G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*CKM2x3*G**2*yb)/(24.*cmath.pi**2) - (3*CKM2x3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (CKM2x3*G**2*yb)/(24.*cmath.pi**2) ) - (CKM2x3*G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_658_60 = Coupling(name = 'UVGC_658_60',
                       value = {-1:'-(CKM2x3*G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_658_61 = Coupling(name = 'UVGC_658_61',
                       value = {-1:'(CKM2x3*G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_659_62 = Coupling(name = 'UVGC_659_62',
                       value = {-1:'( -(ee*complex(0,1)*G**2*complexconjugate(CKM1x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2*complexconjugate(CKM1x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*complexconjugate(CKM1x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*complexconjugate(CKM1x3)*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2*complexconjugate(CKM1x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*complexconjugate(CKM1x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_659_63 = Coupling(name = 'UVGC_659_63',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM1x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_659_64 = Coupling(name = 'UVGC_659_64',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM1x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_660_65 = Coupling(name = 'UVGC_660_65',
                       value = {-1:'( -(G**2*yb*complexconjugate(CKM1x3))/(12.*cmath.pi**2) if MB else (G**2*yb*complexconjugate(CKM1x3))/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb*complexconjugate(CKM1x3))/(24.*cmath.pi**2) + (3*G**2*yb*complexconjugate(CKM1x3)*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yb*complexconjugate(CKM1x3))/(24.*cmath.pi**2) ) + (G**2*yb*complexconjugate(CKM1x3))/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_660_66 = Coupling(name = 'UVGC_660_66',
                       value = {-1:'(G**2*yb*complexconjugate(CKM1x3))/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_660_67 = Coupling(name = 'UVGC_660_67',
                       value = {-1:'-(G**2*yb*complexconjugate(CKM1x3))/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_661_68 = Coupling(name = 'UVGC_661_68',
                       value = {-1:'( -(ee*complex(0,1)*G**2*complexconjugate(CKM2x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2*complexconjugate(CKM2x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*complexconjugate(CKM2x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*complexconjugate(CKM2x3)*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2*complexconjugate(CKM2x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*complexconjugate(CKM2x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_661_69 = Coupling(name = 'UVGC_661_69',
                       value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM2x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_661_70 = Coupling(name = 'UVGC_661_70',
                       value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM2x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_662_71 = Coupling(name = 'UVGC_662_71',
                       value = {-1:'( -(G**2*yb*complexconjugate(CKM2x3))/(12.*cmath.pi**2) if MB else (G**2*yb*complexconjugate(CKM2x3))/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb*complexconjugate(CKM2x3))/(24.*cmath.pi**2) + (3*G**2*yb*complexconjugate(CKM2x3)*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yb*complexconjugate(CKM2x3))/(24.*cmath.pi**2) ) + (G**2*yb*complexconjugate(CKM2x3))/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_662_72 = Coupling(name = 'UVGC_662_72',
                       value = {-1:'(G**2*yb*complexconjugate(CKM2x3))/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_662_73 = Coupling(name = 'UVGC_662_73',
                       value = {-1:'-(G**2*yb*complexconjugate(CKM2x3))/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_663_74 = Coupling(name = 'UVGC_663_74',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_663_75 = Coupling(name = 'UVGC_663_75',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_664_76 = Coupling(name = 'UVGC_664_76',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_664_77 = Coupling(name = 'UVGC_664_77',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_664_78 = Coupling(name = 'UVGC_664_78',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_664_79 = Coupling(name = 'UVGC_664_79',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':2})

UVGC_665_80 = Coupling(name = 'UVGC_665_80',
                       value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_665_81 = Coupling(name = 'UVGC_665_81',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_665_82 = Coupling(name = 'UVGC_665_82',
                       value = {-1:'( 0 if MT else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':3})

UVGC_666_83 = Coupling(name = 'UVGC_666_83',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_666_84 = Coupling(name = 'UVGC_666_84',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_666_85 = Coupling(name = 'UVGC_666_85',
                       value = {-1:'(3*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_666_86 = Coupling(name = 'UVGC_666_86',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(12.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_667_87 = Coupling(name = 'UVGC_667_87',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_667_88 = Coupling(name = 'UVGC_667_88',
                       value = {-1:'(21*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_668_89 = Coupling(name = 'UVGC_668_89',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_668_90 = Coupling(name = 'UVGC_668_90',
                       value = {-1:'-(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_668_91 = Coupling(name = 'UVGC_668_91',
                       value = {-1:'(13*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_668_92 = Coupling(name = 'UVGC_668_92',
                       value = {-1:'( 0 if MT else -(complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( -(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_669_93 = Coupling(name = 'UVGC_669_93',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_669_94 = Coupling(name = 'UVGC_669_94',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_669_95 = Coupling(name = 'UVGC_669_95',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_669_96 = Coupling(name = 'UVGC_669_96',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_669_97 = Coupling(name = 'UVGC_669_97',
                       value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                       order = {'QCD':4})

UVGC_670_98 = Coupling(name = 'UVGC_670_98',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_670_99 = Coupling(name = 'UVGC_670_99',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_671_100 = Coupling(name = 'UVGC_671_100',
                        value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                        order = {'QCD':4})

UVGC_671_101 = Coupling(name = 'UVGC_671_101',
                        value = {-1:'(complex(0,1)*G**4)/(12.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_671_102 = Coupling(name = 'UVGC_671_102',
                        value = {-1:'(-19*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_671_103 = Coupling(name = 'UVGC_671_103',
                        value = {-1:'( 0 if MT else (complex(0,1)*G**4)/(12.*cmath.pi**2) )',0:'( (complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2) if MT else 0 )'},
                        order = {'QCD':4})

UVGC_672_104 = Coupling(name = 'UVGC_672_104',
                        value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_673_105 = Coupling(name = 'UVGC_673_105',
                        value = {-1:'( -(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) if MT else (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) )',0:'( (-5*ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) ) + (ee*complex(0,1)*G**2)/(18.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_674_106 = Coupling(name = 'UVGC_674_106',
                        value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MT else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2) if MT else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_675_107 = Coupling(name = 'UVGC_675_107',
                        value = {-1:'( (complex(0,1)*G**2*MT)/(6.*cmath.pi**2) if MT else -(complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MT)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MT)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2 if MT else (complex(0,1)*G**2*MT)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MT)/(12.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_676_108 = Coupling(name = 'UVGC_676_108',
                        value = {-1:'(CKM3x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_676_109 = Coupling(name = 'UVGC_676_109',
                        value = {-1:'( -(CKM3x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (CKM3x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*CKM3x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (CKM3x1*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(CKM3x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (CKM3x1*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_676_110 = Coupling(name = 'UVGC_676_110',
                        value = {-1:'-(CKM3x1*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_677_111 = Coupling(name = 'UVGC_677_111',
                        value = {-1:'(CKM3x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_677_112 = Coupling(name = 'UVGC_677_112',
                        value = {-1:'( -(CKM3x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (CKM3x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*CKM3x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (CKM3x2*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(CKM3x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (CKM3x2*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_677_113 = Coupling(name = 'UVGC_677_113',
                        value = {-1:'-(CKM3x2*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_678_114 = Coupling(name = 'UVGC_678_114',
                        value = {-1:'( -(CKM3x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (CKM3x3*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_678_115 = Coupling(name = 'UVGC_678_115',
                        value = {-1:'( -(CKM3x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (CKM3x3*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (CKM3x3*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_678_116 = Coupling(name = 'UVGC_678_116',
                        value = {-1:'-(CKM3x3*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_679_117 = Coupling(name = 'UVGC_679_117',
                        value = {-1:'( -(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) if MT else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)',0:'( (-5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) if MT else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_680_118 = Coupling(name = 'UVGC_680_118',
                        value = {-1:'( (ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) if MT else -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)',0:'( (5*ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) if MT else (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) ) - (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_681_119 = Coupling(name = 'UVGC_681_119',
                        value = {-1:'( (CKM3x3*G**2*yb)/(12.*cmath.pi**2) if MB else -(CKM3x3*G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*CKM3x3*G**2*yb)/(24.*cmath.pi**2) - (3*CKM3x3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (CKM3x3*G**2*yb)/(24.*cmath.pi**2) ) - (CKM3x3*G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_681_120 = Coupling(name = 'UVGC_681_120',
                        value = {-1:'( (CKM3x3*G**2*yb)/(12.*cmath.pi**2) if MT else -(CKM3x3*G**2*yb)/(24.*cmath.pi**2) )',0:'( (5*CKM3x3*G**2*yb)/(24.*cmath.pi**2) - (CKM3x3*G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (CKM3x3*G**2*yb)/(24.*cmath.pi**2) ) - (CKM3x3*G**2*yb)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_681_121 = Coupling(name = 'UVGC_681_121',
                        value = {-1:'(CKM3x3*G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_682_122 = Coupling(name = 'UVGC_682_122',
                        value = {-1:'( (G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_683_123 = Coupling(name = 'UVGC_683_123',
                        value = {-1:'( (complex(0,1)*G**2*yt)/(6.*cmath.pi**2*cmath.sqrt(2)) if MT else -(complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yt)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MT else (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yt)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_684_124 = Coupling(name = 'UVGC_684_124',
                        value = {-1:'(CKM3x1*G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_684_125 = Coupling(name = 'UVGC_684_125',
                        value = {-1:'( -(CKM3x1*G**2*yt)/(12.*cmath.pi**2) if MT else (CKM3x1*G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*CKM3x1*G**2*yt)/(24.*cmath.pi**2) + (3*CKM3x1*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(CKM3x1*G**2*yt)/(24.*cmath.pi**2) ) + (CKM3x1*G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_684_126 = Coupling(name = 'UVGC_684_126',
                        value = {-1:'-(CKM3x1*G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_685_127 = Coupling(name = 'UVGC_685_127',
                        value = {-1:'(CKM3x2*G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_685_128 = Coupling(name = 'UVGC_685_128',
                        value = {-1:'( -(CKM3x2*G**2*yt)/(12.*cmath.pi**2) if MT else (CKM3x2*G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*CKM3x2*G**2*yt)/(24.*cmath.pi**2) + (3*CKM3x2*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(CKM3x2*G**2*yt)/(24.*cmath.pi**2) ) + (CKM3x2*G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_685_129 = Coupling(name = 'UVGC_685_129',
                        value = {-1:'-(CKM3x2*G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_686_130 = Coupling(name = 'UVGC_686_130',
                        value = {-1:'( -(CKM3x3*G**2*yt)/(12.*cmath.pi**2) if MB else (CKM3x3*G**2*yt)/(24.*cmath.pi**2) )',0:'( (-5*CKM3x3*G**2*yt)/(24.*cmath.pi**2) + (CKM3x3*G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(CKM3x3*G**2*yt)/(24.*cmath.pi**2) ) + (CKM3x3*G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_686_131 = Coupling(name = 'UVGC_686_131',
                        value = {-1:'( -(CKM3x3*G**2*yt)/(12.*cmath.pi**2) if MT else (CKM3x3*G**2*yt)/(24.*cmath.pi**2) )',0:'( (-13*CKM3x3*G**2*yt)/(24.*cmath.pi**2) + (3*CKM3x3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(CKM3x3*G**2*yt)/(24.*cmath.pi**2) ) + (CKM3x3*G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_686_132 = Coupling(name = 'UVGC_686_132',
                        value = {-1:'-(CKM3x3*G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_687_133 = Coupling(name = 'UVGC_687_133',
                        value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM3x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_687_134 = Coupling(name = 'UVGC_687_134',
                        value = {-1:'( -(ee*complex(0,1)*G**2*complexconjugate(CKM3x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2*complexconjugate(CKM3x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*complexconjugate(CKM3x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x1)*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2*complexconjugate(CKM3x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x1))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_687_135 = Coupling(name = 'UVGC_687_135',
                        value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM3x1))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_688_136 = Coupling(name = 'UVGC_688_136',
                        value = {-1:'-(G**2*yt*complexconjugate(CKM3x1))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_688_137 = Coupling(name = 'UVGC_688_137',
                        value = {-1:'( (G**2*yt*complexconjugate(CKM3x1))/(12.*cmath.pi**2) if MT else -(G**2*yt*complexconjugate(CKM3x1))/(24.*cmath.pi**2) )',0:'( (13*G**2*yt*complexconjugate(CKM3x1))/(24.*cmath.pi**2) - (3*G**2*yt*complexconjugate(CKM3x1)*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt*complexconjugate(CKM3x1))/(24.*cmath.pi**2) ) - (G**2*yt*complexconjugate(CKM3x1))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_688_138 = Coupling(name = 'UVGC_688_138',
                        value = {-1:'(G**2*yt*complexconjugate(CKM3x1))/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_689_139 = Coupling(name = 'UVGC_689_139',
                        value = {-1:'(ee*complex(0,1)*G**2*complexconjugate(CKM3x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_689_140 = Coupling(name = 'UVGC_689_140',
                        value = {-1:'( -(ee*complex(0,1)*G**2*complexconjugate(CKM3x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2*complexconjugate(CKM3x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*complexconjugate(CKM3x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x2)*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2*complexconjugate(CKM3x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x2))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_689_141 = Coupling(name = 'UVGC_689_141',
                        value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM3x2))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_690_142 = Coupling(name = 'UVGC_690_142',
                        value = {-1:'-(G**2*yt*complexconjugate(CKM3x2))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_690_143 = Coupling(name = 'UVGC_690_143',
                        value = {-1:'( (G**2*yt*complexconjugate(CKM3x2))/(12.*cmath.pi**2) if MT else -(G**2*yt*complexconjugate(CKM3x2))/(24.*cmath.pi**2) )',0:'( (13*G**2*yt*complexconjugate(CKM3x2))/(24.*cmath.pi**2) - (3*G**2*yt*complexconjugate(CKM3x2)*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt*complexconjugate(CKM3x2))/(24.*cmath.pi**2) ) - (G**2*yt*complexconjugate(CKM3x2))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_690_144 = Coupling(name = 'UVGC_690_144',
                        value = {-1:'(G**2*yt*complexconjugate(CKM3x2))/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_691_145 = Coupling(name = 'UVGC_691_145',
                        value = {-1:'( -(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x3)*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_691_146 = Coupling(name = 'UVGC_691_146',
                        value = {-1:'( -(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else (ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x3)*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MT else -(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_691_147 = Coupling(name = 'UVGC_691_147',
                        value = {-1:'-(ee*complex(0,1)*G**2*complexconjugate(CKM3x3))/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_692_148 = Coupling(name = 'UVGC_692_148',
                        value = {-1:'( -(G**2*yb*complexconjugate(CKM3x3))/(12.*cmath.pi**2) if MB else (G**2*yb*complexconjugate(CKM3x3))/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb*complexconjugate(CKM3x3))/(24.*cmath.pi**2) + (3*G**2*yb*complexconjugate(CKM3x3)*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yb*complexconjugate(CKM3x3))/(24.*cmath.pi**2) ) + (G**2*yb*complexconjugate(CKM3x3))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_692_149 = Coupling(name = 'UVGC_692_149',
                        value = {-1:'( -(G**2*yb*complexconjugate(CKM3x3))/(12.*cmath.pi**2) if MT else (G**2*yb*complexconjugate(CKM3x3))/(24.*cmath.pi**2) )',0:'( (-5*G**2*yb*complexconjugate(CKM3x3))/(24.*cmath.pi**2) + (G**2*yb*complexconjugate(CKM3x3)*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else -(G**2*yb*complexconjugate(CKM3x3))/(24.*cmath.pi**2) ) + (G**2*yb*complexconjugate(CKM3x3))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_692_150 = Coupling(name = 'UVGC_692_150',
                        value = {-1:'-(G**2*yb*complexconjugate(CKM3x3))/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_693_151 = Coupling(name = 'UVGC_693_151',
                        value = {-1:'( (G**2*yt*complexconjugate(CKM3x3))/(12.*cmath.pi**2) if MB else -(G**2*yt*complexconjugate(CKM3x3))/(24.*cmath.pi**2) )',0:'( (5*G**2*yt*complexconjugate(CKM3x3))/(24.*cmath.pi**2) - (G**2*yt*complexconjugate(CKM3x3)*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yt*complexconjugate(CKM3x3))/(24.*cmath.pi**2) ) - (G**2*yt*complexconjugate(CKM3x3))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_693_152 = Coupling(name = 'UVGC_693_152',
                        value = {-1:'( (G**2*yt*complexconjugate(CKM3x3))/(12.*cmath.pi**2) if MT else -(G**2*yt*complexconjugate(CKM3x3))/(24.*cmath.pi**2) )',0:'( (13*G**2*yt*complexconjugate(CKM3x3))/(24.*cmath.pi**2) - (3*G**2*yt*complexconjugate(CKM3x3)*reglog(MT/MU_R))/(4.*cmath.pi**2) if MT else (G**2*yt*complexconjugate(CKM3x3))/(24.*cmath.pi**2) ) - (G**2*yt*complexconjugate(CKM3x3))/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_693_153 = Coupling(name = 'UVGC_693_153',
                        value = {-1:'(G**2*yt*complexconjugate(CKM3x3))/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

