Requestor: Monika Wielers and Arnau Morancho Tardà
Contents: NLO UFO model file for SM + heavy Majorana neutrinos. Assumes three heavy neutrinos that can couple to all lepton flavors. Gen III (t,b,tau) fermions are massive, Gen I and II fermions are massless, diagonal CKM matrix with unit entries, and nonzero tau width.
Source: https://feynrules.irmp.ucl.ac.be/attachment/wiki/HeavyN/SM_HeavyN_Gen3Mass_NLO.tgz
Webpage: https://feynrules.irmp.ucl.ac.be/wiki/HeavyN
Paper1: https://arxiv.org/abs/1411.7305
Paper2: https://arxiv.org/abs/1602.06957

