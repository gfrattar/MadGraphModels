Requestor: Carlo Gottardo
Content: Charged lepton flavour violation, with effective 4-fermion operators
Paper: https://arxiv.org/abs/1507.07163
Repo: https://gitlab.cern.ch/cgottard/clfvLO_UFO_model
JIRA: https://its.cern.ch/jira/browse/AGENE-1365