# ----------------------------------------------------------------------  
# This model file was automatically created by SARAH version4.12.2 
# SARAH References: arXiv:0806.0538, arXiv:0909.2863, arXiv:1002.0840    
# (c) Florian Staub, 2011  
# ----------------------------------------------------------------------  
# File created at 11:43 on 10.11.2017   
# ----------------------------------------------------------------------  
 
 
from __future__ import division 
from object_library import all_particles,Particle 
import parameters as Param 


ft = Particle(pdg_code =3035, 
	 name = 'ft' ,
	 antiname = 'ftbar' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Mft ,
	 width = Param.Wft ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = 5/3 ,
	 texname = 'ft' ,
	 antitexname = 'ftbar' ) 
 
ftbar = ft.anti() 
 
 
nu1 = Particle(pdg_code =12, 
	 name = 'nu1' ,
	 antiname = 'nu1bar' ,
	 spin = 2 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = 0 ,
	 texname = 'nu1' ,
	 antitexname = 'nu1bar' ) 
 
nu1bar = nu1.anti() 
 
 
nu2 = Particle(pdg_code =14, 
	 name = 'nu2' ,
	 antiname = 'nu2bar' ,
	 spin = 2 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = 0 ,
	 texname = 'nu2' ,
	 antitexname = 'nu2bar' ) 
 
nu2bar = nu2.anti() 
 
 
nu3 = Particle(pdg_code =16, 
	 name = 'nu3' ,
	 antiname = 'nu3bar' ,
	 spin = 2 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = 0 ,
	 texname = 'nu3' ,
	 antitexname = 'nu3bar' ) 
 
nu3bar = nu3.anti() 
 
 
d = Particle(pdg_code =1, 
	 name = 'd' ,
	 antiname = 'd~' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Md1 ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = -1/3 ,
	 texname = 'd' ,
	 antitexname = 'd~' ) 
 
d__tilde__ = d.anti() 
 
 
s = Particle(pdg_code =3, 
	 name = 's' ,
	 antiname = 's~' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Md2 ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = -1/3 ,
	 texname = 's' ,
	 antitexname = 's~' ) 
 
s__tilde__ = d.anti() 
 
 
d3 = Particle(pdg_code =5, 
	 name = 'd3' ,
	 antiname = 'd3bar' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Md3 ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = -1/3 ,
	 texname = 'd3' ,
	 antitexname = 'd3bar' ) 
 
d3bar = d3.anti() 
 
 
fdd1 = Particle(pdg_code =3026, 
	 name = 'fdd1' ,
	 antiname = 'fdd1bar' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Mfdd1 ,
	 width = Param.Wfdd1 ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = -4/3 ,
	 texname = 'fdd1' ,
	 antitexname = 'fdd1bar' ) 
 
fdd1bar = fdd1.anti() 
 
 
fdd2 = Particle(pdg_code =3027, 
	 name = 'fdd2' ,
	 antiname = 'fdd2bar' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Mfdd2 ,
	 width = Param.Wfdd2 ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = -4/3 ,
	 texname = 'fdd2' ,
	 antitexname = 'fdd2bar' ) 
 
fdd2bar = fdd2.anti() 
 
 
u = Particle(pdg_code =2, 
	 name = 'u' ,
	 antiname = 'u~' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Mu1 ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = 2/3 ,
	 texname = 'u' ,
	 antitexname = 'u~' ) 
 
u__tilde__ = u.anti() 
 
 
c = Particle(pdg_code =4, 
	 name = 'c' ,
	 antiname = 'c~' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Mu2 ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = 2/3 ,
	 texname = 'c' ,
	 antitexname = 'c~' ) 
 
c__tilde__ = c.anti() 
 
 
u3 = Particle(pdg_code =6, 
	 name = 'u3' ,
	 antiname = 'u3bar' ,
	 spin = 2 ,
	 color = 3 ,
	 mass = Param.Mu3 ,
	 width = Param.Wu3 ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = 2/3 ,
	 texname = 'u3' ,
	 antitexname = 'u3bar' ) 
 
u3bar = u3.anti() 
 
 
e1 = Particle(pdg_code =11, 
	 name = 'e1' ,
	 antiname = 'e1bar' ,
	 spin = 2 ,
	 color = 1 ,
	 mass = Param.Me1 ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = -1 ,
	 texname = 'e1' ,
	 antitexname = 'e1bar' ) 
 
e1bar = e1.anti() 
 
 
e2 = Particle(pdg_code =13, 
	 name = 'e2' ,
	 antiname = 'e2bar' ,
	 spin = 2 ,
	 color = 1 ,
	 mass = Param.Me2 ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = -1 ,
	 texname = 'e2' ,
	 antitexname = 'e2bar' ) 
 
e2bar = e2.anti() 
 
 
e3 = Particle(pdg_code =15, 
	 name = 'e3' ,
	 antiname = 'e3bar' ,
	 spin = 2 ,
	 color = 1 ,
	 mass = Param.Me3 ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'swavy' ,
	 charge = -1 ,
	 texname = 'e3' ,
	 antitexname = 'e3bar' ) 
 
e3bar = e3.anti() 
 
 
h1 = Particle(pdg_code =25, 
	 name = 'h1' ,
	 antiname = 'h1' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.Mh1 ,
	 width = Param.Wh1 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 texname = 'h1' ,
	 antitexname = 'h1' ) 
 
h2 = Particle(pdg_code =35, 
	 name = 'h2' ,
	 antiname = 'h2' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.Mh2 ,
	 width = Param.Wh2 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 texname = 'h2' ,
	 antitexname = 'h2' ) 
 
h3 = Particle(pdg_code =1045, 
	 name = 'h3' ,
	 antiname = 'h3' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.Mh3 ,
	 width = Param.Wh3 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 texname = 'h3' ,
	 antitexname = 'h3' ) 
 
h4 = Particle(pdg_code =1055, 
	 name = 'h4' ,
	 antiname = 'h4' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.Mh4 ,
	 width = Param.Wh4 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 texname = 'h4' ,
	 antitexname = 'h4' ) 
 
h5 = Particle(pdg_code =1065, 
	 name = 'h5' ,
	 antiname = 'h5' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.Mh5 ,
	 width = Param.Wh5 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 texname = 'h5' ,
	 antitexname = 'h5' ) 
 
Ah1 = Particle(pdg_code =999900, 
	 name = 'Ah1' ,
	 antiname = 'Ah1' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 GoldstoneBoson = True ,
	 texname = 'Ah1' ,
	 antitexname = 'Ah1' ) 
 
Ah2 = Particle(pdg_code =999901, 
	 name = 'Ah2' ,
	 antiname = 'Ah2' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 GoldstoneBoson = True ,
	 texname = 'Ah2' ,
	 antitexname = 'Ah2' ) 
 
Ah3 = Particle(pdg_code =36, 
	 name = 'Ah3' ,
	 antiname = 'Ah3' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MAh3 ,
	 width = Param.WAh3 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 texname = 'Ah3' ,
	 antitexname = 'Ah3' ) 
 
Ah4 = Particle(pdg_code =1046, 
	 name = 'Ah4' ,
	 antiname = 'Ah4' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MAh4 ,
	 width = Param.WAh4 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 texname = 'Ah4' ,
	 antitexname = 'Ah4' ) 
 
Ah5 = Particle(pdg_code =1056, 
	 name = 'Ah5' ,
	 antiname = 'Ah5' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MAh5 ,
	 width = Param.WAh5 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 0 ,
	 texname = 'Ah5' ,
	 antitexname = 'Ah5' ) 
 
Hpm1 = Particle(pdg_code =999902, 
	 name = 'Hpm1' ,
	 antiname = 'Hpm1c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 1 ,
	 GoldstoneBoson = True ,
	 texname = 'Hpm1' ,
	 antitexname = 'Hpm1c' ) 
 
Hpm1c = Hpm1.anti() 
 
 
Hpm2 = Particle(pdg_code =999903, 
	 name = 'Hpm2' ,
	 antiname = 'Hpm2c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 1 ,
	 GoldstoneBoson = True ,
	 texname = 'Hpm2' ,
	 antitexname = 'Hpm2c' ) 
 
Hpm2c = Hpm2.anti() 
 
 
Hpm3 = Particle(pdg_code =37, 
	 name = 'Hpm3' ,
	 antiname = 'Hpm3c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MHpm3 ,
	 width = Param.WHpm3 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 1 ,
	 texname = 'Hpm3' ,
	 antitexname = 'Hpm3c' ) 
 
Hpm3c = Hpm3.anti() 
 
 
Hpm4 = Particle(pdg_code =39, 
	 name = 'Hpm4' ,
	 antiname = 'Hpm4c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MHpm4 ,
	 width = Param.WHpm4 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 1 ,
	 texname = 'Hpm4' ,
	 antitexname = 'Hpm4c' ) 
 
Hpm4c = Hpm4.anti() 
 
 
Hpm5 = Particle(pdg_code =1037, 
	 name = 'Hpm5' ,
	 antiname = 'Hpm5c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MHpm5 ,
	 width = Param.WHpm5 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 1 ,
	 texname = 'Hpm5' ,
	 antitexname = 'Hpm5c' ) 
 
Hpm5c = Hpm5.anti() 
 
 
Hpm6 = Particle(pdg_code =1039, 
	 name = 'Hpm6' ,
	 antiname = 'Hpm6c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MHpm6 ,
	 width = Param.WHpm6 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 1 ,
	 texname = 'Hpm6' ,
	 antitexname = 'Hpm6c' ) 
 
Hpm6c = Hpm6.anti() 
 
 
Dpm1 = Particle(pdg_code =999904, 
	 name = 'Dpm1' ,
	 antiname = 'Dpm1c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 2 ,
	 GoldstoneBoson = True ,
	 texname = 'Dpm1' ,
	 antitexname = 'Dpm1c' ) 
 
Dpm1c = Dpm1.anti() 
 
 
Dpm2 = Particle(pdg_code =43, 
	 name = 'Dpm2' ,
	 antiname = 'Dpm2c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MDpm2 ,
	 width = Param.WDpm2 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 2 ,
	 texname = 'Dpm2' ,
	 antitexname = 'Dpm2c' ) 
 
Dpm2c = Dpm2.anti() 
 
 
Dpm3 = Particle(pdg_code =1051, 
	 name = 'Dpm3' ,
	 antiname = 'Dpm3c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MDpm3 ,
	 width = Param.WDpm3 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 2 ,
	 texname = 'Dpm3' ,
	 antitexname = 'Dpm3c' ) 
 
Dpm3c = Dpm3.anti() 
 
 
Dpm4 = Particle(pdg_code =1061, 
	 name = 'Dpm4' ,
	 antiname = 'Dpm4c' ,
	 spin = 1 ,
	 color = 1 ,
	 mass = Param.MDpm4 ,
	 width = Param.WDpm4 ,
	 GhostNumber = 0, 
	 line = 'dashed' ,
	 charge = 2 ,
	 texname = 'Dpm4' ,
	 antitexname = 'Dpm4c' ) 
 
Dpm4c = Dpm4.anti() 
 
 
g = Particle(pdg_code =21, 
	 name = 'g' ,
	 antiname = 'g' ,
	 spin = 3 ,
	 color = 8 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'wavy' ,
	 charge = 0 ,
	 texname = 'g' ,
	 antitexname = 'g' ) 
 
A = Particle(pdg_code =22, 
	 name = 'A' ,
	 antiname = 'A' ,
	 spin = 3 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 0, 
	 line = 'wavy' ,
	 charge = 0 ,
	 texname = 'A' ,
	 antitexname = 'A' ) 
 
Z = Particle(pdg_code =23, 
	 name = 'Z' ,
	 antiname = 'Z' ,
	 spin = 3 ,
	 color = 1 ,
	 mass = Param.MZ ,
	 width = Param.WZ ,
	 GhostNumber = 0, 
	 line = 'wavy' ,
	 charge = 0 ,
	 texname = 'Z' ,
	 antitexname = 'Z' ) 
 
Zp = Particle(pdg_code =2023, 
	 name = 'Zp' ,
	 antiname = 'Zp' ,
	 spin = 3 ,
	 color = 1 ,
	 mass = Param.MZp ,
	 width = Param.WZp ,
	 GhostNumber = 0, 
	 line = 'wavy' ,
	 charge = 0 ,
	 texname = 'Zp' ,
	 antitexname = 'Zp' ) 
 
Ypp = Particle(pdg_code =44, 
	 name = 'Ypp' ,
	 antiname = 'Yppc' ,
	 spin = 3 ,
	 color = 1 ,
	 mass = Param.MYpp ,
	 width = Param.WYpp ,
	 GhostNumber = 0, 
	 line = 'wavy' ,
	 charge = 2 ,
	 texname = 'Ypp' ,
	 antitexname = 'Yppc' ) 
 
Yppc = Ypp.anti() 
 
 
Wp = Particle(pdg_code =24, 
	 name = 'Wp' ,
	 antiname = 'Wpc' ,
	 spin = 3 ,
	 color = 1 ,
	 mass = Param.MWp ,
	 width = Param.WWp ,
	 GhostNumber = 0, 
	 line = 'wavy' ,
	 charge = 1 ,
	 texname = 'Wp' ,
	 antitexname = 'Wpc' ) 
 
Wpc = Wp.anti() 
 
 
Xp = Particle(pdg_code =224, 
	 name = 'Xp' ,
	 antiname = 'Xpc' ,
	 spin = 3 ,
	 color = 1 ,
	 mass = Param.MXp ,
	 width = Param.WXp ,
	 GhostNumber = 0, 
	 line = 'wavy' ,
	 charge = 1 ,
	 texname = 'Xp' ,
	 antitexname = 'Xpc' ) 
 
Xpc = Xp.anti() 
 
 
gG = Particle(pdg_code =999905, 
	 name = 'gG' ,
	 antiname = 'gGc' ,
	 spin = -1 ,
	 color = 8 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = 0 ,
	 texname = 'gG' ,
	 antitexname = 'gGc' ) 
 
gGc = gG.anti() 
 
 
gA = Particle(pdg_code =999906, 
	 name = 'gA' ,
	 antiname = 'gAc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.ZERO ,
	 width = Param.ZERO ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = 0 ,
	 texname = 'gA' ,
	 antitexname = 'gAc' ) 
 
gAc = gA.anti() 
 
 
gZ = Particle(pdg_code =999907, 
	 name = 'gZ' ,
	 antiname = 'gZc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.MZ ,
	 width = Param.WZ ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = 0 ,
	 texname = 'gZ' ,
	 antitexname = 'gZc' ) 
 
gZc = gZ.anti() 
 
 
gZp = Particle(pdg_code =999908, 
	 name = 'gZp' ,
	 antiname = 'gZpc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.MZp ,
	 width = Param.WZp ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = 0 ,
	 texname = 'gZp' ,
	 antitexname = 'gZpc' ) 
 
gZpc = gZp.anti() 
 
 
gYpp = Particle(pdg_code =999909, 
	 name = 'gYpp' ,
	 antiname = 'gYppc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.MYpp ,
	 width = Param.WYpp ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = 2 ,
	 texname = 'gYpp' ,
	 antitexname = 'gYppc' ) 
 
gYppc = gYpp.anti() 
 
 
gYmm = Particle(pdg_code =999910, 
	 name = 'gYmm' ,
	 antiname = 'gYmmc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.MYpp ,
	 width = Param.WYpp ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = -2 ,
	 texname = 'gYmm' ,
	 antitexname = 'gYmmc' ) 
 
gYmmc = gYmm.anti() 
 
 
gWp = Particle(pdg_code =999911, 
	 name = 'gWp' ,
	 antiname = 'gWpc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.MWp ,
	 width = Param.WWp ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = 1 ,
	 texname = 'gWp' ,
	 antitexname = 'gWpc' ) 
 
gWpc = gWp.anti() 
 
 
gWC = Particle(pdg_code =999912, 
	 name = 'gWC' ,
	 antiname = 'gWCc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.MWp ,
	 width = Param.WWp ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = -1 ,
	 texname = 'gWC' ,
	 antitexname = 'gWCc' ) 
 
gWCc = gWC.anti() 
 
 
gXp = Particle(pdg_code =999913, 
	 name = 'gXp' ,
	 antiname = 'gXpc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.MXp ,
	 width = Param.WXp ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = 1 ,
	 texname = 'gXp' ,
	 antitexname = 'gXpc' ) 
 
gXpc = gXp.anti() 
 
 
gXm = Particle(pdg_code =999914, 
	 name = 'gXm' ,
	 antiname = 'gXmc' ,
	 spin = -1 ,
	 color = 1 ,
	 mass = Param.MXp ,
	 width = Param.WXp ,
	 GhostNumber = 1, 
	 line = 'dotted' ,
	 charge = -1 ,
	 texname = 'gXm' ,
	 antitexname = 'gXmc' ) 
 
gXmc = gXm.anti() 
 
 
