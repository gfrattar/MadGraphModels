Requestor: Henry Meng
Content: A spin-1 gauge boson (Z') and two composite SU(2)L fermion doublets
Paper: https://arxiv.org/abs/1803.02364
Source: Mikael Chala and Michael Spannowsky (Private Communications)
