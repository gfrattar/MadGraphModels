# This file was automatically created by FeynRules 2.3.14
# Mathematica version: 10.3.0 for Mac OS X x86 (64-bit) (October 9, 2015)
# Date: Sat 1 Jun 2019 04:20:40


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



GC_1 = Coupling(name = 'GC_1',
                value = '-(ee*complex(0,1))/3.',
                order = {'QED':1})

GC_2 = Coupling(name = 'GC_2',
                value = '(2*ee*complex(0,1))/3.',
                order = {'QED':1})

GC_3 = Coupling(name = 'GC_3',
                value = '-(ee*complex(0,1))',
                order = {'QED':1})

GC_4 = Coupling(name = 'GC_4',
                value = 'ee*complex(0,1)',
                order = {'QED':1})

GC_5 = Coupling(name = 'GC_5',
                value = 'ee**2*complex(0,1)',
                order = {'QED':2})

GC_6 = Coupling(name = 'GC_6',
                value = '-2*FM0*complex(0,1)',
                order = {'M0':1})

GC_7 = Coupling(name = 'GC_7',
                value = '-2*cw**2*FM0*complex(0,1)',
                order = {'M0':1})

GC_8 = Coupling(name = 'GC_8',
                value = '2*ee*FM0*complex(0,1)',
                order = {'M0':1,'QED':1})

GC_9 = Coupling(name = 'GC_9',
                value = 'ee**2*FM0*complex(0,1)',
                order = {'M0':1,'QED':2})

GC_10 = Coupling(name = 'GC_10',
                 value = '-(FM1*complex(0,1))/2.',
                 order = {'M1':1})

GC_11 = Coupling(name = 'GC_11',
                 value = '-(cw**2*FM1*complex(0,1))/2.',
                 order = {'M1':1})

GC_12 = Coupling(name = 'GC_12',
                 value = '-(ee*FM1*complex(0,1))/2.',
                 order = {'M1':1,'QED':1})

GC_13 = Coupling(name = 'GC_13',
                 value = '(ee**2*FM1*complex(0,1))/4.',
                 order = {'M1':1,'QED':2})

GC_14 = Coupling(name = 'GC_14',
                 value = '(FM7*complex(0,1))/4.',
                 order = {'M7':1})

GC_15 = Coupling(name = 'GC_15',
                 value = '(cw**2*FM7*complex(0,1))/4.',
                 order = {'M7':1})

GC_16 = Coupling(name = 'GC_16',
                 value = '-(ee*FM7*complex(0,1))/8.',
                 order = {'M7':1,'QED':1})

GC_17 = Coupling(name = 'GC_17',
                 value = '-(ee**2*FM7*complex(0,1))/8.',
                 order = {'M7':1,'QED':2})

GC_18 = Coupling(name = 'GC_18',
                 value = '-(ee**2*FM7*complex(0,1))/4.',
                 order = {'M7':1,'QED':2})

GC_19 = Coupling(name = 'GC_19',
                 value = '-(ee**3*FM7*complex(0,1))/4.',
                 order = {'M7':1,'QED':3})

GC_20 = Coupling(name = 'GC_20',
                 value = '-(ee**4*FM7*complex(0,1))/(4.*cw**2)',
                 order = {'M7':1,'QED':4})

GC_21 = Coupling(name = 'GC_21',
                 value = '2*complex(0,1)*FS0',
                 order = {'S0':1})

GC_22 = Coupling(name = 'GC_22',
                 value = '2*complex(0,1)*FS1',
                 order = {'S1':1})

GC_23 = Coupling(name = 'GC_23',
                 value = '2*complex(0,1)*FS2',
                 order = {'S2':1})

GC_24 = Coupling(name = 'GC_24',
                 value = '8*complex(0,1)*FT0',
                 order = {'T0':1})

GC_25 = Coupling(name = 'GC_25',
                 value = '8*cw**2*complex(0,1)*FT0',
                 order = {'T0':1})

GC_26 = Coupling(name = 'GC_26',
                 value = '8*cw**4*complex(0,1)*FT0',
                 order = {'T0':1})

GC_27 = Coupling(name = 'GC_27',
                 value = '-8*ee*complex(0,1)*FT0',
                 order = {'QED':1,'T0':1})

GC_28 = Coupling(name = 'GC_28',
                 value = '-8*cw**2*ee*complex(0,1)*FT0',
                 order = {'QED':1,'T0':1})

GC_29 = Coupling(name = 'GC_29',
                 value = '8*ee**2*complex(0,1)*FT0',
                 order = {'QED':2,'T0':1})

GC_30 = Coupling(name = 'GC_30',
                 value = '-8*cw**2*ee**2*complex(0,1)*FT0',
                 order = {'QED':2,'T0':1})

GC_31 = Coupling(name = 'GC_31',
                 value = '8*ee**3*complex(0,1)*FT0',
                 order = {'QED':3,'T0':1})

GC_32 = Coupling(name = 'GC_32',
                 value = '16*ee**4*complex(0,1)*FT0',
                 order = {'QED':4,'T0':1})

GC_33 = Coupling(name = 'GC_33',
                 value = '4*complex(0,1)*FT1',
                 order = {'T1':1})

GC_34 = Coupling(name = 'GC_34',
                 value = '4*cw**2*complex(0,1)*FT1',
                 order = {'T1':1})

GC_35 = Coupling(name = 'GC_35',
                 value = '8*cw**4*complex(0,1)*FT1',
                 order = {'T1':1})

GC_36 = Coupling(name = 'GC_36',
                 value = '4*ee*complex(0,1)*FT1',
                 order = {'QED':1,'T1':1})

GC_37 = Coupling(name = 'GC_37',
                 value = '4*cw**2*ee*complex(0,1)*FT1',
                 order = {'QED':1,'T1':1})

GC_38 = Coupling(name = 'GC_38',
                 value = '8*ee**2*complex(0,1)*FT1',
                 order = {'QED':2,'T1':1})

GC_39 = Coupling(name = 'GC_39',
                 value = '-4*cw**2*ee**2*complex(0,1)*FT1',
                 order = {'QED':2,'T1':1})

GC_40 = Coupling(name = 'GC_40',
                 value = '-4*ee**3*complex(0,1)*FT1',
                 order = {'QED':3,'T1':1})

GC_41 = Coupling(name = 'GC_41',
                 value = '16*ee**4*complex(0,1)*FT1',
                 order = {'QED':4,'T1':1})

GC_42 = Coupling(name = 'GC_42',
                 value = '2*complex(0,1)*FT2',
                 order = {'T2':1})

GC_43 = Coupling(name = 'GC_43',
                 value = 'cw**2*complex(0,1)*FT2',
                 order = {'T2':1})

GC_44 = Coupling(name = 'GC_44',
                 value = '2*cw**4*complex(0,1)*FT2',
                 order = {'T2':1})

GC_45 = Coupling(name = 'GC_45',
                 value = '-(ee*complex(0,1)*FT2)',
                 order = {'QED':1,'T2':1})

GC_46 = Coupling(name = 'GC_46',
                 value = '-(cw**2*ee*complex(0,1)*FT2)',
                 order = {'QED':1,'T2':1})

GC_47 = Coupling(name = 'GC_47',
                 value = '2*ee**2*complex(0,1)*FT2',
                 order = {'QED':2,'T2':1})

GC_48 = Coupling(name = 'GC_48',
                 value = '-(cw**2*ee**2*complex(0,1)*FT2)',
                 order = {'QED':2,'T2':1})

GC_49 = Coupling(name = 'GC_49',
                 value = '2*ee**3*complex(0,1)*FT2',
                 order = {'QED':3,'T2':1})

GC_50 = Coupling(name = 'GC_50',
                 value = '8*ee**4*complex(0,1)*FT2',
                 order = {'QED':4,'T2':1})

GC_51 = Coupling(name = 'GC_51',
                 value = '-G',
                 order = {'QCD':1})

GC_52 = Coupling(name = 'GC_52',
                 value = 'complex(0,1)*G',
                 order = {'QCD':1})

GC_53 = Coupling(name = 'GC_53',
                 value = 'complex(0,1)*G**2',
                 order = {'QCD':2})

GC_54 = Coupling(name = 'GC_54',
                 value = '-6*complex(0,1)*lam',
                 order = {'QED':2})

GC_55 = Coupling(name = 'GC_55',
                 value = '(ee**4*FM0*complex(0,1))/cw**2 + (2*ee**4*FM0*complex(0,1))/sw**2',
                 order = {'M0':1,'QED':4})

GC_56 = Coupling(name = 'GC_56',
                 value = '-(ee**4*FM0*complex(0,1)) - (cw**4*ee**4*FM0*complex(0,1))/sw**4 - (2*cw**2*ee**4*FM0*complex(0,1))/sw**2',
                 order = {'M0':1,'QED':4})

GC_57 = Coupling(name = 'GC_57',
                 value = '(ee**4*FM1*complex(0,1))/(2.*cw**2) + (ee**4*FM1*complex(0,1))/sw**2',
                 order = {'M1':1,'QED':4})

GC_58 = Coupling(name = 'GC_58',
                 value = '(ee**4*FM1*complex(0,1))/2. + (cw**4*ee**4*FM1*complex(0,1))/(2.*sw**4) + (cw**2*ee**4*FM1*complex(0,1))/sw**2',
                 order = {'M1':1,'QED':4})

GC_59 = Coupling(name = 'GC_59',
                 value = '-(ee**4*FM7*complex(0,1))/4. - (cw**4*ee**4*FM7*complex(0,1))/(4.*sw**4) - (cw**2*ee**4*FM7*complex(0,1))/(2.*sw**2)',
                 order = {'M7':1,'QED':4})

GC_60 = Coupling(name = 'GC_60',
                 value = '(3*ee**4*complex(0,1)*FS0)/(2.*cw**2) + (3*cw**2*ee**4*complex(0,1)*FS0)/(2.*sw**4) + (3*ee**4*complex(0,1)*FS0)/sw**2',
                 order = {'QED':4,'S0':1})

GC_61 = Coupling(name = 'GC_61',
                 value = '(3*ee**4*complex(0,1)*FS1)/cw**2 + (3*cw**2*ee**4*complex(0,1)*FS1)/sw**4 + (6*ee**4*complex(0,1)*FS1)/sw**2',
                 order = {'QED':4,'S1':1})

GC_62 = Coupling(name = 'GC_62',
                 value = '(3*ee**4*complex(0,1)*FS2)/(2.*cw**2) + (3*cw**2*ee**4*complex(0,1)*FS2)/(2.*sw**4) + (3*ee**4*complex(0,1)*FS2)/sw**2',
                 order = {'QED':4,'S2':1})

GC_63 = Coupling(name = 'GC_63',
                 value = '(-3*cw*ee**3*complex(0,1)*FS0)/(4.*sw**3) - (3*ee**3*complex(0,1)*FS0)/(4.*cw*sw)',
                 order = {'QED':3,'S0':1})

GC_64 = Coupling(name = 'GC_64',
                 value = '(3*cw*ee**3*complex(0,1)*FS2)/(4.*sw**3) + (3*ee**3*complex(0,1)*FS2)/(4.*cw*sw)',
                 order = {'QED':3,'S2':1})

GC_65 = Coupling(name = 'GC_65',
                 value = '(3*ee**4*FM0*complex(0,1))/sw**4',
                 order = {'M0':1,'QED':4})

GC_66 = Coupling(name = 'GC_66',
                 value = '-((cw**2*ee**4*FM0*complex(0,1))/sw**4)',
                 order = {'M0':1,'QED':4})

GC_67 = Coupling(name = 'GC_67',
                 value = '(-3*ee**4*FM1*complex(0,1))/(2.*sw**4)',
                 order = {'M1':1,'QED':4})

GC_68 = Coupling(name = 'GC_68',
                 value = '(3*cw**2*ee**4*FM1*complex(0,1))/(2.*sw**4)',
                 order = {'M1':1,'QED':4})

GC_69 = Coupling(name = 'GC_69',
                 value = '(3*ee**4*FM7*complex(0,1))/(4.*sw**4)',
                 order = {'M7':1,'QED':4})

GC_70 = Coupling(name = 'GC_70',
                 value = '(-9*cw**2*ee**4*FM7*complex(0,1))/(4.*sw**4)',
                 order = {'M7':1,'QED':4})

GC_71 = Coupling(name = 'GC_71',
                 value = '(6*ee**4*complex(0,1)*FS0)/sw**4',
                 order = {'QED':4,'S0':1})

GC_72 = Coupling(name = 'GC_72',
                 value = '(3*ee**4*complex(0,1)*FS1)/sw**4',
                 order = {'QED':4,'S1':1})

GC_73 = Coupling(name = 'GC_73',
                 value = '(3*ee**4*complex(0,1)*FS2)/sw**4',
                 order = {'QED':4,'S2':1})

GC_74 = Coupling(name = 'GC_74',
                 value = '(24*ee**4*complex(0,1)*FT0)/sw**4',
                 order = {'QED':4,'T0':1})

GC_75 = Coupling(name = 'GC_75',
                 value = '(-8*cw**2*ee**4*complex(0,1)*FT0)/sw**4',
                 order = {'QED':4,'T0':1})

GC_76 = Coupling(name = 'GC_76',
                 value = '(16*cw**4*ee**4*complex(0,1)*FT0)/sw**4',
                 order = {'QED':4,'T0':1})

GC_77 = Coupling(name = 'GC_77',
                 value = '(24*ee**4*complex(0,1)*FT1)/sw**4',
                 order = {'QED':4,'T1':1})

GC_78 = Coupling(name = 'GC_78',
                 value = '(-16*cw**2*ee**4*complex(0,1)*FT1)/sw**4',
                 order = {'QED':4,'T1':1})

GC_79 = Coupling(name = 'GC_79',
                 value = '(16*cw**4*ee**4*complex(0,1)*FT1)/sw**4',
                 order = {'QED':4,'T1':1})

GC_80 = Coupling(name = 'GC_80',
                 value = '(12*ee**4*complex(0,1)*FT2)/sw**4',
                 order = {'QED':4,'T2':1})

GC_81 = Coupling(name = 'GC_81',
                 value = '(-4*cw**2*ee**4*complex(0,1)*FT2)/sw**4',
                 order = {'QED':4,'T2':1})

GC_82 = Coupling(name = 'GC_82',
                 value = '(8*cw**4*ee**4*complex(0,1)*FT2)/sw**4',
                 order = {'QED':4,'T2':1})

GC_83 = Coupling(name = 'GC_83',
                 value = '-((cw*ee**3*FM0*complex(0,1))/sw**3)',
                 order = {'M0':1,'QED':3})

GC_84 = Coupling(name = 'GC_84',
                 value = '(4*cw*ee**4*FM0*complex(0,1))/sw**3',
                 order = {'M0':1,'QED':4})

GC_85 = Coupling(name = 'GC_85',
                 value = '(cw*ee**3*FM1*complex(0,1))/(4.*sw**3)',
                 order = {'M1':1,'QED':3})

GC_86 = Coupling(name = 'GC_86',
                 value = '-((cw*ee**4*FM1*complex(0,1))/sw**3)',
                 order = {'M1':1,'QED':4})

GC_87 = Coupling(name = 'GC_87',
                 value = '(cw*ee**3*FM7*complex(0,1))/(8.*sw**3)',
                 order = {'M7':1,'QED':3})

GC_88 = Coupling(name = 'GC_88',
                 value = '(3*cw**3*ee**3*FM7*complex(0,1))/(8.*sw**3)',
                 order = {'M7':1,'QED':3})

GC_89 = Coupling(name = 'GC_89',
                 value = '-(cw*ee**4*FM7*complex(0,1))/(2.*sw**3)',
                 order = {'M7':1,'QED':4})

GC_90 = Coupling(name = 'GC_90',
                 value = '(-8*cw*ee**3*complex(0,1)*FT0)/sw**3',
                 order = {'QED':3,'T0':1})

GC_91 = Coupling(name = 'GC_91',
                 value = '(8*cw**3*ee**3*complex(0,1)*FT0)/sw**3',
                 order = {'QED':3,'T0':1})

GC_92 = Coupling(name = 'GC_92',
                 value = '(48*cw*ee**4*complex(0,1)*FT0)/sw**3',
                 order = {'QED':4,'T0':1})

GC_93 = Coupling(name = 'GC_93',
                 value = '(-16*cw**3*ee**4*complex(0,1)*FT0)/sw**3',
                 order = {'QED':4,'T0':1})

GC_94 = Coupling(name = 'GC_94',
                 value = '(4*cw*ee**3*complex(0,1)*FT1)/sw**3',
                 order = {'QED':3,'T1':1})

GC_95 = Coupling(name = 'GC_95',
                 value = '(-8*cw**3*ee**3*complex(0,1)*FT1)/sw**3',
                 order = {'QED':3,'T1':1})

GC_96 = Coupling(name = 'GC_96',
                 value = '(8*cw*ee**4*complex(0,1)*FT1)/sw**3',
                 order = {'QED':4,'T1':1})

GC_97 = Coupling(name = 'GC_97',
                 value = '(-16*cw**3*ee**4*complex(0,1)*FT1)/sw**3',
                 order = {'QED':4,'T1':1})

GC_98 = Coupling(name = 'GC_98',
                 value = '(-2*cw*ee**3*complex(0,1)*FT2)/sw**3',
                 order = {'QED':3,'T2':1})

GC_99 = Coupling(name = 'GC_99',
                 value = '(-2*cw**3*ee**3*complex(0,1)*FT2)/sw**3',
                 order = {'QED':3,'T2':1})

GC_100 = Coupling(name = 'GC_100',
                  value = '(12*cw*ee**4*complex(0,1)*FT2)/sw**3',
                  order = {'QED':4,'T2':1})

GC_101 = Coupling(name = 'GC_101',
                  value = '(-4*cw**3*ee**4*complex(0,1)*FT2)/sw**3',
                  order = {'QED':4,'T2':1})

GC_102 = Coupling(name = 'GC_102',
                  value = '(ee**2*complex(0,1))/(2.*sw**2)',
                  order = {'QED':2})

GC_103 = Coupling(name = 'GC_103',
                  value = '-((ee**2*complex(0,1))/sw**2)',
                  order = {'QED':2})

GC_104 = Coupling(name = 'GC_104',
                  value = '(cw**2*ee**2*complex(0,1))/sw**2',
                  order = {'QED':2})

GC_105 = Coupling(name = 'GC_105',
                  value = '(ee**2*FM0*complex(0,1))/sw**2',
                  order = {'M0':1,'QED':2})

GC_106 = Coupling(name = 'GC_106',
                  value = '(cw**2*ee**2*FM0*complex(0,1))/sw**2',
                  order = {'M0':1,'QED':2})

GC_107 = Coupling(name = 'GC_107',
                  value = '-((ee**3*FM0*complex(0,1))/sw**2)',
                  order = {'M0':1,'QED':3})

GC_108 = Coupling(name = 'GC_108',
                  value = '-((ee**4*FM0*complex(0,1))/sw**2)',
                  order = {'M0':1,'QED':4})

GC_109 = Coupling(name = 'GC_109',
                  value = '-(ee**2*FM1*complex(0,1))/(4.*sw**2)',
                  order = {'M1':1,'QED':2})

GC_110 = Coupling(name = 'GC_110',
                  value = '(cw**2*ee**2*FM1*complex(0,1))/(4.*sw**2)',
                  order = {'M1':1,'QED':2})

GC_111 = Coupling(name = 'GC_111',
                  value = '-(ee**3*FM1*complex(0,1))/(4.*sw**2)',
                  order = {'M1':1,'QED':3})

GC_112 = Coupling(name = 'GC_112',
                  value = '(ee**4*FM1*complex(0,1))/sw**2',
                  order = {'M1':1,'QED':4})

GC_113 = Coupling(name = 'GC_113',
                  value = '(ee**2*FM7*complex(0,1))/(4.*sw**2)',
                  order = {'M7':1,'QED':2})

GC_114 = Coupling(name = 'GC_114',
                  value = '-(cw**2*ee**2*FM7*complex(0,1))/(8.*sw**2)',
                  order = {'M7':1,'QED':2})

GC_115 = Coupling(name = 'GC_115',
                  value = '(ee**3*FM7*complex(0,1))/(4.*sw**2)',
                  order = {'M7':1,'QED':3})

GC_116 = Coupling(name = 'GC_116',
                  value = '-(cw**2*ee**3*FM7*complex(0,1))/(8.*sw**2)',
                  order = {'M7':1,'QED':3})

GC_117 = Coupling(name = 'GC_117',
                  value = '-((ee**4*FM7*complex(0,1))/sw**2)',
                  order = {'M7':1,'QED':4})

GC_118 = Coupling(name = 'GC_118',
                  value = '(-3*ee**4*FM7*complex(0,1))/(2.*sw**2)',
                  order = {'M7':1,'QED':4})

GC_119 = Coupling(name = 'GC_119',
                  value = '-(ee**2*complex(0,1)*FS0)/(2.*sw**2)',
                  order = {'QED':2,'S0':1})

GC_120 = Coupling(name = 'GC_120',
                  value = '-((ee**2*complex(0,1)*FS1)/sw**2)',
                  order = {'QED':2,'S1':1})

GC_121 = Coupling(name = 'GC_121',
                  value = '-(ee**2*complex(0,1)*FS2)/(2.*sw**2)',
                  order = {'QED':2,'S2':1})

GC_122 = Coupling(name = 'GC_122',
                  value = '(8*ee**2*complex(0,1)*FT0)/sw**2',
                  order = {'QED':2,'T0':1})

GC_123 = Coupling(name = 'GC_123',
                  value = '(8*cw**2*ee**2*complex(0,1)*FT0)/sw**2',
                  order = {'QED':2,'T0':1})

GC_124 = Coupling(name = 'GC_124',
                  value = '(-8*cw**4*ee**2*complex(0,1)*FT0)/sw**2',
                  order = {'QED':2,'T0':1})

GC_125 = Coupling(name = 'GC_125',
                  value = '(-8*ee**3*complex(0,1)*FT0)/sw**2',
                  order = {'QED':3,'T0':1})

GC_126 = Coupling(name = 'GC_126',
                  value = '(-16*cw**2*ee**3*complex(0,1)*FT0)/sw**2',
                  order = {'QED':3,'T0':1})

GC_127 = Coupling(name = 'GC_127',
                  value = '(-8*ee**4*complex(0,1)*FT0)/sw**2',
                  order = {'QED':4,'T0':1})

GC_128 = Coupling(name = 'GC_128',
                  value = '(64*cw**2*ee**4*complex(0,1)*FT0)/sw**2',
                  order = {'QED':4,'T0':1})

GC_129 = Coupling(name = 'GC_129',
                  value = '(4*ee**2*complex(0,1)*FT1)/sw**2',
                  order = {'QED':2,'T1':1})

GC_130 = Coupling(name = 'GC_130',
                  value = '(4*cw**2*ee**2*complex(0,1)*FT1)/sw**2',
                  order = {'QED':2,'T1':1})

GC_131 = Coupling(name = 'GC_131',
                  value = '(-4*cw**4*ee**2*complex(0,1)*FT1)/sw**2',
                  order = {'QED':2,'T1':1})

GC_132 = Coupling(name = 'GC_132',
                  value = '(4*ee**3*complex(0,1)*FT1)/sw**2',
                  order = {'QED':3,'T1':1})

GC_133 = Coupling(name = 'GC_133',
                  value = '(-8*cw**2*ee**3*complex(0,1)*FT1)/sw**2',
                  order = {'QED':3,'T1':1})

GC_134 = Coupling(name = 'GC_134',
                  value = '(-16*ee**4*complex(0,1)*FT1)/sw**2',
                  order = {'QED':4,'T1':1})

GC_135 = Coupling(name = 'GC_135',
                  value = '(32*cw**2*ee**4*complex(0,1)*FT1)/sw**2',
                  order = {'QED':4,'T1':1})

GC_136 = Coupling(name = 'GC_136',
                  value = '(2*ee**2*complex(0,1)*FT2)/sw**2',
                  order = {'QED':2,'T2':1})

GC_137 = Coupling(name = 'GC_137',
                  value = '(2*cw**2*ee**2*complex(0,1)*FT2)/sw**2',
                  order = {'QED':2,'T2':1})

GC_138 = Coupling(name = 'GC_138',
                  value = '-((cw**4*ee**2*complex(0,1)*FT2)/sw**2)',
                  order = {'QED':2,'T2':1})

GC_139 = Coupling(name = 'GC_139',
                  value = '(ee**3*complex(0,1)*FT2)/sw**2',
                  order = {'QED':3,'T2':1})

GC_140 = Coupling(name = 'GC_140',
                  value = '(-6*cw**2*ee**3*complex(0,1)*FT2)/sw**2',
                  order = {'QED':3,'T2':1})

GC_141 = Coupling(name = 'GC_141',
                  value = '(-4*ee**4*complex(0,1)*FT2)/sw**2',
                  order = {'QED':4,'T2':1})

GC_142 = Coupling(name = 'GC_142',
                  value = '(24*cw**2*ee**4*complex(0,1)*FT2)/sw**2',
                  order = {'QED':4,'T2':1})

GC_143 = Coupling(name = 'GC_143',
                  value = '(ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_144 = Coupling(name = 'GC_144',
                  value = '(CKM1x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_145 = Coupling(name = 'GC_145',
                  value = '(CKM1x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_146 = Coupling(name = 'GC_146',
                  value = '(CKM2x1*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_147 = Coupling(name = 'GC_147',
                  value = '(CKM2x2*ee*complex(0,1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_148 = Coupling(name = 'GC_148',
                  value = '-(cw*ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_149 = Coupling(name = 'GC_149',
                  value = '(cw*ee*complex(0,1))/(2.*sw)',
                  order = {'QED':1})

GC_150 = Coupling(name = 'GC_150',
                  value = '(cw*ee*complex(0,1))/sw',
                  order = {'QED':1})

GC_151 = Coupling(name = 'GC_151',
                  value = '(-2*cw*ee**2*complex(0,1))/sw',
                  order = {'QED':2})

GC_152 = Coupling(name = 'GC_152',
                  value = '(2*cw*ee*FM0*complex(0,1))/sw',
                  order = {'M0':1,'QED':1})

GC_153 = Coupling(name = 'GC_153',
                  value = '(cw*ee**2*FM0*complex(0,1))/sw',
                  order = {'M0':1,'QED':2})

GC_154 = Coupling(name = 'GC_154',
                  value = '-(cw*ee*FM1*complex(0,1))/(2.*sw)',
                  order = {'M1':1,'QED':1})

GC_155 = Coupling(name = 'GC_155',
                  value = '-(cw*ee**2*FM1*complex(0,1))/(4.*sw)',
                  order = {'M1':1,'QED':2})

GC_156 = Coupling(name = 'GC_156',
                  value = '-(cw*ee*FM7*complex(0,1))/(8.*sw)',
                  order = {'M7':1,'QED':1})

GC_157 = Coupling(name = 'GC_157',
                  value = '(cw*ee**2*FM7*complex(0,1))/(8.*sw)',
                  order = {'M7':1,'QED':2})

GC_158 = Coupling(name = 'GC_158',
                  value = '(ee**3*FM7*complex(0,1))/(8.*cw*sw)',
                  order = {'M7':1,'QED':3})

GC_159 = Coupling(name = 'GC_159',
                  value = '(cw*ee**3*FM7*complex(0,1))/(2.*sw)',
                  order = {'M7':1,'QED':3})

GC_160 = Coupling(name = 'GC_160',
                  value = '-(ee**4*FM7*complex(0,1))/(2.*cw*sw)',
                  order = {'M7':1,'QED':4})

GC_161 = Coupling(name = 'GC_161',
                  value = '(-8*cw*ee*complex(0,1)*FT0)/sw',
                  order = {'QED':1,'T0':1})

GC_162 = Coupling(name = 'GC_162',
                  value = '(-8*cw**3*ee*complex(0,1)*FT0)/sw',
                  order = {'QED':1,'T0':1})

GC_163 = Coupling(name = 'GC_163',
                  value = '(8*cw*ee**2*complex(0,1)*FT0)/sw',
                  order = {'QED':2,'T0':1})

GC_164 = Coupling(name = 'GC_164',
                  value = '(16*cw**3*ee**2*complex(0,1)*FT0)/sw',
                  order = {'QED':2,'T0':1})

GC_165 = Coupling(name = 'GC_165',
                  value = '(8*cw*ee**3*complex(0,1)*FT0)/sw',
                  order = {'QED':3,'T0':1})

GC_166 = Coupling(name = 'GC_166',
                  value = '(-16*cw*ee**4*complex(0,1)*FT0)/sw',
                  order = {'QED':4,'T0':1})

GC_167 = Coupling(name = 'GC_167',
                  value = '(8*cw*ee*complex(0,1)*FT1)/sw',
                  order = {'QED':1,'T1':1})

GC_168 = Coupling(name = 'GC_168',
                  value = '(4*cw**3*ee*complex(0,1)*FT1)/sw',
                  order = {'QED':1,'T1':1})

GC_169 = Coupling(name = 'GC_169',
                  value = '(4*cw*ee**2*complex(0,1)*FT1)/sw',
                  order = {'QED':2,'T1':1})

GC_170 = Coupling(name = 'GC_170',
                  value = '(-4*cw**3*ee**2*complex(0,1)*FT1)/sw',
                  order = {'QED':2,'T1':1})

GC_171 = Coupling(name = 'GC_171',
                  value = '(-8*cw*ee**3*complex(0,1)*FT1)/sw',
                  order = {'QED':3,'T1':1})

GC_172 = Coupling(name = 'GC_172',
                  value = '(-16*cw*ee**4*complex(0,1)*FT1)/sw',
                  order = {'QED':4,'T1':1})

GC_173 = Coupling(name = 'GC_173',
                  value = '-((cw*ee*complex(0,1)*FT2)/sw)',
                  order = {'QED':1,'T2':1})

GC_174 = Coupling(name = 'GC_174',
                  value = '-((cw**3*ee*complex(0,1)*FT2)/sw)',
                  order = {'QED':1,'T2':1})

GC_175 = Coupling(name = 'GC_175',
                  value = '(cw*ee**2*complex(0,1)*FT2)/sw',
                  order = {'QED':2,'T2':1})

GC_176 = Coupling(name = 'GC_176',
                  value = '(-2*cw**3*ee**2*complex(0,1)*FT2)/sw',
                  order = {'QED':2,'T2':1})

GC_177 = Coupling(name = 'GC_177',
                  value = '(-2*cw*ee**3*complex(0,1)*FT2)/sw',
                  order = {'QED':3,'T2':1})

GC_178 = Coupling(name = 'GC_178',
                  value = '(-4*cw*ee**4*complex(0,1)*FT2)/sw',
                  order = {'QED':4,'T2':1})

GC_179 = Coupling(name = 'GC_179',
                  value = '-(ee*complex(0,1)*sw)/(6.*cw)',
                  order = {'QED':1})

GC_180 = Coupling(name = 'GC_180',
                  value = '(ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_181 = Coupling(name = 'GC_181',
                  value = '-2*cw*FM0*complex(0,1)*sw',
                  order = {'M0':1})

GC_182 = Coupling(name = 'GC_182',
                  value = '-(cw*FM1*complex(0,1)*sw)/2.',
                  order = {'M1':1})

GC_183 = Coupling(name = 'GC_183',
                  value = '(cw*FM7*complex(0,1)*sw)/4.',
                  order = {'M7':1})

GC_184 = Coupling(name = 'GC_184',
                  value = '-(ee*FM7*complex(0,1)*sw)/(8.*cw)',
                  order = {'M7':1,'QED':1})

GC_185 = Coupling(name = 'GC_185',
                  value = '(ee**2*FM7*complex(0,1)*sw)/(8.*cw)',
                  order = {'M7':1,'QED':2})

GC_186 = Coupling(name = 'GC_186',
                  value = '(ee**3*FM7*complex(0,1)*sw)/(8.*cw)',
                  order = {'M7':1,'QED':3})

GC_187 = Coupling(name = 'GC_187',
                  value = '8*cw*complex(0,1)*FT0*sw',
                  order = {'T0':1})

GC_188 = Coupling(name = 'GC_188',
                  value = '8*cw**3*complex(0,1)*FT0*sw',
                  order = {'T0':1})

GC_189 = Coupling(name = 'GC_189',
                  value = '-8*cw*ee*complex(0,1)*FT0*sw',
                  order = {'QED':1,'T0':1})

GC_190 = Coupling(name = 'GC_190',
                  value = '-8*cw*ee**2*complex(0,1)*FT0*sw',
                  order = {'QED':2,'T0':1})

GC_191 = Coupling(name = 'GC_191',
                  value = '4*cw*complex(0,1)*FT1*sw',
                  order = {'T1':1})

GC_192 = Coupling(name = 'GC_192',
                  value = '8*cw**3*complex(0,1)*FT1*sw',
                  order = {'T1':1})

GC_193 = Coupling(name = 'GC_193',
                  value = '4*cw*ee*complex(0,1)*FT1*sw',
                  order = {'QED':1,'T1':1})

GC_194 = Coupling(name = 'GC_194',
                  value = '4*cw*ee**2*complex(0,1)*FT1*sw',
                  order = {'QED':2,'T1':1})

GC_195 = Coupling(name = 'GC_195',
                  value = 'cw*complex(0,1)*FT2*sw',
                  order = {'T2':1})

GC_196 = Coupling(name = 'GC_196',
                  value = '2*cw**3*complex(0,1)*FT2*sw',
                  order = {'T2':1})

GC_197 = Coupling(name = 'GC_197',
                  value = '-(cw*ee*complex(0,1)*FT2*sw)',
                  order = {'QED':1,'T2':1})

GC_198 = Coupling(name = 'GC_198',
                  value = 'cw*ee**2*complex(0,1)*FT2*sw',
                  order = {'QED':2,'T2':1})

GC_199 = Coupling(name = 'GC_199',
                  value = '-2*FM0*complex(0,1)*sw**2',
                  order = {'M0':1})

GC_200 = Coupling(name = 'GC_200',
                  value = '-(FM1*complex(0,1)*sw**2)/2.',
                  order = {'M1':1})

GC_201 = Coupling(name = 'GC_201',
                  value = '(FM7*complex(0,1)*sw**2)/4.',
                  order = {'M7':1})

GC_202 = Coupling(name = 'GC_202',
                  value = '-(ee**2*FM7*complex(0,1)*sw**2)/(8.*cw**2)',
                  order = {'M7':1,'QED':2})

GC_203 = Coupling(name = 'GC_203',
                  value = '-(ee**3*FM7*complex(0,1)*sw**2)/(8.*cw**2)',
                  order = {'M7':1,'QED':3})

GC_204 = Coupling(name = 'GC_204',
                  value = '8*complex(0,1)*FT0*sw**2',
                  order = {'T0':1})

GC_205 = Coupling(name = 'GC_205',
                  value = '8*cw**2*complex(0,1)*FT0*sw**2',
                  order = {'T0':1})

GC_206 = Coupling(name = 'GC_206',
                  value = '-8*ee*complex(0,1)*FT0*sw**2',
                  order = {'QED':1,'T0':1})

GC_207 = Coupling(name = 'GC_207',
                  value = '-8*ee**2*complex(0,1)*FT0*sw**2',
                  order = {'QED':2,'T0':1})

GC_208 = Coupling(name = 'GC_208',
                  value = '4*complex(0,1)*FT1*sw**2',
                  order = {'T1':1})

GC_209 = Coupling(name = 'GC_209',
                  value = '8*cw**2*complex(0,1)*FT1*sw**2',
                  order = {'T1':1})

GC_210 = Coupling(name = 'GC_210',
                  value = '4*ee*complex(0,1)*FT1*sw**2',
                  order = {'QED':1,'T1':1})

GC_211 = Coupling(name = 'GC_211',
                  value = '-4*ee**2*complex(0,1)*FT1*sw**2',
                  order = {'QED':2,'T1':1})

GC_212 = Coupling(name = 'GC_212',
                  value = 'complex(0,1)*FT2*sw**2',
                  order = {'T2':1})

GC_213 = Coupling(name = 'GC_213',
                  value = '2*cw**2*complex(0,1)*FT2*sw**2',
                  order = {'T2':1})

GC_214 = Coupling(name = 'GC_214',
                  value = '-2*ee*complex(0,1)*FT2*sw**2',
                  order = {'QED':1,'T2':1})

GC_215 = Coupling(name = 'GC_215',
                  value = '-2*ee**2*complex(0,1)*FT2*sw**2',
                  order = {'QED':2,'T2':1})

GC_216 = Coupling(name = 'GC_216',
                  value = '8*cw*complex(0,1)*FT0*sw**3',
                  order = {'T0':1})

GC_217 = Coupling(name = 'GC_217',
                  value = '8*cw*complex(0,1)*FT1*sw**3',
                  order = {'T1':1})

GC_218 = Coupling(name = 'GC_218',
                  value = '2*cw*complex(0,1)*FT2*sw**3',
                  order = {'T2':1})

GC_219 = Coupling(name = 'GC_219',
                  value = '8*complex(0,1)*FT0*sw**4',
                  order = {'T0':1})

GC_220 = Coupling(name = 'GC_220',
                  value = '8*complex(0,1)*FT1*sw**4',
                  order = {'T1':1})

GC_221 = Coupling(name = 'GC_221',
                  value = '2*complex(0,1)*FT2*sw**4',
                  order = {'T2':1})

GC_222 = Coupling(name = 'GC_222',
                  value = '(cw*ee*complex(0,1))/(2.*sw) + (ee*complex(0,1)*sw)/(2.*cw)',
                  order = {'QED':1})

GC_223 = Coupling(name = 'GC_223',
                  value = '-((cw**3*ee**3*FM0*complex(0,1))/sw**3) - (2*cw*ee**3*FM0*complex(0,1))/sw - (ee**3*FM0*complex(0,1)*sw)/cw',
                  order = {'M0':1,'QED':3})

GC_224 = Coupling(name = 'GC_224',
                  value = '(2*cw**3*ee**4*FM0*complex(0,1))/sw**3 + (4*cw*ee**4*FM0*complex(0,1))/sw + (2*ee**4*FM0*complex(0,1)*sw)/cw',
                  order = {'M0':1,'QED':4})

GC_225 = Coupling(name = 'GC_225',
                  value = '-(cw**3*ee**3*FM1*complex(0,1))/(4.*sw**3) - (cw*ee**3*FM1*complex(0,1))/(2.*sw) - (ee**3*FM1*complex(0,1)*sw)/(4.*cw)',
                  order = {'M1':1,'QED':3})

GC_226 = Coupling(name = 'GC_226',
                  value = '-((cw**3*ee**4*FM1*complex(0,1))/sw**3) - (2*cw*ee**4*FM1*complex(0,1))/sw - (ee**4*FM1*complex(0,1)*sw)/cw',
                  order = {'M1':1,'QED':4})

GC_227 = Coupling(name = 'GC_227',
                  value = '(cw*ee**3*FM7*complex(0,1))/(8.*sw) + (ee**3*FM7*complex(0,1)*sw)/(8.*cw)',
                  order = {'M7':1,'QED':3})

GC_228 = Coupling(name = 'GC_228',
                  value = '(cw**3*ee**4*FM7*complex(0,1))/(2.*sw**3) + (cw*ee**4*FM7*complex(0,1))/sw + (ee**4*FM7*complex(0,1)*sw)/(2.*cw)',
                  order = {'M7':1,'QED':4})

GC_229 = Coupling(name = 'GC_229',
                  value = 'ee**2*complex(0,1) + (cw**2*ee**2*complex(0,1))/(2.*sw**2) + (ee**2*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'QED':2})

GC_230 = Coupling(name = 'GC_230',
                  value = '2*cw**2*ee**2*FM0*complex(0,1) + (cw**4*ee**2*FM0*complex(0,1))/sw**2 + ee**2*FM0*complex(0,1)*sw**2',
                  order = {'M0':1,'QED':2})

GC_231 = Coupling(name = 'GC_231',
                  value = '2*ee**2*FM0*complex(0,1) + (ee**2*FM0*complex(0,1)*sw**2)/cw**2',
                  order = {'M0':1,'QED':2})

GC_232 = Coupling(name = 'GC_232',
                  value = '-2*ee**3*FM0*complex(0,1) - (cw**2*ee**3*FM0*complex(0,1))/sw**2 - (ee**3*FM0*complex(0,1)*sw**2)/cw**2',
                  order = {'M0':1,'QED':3})

GC_233 = Coupling(name = 'GC_233',
                  value = '-2*ee**4*FM0*complex(0,1) - (cw**2*ee**4*FM0*complex(0,1))/sw**2 - (ee**4*FM0*complex(0,1)*sw**2)/cw**2',
                  order = {'M0':1,'QED':4})

GC_234 = Coupling(name = 'GC_234',
                  value = '(cw**2*ee**2*FM1*complex(0,1))/2. + (cw**4*ee**2*FM1*complex(0,1))/(4.*sw**2) + (ee**2*FM1*complex(0,1)*sw**2)/4.',
                  order = {'M1':1,'QED':2})

GC_235 = Coupling(name = 'GC_235',
                  value = '(ee**2*FM1*complex(0,1))/2. + (ee**2*FM1*complex(0,1)*sw**2)/(4.*cw**2)',
                  order = {'M1':1,'QED':2})

GC_236 = Coupling(name = 'GC_236',
                  value = '(ee**3*FM1*complex(0,1))/2. + (cw**2*ee**3*FM1*complex(0,1))/(4.*sw**2) + (ee**3*FM1*complex(0,1)*sw**2)/(4.*cw**2)',
                  order = {'M1':1,'QED':3})

GC_237 = Coupling(name = 'GC_237',
                  value = '-(ee**4*FM1*complex(0,1)) - (cw**2*ee**4*FM1*complex(0,1))/(2.*sw**2) - (ee**4*FM1*complex(0,1)*sw**2)/(2.*cw**2)',
                  order = {'M1':1,'QED':4})

GC_238 = Coupling(name = 'GC_238',
                  value = '-(cw**2*ee**2*FM7*complex(0,1))/4. - (cw**4*ee**2*FM7*complex(0,1))/(8.*sw**2) - (ee**2*FM7*complex(0,1)*sw**2)/8.',
                  order = {'M7':1,'QED':2})

GC_239 = Coupling(name = 'GC_239',
                  value = '(ee**4*FM7*complex(0,1))/2. + (cw**2*ee**4*FM7*complex(0,1))/(4.*sw**2) + (ee**4*FM7*complex(0,1)*sw**2)/(4.*cw**2)',
                  order = {'M7':1,'QED':4})

GC_240 = Coupling(name = 'GC_240',
                  value = '-2*ee**2*complex(0,1)*FS0 - (cw**2*ee**2*complex(0,1)*FS0)/sw**2 - (ee**2*complex(0,1)*FS0*sw**2)/cw**2',
                  order = {'QED':2,'S0':1})

GC_241 = Coupling(name = 'GC_241',
                  value = '-2*ee**2*complex(0,1)*FS1 - (cw**2*ee**2*complex(0,1)*FS1)/sw**2 - (ee**2*complex(0,1)*FS1*sw**2)/cw**2',
                  order = {'QED':2,'S1':1})

GC_242 = Coupling(name = 'GC_242',
                  value = '-2*ee**2*complex(0,1)*FS2 - (cw**2*ee**2*complex(0,1)*FS2)/sw**2 - (ee**2*complex(0,1)*FS2*sw**2)/cw**2',
                  order = {'QED':2,'S2':1})

GC_243 = Coupling(name = 'GC_243',
                  value = '(cw**3*ee**2*FM0*complex(0,1))/sw + 2*cw*ee**2*FM0*complex(0,1)*sw + (ee**2*FM0*complex(0,1)*sw**3)/cw',
                  order = {'M0':1,'QED':2})

GC_244 = Coupling(name = 'GC_244',
                  value = '(cw**3*ee**2*FM1*complex(0,1))/(4.*sw) + (cw*ee**2*FM1*complex(0,1)*sw)/2. + (ee**2*FM1*complex(0,1)*sw**3)/(4.*cw)',
                  order = {'M1':1,'QED':2})

GC_245 = Coupling(name = 'GC_245',
                  value = '-(cw**3*ee**2*FM7*complex(0,1))/(8.*sw) - (cw*ee**2*FM7*complex(0,1)*sw)/4. - (ee**2*FM7*complex(0,1)*sw**3)/(8.*cw)',
                  order = {'M7':1,'QED':2})

GC_246 = Coupling(name = 'GC_246',
                  value = 'cw**2*ee**2*FM0*complex(0,1) + 2*ee**2*FM0*complex(0,1)*sw**2 + (ee**2*FM0*complex(0,1)*sw**4)/cw**2',
                  order = {'M0':1,'QED':2})

GC_247 = Coupling(name = 'GC_247',
                  value = '(cw**2*ee**2*FM1*complex(0,1))/4. + (ee**2*FM1*complex(0,1)*sw**2)/2. + (ee**2*FM1*complex(0,1)*sw**4)/(4.*cw**2)',
                  order = {'M1':1,'QED':2})

GC_248 = Coupling(name = 'GC_248',
                  value = '-(cw**2*ee**2*FM7*complex(0,1))/8. - (ee**2*FM7*complex(0,1)*sw**2)/4. - (ee**2*FM7*complex(0,1)*sw**4)/(8.*cw**2)',
                  order = {'M7':1,'QED':2})

GC_249 = Coupling(name = 'GC_249',
                  value = '18*ee**4*complex(0,1)*FS0 + (3*cw**4*ee**4*complex(0,1)*FS0)/sw**4 + (12*cw**2*ee**4*complex(0,1)*FS0)/sw**2 + (12*ee**4*complex(0,1)*FS0*sw**2)/cw**2 + (3*ee**4*complex(0,1)*FS0*sw**4)/cw**4',
                  order = {'QED':4,'S0':1})

GC_250 = Coupling(name = 'GC_250',
                  value = '18*ee**4*complex(0,1)*FS1 + (3*cw**4*ee**4*complex(0,1)*FS1)/sw**4 + (12*cw**2*ee**4*complex(0,1)*FS1)/sw**2 + (12*ee**4*complex(0,1)*FS1*sw**2)/cw**2 + (3*ee**4*complex(0,1)*FS1*sw**4)/cw**4',
                  order = {'QED':4,'S1':1})

GC_251 = Coupling(name = 'GC_251',
                  value = '18*ee**4*complex(0,1)*FS2 + (3*cw**4*ee**4*complex(0,1)*FS2)/sw**4 + (12*cw**2*ee**4*complex(0,1)*FS2)/sw**2 + (12*ee**4*complex(0,1)*FS2*sw**2)/cw**2 + (3*ee**4*complex(0,1)*FS2*sw**4)/cw**4',
                  order = {'QED':4,'S2':1})

GC_252 = Coupling(name = 'GC_252',
                  value = 'ee**2*FM0*complex(0,1)*vev',
                  order = {'M0':1,'QED':1})

GC_253 = Coupling(name = 'GC_253',
                  value = '(ee**2*FM1*complex(0,1)*vev)/4.',
                  order = {'M1':1,'QED':1})

GC_254 = Coupling(name = 'GC_254',
                  value = '-(ee*FM7*complex(0,1)*vev)/8.',
                  order = {'M7':1})

GC_255 = Coupling(name = 'GC_255',
                  value = '-(ee**2*FM7*complex(0,1)*vev)/8.',
                  order = {'M7':1,'QED':1})

GC_256 = Coupling(name = 'GC_256',
                  value = '-(ee**2*FM7*complex(0,1)*vev)/4.',
                  order = {'M7':1,'QED':1})

GC_257 = Coupling(name = 'GC_257',
                  value = '-(ee**3*FM7*complex(0,1)*vev)/4.',
                  order = {'M7':1,'QED':2})

GC_258 = Coupling(name = 'GC_258',
                  value = '-(ee**4*FM7*complex(0,1)*vev)/(4.*cw**2)',
                  order = {'M7':1,'QED':3})

GC_259 = Coupling(name = 'GC_259',
                  value = '-6*complex(0,1)*lam*vev',
                  order = {'QED':1})

GC_260 = Coupling(name = 'GC_260',
                  value = '(3*ee**4*FM0*complex(0,1)*vev)/sw**4',
                  order = {'M0':1,'QED':3})

GC_261 = Coupling(name = 'GC_261',
                  value = '-((cw**2*ee**4*FM0*complex(0,1)*vev)/sw**4)',
                  order = {'M0':1,'QED':3})

GC_262 = Coupling(name = 'GC_262',
                  value = '(-3*ee**4*FM1*complex(0,1)*vev)/(2.*sw**4)',
                  order = {'M1':1,'QED':3})

GC_263 = Coupling(name = 'GC_263',
                  value = '(3*cw**2*ee**4*FM1*complex(0,1)*vev)/(2.*sw**4)',
                  order = {'M1':1,'QED':3})

GC_264 = Coupling(name = 'GC_264',
                  value = '(3*ee**4*FM7*complex(0,1)*vev)/(4.*sw**4)',
                  order = {'M7':1,'QED':3})

GC_265 = Coupling(name = 'GC_265',
                  value = '(-9*cw**2*ee**4*FM7*complex(0,1)*vev)/(4.*sw**4)',
                  order = {'M7':1,'QED':3})

GC_266 = Coupling(name = 'GC_266',
                  value = '(6*ee**4*complex(0,1)*FS0*vev)/sw**4',
                  order = {'QED':3,'S0':1})

GC_267 = Coupling(name = 'GC_267',
                  value = '(3*ee**4*complex(0,1)*FS1*vev)/sw**4',
                  order = {'QED':3,'S1':1})

GC_268 = Coupling(name = 'GC_268',
                  value = '(3*ee**4*complex(0,1)*FS2*vev)/sw**4',
                  order = {'QED':3,'S2':1})

GC_269 = Coupling(name = 'GC_269',
                  value = '-((cw*ee**3*FM0*complex(0,1)*vev)/sw**3)',
                  order = {'M0':1,'QED':2})

GC_270 = Coupling(name = 'GC_270',
                  value = '(4*cw*ee**4*FM0*complex(0,1)*vev)/sw**3',
                  order = {'M0':1,'QED':3})

GC_271 = Coupling(name = 'GC_271',
                  value = '(cw*ee**3*FM1*complex(0,1)*vev)/(4.*sw**3)',
                  order = {'M1':1,'QED':2})

GC_272 = Coupling(name = 'GC_272',
                  value = '-((cw*ee**4*FM1*complex(0,1)*vev)/sw**3)',
                  order = {'M1':1,'QED':3})

GC_273 = Coupling(name = 'GC_273',
                  value = '(cw*ee**3*FM7*complex(0,1)*vev)/(8.*sw**3)',
                  order = {'M7':1,'QED':2})

GC_274 = Coupling(name = 'GC_274',
                  value = '(3*cw**3*ee**3*FM7*complex(0,1)*vev)/(8.*sw**3)',
                  order = {'M7':1,'QED':2})

GC_275 = Coupling(name = 'GC_275',
                  value = '-(cw*ee**4*FM7*complex(0,1)*vev)/(2.*sw**3)',
                  order = {'M7':1,'QED':3})

GC_276 = Coupling(name = 'GC_276',
                  value = '(ee**2*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'QED':1})

GC_277 = Coupling(name = 'GC_277',
                  value = '(ee**2*FM0*complex(0,1)*vev)/sw**2',
                  order = {'M0':1,'QED':1})

GC_278 = Coupling(name = 'GC_278',
                  value = '(cw**2*ee**2*FM0*complex(0,1)*vev)/sw**2',
                  order = {'M0':1,'QED':1})

GC_279 = Coupling(name = 'GC_279',
                  value = '-((ee**3*FM0*complex(0,1)*vev)/sw**2)',
                  order = {'M0':1,'QED':2})

GC_280 = Coupling(name = 'GC_280',
                  value = '-((ee**4*FM0*complex(0,1)*vev)/sw**2)',
                  order = {'M0':1,'QED':3})

GC_281 = Coupling(name = 'GC_281',
                  value = '-(ee**2*FM1*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'M1':1,'QED':1})

GC_282 = Coupling(name = 'GC_282',
                  value = '(cw**2*ee**2*FM1*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'M1':1,'QED':1})

GC_283 = Coupling(name = 'GC_283',
                  value = '-(ee**3*FM1*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'M1':1,'QED':2})

GC_284 = Coupling(name = 'GC_284',
                  value = '(ee**4*FM1*complex(0,1)*vev)/sw**2',
                  order = {'M1':1,'QED':3})

GC_285 = Coupling(name = 'GC_285',
                  value = '(ee**2*FM7*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'M7':1,'QED':1})

GC_286 = Coupling(name = 'GC_286',
                  value = '-(cw**2*ee**2*FM7*complex(0,1)*vev)/(8.*sw**2)',
                  order = {'M7':1,'QED':1})

GC_287 = Coupling(name = 'GC_287',
                  value = '(ee**3*FM7*complex(0,1)*vev)/(4.*sw**2)',
                  order = {'M7':1,'QED':2})

GC_288 = Coupling(name = 'GC_288',
                  value = '-(cw**2*ee**3*FM7*complex(0,1)*vev)/(8.*sw**2)',
                  order = {'M7':1,'QED':2})

GC_289 = Coupling(name = 'GC_289',
                  value = '-((ee**4*FM7*complex(0,1)*vev)/sw**2)',
                  order = {'M7':1,'QED':3})

GC_290 = Coupling(name = 'GC_290',
                  value = '(-3*ee**4*FM7*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'M7':1,'QED':3})

GC_291 = Coupling(name = 'GC_291',
                  value = '-(ee**2*complex(0,1)*FS0*vev)/(2.*sw**2)',
                  order = {'QED':1,'S0':1})

GC_292 = Coupling(name = 'GC_292',
                  value = '-((ee**2*complex(0,1)*FS1*vev)/sw**2)',
                  order = {'QED':1,'S1':1})

GC_293 = Coupling(name = 'GC_293',
                  value = '-(ee**2*complex(0,1)*FS2*vev)/(2.*sw**2)',
                  order = {'QED':1,'S2':1})

GC_294 = Coupling(name = 'GC_294',
                  value = '(cw*ee**2*FM0*complex(0,1)*vev)/sw',
                  order = {'M0':1,'QED':1})

GC_295 = Coupling(name = 'GC_295',
                  value = '-(cw*ee**2*FM1*complex(0,1)*vev)/(4.*sw)',
                  order = {'M1':1,'QED':1})

GC_296 = Coupling(name = 'GC_296',
                  value = '-(cw*ee*FM7*complex(0,1)*vev)/(8.*sw)',
                  order = {'M7':1})

GC_297 = Coupling(name = 'GC_297',
                  value = '(cw*ee**2*FM7*complex(0,1)*vev)/(8.*sw)',
                  order = {'M7':1,'QED':1})

GC_298 = Coupling(name = 'GC_298',
                  value = '(ee**3*FM7*complex(0,1)*vev)/(8.*cw*sw)',
                  order = {'M7':1,'QED':2})

GC_299 = Coupling(name = 'GC_299',
                  value = '(cw*ee**3*FM7*complex(0,1)*vev)/(2.*sw)',
                  order = {'M7':1,'QED':2})

GC_300 = Coupling(name = 'GC_300',
                  value = '-(ee**4*FM7*complex(0,1)*vev)/(2.*cw*sw)',
                  order = {'M7':1,'QED':3})

GC_301 = Coupling(name = 'GC_301',
                  value = '-(ee*FM7*complex(0,1)*sw*vev)/(8.*cw)',
                  order = {'M7':1})

GC_302 = Coupling(name = 'GC_302',
                  value = '(ee**2*FM7*complex(0,1)*sw*vev)/(8.*cw)',
                  order = {'M7':1,'QED':1})

GC_303 = Coupling(name = 'GC_303',
                  value = '(ee**3*FM7*complex(0,1)*sw*vev)/(8.*cw)',
                  order = {'M7':1,'QED':2})

GC_304 = Coupling(name = 'GC_304',
                  value = '-(ee**2*FM7*complex(0,1)*sw**2*vev)/(8.*cw**2)',
                  order = {'M7':1,'QED':1})

GC_305 = Coupling(name = 'GC_305',
                  value = '-(ee**3*FM7*complex(0,1)*sw**2*vev)/(8.*cw**2)',
                  order = {'M7':1,'QED':2})

GC_306 = Coupling(name = 'GC_306',
                  value = '(ee**2*FM0*complex(0,1)*vev**2)/2.',
                  order = {'M0':1})

GC_307 = Coupling(name = 'GC_307',
                  value = '(ee**2*FM1*complex(0,1)*vev**2)/8.',
                  order = {'M1':1})

GC_308 = Coupling(name = 'GC_308',
                  value = '-(ee**2*FM7*complex(0,1)*vev**2)/16.',
                  order = {'M7':1})

GC_309 = Coupling(name = 'GC_309',
                  value = '-(ee**2*FM7*complex(0,1)*vev**2)/8.',
                  order = {'M7':1})

GC_310 = Coupling(name = 'GC_310',
                  value = '-(ee**3*FM7*complex(0,1)*vev**2)/8.',
                  order = {'M7':1,'QED':1})

GC_311 = Coupling(name = 'GC_311',
                  value = '-(ee**4*FM7*complex(0,1)*vev**2)/(8.*cw**2)',
                  order = {'M7':1,'QED':2})

GC_312 = Coupling(name = 'GC_312',
                  value = '(3*ee**4*FM0*complex(0,1)*vev**2)/(2.*sw**4)',
                  order = {'M0':1,'QED':2})

GC_313 = Coupling(name = 'GC_313',
                  value = '-(cw**2*ee**4*FM0*complex(0,1)*vev**2)/(2.*sw**4)',
                  order = {'M0':1,'QED':2})

GC_314 = Coupling(name = 'GC_314',
                  value = '(-3*ee**4*FM1*complex(0,1)*vev**2)/(4.*sw**4)',
                  order = {'M1':1,'QED':2})

GC_315 = Coupling(name = 'GC_315',
                  value = '(3*cw**2*ee**4*FM1*complex(0,1)*vev**2)/(4.*sw**4)',
                  order = {'M1':1,'QED':2})

GC_316 = Coupling(name = 'GC_316',
                  value = '(3*ee**4*FM7*complex(0,1)*vev**2)/(8.*sw**4)',
                  order = {'M7':1,'QED':2})

GC_317 = Coupling(name = 'GC_317',
                  value = '(-9*cw**2*ee**4*FM7*complex(0,1)*vev**2)/(8.*sw**4)',
                  order = {'M7':1,'QED':2})

GC_318 = Coupling(name = 'GC_318',
                  value = '(3*ee**4*complex(0,1)*FS0*vev**2)/sw**4',
                  order = {'QED':2,'S0':1})

GC_319 = Coupling(name = 'GC_319',
                  value = '(3*ee**4*complex(0,1)*FS1*vev**2)/(2.*sw**4)',
                  order = {'QED':2,'S1':1})

GC_320 = Coupling(name = 'GC_320',
                  value = '(3*ee**4*complex(0,1)*FS2*vev**2)/(2.*sw**4)',
                  order = {'QED':2,'S2':1})

GC_321 = Coupling(name = 'GC_321',
                  value = '-(cw*ee**3*FM0*complex(0,1)*vev**2)/(2.*sw**3)',
                  order = {'M0':1,'QED':1})

GC_322 = Coupling(name = 'GC_322',
                  value = '(2*cw*ee**4*FM0*complex(0,1)*vev**2)/sw**3',
                  order = {'M0':1,'QED':2})

GC_323 = Coupling(name = 'GC_323',
                  value = '(cw*ee**3*FM1*complex(0,1)*vev**2)/(8.*sw**3)',
                  order = {'M1':1,'QED':1})

GC_324 = Coupling(name = 'GC_324',
                  value = '-(cw*ee**4*FM1*complex(0,1)*vev**2)/(2.*sw**3)',
                  order = {'M1':1,'QED':2})

GC_325 = Coupling(name = 'GC_325',
                  value = '(cw*ee**3*FM7*complex(0,1)*vev**2)/(16.*sw**3)',
                  order = {'M7':1,'QED':1})

GC_326 = Coupling(name = 'GC_326',
                  value = '(3*cw**3*ee**3*FM7*complex(0,1)*vev**2)/(16.*sw**3)',
                  order = {'M7':1,'QED':1})

GC_327 = Coupling(name = 'GC_327',
                  value = '-(cw*ee**4*FM7*complex(0,1)*vev**2)/(4.*sw**3)',
                  order = {'M7':1,'QED':2})

GC_328 = Coupling(name = 'GC_328',
                  value = '(ee**2*FM0*complex(0,1)*vev**2)/(2.*sw**2)',
                  order = {'M0':1})

GC_329 = Coupling(name = 'GC_329',
                  value = '(cw**2*ee**2*FM0*complex(0,1)*vev**2)/(2.*sw**2)',
                  order = {'M0':1})

GC_330 = Coupling(name = 'GC_330',
                  value = '-(ee**3*FM0*complex(0,1)*vev**2)/(2.*sw**2)',
                  order = {'M0':1,'QED':1})

GC_331 = Coupling(name = 'GC_331',
                  value = '-(ee**4*FM0*complex(0,1)*vev**2)/(2.*sw**2)',
                  order = {'M0':1,'QED':2})

GC_332 = Coupling(name = 'GC_332',
                  value = '-(ee**2*FM1*complex(0,1)*vev**2)/(8.*sw**2)',
                  order = {'M1':1})

GC_333 = Coupling(name = 'GC_333',
                  value = '(cw**2*ee**2*FM1*complex(0,1)*vev**2)/(8.*sw**2)',
                  order = {'M1':1})

GC_334 = Coupling(name = 'GC_334',
                  value = '-(ee**3*FM1*complex(0,1)*vev**2)/(8.*sw**2)',
                  order = {'M1':1,'QED':1})

GC_335 = Coupling(name = 'GC_335',
                  value = '(ee**4*FM1*complex(0,1)*vev**2)/(2.*sw**2)',
                  order = {'M1':1,'QED':2})

GC_336 = Coupling(name = 'GC_336',
                  value = '(ee**2*FM7*complex(0,1)*vev**2)/(8.*sw**2)',
                  order = {'M7':1})

GC_337 = Coupling(name = 'GC_337',
                  value = '-(cw**2*ee**2*FM7*complex(0,1)*vev**2)/(16.*sw**2)',
                  order = {'M7':1})

GC_338 = Coupling(name = 'GC_338',
                  value = '(ee**3*FM7*complex(0,1)*vev**2)/(8.*sw**2)',
                  order = {'M7':1,'QED':1})

GC_339 = Coupling(name = 'GC_339',
                  value = '-(cw**2*ee**3*FM7*complex(0,1)*vev**2)/(16.*sw**2)',
                  order = {'M7':1,'QED':1})

GC_340 = Coupling(name = 'GC_340',
                  value = '-(ee**4*FM7*complex(0,1)*vev**2)/(2.*sw**2)',
                  order = {'M7':1,'QED':2})

GC_341 = Coupling(name = 'GC_341',
                  value = '(-3*ee**4*FM7*complex(0,1)*vev**2)/(4.*sw**2)',
                  order = {'M7':1,'QED':2})

GC_342 = Coupling(name = 'GC_342',
                  value = '-(ee**2*complex(0,1)*FS0*vev**2)/(4.*sw**2)',
                  order = {'S0':1})

GC_343 = Coupling(name = 'GC_343',
                  value = '-(ee**2*complex(0,1)*FS1*vev**2)/(2.*sw**2)',
                  order = {'S1':1})

GC_344 = Coupling(name = 'GC_344',
                  value = '-(ee**2*complex(0,1)*FS2*vev**2)/(4.*sw**2)',
                  order = {'S2':1})

GC_345 = Coupling(name = 'GC_345',
                  value = '(cw*ee**2*FM0*complex(0,1)*vev**2)/(2.*sw)',
                  order = {'M0':1})

GC_346 = Coupling(name = 'GC_346',
                  value = '-(cw*ee**2*FM1*complex(0,1)*vev**2)/(8.*sw)',
                  order = {'M1':1})

GC_347 = Coupling(name = 'GC_347',
                  value = '(cw*ee**2*FM7*complex(0,1)*vev**2)/(16.*sw)',
                  order = {'M7':1})

GC_348 = Coupling(name = 'GC_348',
                  value = '(ee**3*FM7*complex(0,1)*vev**2)/(16.*cw*sw)',
                  order = {'M7':1,'QED':1})

GC_349 = Coupling(name = 'GC_349',
                  value = '(cw*ee**3*FM7*complex(0,1)*vev**2)/(4.*sw)',
                  order = {'M7':1,'QED':1})

GC_350 = Coupling(name = 'GC_350',
                  value = '-(ee**4*FM7*complex(0,1)*vev**2)/(4.*cw*sw)',
                  order = {'M7':1,'QED':2})

GC_351 = Coupling(name = 'GC_351',
                  value = '(ee**2*FM7*complex(0,1)*sw*vev**2)/(16.*cw)',
                  order = {'M7':1})

GC_352 = Coupling(name = 'GC_352',
                  value = '(ee**3*FM7*complex(0,1)*sw*vev**2)/(16.*cw)',
                  order = {'M7':1,'QED':1})

GC_353 = Coupling(name = 'GC_353',
                  value = '-(ee**2*FM7*complex(0,1)*sw**2*vev**2)/(16.*cw**2)',
                  order = {'M7':1})

GC_354 = Coupling(name = 'GC_354',
                  value = '-(ee**3*FM7*complex(0,1)*sw**2*vev**2)/(16.*cw**2)',
                  order = {'M7':1,'QED':1})

GC_355 = Coupling(name = 'GC_355',
                  value = '(ee**4*complex(0,1)*FS0*vev**3)/sw**4',
                  order = {'QED':1,'S0':1})

GC_356 = Coupling(name = 'GC_356',
                  value = '(ee**4*complex(0,1)*FS1*vev**3)/(2.*sw**4)',
                  order = {'QED':1,'S1':1})

GC_357 = Coupling(name = 'GC_357',
                  value = '(ee**4*complex(0,1)*FS2*vev**3)/(2.*sw**4)',
                  order = {'QED':1,'S2':1})

GC_358 = Coupling(name = 'GC_358',
                  value = '(ee**4*complex(0,1)*FS0*vev**4)/(4.*sw**4)',
                  order = {'S0':1})

GC_359 = Coupling(name = 'GC_359',
                  value = '(ee**4*complex(0,1)*FS1*vev**4)/(8.*sw**4)',
                  order = {'S1':1})

GC_360 = Coupling(name = 'GC_360',
                  value = '(ee**4*complex(0,1)*FS2*vev**4)/(8.*sw**4)',
                  order = {'S2':1})

GC_361 = Coupling(name = 'GC_361',
                  value = '(ee**4*FM0*complex(0,1)*vev)/cw**2 + (2*ee**4*FM0*complex(0,1)*vev)/sw**2',
                  order = {'M0':1,'QED':3})

GC_362 = Coupling(name = 'GC_362',
                  value = '-(ee**4*FM0*complex(0,1)*vev) - (cw**4*ee**4*FM0*complex(0,1)*vev)/sw**4 - (2*cw**2*ee**4*FM0*complex(0,1)*vev)/sw**2',
                  order = {'M0':1,'QED':3})

GC_363 = Coupling(name = 'GC_363',
                  value = '(ee**4*FM1*complex(0,1)*vev)/(2.*cw**2) + (ee**4*FM1*complex(0,1)*vev)/sw**2',
                  order = {'M1':1,'QED':3})

GC_364 = Coupling(name = 'GC_364',
                  value = '(ee**4*FM1*complex(0,1)*vev)/2. + (cw**4*ee**4*FM1*complex(0,1)*vev)/(2.*sw**4) + (cw**2*ee**4*FM1*complex(0,1)*vev)/sw**2',
                  order = {'M1':1,'QED':3})

GC_365 = Coupling(name = 'GC_365',
                  value = '-(ee**4*FM7*complex(0,1)*vev)/4. - (cw**4*ee**4*FM7*complex(0,1)*vev)/(4.*sw**4) - (cw**2*ee**4*FM7*complex(0,1)*vev)/(2.*sw**2)',
                  order = {'M7':1,'QED':3})

GC_366 = Coupling(name = 'GC_366',
                  value = '(3*ee**4*complex(0,1)*FS0*vev)/(2.*cw**2) + (3*cw**2*ee**4*complex(0,1)*FS0*vev)/(2.*sw**4) + (3*ee**4*complex(0,1)*FS0*vev)/sw**2',
                  order = {'QED':3,'S0':1})

GC_367 = Coupling(name = 'GC_367',
                  value = '(3*ee**4*complex(0,1)*FS1*vev)/cw**2 + (3*cw**2*ee**4*complex(0,1)*FS1*vev)/sw**4 + (6*ee**4*complex(0,1)*FS1*vev)/sw**2',
                  order = {'QED':3,'S1':1})

GC_368 = Coupling(name = 'GC_368',
                  value = '(3*ee**4*complex(0,1)*FS2*vev)/(2.*cw**2) + (3*cw**2*ee**4*complex(0,1)*FS2*vev)/(2.*sw**4) + (3*ee**4*complex(0,1)*FS2*vev)/sw**2',
                  order = {'QED':3,'S2':1})

GC_369 = Coupling(name = 'GC_369',
                  value = '(-3*cw*ee**3*complex(0,1)*FS0*vev)/(4.*sw**3) - (3*ee**3*complex(0,1)*FS0*vev)/(4.*cw*sw)',
                  order = {'QED':2,'S0':1})

GC_370 = Coupling(name = 'GC_370',
                  value = '(3*cw*ee**3*complex(0,1)*FS2*vev)/(4.*sw**3) + (3*ee**3*complex(0,1)*FS2*vev)/(4.*cw*sw)',
                  order = {'QED':2,'S2':1})

GC_371 = Coupling(name = 'GC_371',
                  value = '-((cw**3*ee**3*FM0*complex(0,1)*vev)/sw**3) - (2*cw*ee**3*FM0*complex(0,1)*vev)/sw - (ee**3*FM0*complex(0,1)*sw*vev)/cw',
                  order = {'M0':1,'QED':2})

GC_372 = Coupling(name = 'GC_372',
                  value = '(2*cw**3*ee**4*FM0*complex(0,1)*vev)/sw**3 + (4*cw*ee**4*FM0*complex(0,1)*vev)/sw + (2*ee**4*FM0*complex(0,1)*sw*vev)/cw',
                  order = {'M0':1,'QED':3})

GC_373 = Coupling(name = 'GC_373',
                  value = '-(cw**3*ee**3*FM1*complex(0,1)*vev)/(4.*sw**3) - (cw*ee**3*FM1*complex(0,1)*vev)/(2.*sw) - (ee**3*FM1*complex(0,1)*sw*vev)/(4.*cw)',
                  order = {'M1':1,'QED':2})

GC_374 = Coupling(name = 'GC_374',
                  value = '-((cw**3*ee**4*FM1*complex(0,1)*vev)/sw**3) - (2*cw*ee**4*FM1*complex(0,1)*vev)/sw - (ee**4*FM1*complex(0,1)*sw*vev)/cw',
                  order = {'M1':1,'QED':3})

GC_375 = Coupling(name = 'GC_375',
                  value = '(cw*ee**3*FM7*complex(0,1)*vev)/(8.*sw) + (ee**3*FM7*complex(0,1)*sw*vev)/(8.*cw)',
                  order = {'M7':1,'QED':2})

GC_376 = Coupling(name = 'GC_376',
                  value = '(cw**3*ee**4*FM7*complex(0,1)*vev)/(2.*sw**3) + (cw*ee**4*FM7*complex(0,1)*vev)/sw + (ee**4*FM7*complex(0,1)*sw*vev)/(2.*cw)',
                  order = {'M7':1,'QED':3})

GC_377 = Coupling(name = 'GC_377',
                  value = 'ee**2*complex(0,1)*vev + (cw**2*ee**2*complex(0,1)*vev)/(2.*sw**2) + (ee**2*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'QED':1})

GC_378 = Coupling(name = 'GC_378',
                  value = '2*cw**2*ee**2*FM0*complex(0,1)*vev + (cw**4*ee**2*FM0*complex(0,1)*vev)/sw**2 + ee**2*FM0*complex(0,1)*sw**2*vev',
                  order = {'M0':1,'QED':1})

GC_379 = Coupling(name = 'GC_379',
                  value = '2*ee**2*FM0*complex(0,1)*vev + (ee**2*FM0*complex(0,1)*sw**2*vev)/cw**2',
                  order = {'M0':1,'QED':1})

GC_380 = Coupling(name = 'GC_380',
                  value = '-2*ee**3*FM0*complex(0,1)*vev - (cw**2*ee**3*FM0*complex(0,1)*vev)/sw**2 - (ee**3*FM0*complex(0,1)*sw**2*vev)/cw**2',
                  order = {'M0':1,'QED':2})

GC_381 = Coupling(name = 'GC_381',
                  value = '-2*ee**4*FM0*complex(0,1)*vev - (cw**2*ee**4*FM0*complex(0,1)*vev)/sw**2 - (ee**4*FM0*complex(0,1)*sw**2*vev)/cw**2',
                  order = {'M0':1,'QED':3})

GC_382 = Coupling(name = 'GC_382',
                  value = '(cw**2*ee**2*FM1*complex(0,1)*vev)/2. + (cw**4*ee**2*FM1*complex(0,1)*vev)/(4.*sw**2) + (ee**2*FM1*complex(0,1)*sw**2*vev)/4.',
                  order = {'M1':1,'QED':1})

GC_383 = Coupling(name = 'GC_383',
                  value = '(ee**2*FM1*complex(0,1)*vev)/2. + (ee**2*FM1*complex(0,1)*sw**2*vev)/(4.*cw**2)',
                  order = {'M1':1,'QED':1})

GC_384 = Coupling(name = 'GC_384',
                  value = '(ee**3*FM1*complex(0,1)*vev)/2. + (cw**2*ee**3*FM1*complex(0,1)*vev)/(4.*sw**2) + (ee**3*FM1*complex(0,1)*sw**2*vev)/(4.*cw**2)',
                  order = {'M1':1,'QED':2})

GC_385 = Coupling(name = 'GC_385',
                  value = '-(ee**4*FM1*complex(0,1)*vev) - (cw**2*ee**4*FM1*complex(0,1)*vev)/(2.*sw**2) - (ee**4*FM1*complex(0,1)*sw**2*vev)/(2.*cw**2)',
                  order = {'M1':1,'QED':3})

GC_386 = Coupling(name = 'GC_386',
                  value = '-(cw**2*ee**2*FM7*complex(0,1)*vev)/4. - (cw**4*ee**2*FM7*complex(0,1)*vev)/(8.*sw**2) - (ee**2*FM7*complex(0,1)*sw**2*vev)/8.',
                  order = {'M7':1,'QED':1})

GC_387 = Coupling(name = 'GC_387',
                  value = '(ee**4*FM7*complex(0,1)*vev)/2. + (cw**2*ee**4*FM7*complex(0,1)*vev)/(4.*sw**2) + (ee**4*FM7*complex(0,1)*sw**2*vev)/(4.*cw**2)',
                  order = {'M7':1,'QED':3})

GC_388 = Coupling(name = 'GC_388',
                  value = '-2*ee**2*complex(0,1)*FS0*vev - (cw**2*ee**2*complex(0,1)*FS0*vev)/sw**2 - (ee**2*complex(0,1)*FS0*sw**2*vev)/cw**2',
                  order = {'QED':1,'S0':1})

GC_389 = Coupling(name = 'GC_389',
                  value = '-2*ee**2*complex(0,1)*FS1*vev - (cw**2*ee**2*complex(0,1)*FS1*vev)/sw**2 - (ee**2*complex(0,1)*FS1*sw**2*vev)/cw**2',
                  order = {'QED':1,'S1':1})

GC_390 = Coupling(name = 'GC_390',
                  value = '-2*ee**2*complex(0,1)*FS2*vev - (cw**2*ee**2*complex(0,1)*FS2*vev)/sw**2 - (ee**2*complex(0,1)*FS2*sw**2*vev)/cw**2',
                  order = {'QED':1,'S2':1})

GC_391 = Coupling(name = 'GC_391',
                  value = '(cw**3*ee**2*FM0*complex(0,1)*vev)/sw + 2*cw*ee**2*FM0*complex(0,1)*sw*vev + (ee**2*FM0*complex(0,1)*sw**3*vev)/cw',
                  order = {'M0':1,'QED':1})

GC_392 = Coupling(name = 'GC_392',
                  value = '(cw**3*ee**2*FM1*complex(0,1)*vev)/(4.*sw) + (cw*ee**2*FM1*complex(0,1)*sw*vev)/2. + (ee**2*FM1*complex(0,1)*sw**3*vev)/(4.*cw)',
                  order = {'M1':1,'QED':1})

GC_393 = Coupling(name = 'GC_393',
                  value = '-(cw**3*ee**2*FM7*complex(0,1)*vev)/(8.*sw) - (cw*ee**2*FM7*complex(0,1)*sw*vev)/4. - (ee**2*FM7*complex(0,1)*sw**3*vev)/(8.*cw)',
                  order = {'M7':1,'QED':1})

GC_394 = Coupling(name = 'GC_394',
                  value = 'cw**2*ee**2*FM0*complex(0,1)*vev + 2*ee**2*FM0*complex(0,1)*sw**2*vev + (ee**2*FM0*complex(0,1)*sw**4*vev)/cw**2',
                  order = {'M0':1,'QED':1})

GC_395 = Coupling(name = 'GC_395',
                  value = '(cw**2*ee**2*FM1*complex(0,1)*vev)/4. + (ee**2*FM1*complex(0,1)*sw**2*vev)/2. + (ee**2*FM1*complex(0,1)*sw**4*vev)/(4.*cw**2)',
                  order = {'M1':1,'QED':1})

GC_396 = Coupling(name = 'GC_396',
                  value = '-(cw**2*ee**2*FM7*complex(0,1)*vev)/8. - (ee**2*FM7*complex(0,1)*sw**2*vev)/4. - (ee**2*FM7*complex(0,1)*sw**4*vev)/(8.*cw**2)',
                  order = {'M7':1,'QED':1})

GC_397 = Coupling(name = 'GC_397',
                  value = '18*ee**4*complex(0,1)*FS0*vev + (3*cw**4*ee**4*complex(0,1)*FS0*vev)/sw**4 + (12*cw**2*ee**4*complex(0,1)*FS0*vev)/sw**2 + (12*ee**4*complex(0,1)*FS0*sw**2*vev)/cw**2 + (3*ee**4*complex(0,1)*FS0*sw**4*vev)/cw**4',
                  order = {'QED':3,'S0':1})

GC_398 = Coupling(name = 'GC_398',
                  value = '18*ee**4*complex(0,1)*FS1*vev + (3*cw**4*ee**4*complex(0,1)*FS1*vev)/sw**4 + (12*cw**2*ee**4*complex(0,1)*FS1*vev)/sw**2 + (12*ee**4*complex(0,1)*FS1*sw**2*vev)/cw**2 + (3*ee**4*complex(0,1)*FS1*sw**4*vev)/cw**4',
                  order = {'QED':3,'S1':1})

GC_399 = Coupling(name = 'GC_399',
                  value = '18*ee**4*complex(0,1)*FS2*vev + (3*cw**4*ee**4*complex(0,1)*FS2*vev)/sw**4 + (12*cw**2*ee**4*complex(0,1)*FS2*vev)/sw**2 + (12*ee**4*complex(0,1)*FS2*sw**2*vev)/cw**2 + (3*ee**4*complex(0,1)*FS2*sw**4*vev)/cw**4',
                  order = {'QED':3,'S2':1})

GC_400 = Coupling(name = 'GC_400',
                  value = '(ee**4*FM0*complex(0,1)*vev**2)/(2.*cw**2) + (ee**4*FM0*complex(0,1)*vev**2)/sw**2',
                  order = {'M0':1,'QED':2})

GC_401 = Coupling(name = 'GC_401',
                  value = '-(ee**4*FM0*complex(0,1)*vev**2)/2. - (cw**4*ee**4*FM0*complex(0,1)*vev**2)/(2.*sw**4) - (cw**2*ee**4*FM0*complex(0,1)*vev**2)/sw**2',
                  order = {'M0':1,'QED':2})

GC_402 = Coupling(name = 'GC_402',
                  value = '(ee**4*FM1*complex(0,1)*vev**2)/(4.*cw**2) + (ee**4*FM1*complex(0,1)*vev**2)/(2.*sw**2)',
                  order = {'M1':1,'QED':2})

GC_403 = Coupling(name = 'GC_403',
                  value = '(ee**4*FM1*complex(0,1)*vev**2)/4. + (cw**4*ee**4*FM1*complex(0,1)*vev**2)/(4.*sw**4) + (cw**2*ee**4*FM1*complex(0,1)*vev**2)/(2.*sw**2)',
                  order = {'M1':1,'QED':2})

GC_404 = Coupling(name = 'GC_404',
                  value = '-(ee**4*FM7*complex(0,1)*vev**2)/8. - (cw**4*ee**4*FM7*complex(0,1)*vev**2)/(8.*sw**4) - (cw**2*ee**4*FM7*complex(0,1)*vev**2)/(4.*sw**2)',
                  order = {'M7':1,'QED':2})

GC_405 = Coupling(name = 'GC_405',
                  value = '(3*ee**4*complex(0,1)*FS0*vev**2)/(4.*cw**2) + (3*cw**2*ee**4*complex(0,1)*FS0*vev**2)/(4.*sw**4) + (3*ee**4*complex(0,1)*FS0*vev**2)/(2.*sw**2)',
                  order = {'QED':2,'S0':1})

GC_406 = Coupling(name = 'GC_406',
                  value = '(3*ee**4*complex(0,1)*FS1*vev**2)/(2.*cw**2) + (3*cw**2*ee**4*complex(0,1)*FS1*vev**2)/(2.*sw**4) + (3*ee**4*complex(0,1)*FS1*vev**2)/sw**2',
                  order = {'QED':2,'S1':1})

GC_407 = Coupling(name = 'GC_407',
                  value = '(3*ee**4*complex(0,1)*FS2*vev**2)/(4.*cw**2) + (3*cw**2*ee**4*complex(0,1)*FS2*vev**2)/(4.*sw**4) + (3*ee**4*complex(0,1)*FS2*vev**2)/(2.*sw**2)',
                  order = {'QED':2,'S2':1})

GC_408 = Coupling(name = 'GC_408',
                  value = '(-3*cw*ee**3*complex(0,1)*FS0*vev**2)/(8.*sw**3) - (3*ee**3*complex(0,1)*FS0*vev**2)/(8.*cw*sw)',
                  order = {'QED':1,'S0':1})

GC_409 = Coupling(name = 'GC_409',
                  value = '(3*cw*ee**3*complex(0,1)*FS2*vev**2)/(8.*sw**3) + (3*ee**3*complex(0,1)*FS2*vev**2)/(8.*cw*sw)',
                  order = {'QED':1,'S2':1})

GC_410 = Coupling(name = 'GC_410',
                  value = '-(cw**3*ee**3*FM0*complex(0,1)*vev**2)/(2.*sw**3) - (cw*ee**3*FM0*complex(0,1)*vev**2)/sw - (ee**3*FM0*complex(0,1)*sw*vev**2)/(2.*cw)',
                  order = {'M0':1,'QED':1})

GC_411 = Coupling(name = 'GC_411',
                  value = '(cw**3*ee**4*FM0*complex(0,1)*vev**2)/sw**3 + (2*cw*ee**4*FM0*complex(0,1)*vev**2)/sw + (ee**4*FM0*complex(0,1)*sw*vev**2)/cw',
                  order = {'M0':1,'QED':2})

GC_412 = Coupling(name = 'GC_412',
                  value = '-(cw**3*ee**3*FM1*complex(0,1)*vev**2)/(8.*sw**3) - (cw*ee**3*FM1*complex(0,1)*vev**2)/(4.*sw) - (ee**3*FM1*complex(0,1)*sw*vev**2)/(8.*cw)',
                  order = {'M1':1,'QED':1})

GC_413 = Coupling(name = 'GC_413',
                  value = '-(cw**3*ee**4*FM1*complex(0,1)*vev**2)/(2.*sw**3) - (cw*ee**4*FM1*complex(0,1)*vev**2)/sw - (ee**4*FM1*complex(0,1)*sw*vev**2)/(2.*cw)',
                  order = {'M1':1,'QED':2})

GC_414 = Coupling(name = 'GC_414',
                  value = '(cw*ee**3*FM7*complex(0,1)*vev**2)/(16.*sw) + (ee**3*FM7*complex(0,1)*sw*vev**2)/(16.*cw)',
                  order = {'M7':1,'QED':1})

GC_415 = Coupling(name = 'GC_415',
                  value = '(cw**3*ee**4*FM7*complex(0,1)*vev**2)/(4.*sw**3) + (cw*ee**4*FM7*complex(0,1)*vev**2)/(2.*sw) + (ee**4*FM7*complex(0,1)*sw*vev**2)/(4.*cw)',
                  order = {'M7':1,'QED':2})

GC_416 = Coupling(name = 'GC_416',
                  value = 'cw**2*ee**2*FM0*complex(0,1)*vev**2 + (cw**4*ee**2*FM0*complex(0,1)*vev**2)/(2.*sw**2) + (ee**2*FM0*complex(0,1)*sw**2*vev**2)/2.',
                  order = {'M0':1})

GC_417 = Coupling(name = 'GC_417',
                  value = 'ee**2*FM0*complex(0,1)*vev**2 + (ee**2*FM0*complex(0,1)*sw**2*vev**2)/(2.*cw**2)',
                  order = {'M0':1})

GC_418 = Coupling(name = 'GC_418',
                  value = '-(ee**3*FM0*complex(0,1)*vev**2) - (cw**2*ee**3*FM0*complex(0,1)*vev**2)/(2.*sw**2) - (ee**3*FM0*complex(0,1)*sw**2*vev**2)/(2.*cw**2)',
                  order = {'M0':1,'QED':1})

GC_419 = Coupling(name = 'GC_419',
                  value = '-(ee**4*FM0*complex(0,1)*vev**2) - (cw**2*ee**4*FM0*complex(0,1)*vev**2)/(2.*sw**2) - (ee**4*FM0*complex(0,1)*sw**2*vev**2)/(2.*cw**2)',
                  order = {'M0':1,'QED':2})

GC_420 = Coupling(name = 'GC_420',
                  value = '(cw**2*ee**2*FM1*complex(0,1)*vev**2)/4. + (cw**4*ee**2*FM1*complex(0,1)*vev**2)/(8.*sw**2) + (ee**2*FM1*complex(0,1)*sw**2*vev**2)/8.',
                  order = {'M1':1})

GC_421 = Coupling(name = 'GC_421',
                  value = '(ee**2*FM1*complex(0,1)*vev**2)/4. + (ee**2*FM1*complex(0,1)*sw**2*vev**2)/(8.*cw**2)',
                  order = {'M1':1})

GC_422 = Coupling(name = 'GC_422',
                  value = '(ee**3*FM1*complex(0,1)*vev**2)/4. + (cw**2*ee**3*FM1*complex(0,1)*vev**2)/(8.*sw**2) + (ee**3*FM1*complex(0,1)*sw**2*vev**2)/(8.*cw**2)',
                  order = {'M1':1,'QED':1})

GC_423 = Coupling(name = 'GC_423',
                  value = '-(ee**4*FM1*complex(0,1)*vev**2)/2. - (cw**2*ee**4*FM1*complex(0,1)*vev**2)/(4.*sw**2) - (ee**4*FM1*complex(0,1)*sw**2*vev**2)/(4.*cw**2)',
                  order = {'M1':1,'QED':2})

GC_424 = Coupling(name = 'GC_424',
                  value = '-(cw**2*ee**2*FM7*complex(0,1)*vev**2)/8. - (cw**4*ee**2*FM7*complex(0,1)*vev**2)/(16.*sw**2) - (ee**2*FM7*complex(0,1)*sw**2*vev**2)/16.',
                  order = {'M7':1})

GC_425 = Coupling(name = 'GC_425',
                  value = '(ee**4*FM7*complex(0,1)*vev**2)/4. + (cw**2*ee**4*FM7*complex(0,1)*vev**2)/(8.*sw**2) + (ee**4*FM7*complex(0,1)*sw**2*vev**2)/(8.*cw**2)',
                  order = {'M7':1,'QED':2})

GC_426 = Coupling(name = 'GC_426',
                  value = '-(ee**2*complex(0,1)*FS0*vev**2) - (cw**2*ee**2*complex(0,1)*FS0*vev**2)/(2.*sw**2) - (ee**2*complex(0,1)*FS0*sw**2*vev**2)/(2.*cw**2)',
                  order = {'S0':1})

GC_427 = Coupling(name = 'GC_427',
                  value = '-(ee**2*complex(0,1)*FS1*vev**2) - (cw**2*ee**2*complex(0,1)*FS1*vev**2)/(2.*sw**2) - (ee**2*complex(0,1)*FS1*sw**2*vev**2)/(2.*cw**2)',
                  order = {'S1':1})

GC_428 = Coupling(name = 'GC_428',
                  value = '-(ee**2*complex(0,1)*FS2*vev**2) - (cw**2*ee**2*complex(0,1)*FS2*vev**2)/(2.*sw**2) - (ee**2*complex(0,1)*FS2*sw**2*vev**2)/(2.*cw**2)',
                  order = {'S2':1})

GC_429 = Coupling(name = 'GC_429',
                  value = '(cw**3*ee**2*FM0*complex(0,1)*vev**2)/(2.*sw) + cw*ee**2*FM0*complex(0,1)*sw*vev**2 + (ee**2*FM0*complex(0,1)*sw**3*vev**2)/(2.*cw)',
                  order = {'M0':1})

GC_430 = Coupling(name = 'GC_430',
                  value = '(cw**3*ee**2*FM1*complex(0,1)*vev**2)/(8.*sw) + (cw*ee**2*FM1*complex(0,1)*sw*vev**2)/4. + (ee**2*FM1*complex(0,1)*sw**3*vev**2)/(8.*cw)',
                  order = {'M1':1})

GC_431 = Coupling(name = 'GC_431',
                  value = '-(cw**3*ee**2*FM7*complex(0,1)*vev**2)/(16.*sw) - (cw*ee**2*FM7*complex(0,1)*sw*vev**2)/8. - (ee**2*FM7*complex(0,1)*sw**3*vev**2)/(16.*cw)',
                  order = {'M7':1})

GC_432 = Coupling(name = 'GC_432',
                  value = '(cw**2*ee**2*FM0*complex(0,1)*vev**2)/2. + ee**2*FM0*complex(0,1)*sw**2*vev**2 + (ee**2*FM0*complex(0,1)*sw**4*vev**2)/(2.*cw**2)',
                  order = {'M0':1})

GC_433 = Coupling(name = 'GC_433',
                  value = '(cw**2*ee**2*FM1*complex(0,1)*vev**2)/8. + (ee**2*FM1*complex(0,1)*sw**2*vev**2)/4. + (ee**2*FM1*complex(0,1)*sw**4*vev**2)/(8.*cw**2)',
                  order = {'M1':1})

GC_434 = Coupling(name = 'GC_434',
                  value = '-(cw**2*ee**2*FM7*complex(0,1)*vev**2)/16. - (ee**2*FM7*complex(0,1)*sw**2*vev**2)/8. - (ee**2*FM7*complex(0,1)*sw**4*vev**2)/(16.*cw**2)',
                  order = {'M7':1})

GC_435 = Coupling(name = 'GC_435',
                  value = '9*ee**4*complex(0,1)*FS0*vev**2 + (3*cw**4*ee**4*complex(0,1)*FS0*vev**2)/(2.*sw**4) + (6*cw**2*ee**4*complex(0,1)*FS0*vev**2)/sw**2 + (6*ee**4*complex(0,1)*FS0*sw**2*vev**2)/cw**2 + (3*ee**4*complex(0,1)*FS0*sw**4*vev**2)/(2.*cw**4)',
                  order = {'QED':2,'S0':1})

GC_436 = Coupling(name = 'GC_436',
                  value = '9*ee**4*complex(0,1)*FS1*vev**2 + (3*cw**4*ee**4*complex(0,1)*FS1*vev**2)/(2.*sw**4) + (6*cw**2*ee**4*complex(0,1)*FS1*vev**2)/sw**2 + (6*ee**4*complex(0,1)*FS1*sw**2*vev**2)/cw**2 + (3*ee**4*complex(0,1)*FS1*sw**4*vev**2)/(2.*cw**4)',
                  order = {'QED':2,'S1':1})

GC_437 = Coupling(name = 'GC_437',
                  value = '9*ee**4*complex(0,1)*FS2*vev**2 + (3*cw**4*ee**4*complex(0,1)*FS2*vev**2)/(2.*sw**4) + (6*cw**2*ee**4*complex(0,1)*FS2*vev**2)/sw**2 + (6*ee**4*complex(0,1)*FS2*sw**2*vev**2)/cw**2 + (3*ee**4*complex(0,1)*FS2*sw**4*vev**2)/(2.*cw**4)',
                  order = {'QED':2,'S2':1})

GC_438 = Coupling(name = 'GC_438',
                  value = '(ee**4*complex(0,1)*FS0*vev**3)/(4.*cw**2) + (cw**2*ee**4*complex(0,1)*FS0*vev**3)/(4.*sw**4) + (ee**4*complex(0,1)*FS0*vev**3)/(2.*sw**2)',
                  order = {'QED':1,'S0':1})

GC_439 = Coupling(name = 'GC_439',
                  value = '(ee**4*complex(0,1)*FS1*vev**3)/(2.*cw**2) + (cw**2*ee**4*complex(0,1)*FS1*vev**3)/(2.*sw**4) + (ee**4*complex(0,1)*FS1*vev**3)/sw**2',
                  order = {'QED':1,'S1':1})

GC_440 = Coupling(name = 'GC_440',
                  value = '(ee**4*complex(0,1)*FS2*vev**3)/(4.*cw**2) + (cw**2*ee**4*complex(0,1)*FS2*vev**3)/(4.*sw**4) + (ee**4*complex(0,1)*FS2*vev**3)/(2.*sw**2)',
                  order = {'QED':1,'S2':1})

GC_441 = Coupling(name = 'GC_441',
                  value = '-(cw*ee**3*complex(0,1)*FS0*vev**3)/(8.*sw**3) - (ee**3*complex(0,1)*FS0*vev**3)/(8.*cw*sw)',
                  order = {'S0':1})

GC_442 = Coupling(name = 'GC_442',
                  value = '(cw*ee**3*complex(0,1)*FS2*vev**3)/(8.*sw**3) + (ee**3*complex(0,1)*FS2*vev**3)/(8.*cw*sw)',
                  order = {'S2':1})

GC_443 = Coupling(name = 'GC_443',
                  value = '3*ee**4*complex(0,1)*FS0*vev**3 + (cw**4*ee**4*complex(0,1)*FS0*vev**3)/(2.*sw**4) + (2*cw**2*ee**4*complex(0,1)*FS0*vev**3)/sw**2 + (2*ee**4*complex(0,1)*FS0*sw**2*vev**3)/cw**2 + (ee**4*complex(0,1)*FS0*sw**4*vev**3)/(2.*cw**4)',
                  order = {'QED':1,'S0':1})

GC_444 = Coupling(name = 'GC_444',
                  value = '3*ee**4*complex(0,1)*FS1*vev**3 + (cw**4*ee**4*complex(0,1)*FS1*vev**3)/(2.*sw**4) + (2*cw**2*ee**4*complex(0,1)*FS1*vev**3)/sw**2 + (2*ee**4*complex(0,1)*FS1*sw**2*vev**3)/cw**2 + (ee**4*complex(0,1)*FS1*sw**4*vev**3)/(2.*cw**4)',
                  order = {'QED':1,'S1':1})

GC_445 = Coupling(name = 'GC_445',
                  value = '3*ee**4*complex(0,1)*FS2*vev**3 + (cw**4*ee**4*complex(0,1)*FS2*vev**3)/(2.*sw**4) + (2*cw**2*ee**4*complex(0,1)*FS2*vev**3)/sw**2 + (2*ee**4*complex(0,1)*FS2*sw**2*vev**3)/cw**2 + (ee**4*complex(0,1)*FS2*sw**4*vev**3)/(2.*cw**4)',
                  order = {'QED':1,'S2':1})

GC_446 = Coupling(name = 'GC_446',
                  value = '(ee**4*complex(0,1)*FS0*vev**4)/(16.*cw**2) + (cw**2*ee**4*complex(0,1)*FS0*vev**4)/(16.*sw**4) + (ee**4*complex(0,1)*FS0*vev**4)/(8.*sw**2)',
                  order = {'S0':1})

GC_447 = Coupling(name = 'GC_447',
                  value = '(ee**4*complex(0,1)*FS1*vev**4)/(8.*cw**2) + (cw**2*ee**4*complex(0,1)*FS1*vev**4)/(8.*sw**4) + (ee**4*complex(0,1)*FS1*vev**4)/(4.*sw**2)',
                  order = {'S1':1})

GC_448 = Coupling(name = 'GC_448',
                  value = '(ee**4*complex(0,1)*FS2*vev**4)/(16.*cw**2) + (cw**2*ee**4*complex(0,1)*FS2*vev**4)/(16.*sw**4) + (ee**4*complex(0,1)*FS2*vev**4)/(8.*sw**2)',
                  order = {'S2':1})

GC_449 = Coupling(name = 'GC_449',
                  value = '(3*ee**4*complex(0,1)*FS0*vev**4)/4. + (cw**4*ee**4*complex(0,1)*FS0*vev**4)/(8.*sw**4) + (cw**2*ee**4*complex(0,1)*FS0*vev**4)/(2.*sw**2) + (ee**4*complex(0,1)*FS0*sw**2*vev**4)/(2.*cw**2) + (ee**4*complex(0,1)*FS0*sw**4*vev**4)/(8.*cw**4)',
                  order = {'S0':1})

GC_450 = Coupling(name = 'GC_450',
                  value = '(3*ee**4*complex(0,1)*FS1*vev**4)/4. + (cw**4*ee**4*complex(0,1)*FS1*vev**4)/(8.*sw**4) + (cw**2*ee**4*complex(0,1)*FS1*vev**4)/(2.*sw**2) + (ee**4*complex(0,1)*FS1*sw**2*vev**4)/(2.*cw**2) + (ee**4*complex(0,1)*FS1*sw**4*vev**4)/(8.*cw**4)',
                  order = {'S1':1})

GC_451 = Coupling(name = 'GC_451',
                  value = '(3*ee**4*complex(0,1)*FS2*vev**4)/4. + (cw**4*ee**4*complex(0,1)*FS2*vev**4)/(8.*sw**4) + (cw**2*ee**4*complex(0,1)*FS2*vev**4)/(2.*sw**2) + (ee**4*complex(0,1)*FS2*sw**2*vev**4)/(2.*cw**2) + (ee**4*complex(0,1)*FS2*sw**4*vev**4)/(8.*cw**4)',
                  order = {'S2':1})

GC_452 = Coupling(name = 'GC_452',
                  value = '-((complex(0,1)*yt)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_453 = Coupling(name = 'GC_453',
                  value = '-((complex(0,1)*ytau)/cmath.sqrt(2))',
                  order = {'QED':1})

GC_454 = Coupling(name = 'GC_454',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_455 = Coupling(name = 'GC_455',
                  value = '(ee*complex(0,1)*complexconjugate(CKM1x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_456 = Coupling(name = 'GC_456',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x1))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

GC_457 = Coupling(name = 'GC_457',
                  value = '(ee*complex(0,1)*complexconjugate(CKM2x2))/(sw*cmath.sqrt(2))',
                  order = {'QED':1})

