Requestor: Karolos Potamianos
Content: June 2019 version of the Eboli model
Original URL: https://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/AnomalousGaugeCoupling/SM_LS012_LM017_LT012_Ind5_UFO_June19.tar.gz
Website: https://feynrules.irmp.ucl.ac.be/wiki/AnomalousGaugeCoupling
Description: DEPRECATED! Superseded by QAll_5_Aug21v2 which fixes a bug in LM5 and adds T3 and T4.
