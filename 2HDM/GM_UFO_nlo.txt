Requestor: Benjamin Freund
Content: Higgs triplet model (update of GM_UFO for NLO running)
Paper: http://arxiv.org/abs/1404.2640
Link: https://feynrules.irmp.ucl.ac.be/wiki/GeorgiMachacekModel
JIRA: https://its.cern.ch/jira/browse/AGENE-1158