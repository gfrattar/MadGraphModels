Requestor: Robert Hankache
Content: anomalous neutral Triple Gauge Couplings
Paper: Use of model is in https://arxiv.org/abs/2102.01115 (published in PLB)
Source: Model got from Frank Krauss. Original UFO model was apparently written by Shankha Banerjee (cited as private communication in the above paper).
