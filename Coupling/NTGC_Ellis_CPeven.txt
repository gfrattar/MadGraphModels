Requestor: Danning Liu
Content: neutral Triple Gauga couplings of Z(ll)+gamma process 
          model proposed by prof. John Ellis
Paper: https://arxiv.org/abs/2008.04298 
Recent Paper: https://arxiv.org/abs/2206.11676
JIRA: https://its.cern.ch/jira/browse/AGENE-2188 
Source: model got from Dr. Ruiqing Xiao
