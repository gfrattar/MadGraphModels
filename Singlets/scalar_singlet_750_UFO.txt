Requestor: Larry Lee Jr.
Content: Scalar singlet for 750 GeV diphoton resonance
Paper: http://arxiv.org/abs/1008.5302
JIRA: https://its.cern.ch/jira/browse/AGENE-1146