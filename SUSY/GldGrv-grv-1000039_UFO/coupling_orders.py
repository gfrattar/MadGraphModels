# This file was automatically created by FeynRules 2.3.29
# Mathematica version: 11.2.0 for Mac OS X x86 (64-bit) (September 11, 2017)
# Date: Sat 9 Feb 2019 17:36:44


from object_library import all_orders, CouplingOrder


QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

HIW = CouplingOrder(name = 'HIW',
                    expansion_order = 99,
                    hierarchy = 1)

QGR = CouplingOrder(name = 'QGR',
                    expansion_order = 99,
                    hierarchy = 1)

