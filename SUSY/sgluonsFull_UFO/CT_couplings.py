# This file was automatically created by FeynRules 2.4.58
# Mathematica version: 11.0.0 for Mac OS X x86 (64-bit) (July 28, 2016)
# Date: Tue 28 Nov 2017 15:14:54


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_100_1 = Coupling(name = 'R2GC_100_1',
                      value = '(complex(0,1)*G**2*MB*yLd3x3)/(16.*cmath.pi**2) + (complex(0,1)*G**2*MB*yRd3x3)/(16.*cmath.pi**2)',
                      order = {'NP':1,'QCD':2})

R2GC_100_2 = Coupling(name = 'R2GC_100_2',
                      value = '(complex(0,1)*G**2*MT*yLu3x3)/(16.*cmath.pi**2) + (complex(0,1)*G**2*MT*yRu3x3)/(16.*cmath.pi**2)',
                      order = {'NP':1,'QCD':2})

R2GC_101_3 = Coupling(name = 'R2GC_101_3',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_101_4 = Coupling(name = 'R2GC_101_4',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_102_5 = Coupling(name = 'R2GC_102_5',
                      value = '(complex(0,1)*G**2*yb*yLd3x3)/(16.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*yb*yRd3x3)/(16.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'NP':1,'QCD':2,'QED':1})

R2GC_102_6 = Coupling(name = 'R2GC_102_6',
                      value = '(complex(0,1)*G**2*yLu3x3*yt)/(16.*cmath.pi**2*cmath.sqrt(2)) + (complex(0,1)*G**2*yRu3x3*yt)/(16.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'NP':1,'QCD':2,'QED':1})

R2GC_103_7 = Coupling(name = 'R2GC_103_7',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_103_8 = Coupling(name = 'R2GC_103_8',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_104_9 = Coupling(name = 'R2GC_104_9',
                      value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_105_10 = Coupling(name = 'R2GC_105_10',
                       value = '(35*complex(0,1)*G**2*g8g*MB)/(48.*cmath.pi**2) + (complex(0,1)*G**2*yLd3x3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*yRd3x3)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_106_11 = Coupling(name = 'R2GC_106_11',
                       value = '(complex(0,1)*G**2*yLu2x2)/(24.*cmath.pi**2) + (complex(0,1)*G**2*yRu2x2)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_107_12 = Coupling(name = 'R2GC_107_12',
                       value = '(complex(0,1)*G**2*yLd1x1)/(24.*cmath.pi**2) + (complex(0,1)*G**2*yRd1x1)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_108_13 = Coupling(name = 'R2GC_108_13',
                       value = '(complex(0,1)*G**2*yLd2x2)/(24.*cmath.pi**2) + (complex(0,1)*G**2*yRd2x2)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_109_14 = Coupling(name = 'R2GC_109_14',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_110_15 = Coupling(name = 'R2GC_110_15',
                       value = '(35*complex(0,1)*G**2*g8g*MT)/(48.*cmath.pi**2) + (complex(0,1)*G**2*yLu3x3)/(24.*cmath.pi**2) + (complex(0,1)*G**2*yRu3x3)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_111_16 = Coupling(name = 'R2GC_111_16',
                       value = '(complex(0,1)*G**2*yLu1x1)/(24.*cmath.pi**2) + (complex(0,1)*G**2*yRu1x1)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_124_17 = Coupling(name = 'R2GC_124_17',
                       value = '(9*complex(0,1)*G**2*g8g)/(8.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_125_18 = Coupling(name = 'R2GC_125_18',
                       value = '(-37*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_126_19 = Coupling(name = 'R2GC_126_19',
                       value = '(-33*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_127_20 = Coupling(name = 'R2GC_127_20',
                       value = '-(G**3*g8g)/(2.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_128_21 = Coupling(name = 'R2GC_128_21',
                       value = '(-27*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_129_22 = Coupling(name = 'R2GC_129_22',
                       value = '(-17*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_129_23 = Coupling(name = 'R2GC_129_23',
                       value = '(G**3*g8g)/(16.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_130_24 = Coupling(name = 'R2GC_130_24',
                       value = '(-5*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_130_25 = Coupling(name = 'R2GC_130_25',
                       value = '-(G**3*g8g)/(16.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_131_26 = Coupling(name = 'R2GC_131_26',
                       value = '(-3*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_132_27 = Coupling(name = 'R2GC_132_27',
                       value = '-(G**3*g8g)/(8.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_134_28 = Coupling(name = 'R2GC_134_28',
                       value = '(5*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_135_29 = Coupling(name = 'R2GC_135_29',
                       value = '(17*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_136_30 = Coupling(name = 'R2GC_136_30',
                       value = '(23*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_137_31 = Coupling(name = 'R2GC_137_31',
                       value = '(11*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_138_32 = Coupling(name = 'R2GC_138_32',
                       value = '(9*G**3*g8g)/(16.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_140_33 = Coupling(name = 'R2GC_140_33',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_140_34 = Coupling(name = 'R2GC_140_34',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_141_35 = Coupling(name = 'R2GC_141_35',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_141_36 = Coupling(name = 'R2GC_141_36',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_142_37 = Coupling(name = 'R2GC_142_37',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_142_38 = Coupling(name = 'R2GC_142_38',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_143_39 = Coupling(name = 'R2GC_143_39',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_144_40 = Coupling(name = 'R2GC_144_40',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_144_41 = Coupling(name = 'R2GC_144_41',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_146_42 = Coupling(name = 'R2GC_146_42',
                       value = '(-7*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_147_43 = Coupling(name = 'R2GC_147_43',
                       value = '(3*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_147_44 = Coupling(name = 'R2GC_147_44',
                       value = '(-13*G**4)/(256.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_148_45 = Coupling(name = 'R2GC_148_45',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_148_46 = Coupling(name = 'R2GC_148_46',
                       value = '(13*complex(0,1)*G**4)/(256.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_149_47 = Coupling(name = 'R2GC_149_47',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_149_48 = Coupling(name = 'R2GC_149_48',
                       value = '(13*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_150_49 = Coupling(name = 'R2GC_150_49',
                       value = '(-3*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_150_50 = Coupling(name = 'R2GC_150_50',
                       value = '(13*complex(0,1)*G**4)/(128.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_151_51 = Coupling(name = 'R2GC_151_51',
                       value = '(-35*complex(0,1)*G**4)/(256.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_152_52 = Coupling(name = 'R2GC_152_52',
                       value = '(-3*complex(0,1)*G**2*g8g)/(8.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_153_53 = Coupling(name = 'R2GC_153_53',
                       value = '(-3*G**3*g8g)/(8.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_153_54 = Coupling(name = 'R2GC_153_54',
                       value = '(-21*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_155_55 = Coupling(name = 'R2GC_155_55',
                       value = '(-43*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_156_56 = Coupling(name = 'R2GC_156_56',
                       value = '(-11*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_157_57 = Coupling(name = 'R2GC_157_57',
                       value = '(-7*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_158_58 = Coupling(name = 'R2GC_158_58',
                       value = '(9*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_160_59 = Coupling(name = 'R2GC_160_59',
                       value = '-(G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_161_60 = Coupling(name = 'R2GC_161_60',
                       value = '(7*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_162_61 = Coupling(name = 'R2GC_162_61',
                       value = '(15*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_163_62 = Coupling(name = 'R2GC_163_62',
                       value = '(7*G**3*g8g)/(16.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_164_63 = Coupling(name = 'R2GC_164_63',
                       value = '(-7*G**3*g8g)/(16.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_165_64 = Coupling(name = 'R2GC_165_64',
                       value = '(-15*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_166_65 = Coupling(name = 'R2GC_166_65',
                       value = '(-21*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_167_66 = Coupling(name = 'R2GC_167_66',
                       value = '(G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_169_67 = Coupling(name = 'R2GC_169_67',
                       value = '(3*G**3*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_170_68 = Coupling(name = 'R2GC_170_68',
                       value = '(-7*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_170_69 = Coupling(name = 'R2GC_170_69',
                       value = '(G**3*g8g)/(8.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_173_70 = Coupling(name = 'R2GC_173_70',
                       value = '(31*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_176_71 = Coupling(name = 'R2GC_176_71',
                       value = '(-3*complex(0,1)*G**2*yLu2x2)/(32.*cmath.pi**2) - (3*complex(0,1)*G**2*yRu2x2)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_179_72 = Coupling(name = 'R2GC_179_72',
                       value = '(-3*complex(0,1)*G**2*yLd1x1)/(32.*cmath.pi**2) - (3*complex(0,1)*G**2*yRd1x1)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_182_73 = Coupling(name = 'R2GC_182_73',
                       value = '(-3*complex(0,1)*G**2*yLd2x2)/(32.*cmath.pi**2) - (3*complex(0,1)*G**2*yRd2x2)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_187_74 = Coupling(name = 'R2GC_187_74',
                       value = '(-3*complex(0,1)*G**2*yLu1x1)/(32.*cmath.pi**2) - (3*complex(0,1)*G**2*yRu1x1)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_190_75 = Coupling(name = 'R2GC_190_75',
                       value = '(3*complex(0,1)*G**2*m8**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_191_76 = Coupling(name = 'R2GC_191_76',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_193_77 = Coupling(name = 'R2GC_193_77',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_194_78 = Coupling(name = 'R2GC_194_78',
                       value = '(complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_195_79 = Coupling(name = 'R2GC_195_79',
                       value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_196_80 = Coupling(name = 'R2GC_196_80',
                       value = '(-3*complex(0,1)*G**2*yLd3x3)/(32.*cmath.pi**2) - (3*complex(0,1)*G**2*yRd3x3)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_198_81 = Coupling(name = 'R2GC_198_81',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_198_82 = Coupling(name = 'R2GC_198_82',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_199_83 = Coupling(name = 'R2GC_199_83',
                       value = '(11*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_199_84 = Coupling(name = 'R2GC_199_84',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_200_85 = Coupling(name = 'R2GC_200_85',
                       value = '(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_200_86 = Coupling(name = 'R2GC_200_86',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_201_87 = Coupling(name = 'R2GC_201_87',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_202_88 = Coupling(name = 'R2GC_202_88',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_205_89 = Coupling(name = 'R2GC_205_89',
                       value = '(57*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_206_90 = Coupling(name = 'R2GC_206_90',
                       value = '(11*G**3*g8g)/(8.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_207_91 = Coupling(name = 'R2GC_207_91',
                       value = '(-11*G**3*g8g)/(8.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_208_92 = Coupling(name = 'R2GC_208_92',
                       value = '(-59*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_212_93 = Coupling(name = 'R2GC_212_93',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_213_94 = Coupling(name = 'R2GC_213_94',
                       value = '(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_214_95 = Coupling(name = 'R2GC_214_95',
                       value = '-(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_215_96 = Coupling(name = 'R2GC_215_96',
                       value = '(-3*complex(0,1)*G**2*yLu3x3)/(32.*cmath.pi**2) - (3*complex(0,1)*G**2*yRu3x3)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_216_97 = Coupling(name = 'R2GC_216_97',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_217_98 = Coupling(name = 'R2GC_217_98',
                       value = '-(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_218_99 = Coupling(name = 'R2GC_218_99',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_219_100 = Coupling(name = 'R2GC_219_100',
                        value = '(complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                        order = {'QCD':2,'QED':1})

R2GC_78_101 = Coupling(name = 'R2GC_78_101',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_79_102 = Coupling(name = 'R2GC_79_102',
                       value = '(-31*complex(0,1)*G**2*g8g)/(32.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_80_103 = Coupling(name = 'R2GC_80_103',
                       value = '(-5*G**3*g8g)/(64.*cmath.pi**2)',
                       order = {'NP':1,'QCD':3})

R2GC_81_104 = Coupling(name = 'R2GC_81_104',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_82_105 = Coupling(name = 'R2GC_82_105',
                       value = '(5*complex(0,1)*G**2*g8g)/(24.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_89_106 = Coupling(name = 'R2GC_89_106',
                       value = '-(complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_90_107 = Coupling(name = 'R2GC_90_107',
                       value = '-(complex(0,1)*G**2*g8g)/(4.*cmath.pi**2)',
                       order = {'NP':1,'QCD':2})

R2GC_99_108 = Coupling(name = 'R2GC_99_108',
                       value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_99_109 = Coupling(name = 'R2GC_99_109',
                       value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                       order = {'QCD':2})

UVGC_112_1 = Coupling(name = 'UVGC_112_1',
                      value = {-1:'-(complex(0,1)*G**2*g8g)/(4.*cmath.pi**2)'},
                      order = {'NP':1,'QCD':2})

UVGC_113_2 = Coupling(name = 'UVGC_113_2',
                      value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                      order = {'QCD':3})

UVGC_114_3 = Coupling(name = 'UVGC_114_3',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_118_4 = Coupling(name = 'UVGC_118_4',
                      value = {-1:'(-15*complex(0,1)*G**2*g8g)/(8.*cmath.pi**2)'},
                      order = {'NP':1,'QCD':2})

UVGC_124_5 = Coupling(name = 'UVGC_124_5',
                      value = {-1:'(9*complex(0,1)*G**2*g8g)/(8.*cmath.pi**2)'},
                      order = {'NP':1,'QCD':2})

UVGC_125_6 = Coupling(name = 'UVGC_125_6',
                      value = {-1:'(-39*G**3*g8g)/(64.*cmath.pi**2)'},
                      order = {'NP':1,'QCD':3})

UVGC_126_7 = Coupling(name = 'UVGC_126_7',
                      value = {-1:'(-15*G**3*g8g)/(32.*cmath.pi**2)'},
                      order = {'NP':1,'QCD':3})

UVGC_127_8 = Coupling(name = 'UVGC_127_8',
                      value = {-1:'(-7*G**3*g8g)/(16.*cmath.pi**2)'},
                      order = {'NP':1,'QCD':3})

UVGC_129_9 = Coupling(name = 'UVGC_129_9',
                      value = {-1:'-(G**3*g8g)/(4.*cmath.pi**2)'},
                      order = {'NP':1,'QCD':3})

UVGC_130_10 = Coupling(name = 'UVGC_130_10',
                       value = {-1:'(-3*G**3*g8g)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_131_11 = Coupling(name = 'UVGC_131_11',
                       value = {-1:'(-5*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_132_12 = Coupling(name = 'UVGC_132_12',
                       value = {-1:'-(G**3*g8g)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_133_13 = Coupling(name = 'UVGC_133_13',
                       value = {-1:'-(G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_134_14 = Coupling(name = 'UVGC_134_14',
                       value = {-1:'(3*G**3*g8g)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_135_15 = Coupling(name = 'UVGC_135_15',
                       value = {-1:'(G**3*g8g)/(4.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_136_16 = Coupling(name = 'UVGC_136_16',
                       value = {-1:'(25*G**3*g8g)/(64.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_137_17 = Coupling(name = 'UVGC_137_17',
                       value = {-1:'(7*G**3*g8g)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_138_18 = Coupling(name = 'UVGC_138_18',
                       value = {-1:'(9*G**3*g8g)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_139_19 = Coupling(name = 'UVGC_139_19',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_139_20 = Coupling(name = 'UVGC_139_20',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_140_21 = Coupling(name = 'UVGC_140_21',
                       value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_140_22 = Coupling(name = 'UVGC_140_22',
                       value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_141_23 = Coupling(name = 'UVGC_141_23',
                       value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_141_24 = Coupling(name = 'UVGC_141_24',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_143_25 = Coupling(name = 'UVGC_143_25',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_143_26 = Coupling(name = 'UVGC_143_26',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_144_27 = Coupling(name = 'UVGC_144_27',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_144_28 = Coupling(name = 'UVGC_144_28',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_145_29 = Coupling(name = 'UVGC_145_29',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_145_30 = Coupling(name = 'UVGC_145_30',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_145_31 = Coupling(name = 'UVGC_145_31',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_146_32 = Coupling(name = 'UVGC_146_32',
                       value = {-1:'G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_146_33 = Coupling(name = 'UVGC_146_33',
                       value = {-1:'(-19*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_146_34 = Coupling(name = 'UVGC_146_34',
                       value = {-1:'-G**3/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_146_35 = Coupling(name = 'UVGC_146_35',
                       value = {-1:'(-3*G**3)/(16.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_147_36 = Coupling(name = 'UVGC_147_36',
                       value = {-1:'(9*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_147_37 = Coupling(name = 'UVGC_147_37',
                       value = {-1:'(-9*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_148_38 = Coupling(name = 'UVGC_148_38',
                       value = {-1:'(-9*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_148_39 = Coupling(name = 'UVGC_148_39',
                       value = {-1:'(9*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_149_40 = Coupling(name = 'UVGC_149_40',
                       value = {-1:'(-3*complex(0,1)*G**4)/(32.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_149_41 = Coupling(name = 'UVGC_149_41',
                       value = {-1:'(3*complex(0,1)*G**4)/(32.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_150_42 = Coupling(name = 'UVGC_150_42',
                       value = {-1:'(-9*complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_150_43 = Coupling(name = 'UVGC_150_43',
                       value = {-1:'(9*complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_44 = Coupling(name = 'UVGC_151_44',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_45 = Coupling(name = 'UVGC_151_45',
                       value = {-1:'(-47*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_46 = Coupling(name = 'UVGC_151_46',
                       value = {-1:'-(complex(0,1)*G**4)/(64.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_151_47 = Coupling(name = 'UVGC_151_47',
                       value = {-1:'(-39*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_152_48 = Coupling(name = 'UVGC_152_48',
                       value = {-1:'(-9*complex(0,1)*G**2*g8g)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_153_49 = Coupling(name = 'UVGC_153_49',
                       value = {-1:'(-9*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_153_50 = Coupling(name = 'UVGC_153_50',
                       value = {-1:'(-27*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_155_51 = Coupling(name = 'UVGC_155_51',
                       value = {-1:'(-67*G**3*g8g)/(64.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_157_52 = Coupling(name = 'UVGC_157_52',
                       value = {-1:'-(G**3*g8g)/(2.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_159_53 = Coupling(name = 'UVGC_159_53',
                       value = {-1:'(-23*G**3*g8g)/(64.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_159_54 = Coupling(name = 'UVGC_159_54',
                       value = {-1:'(-3*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_160_55 = Coupling(name = 'UVGC_160_55',
                       value = {-1:'(5*G**3*g8g)/(64.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_162_56 = Coupling(name = 'UVGC_162_56',
                       value = {-1:'(17*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_163_57 = Coupling(name = 'UVGC_163_57',
                       value = {-1:'(5*G**3*g8g)/(8.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_164_58 = Coupling(name = 'UVGC_164_58',
                       value = {-1:'(-5*G**3*g8g)/(8.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_164_59 = Coupling(name = 'UVGC_164_59',
                       value = {-1:'(3*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_165_60 = Coupling(name = 'UVGC_165_60',
                       value = {-1:'(-17*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_166_61 = Coupling(name = 'UVGC_166_61',
                       value = {-1:'(-13*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_167_62 = Coupling(name = 'UVGC_167_62',
                       value = {-1:'(-5*G**3*g8g)/(64.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_168_63 = Coupling(name = 'UVGC_168_63',
                       value = {-1:'(3*G**3*g8g)/(64.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_169_64 = Coupling(name = 'UVGC_169_64',
                       value = {-1:'(9*G**3*g8g)/(32.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_170_65 = Coupling(name = 'UVGC_170_65',
                       value = {-1:'(G**3*g8g)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_172_66 = Coupling(name = 'UVGC_172_66',
                       value = {-1:'(39*G**3*g8g)/(64.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_173_67 = Coupling(name = 'UVGC_173_67',
                       value = {-1:'(47*G**3*g8g)/(64.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':3})

UVGC_174_68 = Coupling(name = 'UVGC_174_68',
                       value = {-1:'(-5*G**2*g8g*yLd3x3)/(48.*cmath.pi**2) - (5*G**2*g8g*yRd3x3)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_175_69 = Coupling(name = 'UVGC_175_69',
                       value = {-1:'(5*G**2*g8g*yLd3x3)/(48.*cmath.pi**2) + (5*G**2*g8g*yRd3x3)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_176_70 = Coupling(name = 'UVGC_176_70',
                       value = {-1:'(complex(0,1)*G**2*yLu2x2)/(8.*cmath.pi**2) + (complex(0,1)*G**2*yRu2x2)/(8.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_176_71 = Coupling(name = 'UVGC_176_71',
                       value = {-1:'(-3*complex(0,1)*G**2*yLu2x2)/(16.*cmath.pi**2) - (3*complex(0,1)*G**2*yRu2x2)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_177_72 = Coupling(name = 'UVGC_177_72',
                       value = {-1:'(-5*G**2*g8g*yLu2x2)/(48.*cmath.pi**2) - (5*G**2*g8g*yRu2x2)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_178_73 = Coupling(name = 'UVGC_178_73',
                       value = {-1:'(5*G**2*g8g*yLu2x2)/(48.*cmath.pi**2) + (5*G**2*g8g*yRu2x2)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_179_74 = Coupling(name = 'UVGC_179_74',
                       value = {-1:'(complex(0,1)*G**2*yLd1x1)/(8.*cmath.pi**2) + (complex(0,1)*G**2*yRd1x1)/(8.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_179_75 = Coupling(name = 'UVGC_179_75',
                       value = {-1:'(-3*complex(0,1)*G**2*yLd1x1)/(16.*cmath.pi**2) - (3*complex(0,1)*G**2*yRd1x1)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_180_76 = Coupling(name = 'UVGC_180_76',
                       value = {-1:'(-5*G**2*g8g*yLd1x1)/(48.*cmath.pi**2) - (5*G**2*g8g*yRd1x1)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_181_77 = Coupling(name = 'UVGC_181_77',
                       value = {-1:'(5*G**2*g8g*yLd1x1)/(48.*cmath.pi**2) + (5*G**2*g8g*yRd1x1)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_182_78 = Coupling(name = 'UVGC_182_78',
                       value = {-1:'(complex(0,1)*G**2*yLd2x2)/(8.*cmath.pi**2) + (complex(0,1)*G**2*yRd2x2)/(8.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_182_79 = Coupling(name = 'UVGC_182_79',
                       value = {-1:'(-3*complex(0,1)*G**2*yLd2x2)/(16.*cmath.pi**2) - (3*complex(0,1)*G**2*yRd2x2)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_183_80 = Coupling(name = 'UVGC_183_80',
                       value = {-1:'(-5*G**2*g8g*yLd2x2)/(48.*cmath.pi**2) - (5*G**2*g8g*yRd2x2)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_184_81 = Coupling(name = 'UVGC_184_81',
                       value = {-1:'(5*G**2*g8g*yLd2x2)/(48.*cmath.pi**2) + (5*G**2*g8g*yRd2x2)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_185_82 = Coupling(name = 'UVGC_185_82',
                       value = {-1:'(-5*G**2*g8g*yLu3x3)/(48.*cmath.pi**2) - (5*G**2*g8g*yRu3x3)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_186_83 = Coupling(name = 'UVGC_186_83',
                       value = {-1:'(5*G**2*g8g*yLu3x3)/(48.*cmath.pi**2) + (5*G**2*g8g*yRu3x3)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_187_84 = Coupling(name = 'UVGC_187_84',
                       value = {-1:'(-3*complex(0,1)*G**2*yLu1x1)/(16.*cmath.pi**2) - (3*complex(0,1)*G**2*yRu1x1)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_187_85 = Coupling(name = 'UVGC_187_85',
                       value = {-1:'(complex(0,1)*G**2*yLu1x1)/(8.*cmath.pi**2) + (complex(0,1)*G**2*yRu1x1)/(8.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_188_86 = Coupling(name = 'UVGC_188_86',
                       value = {-1:'(-5*G**2*g8g*yLu1x1)/(48.*cmath.pi**2) - (5*G**2*g8g*yRu1x1)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_189_87 = Coupling(name = 'UVGC_189_87',
                       value = {-1:'(5*G**2*g8g*yLu1x1)/(48.*cmath.pi**2) + (5*G**2*g8g*yRu1x1)/(48.*cmath.pi**2)'},
                       order = {'NP':2,'QCD':2})

UVGC_190_88 = Coupling(name = 'UVGC_190_88',
                       value = {-1:'(9*complex(0,1)*G**2*m8**2)/(16.*cmath.pi**2)',0:'(21*complex(0,1)*G**2*m8**2)/(16.*cmath.pi**2) - (9*complex(0,1)*G**2*m8**2*reglog(m8**2/MU_R**2))/(16.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_191_89 = Coupling(name = 'UVGC_191_89',
                       value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB**2/MU_R**2))/(4.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_192_90 = Coupling(name = 'UVGC_192_90',
                       value = {0:'-(complex(0,1)*G**3)/(3.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB**2/MU_R**2))/(4.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_193_91 = Coupling(name = 'UVGC_193_91',
                       value = {-1:'(complex(0,1)*G**2*MB)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MB)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB**2/MU_R**2))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_194_92 = Coupling(name = 'UVGC_194_92',
                       value = {-1:'(complex(0,1)*G**2*yb)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*yb*cmath.sqrt(2))/(3.*cmath.pi**2) - (complex(0,1)*G**2*yb*reglog(MB**2/MU_R**2))/(2.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_195_93 = Coupling(name = 'UVGC_195_93',
                       value = {-1:'-(G**2*yb)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'-(G**2*yb*cmath.sqrt(2))/(3.*cmath.pi**2) + (G**2*yb*reglog(MB**2/MU_R**2))/(2.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_196_94 = Coupling(name = 'UVGC_196_94',
                       value = {-1:'(5*complex(0,1)*G**2*g8g*MB)/(8.*cmath.pi**2) - (complex(0,1)*G**2*yLd3x3)/(8.*cmath.pi**2) - (complex(0,1)*G**2*yRd3x3)/(8.*cmath.pi**2)',0:'-(complex(0,1)*G**2*yLd3x3)/(3.*cmath.pi**2) - (complex(0,1)*G**2*yRd3x3)/(3.*cmath.pi**2) + (complex(0,1)*G**2*yLd3x3*reglog(MB**2/MU_R**2))/(4.*cmath.pi**2) + (complex(0,1)*G**2*yRd3x3*reglog(MB**2/MU_R**2))/(4.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_196_95 = Coupling(name = 'UVGC_196_95',
                       value = {-1:'(-3*complex(0,1)*G**2*yLd3x3)/(16.*cmath.pi**2) - (3*complex(0,1)*G**2*yRd3x3)/(16.*cmath.pi**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_197_96 = Coupling(name = 'UVGC_197_96',
                       value = {-1:'-(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**2*reglog(MB**2/MU_R**2))/(24.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_197_97 = Coupling(name = 'UVGC_197_97',
                       value = {-1:'-(complex(0,1)*G**2)/(32.*cmath.pi**2)',0:'(complex(0,1)*G**2*reglog(m8**2/MU_R**2))/(32.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_197_98 = Coupling(name = 'UVGC_197_98',
                       value = {-1:'-(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**2*reglog(MT**2/MU_R**2))/(24.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_198_99 = Coupling(name = 'UVGC_198_99',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-(G**3*reglog(MB**2/MU_R**2))/(24.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_198_100 = Coupling(name = 'UVGC_198_100',
                        value = {-1:'-G**3/(48.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_198_101 = Coupling(name = 'UVGC_198_101',
                        value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_198_102 = Coupling(name = 'UVGC_198_102',
                        value = {-1:'G**3/(64.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_198_103 = Coupling(name = 'UVGC_198_103',
                        value = {-1:'G**3/(32.*cmath.pi**2)',0:'-(G**3*reglog(m8**2/MU_R**2))/(32.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_198_104 = Coupling(name = 'UVGC_198_104',
                        value = {-1:'G**3/(24.*cmath.pi**2)',0:'-(G**3*reglog(MT**2/MU_R**2))/(24.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_199_105 = Coupling(name = 'UVGC_199_105',
                        value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'-(complex(0,1)*G**4*reglog(MB**2/MU_R**2))/(24.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_199_106 = Coupling(name = 'UVGC_199_106',
                        value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_199_107 = Coupling(name = 'UVGC_199_107',
                        value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_199_108 = Coupling(name = 'UVGC_199_108',
                        value = {-1:'(17*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_199_109 = Coupling(name = 'UVGC_199_109',
                        value = {-1:'(complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'-(complex(0,1)*G**4*reglog(m8**2/MU_R**2))/(32.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_199_110 = Coupling(name = 'UVGC_199_110',
                        value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'-(complex(0,1)*G**4*reglog(MT**2/MU_R**2))/(24.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_200_111 = Coupling(name = 'UVGC_200_111',
                        value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_200_112 = Coupling(name = 'UVGC_200_112',
                        value = {-1:'(5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_201_113 = Coupling(name = 'UVGC_201_113',
                        value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**4*reglog(MB**2/MU_R**2))/(24.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_201_114 = Coupling(name = 'UVGC_201_114',
                        value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_201_115 = Coupling(name = 'UVGC_201_115',
                        value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_201_116 = Coupling(name = 'UVGC_201_116',
                        value = {-1:'-(complex(0,1)*G**4)/(32.*cmath.pi**2)',0:'(complex(0,1)*G**4*reglog(m8**2/MU_R**2))/(32.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_201_117 = Coupling(name = 'UVGC_201_117',
                        value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**4*reglog(MT**2/MU_R**2))/(24.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_202_118 = Coupling(name = 'UVGC_202_118',
                        value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_202_119 = Coupling(name = 'UVGC_202_119',
                        value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                        order = {'QCD':4})

UVGC_203_120 = Coupling(name = 'UVGC_203_120',
                        value = {0:'(complex(0,1)*G**2*g8g*reglog(MB**2/MU_R**2))/(6.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':2})

UVGC_203_121 = Coupling(name = 'UVGC_203_121',
                        value = {-1:'(complex(0,1)*G**2*g8g)/(6.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':2})

UVGC_203_122 = Coupling(name = 'UVGC_203_122',
                        value = {-1:'-(complex(0,1)*G**2*g8g)/(16.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':2})

UVGC_203_123 = Coupling(name = 'UVGC_203_123',
                        value = {0:'(complex(0,1)*G**2*g8g*reglog(m8**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':2})

UVGC_203_124 = Coupling(name = 'UVGC_203_124',
                        value = {0:'(complex(0,1)*G**2*g8g*reglog(MT**2/MU_R**2))/(6.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':2})

UVGC_204_125 = Coupling(name = 'UVGC_204_125',
                        value = {0:'-(G**3*g8g*reglog(MB**2/MU_R**2))/(6.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_204_126 = Coupling(name = 'UVGC_204_126',
                        value = {-1:'(11*G**3*g8g)/(16.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_204_127 = Coupling(name = 'UVGC_204_127',
                        value = {0:'-(G**3*g8g*reglog(m8**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_204_128 = Coupling(name = 'UVGC_204_128',
                        value = {0:'-(G**3*g8g*reglog(MT**2/MU_R**2))/(6.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_204_129 = Coupling(name = 'UVGC_204_129',
                        value = {-1:'(51*G**3*g8g)/(32.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_205_130 = Coupling(name = 'UVGC_205_130',
                        value = {-1:'(79*G**3*g8g)/(64.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_206_131 = Coupling(name = 'UVGC_206_131',
                        value = {-1:'(27*G**3*g8g)/(16.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_207_132 = Coupling(name = 'UVGC_207_132',
                        value = {0:'(G**3*g8g*reglog(MB**2/MU_R**2))/(6.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_207_133 = Coupling(name = 'UVGC_207_133',
                        value = {-1:'(-27*G**3*g8g)/(16.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_207_134 = Coupling(name = 'UVGC_207_134',
                        value = {0:'(G**3*g8g*reglog(m8**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_207_135 = Coupling(name = 'UVGC_207_135',
                        value = {0:'(G**3*g8g*reglog(MT**2/MU_R**2))/(6.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_207_136 = Coupling(name = 'UVGC_207_136',
                        value = {-1:'(-51*G**3*g8g)/(32.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_208_137 = Coupling(name = 'UVGC_208_137',
                        value = {-1:'(-73*G**3*g8g)/(64.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_209_138 = Coupling(name = 'UVGC_209_138',
                        value = {-1:'(-11*G**3*g8g)/(16.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':3})

UVGC_210_139 = Coupling(name = 'UVGC_210_139',
                        value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_211_140 = Coupling(name = 'UVGC_211_140',
                        value = {0:'-(complex(0,1)*G**3)/(3.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                        order = {'QCD':3})

UVGC_212_141 = Coupling(name = 'UVGC_212_141',
                        value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2)'},
                        order = {'QCD':2})

UVGC_213_142 = Coupling(name = 'UVGC_213_142',
                        value = {-1:'(G**2*yb)/(12.*cmath.pi**2)',0:'(G**2*yb)/(2.*cmath.pi**2) - (3*G**2*yb*reglog(MB**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_213_143 = Coupling(name = 'UVGC_213_143',
                        value = {-1:'(G**2*yb)/(12.*cmath.pi**2)',0:'(G**2*yb)/(6.*cmath.pi**2) - (G**2*yb*reglog(MT**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_213_144 = Coupling(name = 'UVGC_213_144',
                        value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_214_145 = Coupling(name = 'UVGC_214_145',
                        value = {-1:'-(G**2*yb)/(12.*cmath.pi**2)',0:'-(G**2*yb)/(2.*cmath.pi**2) + (3*G**2*yb*reglog(MB**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_214_146 = Coupling(name = 'UVGC_214_146',
                        value = {-1:'-(G**2*yb)/(12.*cmath.pi**2)',0:'-(G**2*yb)/(6.*cmath.pi**2) + (G**2*yb*reglog(MT**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_214_147 = Coupling(name = 'UVGC_214_147',
                        value = {-1:'-(G**2*yb)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_215_148 = Coupling(name = 'UVGC_215_148',
                        value = {-1:'(-3*complex(0,1)*G**2*yLu3x3)/(16.*cmath.pi**2) - (3*complex(0,1)*G**2*yRu3x3)/(16.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':2})

UVGC_215_149 = Coupling(name = 'UVGC_215_149',
                        value = {-1:'(5*complex(0,1)*G**2*g8g*MT)/(8.*cmath.pi**2) - (complex(0,1)*G**2*yLu3x3)/(8.*cmath.pi**2) - (complex(0,1)*G**2*yRu3x3)/(8.*cmath.pi**2)',0:'-(complex(0,1)*G**2*yLu3x3)/(3.*cmath.pi**2) - (complex(0,1)*G**2*yRu3x3)/(3.*cmath.pi**2) + (complex(0,1)*G**2*yLu3x3*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2) + (complex(0,1)*G**2*yRu3x3*reglog(MT**2/MU_R**2))/(4.*cmath.pi**2)'},
                        order = {'NP':1,'QCD':2})

UVGC_216_150 = Coupling(name = 'UVGC_216_150',
                        value = {-1:'(G**2*yt)/(12.*cmath.pi**2)',0:'(G**2*yt)/(6.*cmath.pi**2) - (G**2*yt*reglog(MB**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_216_151 = Coupling(name = 'UVGC_216_151',
                        value = {-1:'(G**2*yt)/(12.*cmath.pi**2)',0:'(G**2*yt)/(2.*cmath.pi**2) - (3*G**2*yt*reglog(MT**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_216_152 = Coupling(name = 'UVGC_216_152',
                        value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_217_153 = Coupling(name = 'UVGC_217_153',
                        value = {-1:'-(G**2*yt)/(12.*cmath.pi**2)',0:'-(G**2*yt)/(6.*cmath.pi**2) + (G**2*yt*reglog(MB**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_217_154 = Coupling(name = 'UVGC_217_154',
                        value = {-1:'-(G**2*yt)/(12.*cmath.pi**2)',0:'-(G**2*yt)/(2.*cmath.pi**2) + (3*G**2*yt*reglog(MT**2/MU_R**2))/(8.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_217_155 = Coupling(name = 'UVGC_217_155',
                        value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_218_156 = Coupling(name = 'UVGC_218_156',
                        value = {-1:'(G**2*yt)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(G**2*yt*cmath.sqrt(2))/(3.*cmath.pi**2) - (G**2*yt*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_219_157 = Coupling(name = 'UVGC_219_157',
                        value = {-1:'(complex(0,1)*G**2*yt)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*yt*cmath.sqrt(2))/(3.*cmath.pi**2) - (complex(0,1)*G**2*yt*reglog(MT**2/MU_R**2))/(2.*cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

