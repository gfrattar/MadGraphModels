Requestor: Henry Meng
Content: Kaluza-Klein gauge bosons and Radions with Radion-gluon couplings
Paper: https://arxiv.org/abs/1612.00047
Source: Kaustubh Agashe and Peizhi Du (private communication)
