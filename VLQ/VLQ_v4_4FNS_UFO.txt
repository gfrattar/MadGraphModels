Requestor: Avik Roy
Contents: VLQ NLO model version 4 (with correct chirality convention for the Higgs associated coupling)
Webpage: https://feynrules.irmp.ucl.ac.be/wiki/NLOModels
Paper: https://arxiv.org/pdf/1610.04622.pdf
JIRA: https://its.cern.ch/jira/browse/AGENE-1826
