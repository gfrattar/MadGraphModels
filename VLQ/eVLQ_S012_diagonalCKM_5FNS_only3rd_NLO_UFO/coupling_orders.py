# This file was automatically created by FeynRules 2.3.47
# Mathematica version: 13.0.0 for Linux x86 (64-bit) (December 3, 2021)
# Date: Mon 9 May 2022 11:24:32


from object_library import all_orders, CouplingOrder


HBBMOD = CouplingOrder(name = 'HBBMOD',
                       expansion_order = 99,
                       hierarchy = 2)

HDSDS = CouplingOrder(name = 'HDSDS',
                      expansion_order = 99,
                      hierarchy = 2)

HTTMOD = CouplingOrder(name = 'HTTMOD',
                       expansion_order = 99,
                       hierarchy = 2)

HVVMOD = CouplingOrder(name = 'HVVMOD',
                       expansion_order = 99,
                       hierarchy = 2)

QCD = CouplingOrder(name = 'QCD',
                    expansion_order = 99,
                    hierarchy = 1,
                    perturbative_expansion = 1)

QED = CouplingOrder(name = 'QED',
                    expansion_order = 99,
                    hierarchy = 2)

SWZ = CouplingOrder(name = 'SWZ',
                    expansion_order = 2,
                    hierarchy = 3)

SX = CouplingOrder(name = 'SX',
                   expansion_order = 99,
                   hierarchy = 2)

VLQ = CouplingOrder(name = 'VLQ',
                    expansion_order = 99,
                    hierarchy = 2)

