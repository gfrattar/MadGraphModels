# This file was automatically created by FeynRules 2.3.24
# Mathematica version: 10.1.0  for Mac OS X x86 (64-bit) (March 24, 2015)
# Date: Tue 23 Aug 2016 20:14:27


from object_library import all_couplings, Coupling

from function_library import complexconjugate, re, im, csc, sec, acsc, asec, cot



R2GC_597_1 = Coupling(name = 'R2GC_597_1',
                      value = '(complex(0,1)*G**2*GH)/(128.*cmath.pi**2)',
                      order = {'HIG':1,'QCD':2})

R2GC_598_2 = Coupling(name = 'R2GC_598_2',
                      value = '-(complex(0,1)*G**2*GH)/(24.*cmath.pi**2)',
                      order = {'HIG':1,'QCD':2})

R2GC_599_3 = Coupling(name = 'R2GC_599_3',
                      value = '-(complex(0,1)*G**2*GH)/(12.*cmath.pi**2)',
                      order = {'HIG':1,'QCD':2})

R2GC_610_4 = Coupling(name = 'R2GC_610_4',
                      value = '-(complex(0,1)*G**2*MB**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_610_5 = Coupling(name = 'R2GC_610_5',
                      value = '-(complex(0,1)*G**2*MT**2)/(8.*cmath.pi**2)',
                      order = {'QCD':2})

R2GC_611_6 = Coupling(name = 'R2GC_611_6',
                      value = '-(complex(0,1)*G**2*MB*yb)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_611_7 = Coupling(name = 'R2GC_611_7',
                      value = '-(complex(0,1)*G**2*MT*yt)/(8.*cmath.pi**2*cmath.sqrt(2))',
                      order = {'QCD':2,'QED':1})

R2GC_612_8 = Coupling(name = 'R2GC_612_8',
                      value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_612_9 = Coupling(name = 'R2GC_612_9',
                      value = '-(complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                      order = {'QCD':2,'QED':2})

R2GC_613_10 = Coupling(name = 'R2GC_613_10',
                       value = '(cBB*ee**3*G**2*vev**2)/(192.*cmath.pi**2*NPl**2) - (cBB*ee**3*G**2*vev**2)/(192.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*G**2*vev**2)/(384.*cmath.pi**2*NPl**2) + (cWW*ee**3*G**2*vev**2)/(384.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**3*G**2*vev**2)/(384.*cmath.pi**2*NPl**2*sw**2) + (cBB*ee**3*G**2*sw**2*vev**2)/(192.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_613_11 = Coupling(name = 'R2GC_613_11',
                       value = '-(cBB*ee**3*G**2*vev**2)/(192.*cmath.pi**2*NPl**2) + (cBB*ee**3*G**2*vev**2)/(192.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*G**2*vev**2)/(384.*cmath.pi**2*NPl**2) - (cWW*ee**3*G**2*vev**2)/(384.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**3*G**2*vev**2)/(384.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*G**2*sw**2*vev**2)/(192.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_615_12 = Coupling(name = 'R2GC_615_12',
                       value = '-(ee*complex(0,1)*G**3)/(144.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**3*vev**2)/(2304.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**3*vev**2)/(2304.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**3*vev**2)/(4608.*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**3*vev**2)/(1536.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*G**3*vev**2)/(1536.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*G**3*sw**2*vev**2)/(2304.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':3})

R2GC_615_13 = Coupling(name = 'R2GC_615_13',
                       value = '(ee*complex(0,1)*G**3)/(72.*cmath.pi**2) + (5*cBB*ee**3*complex(0,1)*G**3*vev**2)/(2304.*cmath.pi**2*NPl**2) - (5*cBB*ee**3*complex(0,1)*G**3*vev**2)/(2304.*cw**2*cmath.pi**2*NPl**2) - (5*cWW*ee**3*complex(0,1)*G**3*vev**2)/(4608.*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**3*vev**2)/(1536.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*G**3*vev**2)/(1536.*cmath.pi**2*NPl**2*sw**2) + (5*cBB*ee**3*complex(0,1)*G**3*sw**2*vev**2)/(2304.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':3})

R2GC_616_14 = Coupling(name = 'R2GC_616_14',
                       value = '(3*cBB*ee**3*complex(0,1)*G**3*vev**2)/(256.*cmath.pi**2*NPl**2) - (3*cBB*ee**3*complex(0,1)*G**3*vev**2)/(256.*cw**2*cmath.pi**2*NPl**2) - (3*cWW*ee**3*complex(0,1)*G**3*vev**2)/(512.*cmath.pi**2*NPl**2) + (3*cWW*ee**3*complex(0,1)*G**3*vev**2)/(512.*cmath.pi**2*NPl**2*sw**2) - (3*cw**2*cWW*ee**3*complex(0,1)*G**3*vev**2)/(512.*cmath.pi**2*NPl**2*sw**2) + (3*cBB*ee**3*complex(0,1)*G**3*sw**2*vev**2)/(256.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':3})

R2GC_616_15 = Coupling(name = 'R2GC_616_15',
                       value = '(-3*cBB*ee**3*complex(0,1)*G**3*vev**2)/(256.*cmath.pi**2*NPl**2) + (3*cBB*ee**3*complex(0,1)*G**3*vev**2)/(256.*cw**2*cmath.pi**2*NPl**2) + (3*cWW*ee**3*complex(0,1)*G**3*vev**2)/(512.*cmath.pi**2*NPl**2) - (3*cWW*ee**3*complex(0,1)*G**3*vev**2)/(512.*cmath.pi**2*NPl**2*sw**2) + (3*cw**2*cWW*ee**3*complex(0,1)*G**3*vev**2)/(512.*cmath.pi**2*NPl**2*sw**2) - (3*cBB*ee**3*complex(0,1)*G**3*sw**2*vev**2)/(256.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':3})

R2GC_617_16 = Coupling(name = 'R2GC_617_16',
                       value = '(cw*ee*G**2)/(48.*cmath.pi**2*sw) + (ee*G**2*sw)/(48.*cw*cmath.pi**2) + (cB*ee**3*G**2*vev**2)/(192.*cw*cmath.pi**2*NPl**2*sw) + (cWW*ee**3*G**2*vev**2)/(384.*cw*cmath.pi**2*NPl**2*sw) + (cB*ee**3*G**2*sw*vev**2)/(192.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*G**2*sw*vev**2)/(192.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*G**2*sw*vev**2)/(192.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*G**2*sw**3*vev**2)/(192.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_617_17 = Coupling(name = 'R2GC_617_17',
                       value = '-(cw*ee*G**2)/(48.*cmath.pi**2*sw) - (ee*G**2*sw)/(48.*cw*cmath.pi**2) - (cB*ee**3*G**2*vev**2)/(192.*cw*cmath.pi**2*NPl**2*sw) - (cWW*ee**3*G**2*vev**2)/(384.*cw*cmath.pi**2*NPl**2*sw) - (cB*ee**3*G**2*sw*vev**2)/(192.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*G**2*sw*vev**2)/(192.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*G**2*sw*vev**2)/(192.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*G**2*sw**3*vev**2)/(192.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_619_18 = Coupling(name = 'R2GC_619_18',
                       value = '-(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) + (ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**3*vev**2)/(2304.*cw*cmath.pi**2*NPl**2*sw) - (cWW*ee**3*complex(0,1)*G**3*vev**2)/(1536.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**3*vev**2)/(1152.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**3*sw*vev**2)/(2304.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**3*sw*vev**2)/(2304.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**3*sw*vev**2)/(2304.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**3*sw**3*vev**2)/(2304.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':3})

R2GC_619_19 = Coupling(name = 'R2GC_619_19',
                       value = '(cw*ee*complex(0,1)*G**3)/(192.*cmath.pi**2*sw) - (5*ee*complex(0,1)*G**3*sw)/(576.*cw*cmath.pi**2) - (5*cB*ee**3*complex(0,1)*G**3*vev**2)/(2304.*cw*cmath.pi**2*NPl**2*sw) + (cWW*ee**3*complex(0,1)*G**3*vev**2)/(1536.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**3*vev**2)/(576.*cmath.pi**2*NPl**2*sw) - (5*cB*ee**3*complex(0,1)*G**3*sw*vev**2)/(2304.*cw**3*cmath.pi**2*NPl**2) + (5*cBB*ee**3*complex(0,1)*G**3*sw*vev**2)/(2304.*cw**3*cmath.pi**2*NPl**2) - (5*cBB*ee**3*complex(0,1)*G**3*sw*vev**2)/(2304.*cw*cmath.pi**2*NPl**2) - (5*cBB*ee**3*complex(0,1)*G**3*sw**3*vev**2)/(2304.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':3})

R2GC_620_20 = Coupling(name = 'R2GC_620_20',
                       value = '(3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) + (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2) + (3*cB*ee**3*complex(0,1)*G**3*vev**2)/(256.*cw*cmath.pi**2*NPl**2*sw) + (3*cWW*ee**3*complex(0,1)*G**3*vev**2)/(512.*cw*cmath.pi**2*NPl**2*sw) + (3*cB*ee**3*complex(0,1)*G**3*sw*vev**2)/(256.*cw**3*cmath.pi**2*NPl**2) - (3*cBB*ee**3*complex(0,1)*G**3*sw*vev**2)/(256.*cw**3*cmath.pi**2*NPl**2) + (3*cBB*ee**3*complex(0,1)*G**3*sw*vev**2)/(256.*cw*cmath.pi**2*NPl**2) + (3*cBB*ee**3*complex(0,1)*G**3*sw**3*vev**2)/(256.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':3})

R2GC_620_21 = Coupling(name = 'R2GC_620_21',
                       value = '(-3*cw*ee*complex(0,1)*G**3)/(64.*cmath.pi**2*sw) - (3*ee*complex(0,1)*G**3*sw)/(64.*cw*cmath.pi**2) - (3*cB*ee**3*complex(0,1)*G**3*vev**2)/(256.*cw*cmath.pi**2*NPl**2*sw) - (3*cWW*ee**3*complex(0,1)*G**3*vev**2)/(512.*cw*cmath.pi**2*NPl**2*sw) - (3*cB*ee**3*complex(0,1)*G**3*sw*vev**2)/(256.*cw**3*cmath.pi**2*NPl**2) + (3*cBB*ee**3*complex(0,1)*G**3*sw*vev**2)/(256.*cw**3*cmath.pi**2*NPl**2) - (3*cBB*ee**3*complex(0,1)*G**3*sw*vev**2)/(256.*cw*cmath.pi**2*NPl**2) - (3*cBB*ee**3*complex(0,1)*G**3*sw**3*vev**2)/(256.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':3})

R2GC_621_22 = Coupling(name = 'R2GC_621_22',
                       value = '(ee**2*complex(0,1)*G**2)/(216.*cmath.pi**2) + (cBB*ee**4*complex(0,1)*G**2*vev**2)/(1728.*cmath.pi**2*NPl**2) - (cBB*ee**4*complex(0,1)*G**2*vev**2)/(1728.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**4*complex(0,1)*G**2*vev**2)/(3456.*cmath.pi**2*NPl**2) - (cWW*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cmath.pi**2*NPl**2*sw**2) + (cBB*ee**4*complex(0,1)*G**2*sw**2*vev**2)/(1728.*cw**2*cmath.pi**2*NPl**2) + (5*cBB**2*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw**4*cmath.pi**2*NPl**4) - (5*cBB**2*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) - (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(3456.*cmath.pi**2*NPl**4) + (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(3456.*cw**2*cmath.pi**2*NPl**4) + (5*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(110592.*cmath.pi**2*NPl**4) + (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(12288.*cmath.pi**2*NPl**4*sw**4) - (cw**2*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(6144.*cmath.pi**2*NPl**4*sw**4) + (cw**4*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(12288.*cmath.pi**2*NPl**4*sw**4) + (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(4608.*cmath.pi**2*NPl**4*sw**2) - (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**2*cmath.pi**2*NPl**4*sw**2) - (cBB*cw**2*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cmath.pi**2*NPl**4*sw**2) - (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cmath.pi**2*NPl**4*sw**2) + (cw**2*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cmath.pi**2*NPl**4*sw**2) - (5*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) - (5*cBB*cWW*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(27648.*cw**2*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*sw**4*vev**4)/(27648.*cw**4*cmath.pi**2*NPl**4)',
                       order = {'NP':2,'QCD':2})

R2GC_621_23 = Coupling(name = 'R2GC_621_23',
                       value = '(ee**2*complex(0,1)*G**2)/(54.*cmath.pi**2) + (5*cBB*ee**4*complex(0,1)*G**2*vev**2)/(864.*cmath.pi**2*NPl**2) - (5*cBB*ee**4*complex(0,1)*G**2*vev**2)/(864.*cw**2*cmath.pi**2*NPl**2) - (5*cWW*ee**4*complex(0,1)*G**2*vev**2)/(1728.*cmath.pi**2*NPl**2) - (cWW*ee**4*complex(0,1)*G**2*vev**2)/(576.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**4*complex(0,1)*G**2*vev**2)/(576.*cmath.pi**2*NPl**2*sw**2) + (5*cBB*ee**4*complex(0,1)*G**2*sw**2*vev**2)/(864.*cw**2*cmath.pi**2*NPl**2) + (17*cBB**2*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw**4*cmath.pi**2*NPl**4) - (17*cBB**2*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) - (7*cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cmath.pi**2*NPl**4) + (7*cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) + (17*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(110592.*cmath.pi**2*NPl**4) + (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(12288.*cmath.pi**2*NPl**4*sw**4) - (cw**2*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(6144.*cmath.pi**2*NPl**4*sw**4) + (cw**4*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(12288.*cmath.pi**2*NPl**4*sw**4) - (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(4608.*cmath.pi**2*NPl**4*sw**2) + (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**2*cmath.pi**2*NPl**4*sw**2) + (cBB*cw**2*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cmath.pi**2*NPl**4*sw**2) + (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cmath.pi**2*NPl**4*sw**2) - (cw**2*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cmath.pi**2*NPl**4*sw**2) - (17*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) - (17*cBB*cWW*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(27648.*cw**2*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*sw**4*vev**4)/(27648.*cw**4*cmath.pi**2*NPl**4)',
                       order = {'NP':2,'QCD':2})

R2GC_622_24 = Coupling(name = 'R2GC_622_24',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2*sw) - (ee**2*complex(0,1)*G**2*sw)/(864.*cw*cmath.pi**2) - (cw*cWW*ee**4*complex(0,1)*G**2*vev**2)/(1536.*cmath.pi**2*NPl**2*sw**3) + (cw**3*cWW*ee**4*complex(0,1)*G**2*vev**2)/(1536.*cmath.pi**2*NPl**2*sw**3) - (cB*ee**4*complex(0,1)*G**2*vev**2)/(3456.*cw*cmath.pi**2*NPl**2*sw) + (cBB*ee**4*complex(0,1)*G**2*vev**2)/(2304.*cw*cmath.pi**2*NPl**2*sw) - (cBB*cw*ee**4*complex(0,1)*G**2*vev**2)/(2304.*cmath.pi**2*NPl**2*sw) + (cWW*ee**4*complex(0,1)*G**2*vev**2)/(4608.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**4*complex(0,1)*G**2*vev**2)/(6912.*cmath.pi**2*NPl**2*sw) - (cB*ee**4*complex(0,1)*G**2*sw*vev**2)/(3456.*cw**3*cmath.pi**2*NPl**2) + (7*cBB*ee**4*complex(0,1)*G**2*sw*vev**2)/(6912.*cw**3*cmath.pi**2*NPl**2) - (5*cBB*ee**4*complex(0,1)*G**2*sw*vev**2)/(3456.*cw*cmath.pi**2*NPl**2) + (5*cWW*ee**4*complex(0,1)*G**2*sw*vev**2)/(13824.*cw*cmath.pi**2*NPl**2) - (7*cBB*ee**4*complex(0,1)*G**2*sw**3*vev**2)/(6912.*cw**3*cmath.pi**2*NPl**2) - (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cw*cmath.pi**2*NPl**4*sw**3) + (cB*cw*cWW*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cmath.pi**2*NPl**4*sw**3) - (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(12288.*cw*cmath.pi**2*NPl**4*sw**3) + (5*cw*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(36864.*cmath.pi**2*NPl**4*sw**3) - (cw**3*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cmath.pi**2*NPl**4*sw**3) + (5*cB*cBB*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw**3*cmath.pi**2*NPl**4*sw) - (5*cB*cBB*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw*cmath.pi**2*NPl**4*sw) - (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cw**3*cmath.pi**2*NPl**4*sw) + (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**3*cmath.pi**2*NPl**4*sw) + (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(6912.*cw*cmath.pi**2*NPl**4*sw) - (7*cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(55296.*cw*cmath.pi**2*NPl**4*sw) + (cBB*cw*cWW*ee**6*complex(0,1)*G**2*vev**4)/(55296.*cmath.pi**2*NPl**4*sw) + (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(36864.*cw*cmath.pi**2*NPl**4*sw) + (cw*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(55296.*cmath.pi**2*NPl**4*sw) + (5*cB*cBB*ee**6*complex(0,1)*G**2*sw*vev**4)/(27648.*cw**5*cmath.pi**2*NPl**4) - (5*cBB**2*ee**6*complex(0,1)*G**2*sw*vev**4)/(27648.*cw**5*cmath.pi**2*NPl**4) - (5*cB*cBB*ee**6*complex(0,1)*G**2*sw*vev**4)/(13824.*cw**3*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*sw*vev**4)/(13824.*cw**3*cmath.pi**2*NPl**4) - (5*cBB**2*ee**6*complex(0,1)*G**2*sw*vev**4)/(27648.*cw*cmath.pi**2*NPl**4) + (5*cB*cWW*ee**6*complex(0,1)*G**2*sw*vev**4)/(55296.*cw**3*cmath.pi**2*NPl**4) - (11*cBB*cWW*ee**6*complex(0,1)*G**2*sw*vev**4)/(55296.*cw**3*cmath.pi**2*NPl**4) + (cBB*cWW*ee**6*complex(0,1)*G**2*sw*vev**4)/(9216.*cw*cmath.pi**2*NPl**4) - (5*cB*cBB*ee**6*complex(0,1)*G**2*sw**3*vev**4)/(27648.*cw**5*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*sw**3*vev**4)/(13824.*cw**5*cmath.pi**2*NPl**4) - (5*cBB**2*ee**6*complex(0,1)*G**2*sw**3*vev**4)/(13824.*cw**3*cmath.pi**2*NPl**4) + (5*cBB*cWW*ee**6*complex(0,1)*G**2*sw**3*vev**4)/(55296.*cw**3*cmath.pi**2*NPl**4) - (5*cBB**2*ee**6*complex(0,1)*G**2*sw**5*vev**4)/(27648.*cw**5*cmath.pi**2*NPl**4)',
                       order = {'NP':2,'QCD':2})

R2GC_622_25 = Coupling(name = 'R2GC_622_25',
                       value = '(cw*ee**2*complex(0,1)*G**2)/(144.*cmath.pi**2*sw) - (5*ee**2*complex(0,1)*G**2*sw)/(432.*cw*cmath.pi**2) - (cw*cWW*ee**4*complex(0,1)*G**2*vev**2)/(1536.*cmath.pi**2*NPl**2*sw**3) + (cw**3*cWW*ee**4*complex(0,1)*G**2*vev**2)/(1536.*cmath.pi**2*NPl**2*sw**3) - (5*cB*ee**4*complex(0,1)*G**2*vev**2)/(1728.*cw*cmath.pi**2*NPl**2*sw) - (cBB*ee**4*complex(0,1)*G**2*vev**2)/(2304.*cw*cmath.pi**2*NPl**2*sw) + (cBB*cw*ee**4*complex(0,1)*G**2*vev**2)/(2304.*cmath.pi**2*NPl**2*sw) + (5*cWW*ee**4*complex(0,1)*G**2*vev**2)/(4608.*cw*cmath.pi**2*NPl**2*sw) - (19*cw*cWW*ee**4*complex(0,1)*G**2*vev**2)/(6912.*cmath.pi**2*NPl**2*sw) - (5*cB*ee**4*complex(0,1)*G**2*sw*vev**2)/(1728.*cw**3*cmath.pi**2*NPl**2) + (37*cBB*ee**4*complex(0,1)*G**2*sw*vev**2)/(6912.*cw**3*cmath.pi**2*NPl**2) - (17*cBB*ee**4*complex(0,1)*G**2*sw*vev**2)/(3456.*cw*cmath.pi**2*NPl**2) + (17*cWW*ee**4*complex(0,1)*G**2*sw*vev**2)/(13824.*cw*cmath.pi**2*NPl**2) - (37*cBB*ee**4*complex(0,1)*G**2*sw**3*vev**2)/(6912.*cw**3*cmath.pi**2*NPl**2) + (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cw*cmath.pi**2*NPl**4*sw**3) - (cB*cw*cWW*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cmath.pi**2*NPl**4*sw**3) - (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(12288.*cw*cmath.pi**2*NPl**4*sw**3) + (7*cw*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(36864.*cmath.pi**2*NPl**4*sw**3) - (cw**3*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cmath.pi**2*NPl**4*sw**3) + (17*cB*cBB*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw**3*cmath.pi**2*NPl**4*sw) - (17*cB*cBB*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw*cmath.pi**2*NPl**4*sw) + (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(18432.*cw**3*cmath.pi**2*NPl**4*sw) - (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**3*cmath.pi**2*NPl**4*sw) + (7*cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw*cmath.pi**2*NPl**4*sw) + (29*cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(55296.*cw*cmath.pi**2*NPl**4*sw) - (23*cBB*cw*cWW*ee**6*complex(0,1)*G**2*vev**4)/(55296.*cmath.pi**2*NPl**4*sw) - (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(36864.*cw*cmath.pi**2*NPl**4*sw) + (5*cw*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cmath.pi**2*NPl**4*sw) + (17*cB*cBB*ee**6*complex(0,1)*G**2*sw*vev**4)/(27648.*cw**5*cmath.pi**2*NPl**4) - (17*cBB**2*ee**6*complex(0,1)*G**2*sw*vev**4)/(27648.*cw**5*cmath.pi**2*NPl**4) - (17*cB*cBB*ee**6*complex(0,1)*G**2*sw*vev**4)/(13824.*cw**3*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*sw*vev**4)/(13824.*cw**3*cmath.pi**2*NPl**4) - (17*cBB**2*ee**6*complex(0,1)*G**2*sw*vev**4)/(27648.*cw*cmath.pi**2*NPl**4) + (17*cB*cWW*ee**6*complex(0,1)*G**2*sw*vev**4)/(55296.*cw**3*cmath.pi**2*NPl**4) - (11*cBB*cWW*ee**6*complex(0,1)*G**2*sw*vev**4)/(55296.*cw**3*cmath.pi**2*NPl**4) - (cBB*cWW*ee**6*complex(0,1)*G**2*sw*vev**4)/(9216.*cw*cmath.pi**2*NPl**4) - (17*cB*cBB*ee**6*complex(0,1)*G**2*sw**3*vev**4)/(27648.*cw**5*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*sw**3*vev**4)/(13824.*cw**5*cmath.pi**2*NPl**4) - (17*cBB**2*ee**6*complex(0,1)*G**2*sw**3*vev**4)/(13824.*cw**3*cmath.pi**2*NPl**4) + (17*cBB*cWW*ee**6*complex(0,1)*G**2*sw**3*vev**4)/(55296.*cw**3*cmath.pi**2*NPl**4) - (17*cBB**2*ee**6*complex(0,1)*G**2*sw**5*vev**4)/(27648.*cw**5*cmath.pi**2*NPl**4)',
                       order = {'NP':2,'QCD':2})

R2GC_623_26 = Coupling(name = 'R2GC_623_26',
                       value = '(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (5*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2) + (cBB*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cmath.pi**2*NPl**2) + (cB*ee**4*complex(0,1)*G**2*vev**2)/(432.*cw**2*cmath.pi**2*NPl**2) - (cBB*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**4*complex(0,1)*G**2*vev**2)/(3456.*cmath.pi**2*NPl**2) + (cWW*ee**4*complex(0,1)*G**2*vev**2)/(2304.*cw**2*cmath.pi**2*NPl**2) + (cB*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cmath.pi**2*NPl**2*sw**2) + (cWW*ee**4*complex(0,1)*G**2*vev**2)/(768.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cmath.pi**2*NPl**2*sw**2) + (5*cB*ee**4*complex(0,1)*G**2*sw**2*vev**2)/(3456.*cw**4*cmath.pi**2*NPl**2) - (5*cBB*ee**4*complex(0,1)*G**2*sw**2*vev**2)/(3456.*cw**4*cmath.pi**2*NPl**2) + (cBB*ee**4*complex(0,1)*G**2*sw**2*vev**2)/(432.*cw**2*cmath.pi**2*NPl**2) + (5*cBB*ee**4*complex(0,1)*G**2*sw**4*vev**2)/(3456.*cw**4*cmath.pi**2*NPl**2) + (5*cB**2*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) - (5*cB*cBB*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) + (5*cB*cBB*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) + (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cmath.pi**2*NPl**4) + (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**4*cmath.pi**2*NPl**4) - (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**4*cmath.pi**2*NPl**4) + (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) + (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw**2*cmath.pi**2*NPl**4) + (5*cB**2*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw**2*cmath.pi**2*NPl**4*sw**2) + (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cmath.pi**2*NPl**4*sw**2) + (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**2*cmath.pi**2*NPl**4*sw**2) - (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cmath.pi**2*NPl**4*sw**2) + (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(12288.*cw**2*cmath.pi**2*NPl**4*sw**2) + (cw**2*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cmath.pi**2*NPl**4*sw**2) + (5*cB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(27648.*cw**6*cmath.pi**2*NPl**4) - (5*cB*cBB*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**6*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(27648.*cw**6*cmath.pi**2*NPl**4) + (5*cB*cBB*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(6912.*cw**4*cmath.pi**2*NPl**4) - (5*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(27648.*cw**2*cmath.pi**2*NPl**4) + (cBB*cWW*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(9216.*cw**4*cmath.pi**2*NPl**4) + (cBB*cWW*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) + (5*cB*cBB*ee**6*complex(0,1)*G**2*sw**4*vev**4)/(13824.*cw**6*cmath.pi**2*NPl**4) - (5*cBB**2*ee**6*complex(0,1)*G**2*sw**4*vev**4)/(13824.*cw**6*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*sw**4*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) + (5*cBB**2*ee**6*complex(0,1)*G**2*sw**6*vev**4)/(27648.*cw**6*cmath.pi**2*NPl**4)',
                       order = {'NP':2,'QCD':2})

R2GC_623_27 = Coupling(name = 'R2GC_623_27',
                       value = '-(ee**2*complex(0,1)*G**2)/(288.*cmath.pi**2) + (cw**2*ee**2*complex(0,1)*G**2)/(192.*cmath.pi**2*sw**2) + (17*ee**2*complex(0,1)*G**2*sw**2)/(1728.*cw**2*cmath.pi**2) - (cBB*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cmath.pi**2*NPl**2) + (7*cB*ee**4*complex(0,1)*G**2*vev**2)/(1728.*cw**2*cmath.pi**2*NPl**2) + (cBB*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cw**2*cmath.pi**2*NPl**2) + (5*cWW*ee**4*complex(0,1)*G**2*vev**2)/(1728.*cmath.pi**2*NPl**2) - (cWW*ee**4*complex(0,1)*G**2*vev**2)/(2304.*cw**2*cmath.pi**2*NPl**2) - (cB*ee**4*complex(0,1)*G**2*vev**2)/(1152.*cmath.pi**2*NPl**2*sw**2) + (cWW*ee**4*complex(0,1)*G**2*vev**2)/(768.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**4*complex(0,1)*G**2*vev**2)/(576.*cmath.pi**2*NPl**2*sw**2) + (17*cB*ee**4*complex(0,1)*G**2*sw**2*vev**2)/(3456.*cw**4*cmath.pi**2*NPl**2) - (17*cBB*ee**4*complex(0,1)*G**2*sw**2*vev**2)/(3456.*cw**4*cmath.pi**2*NPl**2) + (7*cBB*ee**4*complex(0,1)*G**2*sw**2*vev**2)/(1728.*cw**2*cmath.pi**2*NPl**2) + (17*cBB*ee**4*complex(0,1)*G**2*sw**4*vev**2)/(3456.*cw**4*cmath.pi**2*NPl**2) + (17*cB**2*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) - (17*cB*cBB*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) + (17*cB*cBB*ee**6*complex(0,1)*G**2*vev**4)/(13824.*cw**2*cmath.pi**2*NPl**4) + (5*cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(6912.*cmath.pi**2*NPl**4) - (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**4*cmath.pi**2*NPl**4) + (cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**4*cmath.pi**2*NPl**4) + (5*cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(6912.*cw**2*cmath.pi**2*NPl**4) - (23*cBB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw**2*cmath.pi**2*NPl**4) + (17*cB**2*ee**6*complex(0,1)*G**2*vev**4)/(27648.*cw**2*cmath.pi**2*NPl**4*sw**2) + (5*cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(6912.*cmath.pi**2*NPl**4*sw**2) - (cB*cWW*ee**6*complex(0,1)*G**2*vev**4)/(9216.*cw**2*cmath.pi**2*NPl**4*sw**2) - (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(4608.*cmath.pi**2*NPl**4*sw**2) + (cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(12288.*cw**2*cmath.pi**2*NPl**4*sw**2) + (cw**2*cWW**2*ee**6*complex(0,1)*G**2*vev**4)/(3456.*cmath.pi**2*NPl**4*sw**2) + (17*cB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(27648.*cw**6*cmath.pi**2*NPl**4) - (17*cB*cBB*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**6*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(27648.*cw**6*cmath.pi**2*NPl**4) + (17*cB*cBB*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(6912.*cw**4*cmath.pi**2*NPl**4) - (17*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(27648.*cw**2*cmath.pi**2*NPl**4) - (cBB*cWW*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(9216.*cw**4*cmath.pi**2*NPl**4) + (5*cBB*cWW*ee**6*complex(0,1)*G**2*sw**2*vev**4)/(6912.*cw**2*cmath.pi**2*NPl**4) + (17*cB*cBB*ee**6*complex(0,1)*G**2*sw**4*vev**4)/(13824.*cw**6*cmath.pi**2*NPl**4) - (17*cBB**2*ee**6*complex(0,1)*G**2*sw**4*vev**4)/(13824.*cw**6*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*sw**4*vev**4)/(13824.*cw**4*cmath.pi**2*NPl**4) + (17*cBB**2*ee**6*complex(0,1)*G**2*sw**6*vev**4)/(27648.*cw**6*cmath.pi**2*NPl**4)',
                       order = {'NP':2,'QCD':2})

R2GC_624_28 = Coupling(name = 'R2GC_624_28',
                       value = '(-7*complex(0,1)*G**2*GH*MB)/(24.*cmath.pi**2) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'HIG':1,'QCD':2})

R2GC_625_29 = Coupling(name = 'R2GC_625_29',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_626_30 = Coupling(name = 'R2GC_626_30',
                       value = '-(ee*complex(0,1)*G**2)/(9.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(36.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(36.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(72.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(36.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_627_31 = Coupling(name = 'R2GC_627_31',
                       value = '-(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw*cmath.pi**2*NPl**2*sw) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(72.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_628_32 = Coupling(name = 'R2GC_628_32',
                       value = '(ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(36.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(72.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(36.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(36.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(36.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(36.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_629_33 = Coupling(name = 'R2GC_629_33',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_630_34 = Coupling(name = 'R2GC_630_34',
                       value = '(ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(72.*cw**2*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_631_35 = Coupling(name = 'R2GC_631_35',
                       value = '(cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw*cmath.pi**2*NPl**2*sw) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_632_36 = Coupling(name = 'R2GC_632_36',
                       value = '-(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2)',
                       order = {'NP':1,'QCD':2})

R2GC_637_37 = Coupling(name = 'R2GC_637_37',
                       value = '-(complex(0,1)*G**2*yb**2)/(16.*cmath.pi**2) - (complex(0,1)*G**2*yt**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2,'QED':2})

R2GC_638_38 = Coupling(name = 'R2GC_638_38',
                       value = '(-7*complex(0,1)*G**2*GH*MT)/(24.*cmath.pi**2) + (complex(0,1)*G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'HIG':1,'QCD':2})

R2GC_639_39 = Coupling(name = 'R2GC_639_39',
                       value = '(ee**2*complex(0,1)*G**2)/(96.*cmath.pi**2*sw**2)',
                       order = {'QCD':2,'QED':2})

R2GC_648_40 = Coupling(name = 'R2GC_648_40',
                       value = '(complex(0,1)*G**2*GH)/(32.*cmath.pi**2)',
                       order = {'HIG':1,'QCD':2})

R2GC_649_41 = Coupling(name = 'R2GC_649_41',
                       value = '(-13*complex(0,1)*G**2*GH)/(128.*cmath.pi**2)',
                       order = {'HIG':1,'QCD':2})

R2GC_651_42 = Coupling(name = 'R2GC_651_42',
                       value = '(19*complex(0,1)*G**2*GH)/(128.*cmath.pi**2)',
                       order = {'HIG':1,'QCD':2})

R2GC_652_43 = Coupling(name = 'R2GC_652_43',
                       value = '(23*complex(0,1)*G**2*GH)/(128.*cmath.pi**2)',
                       order = {'HIG':1,'QCD':2})

R2GC_653_44 = Coupling(name = 'R2GC_653_44',
                       value = '-G**4/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_653_45 = Coupling(name = 'R2GC_653_45',
                       value = 'G**4/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_654_46 = Coupling(name = 'R2GC_654_46',
                       value = '-(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_654_47 = Coupling(name = 'R2GC_654_47',
                       value = '(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_655_48 = Coupling(name = 'R2GC_655_48',
                       value = '(complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_655_49 = Coupling(name = 'R2GC_655_49',
                       value = '-(complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_656_50 = Coupling(name = 'R2GC_656_50',
                       value = '-(complex(0,1)*G**4)/(48.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_657_51 = Coupling(name = 'R2GC_657_51',
                       value = '(complex(0,1)*G**4)/(288.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_657_52 = Coupling(name = 'R2GC_657_52',
                       value = '-(complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_658_53 = Coupling(name = 'R2GC_658_53',
                       value = '-(complex(0,1)*G**3)/(6.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_659_54 = Coupling(name = 'R2GC_659_54',
                       value = '(complex(0,1)*G**2)/(12.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_672_55 = Coupling(name = 'R2GC_672_55',
                       value = '-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_676_56 = Coupling(name = 'R2GC_676_56',
                       value = '(complex(0,1)*G**2*MB)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_682_57 = Coupling(name = 'R2GC_682_57',
                       value = '-(G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

R2GC_683_58 = Coupling(name = 'R2GC_683_58',
                       value = '(complex(0,1)*G**2)/(48.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_683_59 = Coupling(name = 'R2GC_683_59',
                       value = '(3*complex(0,1)*G**2)/(32.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_684_60 = Coupling(name = 'R2GC_684_60',
                       value = '-(complex(0,1)*G**2)/(16.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_685_61 = Coupling(name = 'R2GC_685_61',
                       value = 'G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_685_62 = Coupling(name = 'R2GC_685_62',
                       value = '(11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_686_63 = Coupling(name = 'R2GC_686_63',
                       value = '-G**3/(24.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_686_64 = Coupling(name = 'R2GC_686_64',
                       value = '(-11*G**3)/(64.*cmath.pi**2)',
                       order = {'QCD':3})

R2GC_687_65 = Coupling(name = 'R2GC_687_65',
                       value = '(11*complex(0,1)*G**4)/(192.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_687_66 = Coupling(name = 'R2GC_687_66',
                       value = '(15*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_688_67 = Coupling(name = 'R2GC_688_67',
                       value = '(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_688_68 = Coupling(name = 'R2GC_688_68',
                       value = '(7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_689_69 = Coupling(name = 'R2GC_689_69',
                       value = '(-3*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_689_70 = Coupling(name = 'R2GC_689_70',
                       value = '(-17*complex(0,1)*G**4)/(64.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_690_71 = Coupling(name = 'R2GC_690_71',
                       value = '-(complex(0,1)*G**4)/(16.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_690_72 = Coupling(name = 'R2GC_690_72',
                       value = '(-7*complex(0,1)*G**4)/(32.*cmath.pi**2)',
                       order = {'QCD':4})

R2GC_691_73 = Coupling(name = 'R2GC_691_73',
                       value = '(31*complex(0,1)*G**2*GH)/(64.*cmath.pi**2)',
                       order = {'HIG':1,'QCD':2})

R2GC_692_74 = Coupling(name = 'R2GC_692_74',
                       value = '(-5*complex(0,1)*G**2*GH)/(16.*cmath.pi**2)',
                       order = {'HIG':1,'QCD':2})

R2GC_693_75 = Coupling(name = 'R2GC_693_75',
                       value = '(45*G**3*GH)/(128.*cmath.pi**2)',
                       order = {'HIG':1,'QCD':3})

R2GC_694_76 = Coupling(name = 'R2GC_694_76',
                       value = '(-45*G**3*GH)/(128.*cmath.pi**2)',
                       order = {'HIG':1,'QCD':3})

R2GC_697_77 = Coupling(name = 'R2GC_697_77',
                       value = '(complex(0,1)*G**2*MT)/(6.*cmath.pi**2)',
                       order = {'QCD':2})

R2GC_703_78 = Coupling(name = 'R2GC_703_78',
                       value = '(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_704_79 = Coupling(name = 'R2GC_704_79',
                       value = '-(G**2*yb)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_705_80 = Coupling(name = 'R2GC_705_80',
                       value = '(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_706_81 = Coupling(name = 'R2GC_706_81',
                       value = '-(G**2*yt)/(3.*cmath.pi**2)',
                       order = {'QCD':2,'QED':1})

R2GC_707_82 = Coupling(name = 'R2GC_707_82',
                       value = '(G**2*yt)/(3.*cmath.pi**2*cmath.sqrt(2))',
                       order = {'QCD':2,'QED':1})

UVGC_644_1 = Coupling(name = 'UVGC_644_1',
                      value = {-1:'-(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                      order = {'QCD':2})

UVGC_648_2 = Coupling(name = 'UVGC_648_2',
                      value = {-1:'(3*complex(0,1)*G**2*GH)/(64.*cmath.pi**2)'},
                      order = {'HIG':1,'QCD':2})

UVGC_649_3 = Coupling(name = 'UVGC_649_3',
                      value = {-1:'(-3*complex(0,1)*G**2*GH)/(32.*cmath.pi**2)'},
                      order = {'HIG':1,'QCD':2})

UVGC_650_4 = Coupling(name = 'UVGC_650_4',
                      value = {-1:'(-9*complex(0,1)*G**2*GH)/(64.*cmath.pi**2)'},
                      order = {'HIG':1,'QCD':2})

UVGC_651_5 = Coupling(name = 'UVGC_651_5',
                      value = {-1:'(9*complex(0,1)*G**2*GH)/(64.*cmath.pi**2)'},
                      order = {'HIG':1,'QCD':2})

UVGC_652_6 = Coupling(name = 'UVGC_652_6',
                      value = {-1:'(3*complex(0,1)*G**2*GH)/(16.*cmath.pi**2)'},
                      order = {'HIG':1,'QCD':2})

UVGC_653_7 = Coupling(name = 'UVGC_653_7',
                      value = {-1:'(3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_653_8 = Coupling(name = 'UVGC_653_8',
                      value = {-1:'(-3*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_654_9 = Coupling(name = 'UVGC_654_9',
                      value = {-1:'(3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                      order = {'QCD':4})

UVGC_654_10 = Coupling(name = 'UVGC_654_10',
                       value = {-1:'(-3*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_656_11 = Coupling(name = 'UVGC_656_11',
                       value = {-1:'-(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_656_12 = Coupling(name = 'UVGC_656_12',
                       value = {-1:'(complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_657_13 = Coupling(name = 'UVGC_657_13',
                       value = {-1:'(-3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_657_14 = Coupling(name = 'UVGC_657_14',
                       value = {-1:'(3*complex(0,1)*G**4)/(256.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_658_15 = Coupling(name = 'UVGC_658_15',
                       value = {-1:'(-13*complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_659_16 = Coupling(name = 'UVGC_659_16',
                       value = {-1:'(complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_660_17 = Coupling(name = 'UVGC_660_17',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**3)/(48.*cmath.pi**2) )'},
                       order = {'QCD':3})

UVGC_660_18 = Coupling(name = 'UVGC_660_18',
                       value = {-1:'(complex(0,1)*G**3)/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_660_19 = Coupling(name = 'UVGC_660_19',
                       value = {-1:'(-19*complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_660_20 = Coupling(name = 'UVGC_660_20',
                       value = {-1:'-(complex(0,1)*G**3)/(128.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_660_21 = Coupling(name = 'UVGC_660_21',
                       value = {-1:'(complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_672_22 = Coupling(name = 'UVGC_672_22',
                       value = {-1:'(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_672_23 = Coupling(name = 'UVGC_672_23',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_674_24 = Coupling(name = 'UVGC_674_24',
                       value = {-1:'( (complex(0,1)*G**2)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2)/(12.*cmath.pi**2)',0:'( (5*complex(0,1)*G**2)/(12.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else (complex(0,1)*G**2)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_675_25 = Coupling(name = 'UVGC_675_25',
                       value = {-1:'( -(complex(0,1)*G**3)/(6.*cmath.pi**2) if MB else (complex(0,1)*G**3)/(12.*cmath.pi**2) )',0:'( (-5*complex(0,1)*G**3)/(12.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MB/MU_R))/(2.*cmath.pi**2) if MB else -(complex(0,1)*G**3)/(12.*cmath.pi**2) ) + (complex(0,1)*G**3)/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_676_26 = Coupling(name = 'UVGC_676_26',
                       value = {-1:'( (complex(0,1)*G**2*MB)/(6.*cmath.pi**2) if MB else -(complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) + (complex(0,1)*G**2*MB)/(3.*cmath.pi**2)',0:'( (3*complex(0,1)*G**2*MB)/(4.*cmath.pi**2) - (complex(0,1)*G**2*MB*reglog(MB/MU_R))/cmath.pi**2 if MB else (complex(0,1)*G**2*MB)/(12.*cmath.pi**2) ) - (complex(0,1)*G**2*MB)/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_677_27 = Coupling(name = 'UVGC_677_27',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(576.*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2) ) + (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(576.*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2)',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (5*cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) + (5*cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2) + (5*cWW*ee**3*complex(0,1)*G**2*vev**2)/(576.*cmath.pi**2*NPl**2) - (5*cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) + (5*cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) - (5*cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(48.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(48.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(96.*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(32.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(32.*cmath.pi**2*NPl**2*sw**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2*reglog(MB/MU_R))/(48.*cw**2*cmath.pi**2*NPl**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(576.*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(576.*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2*sw**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(288.*cw**2*cmath.pi**2*NPl**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_678_28 = Coupling(name = 'UVGC_678_28',
                       value = {-1:'( (ee*complex(0,1)*G**2)/(18.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(72.*cw**2*cmath.pi**2*NPl**2) if MB else -(ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) ) + (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2)',0:'( (5*ee*complex(0,1)*G**2)/(36.*cmath.pi**2) + (5*cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) - (5*cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) - (5*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) + (5*cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) - (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(6.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(24.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(24.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(48.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2*reglog(MB/MU_R))/(24.*cw**2*cmath.pi**2*NPl**2) if MB else (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) ) - (ee*complex(0,1)*G**2)/(36.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(144.*cw**2*cmath.pi**2*NPl**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_679_29 = Coupling(name = 'UVGC_679_29',
                       value = {-1:'( (cw*ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw*cmath.pi**2*NPl**2*sw) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(96.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) if MB else -(cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw*cmath.pi**2*NPl**2*sw) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) ) + (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw*cmath.pi**2*NPl**2*sw) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2)',0:'( (5*cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (5*ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (5*cB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw*cmath.pi**2*NPl**2*sw) + (5*cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cw*cmath.pi**2*NPl**2*sw) - (5*cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) + (5*cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) - (5*cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) + (5*cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw*cmath.pi**2*NPl**2) + (5*cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) - (cw*ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(12.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(48.*cw*cmath.pi**2*NPl**2*sw) - (cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(32.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(48.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MB/MU_R))/(48.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MB/MU_R))/(48.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MB/MU_R))/(48.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2*reglog(MB/MU_R))/(48.*cw**3*cmath.pi**2*NPl**2) if MB else (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw*cmath.pi**2*NPl**2*sw) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) ) - (cw*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw)/(72.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2)/(288.*cw*cmath.pi**2*NPl**2*sw) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(288.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(288.*cw**3*cmath.pi**2*NPl**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_680_30 = Coupling(name = 'UVGC_680_30',
                       value = {-1:'( -(ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2) if MB else (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) ) - (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2)',0:'( (-5*ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (5*cB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw*cmath.pi**2*NPl**2*sw) - (5*cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) - (5*cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (5*cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) - (5*cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw*cmath.pi**2*NPl**2) - (5*cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (ee*complex(0,1)*G**2*sw*reglog(MB/MU_R))/(6.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(24.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MB/MU_R))/(48.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MB/MU_R))/(24.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MB/MU_R))/(24.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MB/MU_R))/(24.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2*reglog(MB/MU_R))/(24.*cw**3*cmath.pi**2*NPl**2) if MB else -(ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) ) + (ee*complex(0,1)*G**2*sw)/(36.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(144.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(288.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(144.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(144.*cw**3*cmath.pi**2*NPl**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_681_31 = Coupling(name = 'UVGC_681_31',
                       value = {-1:'( (complex(0,1)*G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else -(complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*GH*MB)/(4.*cmath.pi**2) + (complex(0,1)*G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (3*complex(0,1)*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) - (complex(0,1)*G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (complex(0,1)*G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'HIG':1,'QCD':2})

UVGC_682_32 = Coupling(name = 'UVGC_682_32',
                       value = {-1:'( -(G**2*yb)/(6.*cmath.pi**2*cmath.sqrt(2)) if MB else (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) - (G**2*yb)/(3.*cmath.pi**2*cmath.sqrt(2))',0:'( (-3*G**2*yb)/(4.*cmath.pi**2*cmath.sqrt(2)) + (G**2*yb*reglog(MB/MU_R))/(cmath.pi**2*cmath.sqrt(2)) if MB else -(G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2)) ) + (G**2*yb)/(12.*cmath.pi**2*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_683_33 = Coupling(name = 'UVGC_683_33',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**2)/(24.*cmath.pi**2) ) + (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_683_34 = Coupling(name = 'UVGC_683_34',
                       value = {-1:'(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'-(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_684_35 = Coupling(name = 'UVGC_684_35',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2)/(24.*cmath.pi**2) ) - (complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**2*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':2})

UVGC_684_36 = Coupling(name = 'UVGC_684_36',
                       value = {-1:'(3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_684_37 = Coupling(name = 'UVGC_684_37',
                       value = {-1:'(-3*complex(0,1)*G**2)/(64.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_684_38 = Coupling(name = 'UVGC_684_38',
                       value = {-1:'-(complex(0,1)*G**2)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**2*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_685_39 = Coupling(name = 'UVGC_685_39',
                       value = {-1:'( 0 if MB else -G**3/(16.*cmath.pi**2) ) + G**3/(24.*cmath.pi**2)',0:'( -(G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_685_40 = Coupling(name = 'UVGC_685_40',
                       value = {-1:'-G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_685_41 = Coupling(name = 'UVGC_685_41',
                       value = {-1:'(21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_685_42 = Coupling(name = 'UVGC_685_42',
                       value = {-1:'G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_685_43 = Coupling(name = 'UVGC_685_43',
                       value = {-1:'G**3/(24.*cmath.pi**2)',0:'-(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_686_44 = Coupling(name = 'UVGC_686_44',
                       value = {-1:'( 0 if MB else G**3/(16.*cmath.pi**2) ) - G**3/(24.*cmath.pi**2)',0:'( (G**3*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':3})

UVGC_686_45 = Coupling(name = 'UVGC_686_45',
                       value = {-1:'G**3/(48.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_686_46 = Coupling(name = 'UVGC_686_46',
                       value = {-1:'(-21*G**3)/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_686_47 = Coupling(name = 'UVGC_686_47',
                       value = {-1:'-G**3/(64.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_686_48 = Coupling(name = 'UVGC_686_48',
                       value = {-1:'-G**3/(24.*cmath.pi**2)',0:'(G**3*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_687_49 = Coupling(name = 'UVGC_687_49',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**4)/(12.*cmath.pi**2) ) + (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( -(complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_687_50 = Coupling(name = 'UVGC_687_50',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_687_51 = Coupling(name = 'UVGC_687_51',
                       value = {-1:'(335*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_687_52 = Coupling(name = 'UVGC_687_52',
                       value = {-1:'(17*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_687_53 = Coupling(name = 'UVGC_687_53',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'-(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_688_54 = Coupling(name = 'UVGC_688_54',
                       value = {-1:'(83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_688_55 = Coupling(name = 'UVGC_688_55',
                       value = {-1:'(5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_689_56 = Coupling(name = 'UVGC_689_56',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**4)/(12.*cmath.pi**2) ) - (complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'( (complex(0,1)*G**4*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'QCD':4})

UVGC_689_57 = Coupling(name = 'UVGC_689_57',
                       value = {-1:'(complex(0,1)*G**4)/(24.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_689_58 = Coupling(name = 'UVGC_689_58',
                       value = {-1:'(-341*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_689_59 = Coupling(name = 'UVGC_689_59',
                       value = {-1:'(-11*complex(0,1)*G**4)/(512.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_689_60 = Coupling(name = 'UVGC_689_60',
                       value = {-1:'-(complex(0,1)*G**4)/(24.*cmath.pi**2)',0:'(complex(0,1)*G**4*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_690_61 = Coupling(name = 'UVGC_690_61',
                       value = {-1:'(-83*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_690_62 = Coupling(name = 'UVGC_690_62',
                       value = {-1:'(-5*complex(0,1)*G**4)/(128.*cmath.pi**2)'},
                       order = {'QCD':4})

UVGC_691_63 = Coupling(name = 'UVGC_691_63',
                       value = {-1:'( 0 if MB else -(complex(0,1)*G**2*GH)/(24.*cmath.pi**2) )',0:'( -(complex(0,1)*G**2*GH*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'HIG':1,'QCD':2})

UVGC_691_64 = Coupling(name = 'UVGC_691_64',
                       value = {-1:'-(complex(0,1)*G**2*GH)/(24.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':2})

UVGC_691_65 = Coupling(name = 'UVGC_691_65',
                       value = {-1:'(25*complex(0,1)*G**2*GH)/(64.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':2})

UVGC_691_66 = Coupling(name = 'UVGC_691_66',
                       value = {-1:'(complex(0,1)*G**2*GH)/(64.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':2})

UVGC_691_67 = Coupling(name = 'UVGC_691_67',
                       value = {0:'-(complex(0,1)*G**2*GH*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':2})

UVGC_692_68 = Coupling(name = 'UVGC_692_68',
                       value = {-1:'( 0 if MB else (complex(0,1)*G**2*GH)/(24.*cmath.pi**2) )',0:'( (complex(0,1)*G**2*GH*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'HIG':1,'QCD':2})

UVGC_692_69 = Coupling(name = 'UVGC_692_69',
                       value = {-1:'(complex(0,1)*G**2*GH)/(24.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':2})

UVGC_692_70 = Coupling(name = 'UVGC_692_70',
                       value = {-1:'-(complex(0,1)*G**2*GH)/(4.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':2})

UVGC_692_71 = Coupling(name = 'UVGC_692_71',
                       value = {-1:'-(complex(0,1)*G**2*GH)/(64.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':2})

UVGC_692_72 = Coupling(name = 'UVGC_692_72',
                       value = {0:'(complex(0,1)*G**2*GH*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':2})

UVGC_693_73 = Coupling(name = 'UVGC_693_73',
                       value = {-1:'( 0 if MB else -(G**3*GH)/(16.*cmath.pi**2) )',0:'( -(G**3*GH*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'HIG':1,'QCD':3})

UVGC_693_74 = Coupling(name = 'UVGC_693_74',
                       value = {-1:'-(G**3*GH)/(16.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':3})

UVGC_693_75 = Coupling(name = 'UVGC_693_75',
                       value = {-1:'(75*G**3*GH)/(128.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':3})

UVGC_693_76 = Coupling(name = 'UVGC_693_76',
                       value = {-1:'(3*G**3*GH)/(128.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':3})

UVGC_693_77 = Coupling(name = 'UVGC_693_77',
                       value = {0:'-(G**3*GH*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':3})

UVGC_694_78 = Coupling(name = 'UVGC_694_78',
                       value = {-1:'( 0 if MB else (G**3*GH)/(16.*cmath.pi**2) )',0:'( (G**3*GH*reglog(MB/MU_R))/(12.*cmath.pi**2) if MB else 0 )'},
                       order = {'HIG':1,'QCD':3})

UVGC_694_79 = Coupling(name = 'UVGC_694_79',
                       value = {-1:'(G**3*GH)/(16.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':3})

UVGC_694_80 = Coupling(name = 'UVGC_694_80',
                       value = {-1:'(-75*G**3*GH)/(128.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':3})

UVGC_694_81 = Coupling(name = 'UVGC_694_81',
                       value = {-1:'(-3*G**3*GH)/(128.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':3})

UVGC_694_82 = Coupling(name = 'UVGC_694_82',
                       value = {0:'(G**3*GH*reglog(MT/MU_R))/(12.*cmath.pi**2)'},
                       order = {'HIG':1,'QCD':3})

UVGC_695_83 = Coupling(name = 'UVGC_695_83',
                       value = {-1:'(complex(0,1)*G**2)/(4.*cmath.pi**2)',0:'(complex(0,1)*G**2)/(3.*cmath.pi**2) - (complex(0,1)*G**2*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':2})

UVGC_696_84 = Coupling(name = 'UVGC_696_84',
                       value = {-1:'-(complex(0,1)*G**3)/(6.*cmath.pi**2)',0:'-(complex(0,1)*G**3)/(3.*cmath.pi**2) + (complex(0,1)*G**3*reglog(MT/MU_R))/(2.*cmath.pi**2)'},
                       order = {'QCD':3})

UVGC_697_85 = Coupling(name = 'UVGC_697_85',
                       value = {-1:'(complex(0,1)*G**2*MT)/(2.*cmath.pi**2)',0:'(2*complex(0,1)*G**2*MT)/(3.*cmath.pi**2) - (complex(0,1)*G**2*MT*reglog(MT/MU_R))/cmath.pi**2'},
                       order = {'QCD':2})

UVGC_698_86 = Coupling(name = 'UVGC_698_86',
                       value = {-1:'( -(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) )',0:'( (-5*ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MB/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2)) if MB else -(ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2)) ) + (ee*complex(0,1)*G**2)/(24.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_698_87 = Coupling(name = 'UVGC_698_87',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(12.*cmath.pi**2*sw*cmath.sqrt(2))',0:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw*cmath.sqrt(2)) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw*cmath.sqrt(2))'},
                       order = {'QCD':2,'QED':1})

UVGC_699_88 = Coupling(name = 'UVGC_699_88',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(96.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(96.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(192.*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(64.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(64.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(96.*cw**2*cmath.pi**2*NPl**2)',0:'(-2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(144.*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(48.*cmath.pi**2*NPl**2*sw**2) - (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2)/(48.*cmath.pi**2*NPl**2*sw**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(72.*cw**2*cmath.pi**2*NPl**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(48.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(48.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(96.*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(32.*cmath.pi**2*NPl**2*sw**2) + (cw**2*cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(32.*cmath.pi**2*NPl**2*sw**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2*reglog(MT/MU_R))/(48.*cw**2*cmath.pi**2*NPl**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_700_89 = Coupling(name = 'UVGC_700_89',
                       value = {-1:'-(ee*complex(0,1)*G**2)/(6.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(24.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(24.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(48.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(24.*cw**2*cmath.pi**2*NPl**2)',0:'(-2*ee*complex(0,1)*G**2)/(9.*cmath.pi**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2)/(18.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2)/(18.*cw**2*cmath.pi**2*NPl**2) + (cWW*ee**3*complex(0,1)*G**2*vev**2)/(36.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2)/(18.*cw**2*cmath.pi**2*NPl**2) + (ee*complex(0,1)*G**2*reglog(MT/MU_R))/(3.*cmath.pi**2) + (cBB*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(12.*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(12.*cw**2*cmath.pi**2*NPl**2) - (cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(24.*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**2*vev**2*reglog(MT/MU_R))/(12.*cw**2*cmath.pi**2*NPl**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_701_90 = Coupling(name = 'UVGC_701_90',
                       value = {-1:'-(cw*ee*complex(0,1)*G**2)/(8.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(24.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(96.*cw*cmath.pi**2*NPl**2*sw) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(64.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(48.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(96.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(96.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(96.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(96.*cw**3*cmath.pi**2*NPl**2)',0:'-(cw*ee*complex(0,1)*G**2)/(6.*cmath.pi**2*sw) + (ee*complex(0,1)*G**2*sw)/(18.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(72.*cw*cmath.pi**2*NPl**2*sw) - (cWW*ee**3*complex(0,1)*G**2*vev**2)/(48.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(36.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(72.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(72.*cw**3*cmath.pi**2*NPl**2) + (cw*ee*complex(0,1)*G**2*reglog(MT/MU_R))/(4.*cmath.pi**2*sw) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(12.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(48.*cw*cmath.pi**2*NPl**2*sw) + (cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(32.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(24.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MT/MU_R))/(48.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MT/MU_R))/(48.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MT/MU_R))/(48.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2*reglog(MT/MU_R))/(48.*cw**3*cmath.pi**2*NPl**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_702_91 = Coupling(name = 'UVGC_702_91',
                       value = {-1:'(ee*complex(0,1)*G**2*sw)/(6.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(24.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(48.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(24.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(24.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(24.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(24.*cw**3*cmath.pi**2*NPl**2)',0:'(2*ee*complex(0,1)*G**2*sw)/(9.*cw*cmath.pi**2) + (cB*ee**3*complex(0,1)*G**2*vev**2)/(18.*cw*cmath.pi**2*NPl**2*sw) + (cw*cWW*ee**3*complex(0,1)*G**2*vev**2)/(36.*cmath.pi**2*NPl**2*sw) + (cB*ee**3*complex(0,1)*G**2*sw*vev**2)/(18.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(18.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2)/(18.*cw*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2)/(18.*cw**3*cmath.pi**2*NPl**2) - (ee*complex(0,1)*G**2*sw*reglog(MT/MU_R))/(3.*cw*cmath.pi**2) - (cB*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(12.*cw*cmath.pi**2*NPl**2*sw) - (cw*cWW*ee**3*complex(0,1)*G**2*vev**2*reglog(MT/MU_R))/(24.*cmath.pi**2*NPl**2*sw) - (cB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MT/MU_R))/(12.*cw**3*cmath.pi**2*NPl**2) + (cBB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MT/MU_R))/(12.*cw**3*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw*vev**2*reglog(MT/MU_R))/(12.*cw*cmath.pi**2*NPl**2) - (cBB*ee**3*complex(0,1)*G**2*sw**3*vev**2*reglog(MT/MU_R))/(12.*cw**3*cmath.pi**2*NPl**2)'},
                       order = {'NP':1,'QCD':2})

UVGC_703_92 = Coupling(name = 'UVGC_703_92',
                       value = {-1:'( (G**2*yb)/(12.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) )',0:'( (13*G**2*yb)/(24.*cmath.pi**2) - (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) ) - (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_703_93 = Coupling(name = 'UVGC_703_93',
                       value = {-1:'(G**2*yb)/(12.*cmath.pi**2)',0:'(G**2*yb)/(6.*cmath.pi**2) - (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_703_94 = Coupling(name = 'UVGC_703_94',
                       value = {-1:'(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_704_95 = Coupling(name = 'UVGC_704_95',
                       value = {-1:'( -(G**2*yb)/(12.*cmath.pi**2) if MB else (G**2*yb)/(24.*cmath.pi**2) )',0:'( (-13*G**2*yb)/(24.*cmath.pi**2) + (3*G**2*yb*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yb)/(24.*cmath.pi**2) ) + (G**2*yb)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_704_96 = Coupling(name = 'UVGC_704_96',
                       value = {-1:'-(G**2*yb)/(12.*cmath.pi**2)',0:'-(G**2*yb)/(6.*cmath.pi**2) + (G**2*yb*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_704_97 = Coupling(name = 'UVGC_704_97',
                       value = {-1:'-(G**2*yb)/(3.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_705_98 = Coupling(name = 'UVGC_705_98',
                       value = {-1:'( (G**2*yt)/(12.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) )',0:'( (5*G**2*yt)/(24.*cmath.pi**2) - (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) ) - (G**2*yt)/(24.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_705_99 = Coupling(name = 'UVGC_705_99',
                       value = {-1:'(G**2*yt)/(12.*cmath.pi**2)',0:'(G**2*yt)/(2.*cmath.pi**2) - (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                       order = {'QCD':2,'QED':1})

UVGC_705_100 = Coupling(name = 'UVGC_705_100',
                        value = {-1:'(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_706_101 = Coupling(name = 'UVGC_706_101',
                        value = {-1:'( -(G**2*yt)/(12.*cmath.pi**2) if MB else (G**2*yt)/(24.*cmath.pi**2) )',0:'( (-5*G**2*yt)/(24.*cmath.pi**2) + (G**2*yt*reglog(MB/MU_R))/(4.*cmath.pi**2) if MB else -(G**2*yt)/(24.*cmath.pi**2) ) + (G**2*yt)/(24.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_706_102 = Coupling(name = 'UVGC_706_102',
                        value = {-1:'-(G**2*yt)/(12.*cmath.pi**2)',0:'-(G**2*yt)/(2.*cmath.pi**2) + (3*G**2*yt*reglog(MT/MU_R))/(4.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_706_103 = Coupling(name = 'UVGC_706_103',
                        value = {-1:'-(G**2*yt)/(3.*cmath.pi**2)'},
                        order = {'QCD':2,'QED':1})

UVGC_707_104 = Coupling(name = 'UVGC_707_104',
                        value = {-1:'(G**2*yt)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(G**2*yt*cmath.sqrt(2))/(3.*cmath.pi**2) - (G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'QCD':2,'QED':1})

UVGC_708_105 = Coupling(name = 'UVGC_708_105',
                        value = {-1:'-(complex(0,1)*G**2*GH*MT)/(4.*cmath.pi**2) + (complex(0,1)*G**2*yt)/(2.*cmath.pi**2*cmath.sqrt(2))',0:'(complex(0,1)*G**2*yt*cmath.sqrt(2))/(3.*cmath.pi**2) - (complex(0,1)*G**2*yt*reglog(MT/MU_R))/(cmath.pi**2*cmath.sqrt(2))'},
                        order = {'HIG':1,'QCD':2})

