Contents: Higgs pair-production via VBF
Requestor: Puja Saha
Paper: https://arxiv.org/abs/1611.03860
Source: Fady Bishara, private communication
JIRA: https://its.cern.ch/jira/browse/AGENE-1487