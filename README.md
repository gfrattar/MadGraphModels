This project contains all the external models that can be used in production with the ATLAS MadGraph5\_aMC@NLO / MadGraphControl setup.  

Please open a merge request for any new models.  Note that mdoels should all come with a short description in a text file, including the source of model (website, private communication, etc), a brief description, and links to the relevant websites or papers describing the model.  This documentation is also available in the model\_list.txt text file in this project. If any models have been received via private communication, please encourage the authors to post them on a website; this could be their personal website or <a href="http://feynrules.irmp.ucl.ac.be">FeynRules</a>. This helps greatly with bookkeeping and communication with other communities that might be interested in the details of the model (CMS or pheno colleagues).

This repository is replicated to <code>/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest</code> every few hours. After the replication following your merge request, any code you have requested will be available for you to run worldwide in any release. There is no need to wait for a new production cache before submitting your request.

To test locally before requesting installation, put your model into a directory other than the one in which you are running the job, and do:

<code>export PYTHONPATH=/path/to/my/model:$PYTHONPATH
</code>

The job should then be able to pick up your model. Note that the repository has now been structured so that models are organized. You can put your preferred model into one of the subdirectories here, and the deployment should automatically pick it up. To test, add your model into a clone of this repository and then:

<code>source setup.sh</code>

to set up the relevant links and modify your PYTHONPATH appropriately. Note: because the developers are lazy, you do need to source or execute that script from the main directory of the repository, not from elsewhere, in order for it to work its magic properly.

There is a DEPRECATED folder that contains models that are superseded and potentially buggy. These might be useful for comparisons but should not be used otherwise. To use the models, the folder as to be added to the PYTHONPATH manually:

<code>export PYTHONPATH=$PYTHONPATH:/cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/DEPRECATED
</code>

This package previously was held in svn <a href="https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MadGraphModels/trunk">here</a>.  The old change log containing previous commits is <a href="https://svnweb.cern.ch/trac/atlasoff/browser/Generators/MadGraphModels/trunk/ChangeLog">here</a> (warning: links are now dead)

Further documentation for extra models is <a href="https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MadGraph5aMCatNLOForAtlas#Extra_models">here</a>, and further documentation for the use of MadGraph in Athena is <a href="https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MadGraph5aMCatNLOForAtlas">here</a>.

Because Athena main (release 22+) now uses Python3, we must ensure that all models are Python3 compatible. To check if your model is Python3 compatible, try this:

<code>lsetup "asetup Athena,here,latest,main"
export PYTHONPATH=/path/to/my/model:$PYTHONPATH
$MADPATH/bin/mg5_aMC
</code>

Then, at the MadGraph command prompt, simply type `import model`. If that doesn't work, then you might be able to covert your model by typing:

<code>convert model /path/to/my/model
</code>

Be sure to check the differences that were applied to make sure that none of them were particularly strange; you might want to generate a few events to cross-check that things are working as intended.